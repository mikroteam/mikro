# Makefile

-include Makefile.rules

build_daemon_list=$(foreach d, $1, $(addprefix $(BUILD_PATH)$(USERLAND_PATH)$(d)/, $d))
DAEMONS = $(call build_daemon_list, $(BOOT_DAEMONS))

#verbose?
ifeq ($(VERBOSE), 1)
	MAKEFLAGS =
else
	MAKEFLAGS = --silent --no-print-directory
endif

all: 
	$(MAKE) -I $(MIKRO_ROOT_PATH) -C $(KERNEL_PATH) BUILD_PATH=$(BUILD_PATH)$(KERNEL_PATH)
ifdef BOOTLOADER_PATH
	$(MAKE) -I $(MIKRO_ROOT_PATH) -C $(BOOTLOADER_PATH) BUILD_PATH=$(BUILD_PATH)$(BOOTLOADER_PATH)
endif
ifdef USERLAND_PATH
	$(MAKE) -C $(USERLAND_PATH) BUILD_PATH=$(BUILD_PATH)$(USERLAND_PATH)
endif

clean:
	$(MAKE) -I $(MIKRO_ROOT_PATH) -C $(KERNEL_PATH) BUILD_PATH=$(BUILD_PATH)$(KERNEL_PATH) clean
ifdef BOOTLOADER_PATH
	$(MAKE) -I $(MIKRO_ROOT_PATH) -C $(BOOTLOADER_PATH) BUILD_PATH=$(BUILD_PATH)$(BOOTLOADER_PATH) clean
endif
ifdef USERLAND_PATH
	$(MAKE) -C $(USERLAND_PATH) BUILD_PATH=$(BUILD_PATH)$(USERLAND_PATH) clean
endif

distclean:
	$(MAKE) -I $(MIKRO_ROOT_PATH) -C $(KERNEL_PATH) BUILD_PATH=$(BUILD_PATH)$(KERNEL_PATH) distclean
ifdef BOOTLOADER_PATH
	$(MAKE) -I $(MIKRO_ROOT_PATH) -C $(BOOTLOADER_PATH) BUILD_PATH=$(BUILD_PATH)$(BOOTLOADER_PATH) distclean
endif
ifdef USERLAND_PATH
	$(MAKE) -C $(USERLAND_PATH) BUILD_PATH=$(BUILD_PATH)$(USERLAND_PATH) distclean
endif
	$(RM) -R build-* $(MNT_POINT) Makefile.rules $(MIKROB_CONFIG)
	@$(ECHO) "\t[RM] build-* $(MNT_POINT) Makefile.rules"

boot: all vdisk $(EMUL)

boot-test: all vdisk $(EMUL)-test

boot-debug: all vdisk $(EMUL)-debug

boot-debug-monitor: all vdisk $(EMUL)-debug-monitor

## Emulators managment

qemu:
	$(QEMU) $(QEMU_OPTION) -serial file:serial.out -monitor stdio -hda $(BUILD_PATH)$(VDISK)

qemu-test:
	$(QEMU) $(QEMU_OPTION) -serial tcp:127.0.0.1:33333 -hda $(BUILD_PATH)$(VDISK)

qemu-debug:
	$(QEMU) $(QEMU_OPTION) -serial file:serial.out -hda $(BUILD_PATH)$(VDISK) -s -S&
	sleep 1
ifdef DEBUG_DEAMON
	$(GDB) $(BUILD_PATH)$(USERLAND_PATH)/$(DEBUG_DEAMON)/$(DEBUG_DEAMON)
else
	$(GDB) $(KERNEL_BIN)_debug
endif

qemu-debug-monitor:
	$(QEMU) $(QEMU_OPTION) -serial file:serial.out  -monitor stdio -s -S -hda $(BUILD_PATH)$(VDISK)

bochs:
	$(ECHO) 6 | $(BOCHS)  -q 'megs:256' 'boot:disk' 'ata0-master: type=disk, path="$(BUILD_PATH)$(VDISK)"' 'com1: enabled=1, mode=file, dev=serial.out'

bochs-debug:
	$(ECHO) 6 | $(BOCHS)-gdb  -q 'megs:256' 'boot:disk' 'ata0-master: type=disk, path="$(BUILD_PATH)$(VDISK)"' 'gdbstub: enabled=1, port=1234, text_base=0, data_base=0, bss_base=0' 'log: bochsout.txt' 'com1: enabled=1, mode=file, dev=serial.out'&
	sleep 1
	$(GDB) $(KERNEL_BIN)_debug

vbox:
	./$(TOOL_PATH)boot-virtualbox.sh $(BUILD_PATH)$(VDISK) $(BUILD_PATH)$(VDISK).vdi

vbox-debug:
	./$(TOOL_PATH)boot-virtualbox.sh $(BUILD_PATH)$(VDISK) $(BUILD_PATH)$(VDISK).vdi
	sleep 5
	cd tools/; $(GDB) $(KERNEL_BIN)_debug

ifeq ($(FIRMWARE), uefi)
qemu: firmware/bios.bin
qemu-debug: firmware/bios.bin

firmware/bios.bin:
	mkdir -p firmware
	curl -L http://sourceforge.net/projects/edk2/files/OVMF/OVMF-IA32-r11337-alpha.zip/download -o firmware/tmp.zip
	cd firmware && unzip -n tmp.zip
	mv firmware/OVMF.fd $@
	$(RM) tmp.zip
endif

## Files managment

stackdump:
	$(PYTHON2) tools/stack.py $(ADDR2LINE) $(KERNEL_BIN)_debug serial.out

## Virtual disk update/creation

vdisk: $(BUILD_PATH)$(VDISK)


$(BUILD_PATH)$(VDISK)::
	$(MKDIR) -p $(MNT_POINT)
	./$(TOOL_PATH)mk-img-$(ARCH)-$(HOST).sh $(BUILD_PATH)$(VDISK)
ifeq ($(BOOTLOADER), extlinux)
	./$(TOOL_PATH)install-extlinux.sh $(BUILD_PATH)$(VDISK) $(MNT_POINT)
endif

ifeq ($(BOOTLOADER), mikrob)
ifeq ($(FIRMWARE), uefi)
$(BUILD_PATH)$(VDISK):: $(BUILD_PATH)bootloader/stage2/x86/mikrob.efi
	./$(TOOL_PATH)install-mikrob-uefi.sh $(BUILD_PATH)$(VDISK) $(MNT_POINT) $<
else
$(BUILD_PATH)$(VDISK):: $(BUILD_PATH)bootloader/stage1/x86/stage1.bin $(BUILD_PATH)bootloader/stage2/x86/stage2.bin
	./$(TOOL_PATH)install-mikrob.sh $(BUILD_PATH)$(BOOTLOADER_PATH) $(BUILD_PATH)$(VDISK)
endif
endif

$(BUILD_PATH)$(VDISK):: $(KERNEL_BIN) $(EXTRA_FILES) $(DAEMONS) Makefile.img
	./$(TOOL_PATH)generate-bootloader-conf.sh $(BOOTLOADER) $(BOOTLOADER_CONFIG_FILE) --args="$(KERNEL_ARGS)" --daemons="$(BOOT_DAEMONS)"
	./$(TOOL_PATH)cp-img-$(ARCH)-$(HOST).sh $(BUILD_PATH)$(VDISK) $(MNT_POINT) $(filter-out Makefile.img, $^) $(BOOTLOADER_CONFIG_FILE)

## Misc

Makefile.rules:
	./configure

# EOF
