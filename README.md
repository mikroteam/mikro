mikro
=====

mikro is an experimental Microkernel written in C++.
It aims to be lightweight and flexible.
The default compiler for mikro is clang.

Features:
---------
- Paging
- Symmetric Multi Processor 
- Inter Process Communication
- VM86 (on x86)

Supported architectures:
------------------------
Currently only i386 is supported but we aim to support x86_64 and armv7.

Bootloader:
-----------
mikro can be booted using any multiboot compliant bootloader. By default,
it comes with a lightweight bootloader called mikrob.

Getting started:
----------------
To build and test the project for the default architecture (i386):

	./configure && make

To boot the kernel with the default emulator (qemu):

	make boot

Emulators:
----------
VirtualBox, Qemu or Bochs

Founders:
---------
Julien Freche, Victor Apercé
