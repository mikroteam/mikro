.code16 // A x86 CPU starts in real mode (16 bit)

.org 0 // BIOS will load this code in a separate segment starting at 0
       // The location of this segment in physical memory is 0x7c0

.set BASE_ADDR, 0x100
.set SECT_NUMBER, 50

.globl _start // Useless but avoid a warning from ld
_start:

  mov $0x07c0, %ax
  mov %ax, %ds // Set ds to 0x7c0 to access data in this segment
  mov %ax, %es

  mov $0x8000, %ax
  mov %ax, %ss // Set ss to 0x8000, this is the stack segment
  mov $0xf000, %sp // Set the stack pointer to an arbitrary value in the segment

  // Reset drive
  xor %ax, %ax
  int $0x13

  // Load the stage2 in memory with help of the BIOS
  push %es

  mov $BASE_ADDR, %ax
  mov %ax, %es
  mov $0, %bx
  mov $2, %ah
  mov $SECT_NUMBER, %al
  mov $0, %ch
  mov $2, %cl
  mov $0, %dh
  int $0x13

  pop %es

  // Compute the size of the gdt
  mov $gdt_end, %ax
  mov $gdt, %bx
  sub %bx, %ax
  mov %ax, (gdt_ptr)

  // Store addr of the gdt in gdt_ptr
  movl $gdt, (gdt_ptr + 2)

  // e820 memory detection
  mov %ss, %ax
  mov %ax, %es
  xor %ebx, %ebx
  mov $0x534D4150, %edx // "SMAP"
  xor %bp, %bp

next_entry:
  inc %bp
  mov $0xE820, %eax
  sub $20, %sp
  mov %sp, %di
  mov $20, %ecx
  int $0x15
  pushl %ecx
  test %ebx, %ebx
  jne next_entry

  // Compute the absolute address once in protected mode
  movl %ss, %ebx
  imul $0x10, %ebx
  addl %esp, %ebx

  // Enable A20 line -- Required in protected mode
  mov $0x2401, %ax
  int $0x15

  // Load the gdt 
  cli
  lgdt (gdt_ptr)

  // Init protected mode
  mov %cr0, %eax
  or $1, %ax
  mov %eax, %cr0
  jmp flush
flush:

  // Change all segments selectors (except cs) to the second entry in the gdt
  mov $0x10, %ax
  mov %ax, %ds
  mov %ax, %fs
  mov %ax, %gs
  mov %ax, %es
  mov %ax, %ss
  mov $0x90000, %esp

  // Push arguments on the stack
  pushl %ebp
  pushl %ebx
  pushl $42 // Dummy value to create a correct stack for the C code

  // Jump to stage2 in protected mode
  ljmp $0x8, $0x1000

gdt:
 .byte 0, 0, 0, 0, 0, 0, 0, 0
gdt_cs:
 .byte 0xFF, 0xFF, 0x0, 0x0, 0x0, 0b10011011, 0b11011111, 0x0
gdt_ds:
 .byte 0xFF, 0xFF, 0x0, 0x0, 0x0, 0b10010011, 0b11011111, 0x0
gdt_end:

gdt_ptr:
 .word 0
 .long 0

. = 0x200 - 2 // The total size of this program must be 0x200 (512 bytes)
.word 0xAA55 // Magic number for the bios
