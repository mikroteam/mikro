/*
 * File: abort.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Print an error message and wait forever
 *
 */

#include "abort.h"

void abort(char* msg)
{
  log_display_string("\nFatal error during boot :\n");
  log_display_string(msg);

  while (1)
    continue;
}

