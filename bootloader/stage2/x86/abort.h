/*
 * File: abort.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Print an error message and wait forever
 *
 */

#ifndef ABORT_H_
# define ABORT_H_

# include "logger.h"

void abort(char* msg) __attribute__((noreturn));

#endif /* !ABORT_H_ */

