/*
 * File: allocator.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Dummy memory allocation for the bootloader
 *
 */

#include "abort.h"
#include "allocator.h"

static char* mem_limit = 0;

void allocator_init(void* kernel_end_addr)
{
  mem_limit = kernel_end_addr;
}

void* malloc(size_t sz)
{
  if (!mem_limit)
    abort("Trying to allocate data before allocator init");

  void* block = mem_limit;
  mem_limit += sz;
  return block;
}

void* malloc_page_aligned(size_t sz)
{
  if (!mem_limit)
    abort("Trying to allocate data before allocator init");

  if (((size_t)mem_limit % PAGE_SIZE) != 0)
    mem_limit = (void*)((~0xfff & (size_t)mem_limit) + PAGE_SIZE);
  void* block = mem_limit;
  mem_limit += sz;
  return block;
}

void* get_mem_limit()
{
  return mem_limit;
}
