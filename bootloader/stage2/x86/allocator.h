/*
 * File: allocator.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Dummy allocator for the bootloader
 *
 */

#ifndef ALLOCATOR_H_
# define ALLOCATOR_H_

# include "memory_map.h"

# define PAGE_SIZE 4096

void allocator_init(void* kernel_end_addr);
void* malloc(size_t sz);
void* malloc_page_aligned(size_t sz);
void* get_mem_limit();

#endif /* !ALLOCATOR_H_ */
