/*
 * File: ata_pio_48.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ATA PIO 48 bits driver
 *
 */

#include "io.hh"

#include "ata_pio_48.h"

#define DATA_PORT 0x1f0
#define DRIVE_SELECT_PORT 0x1f6
#define SECTOR_COUNT_PORT 0x1f2
#define LBA_LOW_PORT 0x1f3
#define LBA_MID_PORT 0x1f4
#define LBA_HIGH_PORT 0x1f5
#define COMMAND_PORT 0x1f7
#define STATUS_PORT 0x1f7

#define READ_CMD 0x24

void readATA(enum Drive drive, u64 lba, u16 count, short* buff)
{
  OUTB(DRIVE_SELECT_PORT, drive);

  OUTB(SECTOR_COUNT_PORT, (u8)(count >> 8));
  OUTB(LBA_LOW_PORT, (u8)(lba >> 3 * 8));
  OUTB(LBA_MID_PORT, (u8)(lba >> 4 * 8));
  OUTB(LBA_HIGH_PORT, (u8)(lba >> 5 * 8));
  OUTB(SECTOR_COUNT_PORT, (u8)(count));
  OUTB(LBA_LOW_PORT, (u8)(lba >> 0 * 8));
  OUTB(LBA_MID_PORT, (u8)(lba >> 1 * 8));
  OUTB(LBA_HIGH_PORT, (u8)(lba >> 2 * 8));

  OUTB(COMMAND_PORT, READ_CMD);

  for (u16 i = 0; i < count; i++)
  {
    /* Polling here */
    while (1)
    {
      u8 status = 0;

      INB(STATUS_PORT, status);
      if (!(status & 0x08) && (status & 0x80))
        continue;
      break;
    }

    for (u16 j = 0; j < 256; j++)
    {
      u16 tmp;

      INW(DATA_PORT, tmp);
      buff[0] = tmp;
      buff++;
    }
  }
}
