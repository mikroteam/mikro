/*
 * File: ata_pio_48.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ATA PIO 48 bits driver
 *
 */

#ifndef ATA_PIO_48_H_
# define ATA_PIO_48_H_

# include <types.hh>

enum Drive
{
  Master = 0x40,
  Slave = 0x50
};

void readATA(enum Drive drive, u64 lba, u16 count, short* buff);

#endif /* !ATA_PIO_48_H_ */
