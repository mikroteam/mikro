/*
 * File: elf_loader.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Useful operations on elf format
 *
 */

#include "memory_map.h"
#include "abort.h"
#include "ext2.h"
#include "utils.h"
#include "allocator.h"

#include "elf_loader.h"

static int elfValidateLoadAddr(size_t begin_addr,
                               size_t end_addr,
                               struct e820entry* mem_map,
                               u32 entry_number)
{
  if (begin_addr < MEM_UPPER)
    abort("Loading of kernel in low mem is not supported");

  for (u32 i = 0; i < entry_number; i++)
    if (mem_map[i].type == E820_FREE)
      if (begin_addr >= mem_map[i].addr &&
          end_addr <= (mem_map[i].addr + mem_map[i].len))
          return 1;

  return 0;
}

ELF_EHDR* elfLoad(struct Ext2Infos* info, struct Ext2Inode* k_ino,
                  void** kernel_end_addr,
                  struct e820entry* mem_map, u32 entry_number)
{
  /* Load only the first block, it is probably enough for header and PH */
  char buff[1024 << info->spb->s_log_block_size];
  ext2ReadFileContent(info, k_ino, buff, 0, 1024 << info->spb->s_log_block_size);
  ELF_EHDR* header = (ELF_EHDR*)buff;

  if (strncmp((const char*)header->e_ident, ELFMAG, SELFMAG))
    abort("Kernel Image is not a valid ELF");

  uint16_t entries = header->e_phnum;
  if (header->e_phoff + entries * header->e_phentsize > (1024u << info->spb->s_log_block_size))
    // FIXME: This case is unlikely but can be handled
    abort("Program Header is not in the first page");

  for (uint16_t i = 0; i < entries; i++)
  {
    ELF_PHDR* ph = (ELF_PHDR*)(buff + header->e_phoff
                                    + i * header->e_phentsize);

    if (ph->p_type == PT_LOAD)
    {
      if (!elfValidateLoadAddr(ph->p_paddr, ph->p_memsz, mem_map, entry_number))
        abort("Kernel cannot be loaded in a reserved area");

      ext2ReadFileContent(info, k_ino, (void*)ph->p_paddr, ph->p_offset,
                          ph->p_filesz);
      memset((char*)ph->p_paddr + ph->p_filesz, 0, ph->p_memsz - ph->p_filesz);
      *kernel_end_addr = MAX(*kernel_end_addr, (void*)(ph->p_paddr + ph->p_memsz));
    }
  }

  // Copy the header just behind the kernel
  if (!elfValidateLoadAddr((size_t)*kernel_end_addr, header->e_ehsize, mem_map, entry_number))
    // FIXME: This case is very unlikely but can be handled
    abort("Kernel header cannot be loaded at this addr");

  memcpy(*kernel_end_addr, buff, header->e_ehsize);
  header = *kernel_end_addr;
  *kernel_end_addr = (char*)(*kernel_end_addr) + header->e_ehsize;
  return header;
}
