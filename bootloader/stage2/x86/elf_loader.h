/*
 * File: elf_loader.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Useful operations on elf format
 *
 */

#ifndef ELF_H_
# define ELF_H_

# include <elf.h>

# ifdef MIKRO64

# define ELF_EHDR Elf64_Ehdr
# define ELF_PHDR Elf64_Phdr

# else /* !MIKRO64 */

# define ELF_EHDR Elf32_Ehdr
# define ELF_PHDR Elf32_Phdr

# endif

ELF_EHDR* elfLoad(struct Ext2Infos* info, struct Ext2Inode* k_ino,
                  void** kernel_end_addr,
                  struct e820entry* mem_map, u32 entry_number);

#endif /* !ELF_H_ */
