/*
 * File: entry.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Entry point of the bootloader
 *
 */

#include <types.hh>
#include <multiboot.h>
#include <elf.h>

#include "logger.h"
#include "memory_map.h"
#include "ata_pio_48.h"
#include "mbr.h"
#include "ext2.h"
#include "abort.h"
#include "utils.h"
#include "multiboot_info.h"
#include "elf_loader.h"
#include "allocator.h"

#include "entry.h"

#define CONFIG_FILE_NAME "mikrob.conf"
#define CONFIG_FILE_SIZE_MAX 2048

static ELF_EHDR* loadKernelOnExt2(struct Ext2Infos* infos,
                                  struct Ext2Inode* root_ino,
                                  const char* k_name,
                                  void** kernel_end_addr,
                                  struct e820entry* mem_map,
                                  u32 entry_number)
{
  char kernel_ino_buff[1024 << infos->spb->s_log_block_size];
  struct Ext2Inode* kernel_ino = ext2GetFileFromDir(infos, root_ino,
                                                    kernel_ino_buff,
                                                    k_name);
  if (kernel_ino == 0)
    abort("Kernel image not found.");

  return elfLoad(infos, kernel_ino, kernel_end_addr, mem_map, entry_number);
}

static void* loadModuleOnExt2(struct Ext2Infos* infos,
                              struct Ext2Inode* root_ino,
                              const char* m_name,
                              u64* end_addr)
{
  char module_ino_buff[1024 << infos->spb->s_log_block_size];
  struct Ext2Inode* module_ino = ext2GetFileFromDir(infos, root_ino,
                                                    module_ino_buff,
                                                    m_name);
  if (module_ino == 0)
    abort("Module image not found.");

  u64 len = module_ino->i_size;
  char* data = malloc_page_aligned(len);
  *end_addr = (u64)(data + len);

  ext2ReadFileContent(infos, module_ino, data, 0, module_ino->i_size);

  return data;
}

static void boot(ELF_EHDR* elf_h, multiboot_info_t* info)
{
  if (elf_h == 0)
    abort("Kernel loading fail");

  if (info == 0)
    abort("Kernel is not multiboot compliant");

  void* ep = (void*)elf_h->e_entry;
  JUMP_TO_KERNEL(MULTIBOOT_BOOTLOADER_MAGIC, info, ep);

  /* Should never be reached */
  abort("Fail to jump to the kernel code");
}

static void parse_config_file(u64 lba_start, struct e820entry* mem_map,
                              u32 entry_number)
{
  struct Ext2Infos infos;
  struct Ext2Super spb;
  infos.spb = &spb;
  infos.lba = &lba_start;

  ext2GetSuper(&infos);

  char bgd_buff[1024 << spb.s_log_block_size];
  infos.bgd = (struct Ext2BlockGroupEntry*)&bgd_buff;
  ext2GetBlock(&infos);

  char root_ino_buff[1024 << spb.s_log_block_size];
  struct Ext2Inode* root_ino = ext2GetInode(&infos, root_ino_buff,
                                            EXT2_ROOT_INO);

  char config_ino_buff[1024 << spb.s_log_block_size];
  struct Ext2Inode* config_ino = ext2GetFileFromDir(&infos, root_ino,
                                                    config_ino_buff,
                                                    CONFIG_FILE_NAME);

  char config_buff[CONFIG_FILE_SIZE_MAX];
  char* config_file = config_buff;

  if (config_ino->i_size > CONFIG_FILE_SIZE_MAX)
    abort("Config file is too big");
  ext2ReadFileContent(&infos, config_ino, config_file, 0, config_ino->i_size);

  multiboot_info_t info;
  struct multiboot_header* multi_h = 0;
  ELF_EHDR* elf_h = 0;
  void* kernel_end_addr = 0;

  while (1)
  {
    switch (config_file[0])
    {
      case ('k'):
        {
          char* k_name = &(config_file[2]);
          char* cmd_line = 0;

          config_file += 2;
          while (config_file[0] != '\n' && config_file[0] != ' ')
            config_file++;

          // Retrieve the command line
          if (config_file[0] == ' ')
          {
            config_file[0] = 0;
            cmd_line = ++config_file;
            while (config_file[0] != '\n')
              config_file++;
          }
          config_file[0] = 0;
          config_file++;

          log_display_string("=> Load kernel ");
          log_display_string(k_name);
          elf_h = loadKernelOnExt2(&infos, root_ino, k_name,
                                   &kernel_end_addr, mem_map, entry_number);
          log_display_string(" - OK\n");
          allocator_init(kernel_end_addr);
          multi_h = findMultibootHeader((char*)elf_h);
          fillMultibootInfo(multi_h, &info, mem_map, entry_number);
          addMultibootCommandLine(&info, cmd_line);
          break;
        }
      case ('b'):
        {
          log_display_string("=> Booting the kernel...\n");
          boot(elf_h, &info);
          break;
        }
      case ('m'):
        {
          if (!elf_h)
            abort("Kernel must be loaded before modules\n");

          char* m_name = &(config_file[2]);

          while (config_file[0] != '\n')
            config_file++;
          config_file[0] = 0;
          config_file++;

          u64 end = 0;
          log_display_string("=> Load module ");
          log_display_string(m_name);
          void* start = loadModuleOnExt2(&infos, root_ino, m_name, &end);
          log_display_string(" - OK\n");
          addMultibootModule(&info, start, (void*)end, m_name);
          break;
        }
      default:
        {
          abort("Unknown command in the config file");
        }
    }

  }
}

void entry(struct e820entry* mem_map, u32 entry_number)
{
  char* buff[512];
  struct e820entry saved_mem_map[entry_number];

  log_init();
  log_display_string("mikrob - Stage 2\n\n");

  /* Save the memory map to a safe location */
  memcpy(saved_mem_map, mem_map, sizeof (struct e820entry) * entry_number);
  mem_map = saved_mem_map;

  dump_memory_map(mem_map, entry_number);

  enum Drive drv = Master;
  struct BootSector* bsector = (struct BootSector*)buff;
  u32 lba_start = 0;

  readATA(drv, 0, 1, (short*)buff);
  lba_start = findBootable(bsector);

  parse_config_file(lba_start, mem_map, entry_number);
}
