/*
 * File: entry.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Entry point of the bootloader
 *
 */

#ifndef ENTRY_H_
# define ENTRY_H_

# ifdef MIKRO64

# define JUMP_TO_KERNEL(Magic, Multiboot, Addr)                   \
    __asm__ __volatile__ ("movq %0, %%rax\n\t"                    \
                          "movq %1, %%rbx\n\t"                    \
                          "movq %2, %%rcx\n\t"                    \
                          "jmp *%%rcx"                            \
                          :                                       \
                          : "g" (Magic),     \
                            "g" (Multiboot), "g" (Addr)           \
                          : "%rax", "%rbx", "%rcx");
# else /* !MIKRO64 */

# define JUMP_TO_KERNEL(Magic, Multiboot, Addr)                   \
    __asm__ __volatile__ ("movl %0, %%eax\n\t"                    \
                          "movl %1, %%ebx\n\t"                    \
                          "movl %2, %%ecx\n\t"                    \
                          "jmp *%%ecx"                            \
                          :                                       \
                          : "g" (Magic),     \
                            "g" (Multiboot), "g" (Addr)           \
                          : "%eax", "%ebx", "%ecx");

# endif /* MIKRO64 */

void entry(struct e820entry* mem_map, u32 entry_number)
          __attribute__((section(".entry")));

#endif /* !ENTRY_H_ */
