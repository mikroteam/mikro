/*
 * File: ext2.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Minimalist read only ext2 implementation
 *
 */

#include "abort.h"
#include "ata_pio_48.h"
#include "utils.h"

#include "ext2.h"

void ext2GetSuper(struct Ext2Infos* info)
{
  enum Drive d = Master;

  readATA(d, *info->lba + EXT2_SPB_OFFSET,
          EXT2_SPB_SIZE / 512, (short*)info->spb);

  if (info->spb->s_magic != EXT2_SUPER_MAGIC)
    abort("Invalid super block in ext2 partition");
}

void ext2GetBlock(struct Ext2Infos* info)
{
  enum Drive d = Master;

  readATA(d, *info->lba + EXT2_BGD_OFFSET,
          (1024 << info->spb->s_log_block_size) / 512,
          (short*)info->bgd);
}

struct Ext2Inode* ext2GetInode(const struct Ext2Infos* info,
                               char* buff, u32 ino_nb)
{
  enum Drive d = Master;
  u32 bg = (ino_nb - 1) / info->spb->s_inodes_per_group;
  u32 ino_idx = (ino_nb - 1) % info->spb->s_inodes_per_group;
  u32 blk = info->bgd[bg].bg_inode_table *
            (1024 << (info->spb->s_log_block_size))+
            ino_idx * info->spb->s_inode_size;

  readATA(d, *info->lba + blk / 512, 1, (short*)buff);

  return (struct Ext2Inode*)(buff + (blk % 512));
}

static void ext2ReadBlkFromDisk(u32 ext2_blk_idx, u32 ext2_blk_size,
                                char* buff, u64 lba)
{
  enum Drive d = Master;

  readATA(d, lba + ((ext2_blk_size / 512) * ext2_blk_idx),
          ext2_blk_size / 512, (short*)buff);
}

static u32 findBlockID(const struct Ext2Inode* file, u32 blk_nb,
                       u32 blk_size, u64 lba)
{
  if (blk_nb < EXT2_SIMPLE_INDIR)
    return file->i_block[blk_nb];

  u32 blk_id_per_blk = blk_size / sizeof (u32);
  u32 buff[blk_id_per_blk];
  u32 limit = blk_id_per_blk;
  u32 pow = blk_id_per_blk;
  u16 count = 0;
  u32 current_id = EXT2_SIMPLE_INDIR;

  blk_nb -= EXT2_SIMPLE_INDIR;
  while (blk_nb > limit)
  {
    count++;
    blk_nb -= pow;
    pow *= blk_id_per_blk;
    limit += pow;
  }

  if (current_id + count > EXT2_TRIPLE_INDIR)
    abort("Incorrect block id");

  memcpy(buff, (void*)file->i_block, 15 * sizeof (u32));
  current_id += count;
  ext2ReadBlkFromDisk(buff[current_id], blk_size, (char*)buff, lba);

  while (count)
  {
    pow /= blk_id_per_blk;
    current_id = blk_nb / pow;
    blk_nb = blk_nb % pow;
    ext2ReadBlkFromDisk(buff[current_id], blk_size, (char*)buff, lba);
    --count;
  }

  return buff[blk_nb];
}

struct Ext2Inode* ext2GetFileFromDir(const struct Ext2Infos* info,
                                     const struct Ext2Inode* dir,
                                     char* buff, const char* name)
{
  if ((dir->i_mode & EXT2_S_IFDIR) == 0)
    abort("Inode does not point to a directory");

  u32 blk_size = 1024 << info->spb->s_log_block_size;
  char blk_buff[blk_size];
  char *current;
  u32 blocks = dir->i_blocks / (blk_size / 512);
  u32 name_len = strlen(name);
  u32 total = dir->i_size;
  u32 current_read = 0;

  for (u32 i = 0; i < blocks; i++)
  {
    u32 blk_current = findBlockID(dir, i, blk_size, *info->lba);
    if (blk_current == 0)
      break;

    ext2ReadBlkFromDisk(blk_current, blk_size, blk_buff, *info->lba);
    current = blk_buff;

    while (1)
    {
      struct Ext2DirectoryEntry* dirent = (struct Ext2DirectoryEntry*)current;

      if (dirent->name_len == name_len)
        if (!strncmp(name, dirent->name, name_len))
          return ext2GetInode(info, buff, dirent->inode);

      current_read += dirent->rec_len;
      if (current_read >= total)
        return 0;
      if ((((u32)current - (u32)blk_buff) + dirent->rec_len) >= blk_size)
        break;
      current += dirent->rec_len;
    }
  }

  return 0;
}

void ext2ReadFileContent(const struct Ext2Infos* info,
                         const struct Ext2Inode* file,
                         char* buff, u32 off, u32 size)
{
  u32 blk_size = 1024 << info->spb->s_log_block_size;
  u32 current_block_nb = off / blk_size;

  off %= blk_size;
  while (size)
  {
    u32 blk_current = findBlockID(file, current_block_nb++, blk_size, *info->lba);
    if (blk_current == 0)
      break;

    if (size > blk_size && off == 0)
    {
      ext2ReadBlkFromDisk(blk_current, blk_size, buff, *info->lba);
      size -= blk_size;
      buff += blk_size;
    }
    else
    {
      char tmp_buff[blk_size];
      u32 to_read = MIN(blk_size - off, size);

      ext2ReadBlkFromDisk(blk_current, blk_size, tmp_buff, *info->lba);
      memcpy(buff, tmp_buff + off, to_read);
      size -= to_read;
      buff += to_read;
      off = 0;
    }
  }
}
