/*
 * File: io.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Some useful macros for IO
 *
 */

#ifndef IO_HH_
# define IO_HH_

# define OUTB(Port, Value)			       \
  __asm__ __volatile__ ("outb %0, %1"  \
                        :              \
                        : "a" ((char)(Value)), "d" ((short)(Port)))

# define INB(Port, Value)			\
  __asm__ __volatile__ ("inb %1, %0" \
                        : "=a" (Value) \
                        : "d" ((short)(Port)))

# define INW(Port, Value)			\
  __asm__ __volatile__ ("inw %1, %0" \
                        : "=a" (Value) \
                        : "d" ((short)(Port)))

#endif /* !IO_HH_ */
