/*
 * File: logger.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Simple log system for the bootloader
 *
 */

#include "io.hh"
#include "utils.h"

#include "logger.h"

#define COLUMN_NUMBER 80
#define LINE_NUMBER 25

static char* log_video_mem;

static unsigned char log_cursor_c;
static unsigned char log_cursor_l;

static unsigned char log_attr;

static void log_move_cursor()
{
  unsigned short position = (log_cursor_l * 80) + log_cursor_c;

  OUTB(0x3D4, 14);
  OUTB(0x3D5, (position >> 8));
  OUTB(0x3D4, 15);
  OUTB(0x3D5, position);
}

static void log_display_tab()
{
  unsigned char i = 0;

  for ( ; i < 8; i++)
  {
    log_video_mem[(log_cursor_l * 80 + log_cursor_c) * 2] = ' ';
    log_video_mem[(log_cursor_l * 80 + log_cursor_c) * 2 + 1] = log_attr;
    log_cursor_c++;
    if (log_cursor_c >= 80)
    {
      log_cursor_l++;
      log_cursor_c = 0;
    }
    if (log_cursor_l >= 25)
      log_cursor_l = 0;
    log_move_cursor();
  }
}

static void log_scroll()
{
  memcpy(log_video_mem, log_video_mem + COLUMN_NUMBER * 2,
          COLUMN_NUMBER * (LINE_NUMBER - 1) * 2);

  for (unsigned int i = 0; i < COLUMN_NUMBER; i++)
  {
    log_video_mem[((LINE_NUMBER - 1) * COLUMN_NUMBER + i) * 2] = 0;
    log_video_mem[((LINE_NUMBER - 1) * COLUMN_NUMBER + i) * 2 + 1] = log_attr;
  }
}

static void log_clear_screen()
{
  unsigned int i = 0, j = 0;

  for ( ; j < LINE_NUMBER; j++)
    for (i = 0; i < COLUMN_NUMBER; i++)
    {
      log_video_mem[(j * 80 + i) * 2] = 0;
      log_video_mem[(j * 80 + i) * 2 + 1] = log_attr;
    }
}

void log_init()
{
  log_video_mem = (char*)VGA_MEM;
  log_cursor_c = 0;
  log_cursor_l = 0;
  log_move_cursor();
  log_attr = DEFAULT_ATTR;
  log_clear_screen();
}

static void log_update_cursor(void)
{
  if (log_cursor_c >= COLUMN_NUMBER)
  {
    log_cursor_l++;
    log_cursor_c = 0;
  }
  if (log_cursor_l >= LINE_NUMBER)
  {
    log_cursor_l = LINE_NUMBER - 1;
    log_scroll();
  }
  log_move_cursor();
}

static void log_display_char(char c)
{
  if (c == '\n')
  {
    log_cursor_l++;
    log_cursor_c = 0;
    log_update_cursor();
    return;
  }
  else if (c == '\r')
  {
    log_update_cursor();
    log_cursor_c = 0;
    log_update_cursor();
    return;
  }
  else if (c == '\t')
  {
    log_display_tab();
    log_update_cursor();
    return;
  }
  else
  {
    log_video_mem[(log_cursor_l * 80 + log_cursor_c) * 2] = c;
    log_video_mem[(log_cursor_l * 80 + log_cursor_c) * 2 + 1] = log_attr;
  }

  log_cursor_c++;
  log_update_cursor();
}

void log_display_string(char* s)
{
  unsigned int i = 0;
  for ( ; s[i]; i++)
    log_display_char(s[i]);
}

void log_display_string_n(char* s, u32 len)
{
  unsigned int i = 0;
  for ( ; s[i] && len; i++, len--)
    log_display_char(s[i]);
}

void log_display_number(int val, unsigned int base)
{
  int i = 32;
  char buff[33];

  if (base == 0 || base < 2 || base > 16)
    return;

  if (val < 0 && base == 10)
  {
    val = -val;
    log_display_char('-');
  }

  if (val == 0)
  {
    log_display_string("0");
    return;
  }

  buff[i--] = 0;

  for( ; val && i; --i, val /= base)
    buff[i] = "0123456789abcdef"[val % base];

  log_display_string(buff + i + 1);
}

