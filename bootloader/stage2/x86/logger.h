/*
 * File: logger.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Simple log system for the bootloader
 *
 */

#ifndef LOGGER_H_
# define LOGGER_H_

# include <types.hh>

# define VGA_MEM 0xb8000
# define DEFAULT_ATTR 0x07
# define BLUE_ATTR 0x17
# define LINE_NUMBER 25
# define COLUMN_NUMBER 80

/* Initialize the console manager */
void log_init();

/* Display a string on screen */
void log_display_string(char* s);

/* Display a string on screen */
void log_display_string_n(char* s, u32 len);

/* Small print function */
void log_display_number(int val, unsigned int base);

#endif /* !LOGGER_H_ */
