/*
 * File: mbr.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Functions that manipulates the boot sector
 *
 */

#include "abort.h"
#include "logger.h"

#include "mbr.h"

u32 findBootable(struct BootSector* bsector)
{
  if (bsector->signature != MBR_SIGNATURE)
    abort("Boot Sector is not valid.");

  for (u8 i = 0; i < 4; i++)
  {
    struct PartitionEntry* current = &(bsector->part[i]);

    if (current->boot_id == PART_BOOTABLE)
    {
      if (current->system_id == LINUX_PARTITION)
        return current->lba_start;
    }
  }

  abort("No bootable ext2 partition found.");

  /* Never reached since abort never return */
  return 0;
}
