/*
 * File: mbr.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Structure of the boot sector
 *
 */

#ifndef MBR_H_
# define MBR_H_

# include <types.hh>

# define MBR_SIGNATURE 0xaa55
# define PART_BOOTABLE 0x80
# define LINUX_PARTITION 0x83

struct PartitionEntry
{
  u8 boot_id;
  u8 begin_head;
  u8 begin_sect;
  u8 begin_cycl;
  u8 system_id;
  u8 end_head;
  u8 end_sect;
  u8 end_cycl;
  u32 lba_start;
  u32 lba_number;
} __attribute__((packed));

struct BootSector
{
  u8 stage1[446];
  struct PartitionEntry part[4];
  u16 signature;
} __attribute__((packed));

/*
 * Return the lba of the first sector of the first
 * ext2 bootable parition
 */
u32 findBootable(struct BootSector* bsector);

#endif /* !MBR_H_ */
