/*
 * File: memory_map.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Utils for the memory map
 *
 */

#include "logger.h"

#include "memory_map.h"

void dump_memory_map(struct e820entry* mmap, u32 entry_number)
{
  log_display_string("Memory map with ");
  log_display_number(entry_number, 10);
  log_display_string(" entries : \n");

  for (u32 i = 0; i < entry_number; i++)
  {
    log_display_number(i, 10);
    log_display_string(" : ");
    log_display_number(mmap[i].addr, 16);
    log_display_string(" - ");
    log_display_number(mmap[i].len, 16);
    log_display_string(" - ");

    if (mmap[i].type == E820_FREE)
      log_display_string("Free");
    else
      log_display_string("Reserved");
    log_display_string("\n");
  }
  log_display_string("\n");
}
