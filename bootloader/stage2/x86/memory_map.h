/*
 * File: memory_map.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Useful types for e820 memory detection
 *
 */

#ifndef MEMORY_MAP_H_
# define MEMORY_MAP_H_

# include <types.hh>

# define E820_FREE 1
# define E820_RESERVED 2
# define E820_ACPI 3
# define E820_ACPI_NVS 4
# define E820_BAD 5

# define MEM_LOWER 0
# define MEM_UPPER 0x100000

struct e820entry
{
  u32 size;
  u64 addr; /* start of memory segment */
  u64 len; /* size of memory segment */
  u32 type; /* type of memory segment */
} __attribute__((packed));

void dump_memory_map(struct e820entry* mmap, u32 entry_number);

#endif /* !MEMORY_MAP_H_ */

