#include <efi.h>
#include <efilib.h>
#include "multiboot.h"
#include <efiapi.h>
#include "elf.h"

#ifdef X64

# define ELF_EHDR Elf64_Ehdr
# define ELF_PHDR Elf64_Phdr

#else

# define ELF_EHDR Elf32_Ehdr
# define ELF_PHDR Elf32_Phdr

#endif

#define MAX(A,B) ((A) > (B) ? (A) : (B))

EFI_BOOT_SERVICES	*bootServices;
/* EFI_RUNTIME_SERVICES	*runtime; */
EFI_HANDLE		 hImage;
EFI_FILE_HANDLE	         fh;

UINTN end_kernel_addr = 0;

static void jumpToKernel(void *entry_point, void *multiboot)
{
  __asm__ __volatile__ (
      "push %0\n"
      "pop %%ebx\n"
      "jmp *%%ecx \n"
      :
      : "r" (multiboot), "a" (MULTIBOOT_BOOTLOADER_MAGIC), "c" (entry_point)
      );
}

static void abort(char* msg)
{
  Print(L"%a\n", msg);
  uefi_call_wrapper(bootServices->Exit, 4, hImage, EFI_ABORTED, 0, 0);
}

static void open(CHAR16* filename,
                 UINT64  open_mode)
{
  EFI_STATUS err;

  err = uefi_call_wrapper(fh->Open, 5, fh, &fh, filename, open_mode, (UINT64)0);
  if (err != EFI_SUCCESS)
  {
    Print(L"Cannot open file, error = %d\n", err);
    abort("Leaving...");
  }
}

static UINTN read(char* destination,
                  UINTN size)
{
  EFI_STATUS err;

  err = uefi_call_wrapper(fh->Read, 3, fh, &size, destination);
  if (err != EFI_SUCCESS)
  {
    Print(L"Cannot read file, error = %d\n", err);
    abort("Leaving...");
  }

  return size;
}

static void seek(UINT64 position)
{
  EFI_STATUS err;

  err = uefi_call_wrapper(fh->SetPosition, 2, fh, position);
  if (err != EFI_SUCCESS)
  {
    Print(L"Cannot set position, error = %d\n", err);
    abort("Leaving...");
  }
}

static void* page_alloc(EFI_MEMORY_TYPE memory_type,
                        UINTN nb_pages,
                        EFI_PHYSICAL_ADDRESS address)
{
  EFI_STATUS err;
  EFI_ALLOCATE_TYPE type;

  if (address != 0)
    type = AllocateAddress;
  else
    type = AllocateAnyPages;

  err = uefi_call_wrapper(bootServices->AllocatePages, 4, type,
                          memory_type, nb_pages, &address);
  if (err != EFI_SUCCESS)
  {
    Print(L"Error during allocating memory:\n ");
    switch (err)
    {
    case EFI_OUT_OF_RESOURCES:
      Print(L"Out of resources\n");
    case EFI_INVALID_PARAMETER:
      Print(L"Invalid parameter\n");
    case EFI_NOT_FOUND:
      Print(L"Requested pages cannot be found\n");
    default:
      Print(L"Unknown error\n");
    }
    abort("Leaving...");
  }

  return (void*)address;
}

static void* alloc(EFI_MEMORY_TYPE memory_type,
                   UINTN size)
{
  EFI_STATUS err;
  void* buff;

  err = uefi_call_wrapper(bootServices->AllocatePool, 3,
                          memory_type, size, (void**)&buff);
  if (err != EFI_SUCCESS)
  {
    Print(L"Error during allocating memory:\n");
    switch (err)
    {
    case EFI_OUT_OF_RESOURCES:
      Print(L"Out of resources\n");
    case EFI_INVALID_PARAMETER:
      Print(L"Invalid parameter\n");
    default:
      Print(L"Unknown error\n");
    }
    abort("Leaving...");
  }

  return buff;
}

static void* elf_load(CHAR16* kernel_name)
{
  // Open the kernel file and read header and PH at least
  Print(L"Loading kernel: %s\n", kernel_name);
  open(kernel_name, EFI_FILE_MODE_READ);
  char* buff = alloc(EfiLoaderData, 1024);
  read(buff, 1024);

  ELF_EHDR* header = (ELF_EHDR*)buff;

  if (CompareMem(header->e_ident, ELFMAG, SELFMAG))
    abort("Kernel Image is not a valid ELF");

  uint16_t entries = header->e_phnum;
  for (uint16_t i = 0; i < entries; i++)
  {
    ELF_PHDR* ph = (ELF_PHDR*)(buff + header->e_phoff
                                    + i * header->e_phentsize);

    if (ph->p_type == PT_LOAD)
    {
      Print(L"-> Load program header at %x with size %x...",
            ph->p_paddr, ph->p_memsz);

      // Read or copy program header to the right location
      char* segment = page_alloc(EfiLoaderCode, (ph->p_memsz - 1)/ 4096 + 1,
                                 ph->p_paddr);
      seek(ph->p_offset);
      read(segment, ph->p_filesz);

      SetMem((char*)ph->p_paddr + ph->p_filesz, 0, ph->p_memsz - ph->p_filesz);
      end_kernel_addr = MAX(end_kernel_addr, ph->p_paddr + ph->p_memsz);
      Print(L"OK\n");
    }
  }

  return (void*)header->e_entry;
}

static EFI_STATUS
fs_init(EFI_HANDLE image)
{
  EFI_STATUS	err;

#define HANDLES_NO	10
  EFI_HANDLE	handles[HANDLES_NO];
  UINTN		size = HANDLES_NO * sizeof(EFI_HANDLE);

  EFI_FILE_IO_INTERFACE *io;
  EFI_HANDLE		dev_handle;


  // query the handle of array's size
  err = uefi_call_wrapper(bootServices->LocateHandle, 5,
			  ByProtocol, &FileSystemProtocol,
			  NULL, &size, handles);
  if (err != EFI_SUCCESS)
  {
    if (size == 0)
      return err;
    else
    {
      Print(L"Size of handles : %d\n", size);
      return err;
    }
  }

  dev_handle = handles[0];
  err = uefi_call_wrapper(bootServices->OpenProtocol, 6,
			  dev_handle, &FileSystemProtocol,
			  (void*)&io, hImage, NULL,
			  EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL);
  if (err != EFI_SUCCESS)
  {
    Print(L"Cannot retreive protocol interface\n");
    return err;
  }

  err = uefi_call_wrapper(io->OpenVolume, 2, io, &fh);
  if (err != EFI_SUCCESS)
  {
    Print(L"Cannot open volume!\n");
    return err;
  }

  return err;
}

static EFI_STATUS
set_multiboot(EFI_MEMORY_DESCRIPTOR *mem_desc, UINTN mem_desc_size,
              UINTN mem_desc_it_size, multiboot_info_t** multiboot)
{
  multiboot_info_t *multi;
  multiboot_memory_map_t *maps;
  UINTN mapNr = mem_desc_size / mem_desc_it_size;
  UINTN mapSize = sizeof(multiboot_memory_map_t) * mapNr;
  UINTN nrPages = sizeof(multiboot_info_t) + mapSize;

  nrPages = (nrPages - 1) / 0x1000 + 1;
  multi = page_alloc(EfiLoaderData, nrPages,
      (end_kernel_addr & ~0xFFF) + 0x1000);

  maps = (multiboot_memory_map_t*)((char *)multi + sizeof(multiboot_memory_map_t));

  *multiboot = multi;

  multi->flags = MULTIBOOT_INFO_MEM_MAP;
  multi->mem_lower = 0; // TODO: correct this
  multi->mem_upper = 0; // TODO: correct this
  multi->cmdline = 0; // TODO: correct this

  multi->mods_count = 0; // TODO: correct this
  multi->mods_addr = 0; // TODO: correct this

  multi->mmap_addr = (multiboot_uint32_t) maps;
  multi->mmap_length = mapSize;

  for (UINTN i = 0; i < mapNr; i++)
  {
    maps[i].size = (mem_desc->NumberOfPages << 12);
    maps[i].addr = mem_desc->PhysicalStart;
    maps[i].len = sizeof(multiboot_memory_map_t);

    if (mem_desc->Type == EfiLoaderCode ||
        mem_desc->Type == EfiLoaderData ||
        mem_desc->Type == EfiBootServicesCode ||
        mem_desc->Type == EfiBootServicesData ||
        mem_desc->Type == EfiConventionalMemory
       )
      maps[i].type = MULTIBOOT_MEMORY_AVAILABLE;
    else
      maps[i].type = MULTIBOOT_MEMORY_RESERVED;

    mem_desc = (EFI_MEMORY_DESCRIPTOR *) ((char *) mem_desc + mem_desc_it_size);
  }

  return EFI_SUCCESS;
}

#define MAP_SIZE 4096

EFI_STATUS efi_main(EFI_HANDLE image,
                    EFI_SYSTEM_TABLE *systab)
{
  EFI_STATUS err;

  bootServices = systab->BootServices;
  hImage = image;

  InitializeLib(image, systab);


  if ((err = fs_init(image)) != EFI_SUCCESS)
  {
    Print(L"Error during init fs\n");
    return err;
  }

  void* entry = elf_load(L"mikro");
  multiboot_info_t* multi = 0;
  EFI_MEMORY_DESCRIPTOR* mem_map = alloc(EfiLoaderData, MAP_SIZE);
  UINTN mem_map_size = MAP_SIZE;
  UINTN map_key;
  UINTN descriptor_size;
  UINTN descriptor_version;

  err = uefi_call_wrapper(bootServices->GetMemoryMap, 5, &mem_map_size, mem_map,
                          &map_key, &descriptor_size, &descriptor_version);
    if (err != EFI_SUCCESS || mem_map_size > MAP_SIZE)
      abort("Get memory map error");

  err = set_multiboot(mem_map, mem_map_size, descriptor_size, &multi);
  if (err != EFI_SUCCESS)
    abort("Multiboot error");

  Print(L"Exit boot services...");
  while (err != EFI_SUCCESS)
  {
    err = uefi_call_wrapper(bootServices->GetMemoryMap, 5, &mem_map_size, mem_map,
                          &map_key, &descriptor_size, &descriptor_version);
    if (err != EFI_SUCCESS || mem_map_size > MAP_SIZE)
      abort("Get memory map error");

    err = uefi_call_wrapper(bootServices->ExitBootServices, 2,  hImage, map_key);
  }

  // At this point it is forbidden to use BootServices !
  jumpToKernel(entry, multi);

  return EFI_SUCCESS;
}
