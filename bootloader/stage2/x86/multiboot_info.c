/*
 * File: multiboot_info.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Fill the multiboot info struct
 *
 */

#include <types.hh>

#include "allocator.h"
#include "abort.h"

#include "multiboot_info.h"

#define MODULE_MAX 256

static struct multiboot_mod_list* mods;

struct multiboot_header* findMultibootHeader(char* file_content)
{
  struct multiboot_header* header = (struct multiboot_header*)file_content;
  u32 tries = MULTIBOOT_SEARCH;

  for (u32 i = 0; i < tries; i++)
  {
    if (header->magic == MULTIBOOT_HEADER_MAGIC)
      if (header->checksum == (- (header->flags + header->magic)))
        return header;
    header = (struct multiboot_header*)++file_content;
  }

  return 0;
}

u8 multibootAlignModule(struct multiboot_header* header)
{
  return ((header->flags & MULTIBOOT_PAGE_ALIGN) > 0);
}

void fillMultibootMemInfo(multiboot_info_t* info,
                          struct e820entry* mmap,
                          u32 entry_number)
{
  info->mmap_addr = (u32)mmap;
  info->mmap_length = entry_number * sizeof (struct e820entry);

  for (u32 i = 0; i < entry_number; i++)
  {
    if (mmap->addr == MEM_LOWER && mmap->type == E820_FREE)
      info->mem_lower = (mmap->len / 1024);
    if (mmap->addr == MEM_UPPER && mmap->type == E820_FREE)
      info->mem_upper = (mmap->len / 1024);
    mmap++;
  }
}

static void create_mods_info(multiboot_info_t* info)
{
  mods = malloc(sizeof (struct multiboot_mod_list) * MODULE_MAX);
  info->mods_count = 0;
  info->mods_addr = (multiboot_uint32_t)mods;
}

void addMultibootCommandLine(multiboot_info_t* info, char* start)
{
  if (!start)
    return;

  info->flags |= MULTIBOOT_INFO_CMDLINE;
  info->cmdline = (multiboot_uint32_t)start;
}

int addMultibootModule(multiboot_info_t* info, void* start, void* end,
                       const char* name)
{
  if (info->mods_count == MODULE_MAX)
    return -1;

  mods[info->mods_count].mod_start = (multiboot_uint32_t)start;
  mods[info->mods_count].mod_end = (multiboot_uint32_t)end;
  mods[info->mods_count].cmdline = (multiboot_uint32_t)name;
  mods[info->mods_count].pad = 0;
  info->mods_count++;

  return 0;
}

int fillMultibootInfo(struct multiboot_header* header,
                      multiboot_info_t* info,
                      struct e820entry* mmap,
                      u32 entry_number)
{
  info->flags = 0;
  info->cmdline = 0;

  if ((header->flags & MULTIBOOT_MEMORY_INFO) > 0)
  {
    info->flags |= MULTIBOOT_INFO_MEMORY;
    info->flags |= MULTIBOOT_INFO_MEM_MAP;
    fillMultibootMemInfo(info, mmap, entry_number);
  }
  if ((header->flags & MULTIBOOT_VIDEO_MODE) > 0)
    abort("Multiboot video info is not currently supported");
  if ((header->flags & MULTIBOOT_AOUT_KLUDGE) > 0)
    abort("Multiboot aout kludge is not currently supported");

  create_mods_info(info);

  return 1;
}
