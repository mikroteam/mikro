/*
 * File: multiboot_info.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Fill the multiboot info struct
 *
 */

#ifndef MULTIBOOT_INFO_H_
# define MULTIBOOT_INFO_H_

# include <multiboot.h>
# include "memory_map.h"

/* Search for a multiboot header in some file content */
struct multiboot_header* findMultibootHeader(char* file_content);

/* Fill the multiboot_info */
int fillMultibootInfo(struct multiboot_header* header,
                      multiboot_info_t* info,
                      struct e820entry* mmap,
                      u32 entry_number);

/* True if the kernel wants aligned module */
u8 multibootAlignModule(struct multiboot_header* header);

int addMultibootModule(multiboot_info_t* info, void* start, void* end,
                       const char* name);
void addMultibootCommandLine(multiboot_info_t* info, char* start);

#endif /* !MULTIBOOT_INFO_H_ */
