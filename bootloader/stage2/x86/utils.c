/*
 * File: utils.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Some useful tools for the bootloader
 *
 */

#include "utils.h"

void* memcpy(void* s1, void* s2, unsigned int n)
{
  char* s1c = (char*)s1;
  char* s2c = (char*)s2;

  for ( ; n; s1c++, s2c++, n--)
    s1c[0] = s2c[0];

  return s1;
}

void* memset(void* s1, u8 c, unsigned int n)
{
  char* s1c = (char*)s1;

  for ( ; n; s1c++, n--)
    s1c[0] = c;

  return s1;
}

u32 strlen(const char* str)
{
  u32 i = 0;

  for ( ; str[i]; i++)
    ;

  return i;
}

u8 strncmp(const char* a, const char* b, u32 len)
{
  u32 i = 0;

  for ( ; a[i] && b[i] && len; i++, len--)
    if (a[i] != b[i])
      return b[i] - a[i];

  if (len == 0)
    return 0;

  return b[i] - a[i];
}
