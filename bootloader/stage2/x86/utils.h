/*
 * File: utils.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Some useful tools for the bootloader
 *
 */

#ifndef UTILS_H_
# define UTILS_H_

# include <types.hh>

# define MAX(A,B) ((A) > (B) ? (A) : (B))
# define MIN(A,B) ((A) > (B) ? (B) : (A))

void* memcpy(void* s1, void* s2, unsigned int n);

void* memset(void* s1, u8 c, unsigned int n);

u32 strlen(const char* str);

u8 strncmp(const char* a, const char* b, u32 len);

#endif /* !UTILS_H_ */
