/*
 * File: __cxa_guard.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Guard that init static variables in C++
 *
 */

#ifndef __CXA_GUARD_HH_
# define __CXA_GUARD_HH_

# include <types.hh>
# include <spinlock.hh>

/* 64 bits to be compliant with the ABI */
typedef u64 __guard;

extern "C" int __cxa_guard_acquire(__guard*);
extern "C" void __cxa_guard_release(__guard*);
extern "C" void __cxa_guard_abort(__guard*);

#endif /* !__CXA_GUARD_HH_ */
