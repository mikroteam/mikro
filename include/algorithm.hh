/*
 * File: algorithm.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Basic alogrithm
 *
 */

#ifndef ALGORITHM_H_
# define ALGORITHM_H_

template<typename T>
T max(T a, T b)
{
  return (a > b ? a : b);
}

#endif /* !ALGORITHM_H_ */
