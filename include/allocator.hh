/**
 * \file allocator.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief functions needed to allocate memory
 */

#ifndef ALLOCATOR_HH
#define ALLOCATOR_HH

#include <types.hh>

enum NewFlags
{
  STANDARD        = 0x0,
  CRITICAL        = 0x1,
  ZONE_DMA        = 0x8,
};

/**
 * \fn operator new
 * \brief define operator new in kernel
 *
 * \param size requested memory size
 * \param flags requested memory flags
 * \return NULL if something wrong append, memory address if OK
 */
void *operator new(size_t size, u16 flags) noexcept;

/**
 * \fn operator new
 * \brief define operator new in kernel (Allocate with ZONE_NORMAL flag only)
 *
 * \param size requested memory size
 * \return NULL if something wrong append, memory address if OK
 */
void *operator new(size_t size) noexcept;

/**
 * \fn operator new[]
 * \brief define operator new in kernel (Allocate with ZONE_NORMAL flag only)
 *
 * \param size requested memory size
 * \return NULL if something wrong append, memory address if OK
 */
void *operator new[](size_t size, u16 flags) noexcept;

/**
 * \fn operator new[]
 * \brief define operator new in kernel (Allocate with ZONE_NORMAL flag only)
 *
 * \param size requested memory size
 * \return NULL if something wrong append, memory address if OK
 */
void *operator new[](size_t size) noexcept;

/**
 * \fn operator delete
 * \brief define operator delete in kernel
 *
 * \param p pointer to delete
 */
void operator delete(void *p);

/**
 * \fn operator delete[]
 * \brief define operator delete in kernel
 *
 * \param p pointer to delete
 */
void operator delete[](void *p);

#endif /* !ALLOCATOR_HH */
