/*
 * File: arch_template.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Utilities classes for arch templates
 *
 */

#ifndef ARCH_TEMPLATE_HH_
# define ARCH_TEMPLATE_HH_

struct x86;

struct i386;

struct x86_64;

#endif /* !ARCH_TEMPLATE_HH_ */
