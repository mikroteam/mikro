/**
 * \file arm_atomic.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Every atomic assembly operations that can be useful for Arm
 *	  architecture
 */

#ifndef ATOMIC_HH
#define ATOMIC_HH

/**
 * \class Atomic
 * \brief Every atomic assembly operations that can be useful for Arm
 *	  architecture
 */

class Atomic
{
  /**
   * \fn atomic_add
   * \brief compute an atomic add (b <- a + b)
   *
   * \param a input
   * \param b input and output
   */
  static inline void atomic_add(int a, int b)
  {
    __asm__ __volatile__ ( \
      "1: dmb\n" \
      "ldrex r1, [%0]\n" \
      "add r1, r1, %1\n" \
      "strex r2, r1, [%0]\n" \
      "teq r2, #0\n" \
      "bne 1b\n" \
      : "r" (a) \
      : "r" (b) \
    );
  }

  /**
   * \fn atomic_sub
   * \brief compute an atomic sub (b <- a - b)
   *
   * \param a input
   * \param b input and output
   */
  static inline void atomic_sub(int a, int b)
  {
    __asm__ __volatile__ ( \
      "1: dmb\n" \
      "ldrex r1, [%0]\n" \
      "sub r1, r1, %1\n" \
      "strex r2, r1, [%0]\n" \
      "teq r2, #0\n" \
      "bne 1b\n" \
      : "r" (a) \
      : "r" (b) \
    );
  }

  /**
   * \fn xchg
   * \brief exchange a register and memory
   *
   * \param a register
   * \param b memory
   */
  static inline void xchg(int a, int *b)
  {
    __asm__ __volatile__ ( \
      "1: dmb\n" \
      "ldrex r1, [%0]\n" \
      "strex r2, %1, [%0]\n" \
      "teq r2, #0\n" \
      "bne 1b\n" \
      "mov r1, %0\n" \
      : "r" (a) \
      : "r" (b) \
    );
  }

  /**
   * \fn cmpxchg
   * \brief compare a and b and exchange c register and memory
   *
   * \param a register
   * \param b register
   * \param c memory
   */
  static inline void cmpxchg(int a, int b, int *c)
  {
    // TODO
    __asm__ __volatile__  ( \
    "1: dmb\n" \
    "ldrex r1, [%0]\n" \
    "strex r2, %1, [%0]\n" \
    "teq r2, #0\n" \
    "bne 1b\n" \
    "mov r1, %0\n" \
    : "r" (a) \
    : "r" (b) \
    );
  }

};
#endif /* !ATOMIC_HH */
