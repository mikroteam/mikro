/*
 * File: compat_checker.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Check platform compatibility
 *
 */

#ifndef COMPAT_CHECKER_HH_
# define COMPAT_CHECKER_HH_

# include <config.hh>
# include <arch/compat_checker_i.hh>

class CompatibilityChecker
{
  public:
    inline void check()
    {
      arch.check();
    }

  CompatibilityCheckerImpl<ARCH_GENERIC> arch;
};

extern CompatibilityChecker compat_checker;

#endif /* !COMPAT_CHECKER_HH_ */
