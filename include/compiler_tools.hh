/**
 * \file compiler_tools.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief All built-in compiler tools
 */

#ifndef COMPILER_TOOLS_HH_
# define COMPILER_TOOLS_HH_

# define likely(x) __builtin_expect((x), true)
# define unlikely(x) __builtin_expect((x), false)
# define isConstant(x) (__builtin_constant_p(x))

# define __asmcall __attribute__ ((regparm(0)))
# define __noreturn __attribute__((noreturn))

# define FS_RELATIVE address_space(257)
# define __percpu __attribute__((FS_RELATIVE, section(".cpuvar")))


template <typename T> inline T alwaysFromMem(T var)
{
  return (*(volatile T *)&(var));
}

#endif /* !COMPILER_TOOLS_HH_ */
