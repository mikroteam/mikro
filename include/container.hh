/**
 * \file container.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief generic class for containers
 */

#ifndef CONTAINER_HH_
#define CONTAINER_HH_

#include "types.hh"

class Container
{
  public:
    Container(u16 newFlags)
      : _newFlags(newFlags), _size(0)
    { }

    bool empty() const
    {
      return !_size;
    }

    size_t size() const
    {
      return _size;
    }

    void setNewFlags(u16 newFlags)
    {
      _newFlags = newFlags;
    }

  protected:
    u16 _newFlags;
    size_t _size;
};

#endif /* !CONTAINER_HH_ */
