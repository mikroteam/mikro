/*
 * File: cpu.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Useful informations about a CPU
 *
 */

#ifndef CPU_HH_
# define CPU_HH_

# include <compiler_tools.hh>
# include <thread.hh>

namespace cpu
{
  struct CPU
  {
    CPU();
    CPU(u32);

    u32 id;
    task::Thread* current_thread;
  };

  extern CPU* __percpu myself;
  extern u8 number;
  extern CPU** cpus;
}

#endif /* !CPU_HH_ */
