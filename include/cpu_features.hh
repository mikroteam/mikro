/*
 * File: cpu_features.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Enable/Disalbe some CPU features
 *
 */

#ifndef CPU_FEATURES_HH_
# define CPU_FEATURES_HH_

# include <config.hh>
# include <arch/cpu_features_i.hh>

namespace cpu
{
  class CpuFeatures
  {
    public:
      inline void init()
      {
        arch.init();
      }

    CpuFeaturesImpl<ARCH_GENERIC> arch;
  };

  extern CpuFeatures features;
}

#endif /* !CPU_FEATURES_HH_ */
