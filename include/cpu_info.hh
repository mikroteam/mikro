/*
 * File: cpu_info.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Retrieve informations about the CPU
 *
 */

#ifndef CPU_INFO_HH_
# define CPU_INFO_HH_

# include <config.hh>
# include <types.hh>
# include <arch/cpu_info_i.hh>

namespace cpu
{
  class CpuInfo
  {
    public:
      inline void init()
      {
        arch.init();
      }

      inline u32 getLogicals()
      {
        return arch.getLogicals();
      }

      inline u32 getCores()
      {
        return arch.getCores();
      }

      inline bool isHyperThreaded()
      {
        return arch.isHyperThreaded();
      }

      inline void printInfos()
      {
        arch.printInfos();
      }

      CpuInfoImpl<ARCH_GENERIC> arch;
  };

  extern CpuInfo info;
}

#endif /* !CPU_INFO_HH_ */
