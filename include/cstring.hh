/*
 * File: cstring.hh 
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Useful operation on C strings
 *
 */

#ifndef CSTRING_HH_
# define CSTRING_HH_

# include <types.hh>

extern "C"
{

void* memcpy(void* s1, const void* s2, size_t n);
void* memset(void* s1, int c, size_t n);
size_t strlen(const char* str);
int strcmp(const char* a, const char* b);
int strncmp(const char* a, const char* b, size_t len);

}

#endif /* !CSTRING_HH_ */
