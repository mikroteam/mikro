/*
 * File: display_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class that defines stream operators to write on screen
 *
 */

#ifndef DISPLAY_HH_
# define DISPLAY_HH_

# include <types.hh>
# include <display_writer.hh>

# define ATTR_DEFAULT 0x07

# define COLOR_BLACK 0
# define COLOR_BLUE 1
# define COLOR_GREEN 2
# define COLOR_CYAN 3
# define COLOR_RED 4
# define COLOR_MAGENTA 5
# define COLOR_YELLOW 6
# define COLOR_WHITE 7
# define COLOR_LIGHT (1 << 3)
# define COLOR_BLINK (1 << 7)

# define COLOR_FG(Color) (Color)
# define COLOR_BG(Color) (Color << 4)

namespace dsp
{
  struct StreamBaseHelper
  {
    StreamBaseHelper(unsigned char b)
      : base (b)
    {
    }

    unsigned char base;
  };

  struct StreamAttributes
  {
    StreamAttributes(unsigned char c)
      : attr (c)
    {
    }

    StreamAttributes& operator()(unsigned char c)
    {
      attr = c;
      return *this;
    }

    unsigned char attr;
  };

  class DisplayManager
  {
    public:
      DisplayManager();

      void init();

      DisplayManager& operator<<(const char* s);
      DisplayManager& operator<<(char c);
      DisplayManager& operator<<(int num);
      DisplayManager& operator<<(unsigned int num);
      DisplayManager& operator<<(unsigned long num);
      DisplayManager& operator<<(void* ptr);
      DisplayManager& operator<<(StreamBaseHelper& sbh);
      DisplayManager& operator<<(StreamAttributes& attr);

    private:
      unsigned char _base;
  };

  /* Used by all objects to print info on screen */
  extern DisplayWriter* dwriter;
  extern DisplayManager kout;
  extern StreamBaseHelper hex;
  extern StreamBaseHelper dec;
  extern StreamBaseHelper bin;

  /* Define some attributes for the stream */
  extern StreamAttributes attr;
}

#endif /* !DISPLAY_HH_ */
