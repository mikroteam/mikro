/*
 * File: display_writer.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Arch independent class that print char on screen
 *
 */

#ifndef DISPLAY_WRITER_HH_
# define DISPLAY_WRITER_HH_

namespace dsp
{

  class DisplayWriter
  {
    public:
      DisplayWriter();

      virtual void init() = 0;
      virtual void writeChar(char c) = 0;

      /* Do nothing by default */
      virtual void setAttr(unsigned char) {};
  };

}

#endif /* !DISPLAY_WRITER_HH_ */
