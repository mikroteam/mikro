/**
 * \file errors.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief All error handling
 */

#ifndef ERRORS_HH_
#define ERRORS_HH_

#include "compiler_tools.hh"
#include "logger.hh"

#define assert(x) \
  do { \
    if (unlikely(!(x))) { \
      log::fatal("PANIC") << "Assertion failed in " << __FUNCTION__  << \
      " at " << __FILE__ << ":" << __LINE__; \
      panic(); \
    } \
  } while(0)

#define assertFalse(x) \
  do { \
    if (unlikely(x)) { \
      log::fatal("PANIC") << "Assertion failed in " << __FUNCTION__  << \
      " at " << __FILE__ << ":" << __LINE__; \
      panic(); \
    } \
  } while(0)

__noreturn void panic();
__noreturn void earlyAbort(const char* err);

enum SyscallErrors
{
  ENOMEM  = 1, /* Memory error */
  EPERM   = 2, /* Operation not permitted */
  EINVAL  = 3, /* Invalid argument */
  ESRCH   = 4, /* No such process */
  EFAULT  = 5, /* Bad address */

  ECHAN   = 6, /* Channel error */
  ECHAND  = 7, /* Chandle error */
  EMHAND  = 8, /* Mhandle error */
  EMSIZE  = 9, /* Message size error */
  ENOPID  = 10 /* Too many pid created */
};

#endif /* !ERRORS_HH_ */
