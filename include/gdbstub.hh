/**
 * \file gdbstub.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Gdb remote stub
 */

#ifndef GDBSTUB_HH_
#define GDBSTUB_HH_

#include "arch/gdbstub_i.hh"
#include "config.hh"
#include "irq_manager.hh"
#include "types.hh"

namespace debug
{
  void irqHandler(irq::RegsDump *regs);

  class GdbStub
  {
    public:
      void init()
      {
        _impl.init();
      }

    private:
      GdbStubImpl<ARCH_GENERIC> _impl;

      bool handleRequest(u32 size, irq::RegsDump *regs);
      bool readMemory(u32 size);
      bool writeMemory(u32 size);

      friend void irqHandler(irq::RegsDump *regs);
  };

  extern GdbStub gdbStub;
}

#endif /* !GDBSTUB_HH_ */
