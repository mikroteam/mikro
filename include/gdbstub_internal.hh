/**
 * \file gdbstub_internal.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Gdb remote stub internal
 */

#ifndef GDBSTUB_INTERNAL_HH_
#define GDBSTUB_INTERNAL_HH_

#include "irq_manager.hh"
#include "types.hh"

namespace debug
{

  class GdbStubInternal
  {
    public:
      GdbStubInternal(const u32 port)
        : _port(port)
      { }

      void send(const char *buff, u32 size);
      u32 receive();

      u8 checksum(const char *buff, u32 size);

      u8 hex(char in);
      void hex2mem(const char *buff, char *to, u32 size);
      u32 hex2int(const char *buff, u32 &res, u32 size);
      void mem2hex(const char *in, char *buff, u32 size);

      u32 getPort()
      {
        return _port;
      }

      char buff[1024];

    private:
      const u32 _port;
  };

}

#endif /* !GDBSTUB_INTERNAL_HH_ */
