/*
 * File: irq_manager_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Irq manager for i386
 *
 */

#ifndef IRQ_MANAGER_I_HH_
# define IRQ_MANAGER_I_HH_

# include <compiler_tools.hh>
# include <arch_template.hh>
# include <arch/pic_manager.hh>
# include <arch/asm.hh>
# include <interrupt.hh>
# include <irq_common.hh>

# define IRQ_VECTOR_SIZE 256

# define IRQ_VECTOR_DOUBLEFAULT 0x8
# define IRQ_VECTOR_GPE 0xd
# define IRQ_VECTOR_PAGEFAULT 0xe

# define IRQ_VECTOR_PIC_BASE 0x40
# define IRQ_VECTOR_PIC_MASTER_BASE IRQ_VECTOR_PIC_BASE
# define IRQ_VECTOR_PIC_SLAVE_BASE (IRQ_VECTOR_PIC_BASE + 8)
# define IRQ_VECTOR_IOAPIC_BASE (IRQ_VECTOR_PIC_BASE)

# define IRQ_VECTOR_PIT (IRQ_VECTOR_IOAPIC_BASE + 0)
# define IRQ_VECTOR_KEYBOARD (IRQ_VECTOR_IOAPIC_BASE + 1)

# define IRQ_VECTOR_SYSCALL 0x80

# define IRQ_VECTOR_LAPIC_BASE 0x90


namespace irq
{

  struct RegsDumpI386 : public RegsDump
  {
    u32 ds;
    u32 es;
    u32 fs;
    u32 gs;
    u32 edi;
    u32 esi;
    u32 ebp;
    u32 esp;
    u32 ebx;
    u32 edx;
    u32 ecx;
    u32 eax;
    u32 int_num;
    /* These registers are pushed by the CPU */
    u32 err_code; /* We add a dummy error if needed */
    u32 eip;
    u32 cs;
    u32 eflags;
    u32 esp_prev;
    u32 ss;
  } __attribute((packed));

  struct ErrorCodePageFault
  {
    u8 present : 1;
    u8 wr : 1;
    u8 us : 1;
    u8 rsvd: 1;
    u8 id : 1;
    u8 unused : 3;
  } __attribute((packed));

  template<typename T>
  class IRQManagerImpl
  {
  };

  template<>
  class IRQManagerImpl<i386>
  {
    public:
      void init();
      void initAp();
      void initGlobalController();
      bool setHandler(u32 num, irq_handler h);
      bool setInterruptController(u32 num, InterruptController* c);
      InterruptController* getInterruptController(u32 num);

      RegsDump* getCurrentRegsDump();
      void setCurrentRegsDump(RegsDump *regs);

      void maskInterrupt(u32 num);
      void unmaskInterrupt(u32 num);

    private:
      void _fillIdt(u32 idx, size_t function_addr, u8 dpl);
      void _zeroIdt(u32 idx);

      struct Idt
      {
        u32 off_0_15 : 16;
        u32 seg_select : 16;
        u32 constant1 : 8;
        u32 t : 1;
        u32 constant2 : 2;
        u32 d : 1;
        u32 constant3 : 1;
        u32 dpl : 2;
        u32 present : 1;
        u32 off_16_31 : 16;
      } __attribute__((packed));

      struct IdtPtr
      {
        u16 limit;
        u32 base;
      } __attribute__((packed));

      struct Idt _idt_table[IRQ_VECTOR_SIZE];
      struct IdtPtr _idt_ptr;
  };
}

#endif /* !IRQ_MANAGER_I_HH_ */
