#ifndef SERIAL_MANAGER_I_HH_
# define SERIAL_MANAGER_I_HH_

# define SERIAL_COM_1 0x3F8
# define SERIAL_COM_2 0x2F8
# define SERIAL_COM_3 0x3E8
# define SERIAL_COM_4 0x2E8

# define SERIAL_BR57600 0x02
# define SERIAL_8N1     0x03
# define SERIAL_FIFO_8  0x07

# include <arch/asm.hh>
# include <serial_manager.hh>
# include <types.hh>

namespace serial
{
  class SerialManagerI386 : public SerialManager
  {
    public:
      SerialManagerI386();

      /* writer driver management */
      virtual void writeChar(char c);
      virtual void setDriverWriterCom(int com);

      /* Serial management */
      virtual void init();
      virtual void init(int com);
      virtual void write(uint32_t port, const char* data, uint32_t size);
      virtual void read(uint32_t port, char* data, uint32_t size);


    private:
      int _get_real_com(int com);

      /* Attributes */
    private:
      uint16_t _port_driver_writer;
  };
}
#endif /* SERIAL_MANAGER_I_HH_ */
