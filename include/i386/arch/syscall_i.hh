/*
 * File: syscall_arch.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: x86 specific syscalls
 *
 */

#ifndef SYSCALL_ARCH_HH_
# define SYSCALL_ARCH_HH_

# include <arch/irq_manager_i.hh>

# define SYSCALL_GATE IRQ_VECTOR_SYSCALL

# define SYS_IN (SYSCALL_COMMON_LAST + 1)
# define SYS_OUT (SYSCALL_COMMON_LAST + 2)
# define SYS_VM86 (SYSCALL_COMMON_LAST + 3)

# define SYSCALL_SPECIFIC_LAST SYS_VM86

# include <arch/paging_macro_i.hh>
# include <arch_template.hh>
# include <compiler_tools.hh>
# include <cstring.hh>
# include <types.hh>

namespace syscall
{
  template<typename T>
  class SyscallManagerImpl
  {
  };

  template<>
  class SyscallManagerImpl<i386>
  {
    public:
      void init();

      bool copyFromUser(void *to, void *from, size_t size)
      {
        if (!checkAddress((size_t) from) ||
            !checkAddress((size_t) from + size))
          return false;

        memcpy(to, from, size);
        return true;
      }

      bool copyToUser(void *to, void *from, size_t size)
      {
        if (!checkAddress((size_t) to) || !checkAddress((size_t) to + size))
          return false;

        memcpy(to, from, size);
        return true;
      }

      size_t strncpyFromUser(void *to, void *from, size_t size);

      bool checkAddress(size_t addr)
      {
        if (addr == 0)
          return false;

        if ((size_t) addr >= KERNEL_AS_BEGIN)
          return false;

        return true;
      }
  };
}

struct VM86Regs
{
  u16 di;
  u16 si;
  u16 bp;
  u16 bx;
  u16 dx;
  u16 cx;
  u16 ax;
};

/* Arch specific syscalls */
int sys_in(u16 port, u8 size);
int sys_out(u16 port, u32 value, u8 size);
int sys_vm86(VM86Regs *regs, u8 intNum);

#endif /* !SYSCALL_ARCH_HH_ */
