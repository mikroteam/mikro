#ifndef TASK_MANAGER_I_HH_
# define TASK_MANAGER_I_HH_

# define KERNEL_STACK_PAGE_SIZE 2

# include <process.hh>
# include <irq_manager.hh>
# include <arch/irq_manager_i.hh>
# include <paging.hh>
# include <arch/asm.hh>
# include <arch/gdt_manager.hh>
# include <memory_manager.hh>
# include <cstring.hh>
# include <logger.hh>

namespace task
{
  struct tss
  {
    uint16_t		prev;
    uint16_t		prev_align;
    uint32_t		esp0;
    uint16_t		ss0;
    uint16_t		ss0_align;
    uint32_t		esp1;
    uint16_t		ss1;
    uint16_t		ss1_align;
    uint32_t		esp2;
    uint16_t		ss2;
    uint16_t		ss2_align;
    uint32_t		cr3;
    uint32_t		eip;
    uint32_t		eflags;
    uint32_t		eax;
    uint32_t		ecx;
    uint32_t		edx;
    uint32_t		ebx;
    uint32_t		esp;
    uint32_t		ebp;
    uint32_t		esi;
    uint32_t		edi;
    uint16_t		es;
    uint16_t		es_align;
    uint16_t		cs;
    uint16_t		cs_align;
    uint16_t		ss;
    uint16_t		ss_align;
    uint16_t		ds;
    uint16_t		ds_align;
    uint16_t		fs;
    uint16_t		fs_align;
    uint16_t		gs;
    uint16_t		gs_align;
    uint16_t		ldt;
    uint16_t		ldt_align;
    uint16_t		trap;
    uint16_t		io;
    u8              v8086_int_mask[32];
    u8              io_bitmap[8192];
    u8              io_end;
  }			__attribute__ ((packed));

  template<typename T>
  class TaskManagerImpl
  {
  };

  template<>
  class TaskManagerImpl<i386>
  {
    public:
      void init();
      __noreturn void idle();
  };
}

#endif /* TASK_MANAGER_I_HH_ */
