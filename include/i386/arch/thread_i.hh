/**
 * \file thread_i.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Thread implementation for i386
 */

#ifndef THREAD_I_HH_
#define THREAD_I_HH_

#include "irq_manager.hh"
#include "arch_template.hh"
#include "paging.hh"

namespace task
{

  template<typename T>
  struct ThreadImpl
  {
  };

  template<>
  struct ThreadImpl<i386>
  {
    ThreadImpl<i386>();
    bool init(mem::AddressSpace *as, uint32_t entry);

    void save(irq::RegsDump *curDump);
    void restore(mem::AddressSpace* as, irq::RegsDump *curDump);
    __noreturn void gotoUser(mem::AddressSpace* as);
    __noreturn void gotoTask(mem::AddressSpace* as, irq::RegsDump *curDump);

    void setReturn(size_t val)
    {
      regs.eax = val;
    }

    size_t getReturn()
    {
      return regs.eax;
    }

    /* Register Dump */
    irq::RegsDumpI386 regs;
  };

}

#endif /* !THREAD_I_HH_ */
