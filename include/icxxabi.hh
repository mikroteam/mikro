/*
 * File: icxxabi.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: atexit and finalize functions defined by the abi
 *
 */

#ifndef ICXXABI_HH_
# define ICXXABI_HH_

# include <compiler_tools.hh>

extern "C"
{

/*
 * Register a function to be called at the exit of the kernel.
 * This function returns -1 in case of failure, 0 otherwise.
 */
int __cxa_atexit(void (*destructor) (void *), void* arg, void* dso);

/*
 * If this functions is called with the null function, call EVERY destructor.
 * Otherwise call only the specified destructor.
 */
void __asmcall __cxa_finalize(void* f);

}

#endif /* !ICXXABI_HH_ */
