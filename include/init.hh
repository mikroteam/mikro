/*
 * File: init.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Init functions
 *
 */

#ifndef INIT_H_
# define INIT_H_

#include <multiboot.h>

extern char *k_cmd;

int initBsp(multiboot_info_t* info);

int initAp(u32 id);

#endif /* !INIT_H_ */
