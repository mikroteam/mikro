/**
 * \file init_daemon.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Handle init daemons
 */

#ifndef INIT_DAEMON_HH_
#define INIT_DAEMON_HH_

#include "elf.h"
#include "list.hh"
#include "paging.hh"

#ifdef MIKRO64
# define ElfN_Ehdr Elf64_Ehdr
# define ElfN_Phdr Elf64_Phdr
# define ElfN_Addr Elf64_Addr
#else
# define ElfN_Ehdr Elf32_Ehdr
# define ElfN_Phdr Elf32_Phdr
# define ElfN_Addr Elf32_Addr
#endif /*! MIKRO64 */

namespace task
{
  class InitDaemon
  {
    public:
      InitDaemon(void *vaddr, size_t nrPages)
        : _vaddr(vaddr), _nrPages(nrPages)
      { }

      void *load(mem::AddressSpace* as);

    private:
      void *_vaddr;
      size_t _nrPages;

      bool checkElfValid(ElfN_Ehdr* t);
      u16 loadRights(ElfN_Phdr* ph);
      int loadProgramHeader(void* elf, ElfN_Phdr* ph, mem::AddressSpace* as);
  };

  extern List<InitDaemon> initDaemons;
}

#endif /* !INIT_DAEMON_HH_ */
