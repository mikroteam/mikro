/*
 * File: interrupt.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Interruption
 *
 */

#ifndef INTERRUPT_HH_
# define INTERRUPT_HH_

# include <logger.hh>
# include <irq_common.hh>
# include <interrupt_controller.hh>

namespace irq
{
  struct Interrupt
  {
    Interrupt()
      : handler (0),
        controller (0)
    {
    }

    ~Interrupt() {};

    inline bool handle(struct RegsDump* regs)
    {
      if (handler)
      {
        handler(regs);
        return true;
      }
      return false;
    }

    inline void eoi(u8 intr)
    {
      if (controller)
        controller->acknowledge(intr);
    }

    inline void mask(u8 intr)
    {
      if (controller)
        controller->unmask(intr);
      else
        log::err("IRQ") << "Try to mask an irq with no controller";
    }

    inline void unmask(u8 intr)
    {
      if (controller)
        controller->unmask(intr);
      else
        log::err("IRQ") << "Try to unmask an irq with no controller";
    }

    irq_handler handler;
    InterruptController* controller;
  };

}

#endif /* !INTERRUPT_HH_ */
