/*
 * File: interrupt_controller.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Global interruption management
 *
 */

#ifndef INTERRUPT_CONTROLLER_HH_
# define INTERRUPT_CONTROLLER_HH_

# include <types.hh>

namespace irq
{
  class InterruptController
  {
    public:
      virtual ~InterruptController() {};
      virtual void mask(u8 intr) = 0;
      virtual void unmask(u8 intr) = 0;
      virtual void acknowledge(u8 intr) = 0;
  };

  extern InterruptController* global_controller;
}

#endif /* !INTERRUPT_CONTROLLER_HH_ */
