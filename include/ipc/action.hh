/**
 * \file action.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Actions
 */

#ifndef ACTION_HH_
#define ACTION_HH_

#include "arch/asm.hh"
#include "ipc/message.hh"

namespace ipc
{

  class Action
  {
    public:
      void setReceiving(void *data, size_t size)
      {
        _data = data;
        _size = size;
      }

      bool checkSize(Message *msg)
      {
        return (_size >= msg->msgSize());
      }

      int doCopyFromSender(Message &msg, task::Thread *rcver);
      int doCopyFromRcver(Message *msg);

    private:
      void *_data;
      size_t _size;
  };

}

#endif /* !ACTION_HH_ */
