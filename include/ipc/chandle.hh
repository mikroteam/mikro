/**
 * \file chandle.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Channel handle for IPC
 */

#ifndef CHANDLE_HH_
#define CHANDLE_HH_

#include "arch/asm.hh"
#include "ipc/connection.hh"
#include "ipc/ipc_data.hh"
#include "ipc/message.hh"
#include "ipc/message.hh"
#include "list.hh"
#include "spinlock.hh"

#define ONLINE_BIT    0
#define READY_BIT     1
#define DYING_BIT     2
#define RECEIVING_BIT 3

namespace ipc
{

  class Channel;

  class Chandle
  {
    public:
      enum Mode
      {
        ONE_TO_ONE,
        ONE_TO_N_SERVER,
        ONE_TO_N_CLIENT
      };

      Chandle()
        : _flags(0)
      { }

      /* All the following functions MUST be used only by owner thread */
      void offline()
      {
        _flags = 0;
        _lock.lock();
        _msgQueue.clear();
        _lock.unlock();
      }

      void online(Mode mode, Channel *channel, IpcData *data)
      {
        _mode = mode;
        _channel = channel;
        _data = data;
        asmf::Atomic<ARCH_GENERIC>::setBit(&_flags, ONLINE_BIT);
      }

      Channel *channel()
      {
        return _channel;
      }

      Mode mode()
      {
        return _mode;
      }

      void setReceiving()
      {
        asmf::Atomic<ARCH_GENERIC>::setBit(&_flags, RECEIVING_BIT);
      }

      Message *getMessage();

      /* All the following functions can be used by any thread */
      int putMessage(Message &msg);

      void setDying()
      {
        asmf::Atomic<ARCH_GENERIC>::setBit(&_flags, DYING_BIT);
      }

      bool isOnline()
      {
        return _flags & (1 << ONLINE_BIT);
      }

      void setConnection(const Connection &connection)
      {
        _connection = connection;
        asmf::Atomic<ARCH_GENERIC>::setBit(&_flags, READY_BIT);
      }

      bool getConnection(Connection &conn)
      {
        if (_flags & (1 << READY_BIT))
        {
          conn = _connection;
          return true;
        }

        return false;
      }

      /* Functions that can be used by all */
      IpcData *getData()
      {
        return _data;
      }

    private:
      Channel *_channel;
      Connection _connection;
      Mode _mode;
      IpcData *_data;
      volatile u32 _flags;

      List<Message *> _msgQueue;
      Spinlock _lock;
  };

}

#endif /* !CHANDLE_HH_ */
