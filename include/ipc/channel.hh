/**
 * \file channel.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC channel management
 */

#ifndef CHANNEL_HH_
#define CHANNEL_HH_

#include "ipc/chandle.hh"
#include "ipc/connection.hh"
#include "ipc/mhandle.hh"
#include "map.hh"
#include "spinlock.hh"

#define CHANNEL_NAME_MAX 254

namespace ipc
{

  class Channel
  {
    public:
      enum Type
      {
        ONE_TO_ONE,
        ONE_TO_N
      };

      Channel(char *name, Type type)
        : _name(name), _type(type)
      { }

      ~Channel()
      {
        delete _name;
        // TODO: Clear all connections
      }

      void init(const Connection &owner)
      {
        _owner = owner;
      }

      char *getName()
      {
        return _name;
      }

      Type getType()
      {
        return _type;
      }

      const Connection &getOwner()
      {
        return _owner;
      }

      int addConnection(Connection &connection);
      int removeConnection(pid_t pid);
      bool getConnection(pid_t pid, Connection &conn)
      {
        _conLock.lock();
        Map<pid_t, Connection>::Iterator it_conn =
          _connections.find(pid);

        if (it_conn == _connections.end())
        {
          _conLock.unlock();
          return false;
        }

        conn = (*it_conn).second;
        _conLock.unlock();
        return true;
      }

    private:
      char *_name;
      Type _type;

      Connection _owner; // FIXME: see if thread part of Connection is needed
      Map<pid_t, Connection> _connections;
      Spinlock _conLock;
  };

}

#endif /* !CHANNEL_HH_ */
