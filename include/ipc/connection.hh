/**
 * \file connection.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC connection abstraction
 */

#ifndef CONNECTION_HH_
#define CONNECTION_HH_

namespace task
{
  class Thread;
}

namespace ipc
{
  class Chandle;

  class Connection
  {
    public:
      Connection()
        : _chandle(0), _thread(0)
      { }

      Connection(Chandle *chandle, task::Thread *thread)
        : _chandle(chandle), _thread(thread)
      { }

      bool isConnected() const
      {
        return _chandle;
      }

      Chandle *chandle() const
      {
        return _chandle;
      }

      task::Thread *thread() const
      {
        return _thread;
      }

      void clear()
      {
        _chandle = 0;
      }

    private:
      Chandle *_chandle;
      task::Thread *_thread;
  };
}

#endif /* !CONNECTION_HH_ */
