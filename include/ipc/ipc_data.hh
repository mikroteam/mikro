/**
 * \file ipc_data.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Data Management
 */

#ifndef IPC_DATA_HH_
#define IPC_DATA_HH_

#include "ref_count.hh"

class IpcData
  : public RefCount
{
  public:
    IpcData(size_t data = 0)
      : _data(data), _pid(true)
    { }

    size_t getData()
    {
      return _data;
    }

    void setData(size_t d, bool pid)
    {
      _data = d;
      _pid = pid;
    }

    bool isPID()
    {
      return _pid;
    }

  private:
    size_t _data;
    bool _pid;
};

#endif /* !IPC_DATA_HH_ */
