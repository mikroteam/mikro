/**
 * \file ipc_process.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Process Manager
 */

#ifndef IPC_PROCESS_HH_
#define IPC_PROCESS_HH_

#define MIKRO_DEFAULT_MHANDLE_NR 27

#include "ipc/ipc_data.hh"
#include "ipc/mhandle.hh"
#include "map.hh"
#include "spinlock.hh"

namespace ipc
{

  class IPCProcessManager
  {
    public:
      IPCProcessManager()
        : _mhandles(0), _mhandleSize(0)
      { }

      bool init(size_t nrMhandles);

      IpcData *getPrivate(pid_t pid_server);
      void clearPrivate(pid_t pid_server);

      int getFreeMhandle();
      void setMhandleFree(int mid);
      bool replyMhandle(int mid, Connection &conn);
      bool getMhandle(int mid, Connection &conn);
      void setMhandleConn(int mid, Connection &conn)
      {
        assert(mid < _mhandleSize);
        _mhandles[mid].setConn(conn);
      }

    private:
      Map<pid_t, IpcData> _privateData;
      Spinlock _dataLock;

      Mhandle *_mhandles;
      int _mhandleSize;
      Spinlock _mhandleLock;
  };

}

#endif /* !IPC_PROCESS_HH_ */
