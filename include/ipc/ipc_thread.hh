/**
 * \file ipc_thread.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Thread Manager
 */

#ifndef IPC_MANAGER_HH_
#define IPC_MANAGER_HH_

#include "ipc/action.hh"
#include "ipc/chandle.hh"
#include "ipc/channel.hh"
#include "ipc/ipc_data.hh"
#include "ipc/mhandle.hh"
#include "spinlock.hh"

#define MIKRO_DEFAULT_CHANDLE_NR 27

namespace ipc
{

  struct LessString
  {
    bool operator ()(const char *str1, const char *str2) const
    {
      return (strcmp(str1, str2) < 0);
    }
  };

  class IPCThreadManager
  {
    public:
      IPCThreadManager()
        : _chandles(0), _chandleSize(0)
      { }

      bool init(size_t nrChandles)
      {
        assert(!_chandles);

        _chandles = new Chandle[nrChandles];

        if (!_chandles)
          return false;

        _chandleSize = nrChandles;
        return true;
      }

      int createChannel(char *name, Channel::Type type);
      int openChannel(char *name);
      int closeChannel(size_t cid);

      int send(size_t cid, Message &msg);
      int send(size_t cid, pid_t pid, Message &msg);
      int send(size_t cid, Message &msg, size_t mid);

      int receive(size_t cid, void *recvData, size_t recvSize);
      int srcv(size_t cid, Message &msg, void *recvData, size_t recvSize);

      int setPrivate(size_t cid, size_t mid, size_t data);

      int clear();

    private:
      Chandle *_chandles;
      size_t _chandleSize;
      Action _action;

      Chandle *getChandle(size_t id)
      {
        if (id-- >= _chandleSize)
          return 0;

        return &_chandles[id];
      }

      Chandle *getFreeChandle(size_t &id)
      {
        for (id = 0; id < _chandleSize; id++)
          if (!_chandles[id].isOnline())
            return &_chandles[id++];

        return 0;
      }

      int mhandleSetup(task::Process *process, Chandle *chandle, int &mid);
      void mhandleAccept(task::Process *process, Message *msg, int mid);
      void mhandleRelease(task::Process *process, int mid);

      int doSend(Message &msg, Connection &conn);
      int doReceive(Chandle *chandle, void *recvData, size_t recvSize);

      static Map<char *, Channel*, LessString> _channels;
      static Spinlock _channelLock;
  };

}

#endif /* !IPC_THREAD_HH_ */
