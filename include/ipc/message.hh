/**
 * \file message.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Message abstraction
 */

#ifndef MESSAGE_HH_
#define MESSAGE_HH_

#include "ipc/connection.hh"
#include "ipc/ipc_data.hh"
#include "types.hh"

namespace task
{
  class Thread;
  class Process;
}

namespace ipc
{

  class Message
  {
    public:
      Message(task::Thread *sender, void *payload, size_t payloadSize)
        : _sender(sender), _chandle(0), _payload(payload),
          _payloadSize(payloadSize), _msgSize(payloadSize), _mhandleId(-1)
      { }

      void setChandle(Chandle *chandle)
      {
        _chandle = chandle;
      }

      task::Thread *sender()
      {
        return _sender;
      }

      Chandle *chandle()
      {
        return _chandle;
      }

      Connection connection()
      {
        return Connection(_chandle, _sender);
      }

      void *payload()
      {
        return _payload;
      }

      size_t payloadSize()
      {
        return _payloadSize;
      }

      size_t msgSize()
      {
        return _msgSize;
      }

      void setClient(size_t mhandleId, bool del = false);
      void write(void *to, void *payload);

    private:
      task::Thread *_sender;
      Chandle *_chandle;
      void *_payload;
      size_t _payloadSize;
      size_t _msgSize;

      int _mhandleId;
  };

}

#endif /* !MESSAGE_HH_ */
