/**
 * \file mhandle.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Channel message handle for IPC
 */

#ifndef MHANDLE_HH_
#define MHANDLE_HH_

#include "ipc/connection.hh"

namespace ipc
{

  class Mhandle
  {
    public:
      Mhandle()
        : _used(false)
      { }

      bool reply(Connection &conn)
      {
        if (!_used)
          return false;

        _used = false;
        conn = _connection;
        return true;
      }

      void use()
      {
        _used = true;
      }

      void unuse()
      {
        _used = false;
      }

      bool isUsed()
      {
        return _used;
      }

      void setConn(Connection &conn)
      {
        _connection = conn;
      }

      bool getConn(Connection &conn)
      {
        if (!_used)
          return false;

        conn = _connection;
        return true;
      }

    private:
      bool _used;
      Connection _connection;
  };

}

#endif /* !MHANDLE_HH_ */
