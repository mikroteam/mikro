/**
 * \file syscall_ipc.hh
 * \author Julien Freche <julien.freche@lse.epita.fr>
 * \brief Interprocess communication syscalls
 */

#ifndef SYSCALL_IPC_HH_
# define SYSCALL_IPC_HH_

# define CHANNEL_ONE 0x0
# define CHANNEL_SERVER 0x1

# define CHANNEL_INVALID -1

# define IPC_RECV_ALL 0x0
# define IPC_RECV_COPY_ONLY 0x1

# include "types.hh"

typedef size_t pid_t;

/* --------------------- Common --------------------- */
/**
 * \fn sys_channel_create
 * \brief Create a channel to exchange between processes
 *
 * \param name Name of the process
 * \param flags Options associated with the channel
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int sys_channel_create(const char* name, u32 flags);

/**
 * \fn sys_channel_close
 * \brief Close a channel
 *
 * \param int chandle
 *
 * \return < 0 on error, 0 on success
 *
 */
int sys_channel_close(int ch);

/* --------------------- Client --------------------- */
/**
 * \fn sys_channel_open
 * \brief Open a channel as a client
 *
 * \param name Name of the process
 * \param flags Options associated with the channel
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int sys_channel_open(const char* name, u32 flags);

/**
 * \fn sys_channel_send
 * \brief Send a message to a process
 *
 * \param ch chandle
 * \param msg buffer message, cannot be NULL
 * \param size message size
 * \param flags Options for IPC
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int sys_channel_send(int ch, void* msg, size_t size, u32 flags);

/**
 * \fn sys_channel_recv
 * \brief Receive a message from a process
 *
 * \param ch chandle
 * \param msg buffer message, cannot be NULL. This parameter can be changed
 *        in case of map shared ipc.
 * \param size buffer size, message size on success
 * \param flags options for the IPC
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int sys_channel_recv(int ch, void* msg, size_t size, u32 flags);

/**
 * \fn sys_channel_srecv
 * \brief Send a message to a process and receive
 *
 * This is equivalent to channel_send and then channel_recv but
 * optimized for better performance.
 *
 * \param ch chandle to send/receive
 * \param msg_out buffer message for output, cannot be NULL
 * \param size_out buffer size for output
 * \param msg_in buffer message for input, cannot be NULL. This parameter can
 *        be changed in case of map shared ipc.
 * \param size_in buffer size for input, message size on success
 * \param flags options for the IPC
 *
 * \return < 0 on error, chandle otherwise
 *
 */
int sys_channel_srecv(int ch, void* msg_out, size_t size_out, void* msg_in,
    size_t size_in, u32 flags);

/* --------------------- Server --------------------- */
/**
 * \fn sys_channel_reply_close
 * \brief Reply to a client and close the mhandle
 *
 * \param mh mhandle, cannot be NULL
 * \param msg buffer message, can be NULL if the server doesn't want to reply
 * \param size buffer size, can be zero if the server doesn't want to reply
 *
 * \return < 0 on error, 0 on success
 *
 */
int sys_channel_reply_close(int ch, int mh, void* msg, size_t size, u32 flags);

/**
 * \fn sys_channel_send_event
 * \brief Send an event to a client
 *
 * \param ch chandle
 * \param pid process id
 * \param msg buffer message, cannot be NULL
 * \param size buffer size
 *
 * \return < 0 on error, 0 on success
 *
 */
int sys_channel_send_event(int ch, pid_t pid, void* msg, size_t size,
    u32 flags);

/**
 * \fn sys_channel_change_private
 * \brief Change private data associated with a client
 *
 * \param mh mhandle
 * \param dprivate new private data
 *
 * \return < 0 on error, 0 on success
 *
 */
int sys_channel_change_private(int ch, int mh, size_t dprivate);

#endif /* !SYSCALL_IPC_H_ */
