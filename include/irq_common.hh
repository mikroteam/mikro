/*
 * File: irq_common.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Common definitions for IRQ Handling
 *
 */

#ifndef IRQ_COMMON_HH_
# define IRQ_COMMON_HH_

namespace irq
{

  struct RegsDump
  {
  }__attribute__((packed));

  typedef void (*irq_handler)(struct RegsDump* regs);
}

#endif /* !IRQ_COMMON_HH_ */
