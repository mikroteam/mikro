/*
 * File: irq_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Abstract irq manager
 *
 */

#ifndef IRQ_MANAGER_HH_
# define IRQ_MANAGER_HH_

# include <types.hh>
# include <config.hh>
# include <irq_common.hh>
# include <interrupt.hh>
# include <arch/irq_manager_i.hh>

namespace irq
{
  class IRQManager
  {
    public:
      inline void init()
      {
        arch.init();
      }

      inline void initAp()
      {
        arch.initAp();
      }

      inline void initGlobalController()
      {
        arch.initGlobalController();
      }

      inline bool setHandler(u32 num, irq_handler h)
      {
        return arch.setHandler(num, h);
      }

      inline RegsDump* getCurrentRegsDump()
      {
        return arch.getCurrentRegsDump();
      }

      inline bool setInterruptController(u32 num, InterruptController* c)
      {
        return arch.setInterruptController(num, c);
      }

      inline InterruptController* getInterrupt(u32 num)
      {
        return arch.getInterruptController(num);
      }

      inline void maskInterrupt(u32 num)
      {
        arch.maskInterrupt(num);
      }

      inline void unmaskInterrupt(u32 num)
      {
        arch.unmaskInterrupt(num);
      }

      IRQManagerImpl<ARCH> arch;
  };

  extern IRQManager manager;
}

#endif /* !IRQ_MANAGER_HH_ */
