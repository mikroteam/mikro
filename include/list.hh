/**
 * \file list.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief handle lists
 */

#ifndef LIST_HH
#define LIST_HH

#include <allocator.hh>
#include <container.hh>
#include <errors.hh>

// FIXME: some STL functions are missing, I guess I'm lazy.
// TODO: add some documentation

template<typename A>
class List
  : public Container
{
  private:
    class ListItem
    {
      public:
        ListItem(const A &item)
          : _item(item), _next(0), _prev(0)
        { }

        A _item;
        ListItem *_next;
        ListItem *_prev;
    };

  public:
    List()
      : Container(STANDARD), _head(0), _tail(0)
    { }

    List(u16 newFlags)
      : Container(newFlags), _head(0), _tail(0)
    { }

    ~List()
    {
      clear();
    }

    bool copy(const List<A> &list)
    {
      clear();
      ListItem *lit = list._head;

      while (lit)
      {
        if (!push_back(lit->_item))
        {
          clear();
          return false;
        }
        lit = lit->_prev;
      }

      return true;
    }

    A &front()
    {
      assert(_head);
      return _head->_item;
    }

    A &back()
    {
      assert(_tail);
      return _tail->_item;
    }

    bool push_front(const A &item)
    {
      ListItem *lit = new(_newFlags) ListItem(item);

      if (!lit)
        return false;

      if (!_head)
      {
        _head = lit;
        _tail = lit;
      }
      else
      {
        lit->_next = _head;
        _head->_prev = lit;
        _head = lit;
      }

      _size++;
      return true;
    }

    A pop_front()
    {
      assert(_head);
      A res = _head->_item;

      if (_head == _tail)
      {
        delete _head;
        _head = 0;
        _tail = 0;
      }
      else
      {
        ListItem *lit = _head;
        _head = _head->_next;

        delete lit;
      }

      _size--;
      return res;
    }

    bool push_back(const A &item)
    {
      ListItem *lit = new(_newFlags) ListItem(item);

      if (!lit)
        return false;

      if (!_tail)
      {
        _head = lit;
        _tail = lit;
      }
      else
      {
        lit->_prev = _tail;
        _tail->_next = lit;
        _tail = lit;
      }

      _size++;
      return true;
    }

    A pop_back()
    {
      assert(_tail);
      A res = _tail->_item;

      if (_head == _tail)
      {
        delete _head;
        _head = 0;
        _tail = 0;
      }
      else
      {
        ListItem *lit = _tail;
        _tail = _tail->_prev;

        delete lit;
      }

      _size--;
      return res;
    }

    class Iterator
    {
      public:
        Iterator()
          : _cur_it(0)
        { }

        A &operator *()
        {
          assert(_cur_it);
          return _cur_it->_item;
        }

        bool operator ==(const Iterator &it)
        {
          return it._cur_it == _cur_it;
        }

        bool operator !=(const Iterator &it)
        {
          return it._cur_it != _cur_it;
        }

        Iterator &operator ++() // Prefix
        {
          if (_cur_it)
            _cur_it = _cur_it->_next;

          return *this;
        }

        Iterator &operator --() // Prefix
        {
          if (_cur_it)
            _cur_it = _cur_it->_prev;

          return *this;
        }


      private:
        List::ListItem *_cur_it;

        Iterator(List::ListItem *cur_it)
          : _cur_it(cur_it)
        { }

      friend class List;
    };

    Iterator begin()
    {
      return Iterator(_head);
    }

    Iterator end()
    {
      return Iterator();
    }

    bool insert(const Iterator &it, const A &new_it)
    {
      if (!it._cur_it)
      {
        push_back(new_it);
        return true;
      }

      ListItem *lit = new(_newFlags) ListItem(new_it);

      if (!lit)
        return false;

      if (it._cur_it == _head)
        _head = lit;

      lit->_next = it._cur_it->_next;
      it._cur_it->_next = lit;
      lit->_prev = it._cur_it;

      _size++;
      return true;
    }

    void erase(Iterator &it)
    {
      if (it._cur_it)
      {
        if (it._cur_it == _head && it._cur_it == _tail)
        {
          _head = 0;
          _tail = 0;
        }
        else if (it._cur_it == _head)
          _head = _head->_next;
        else if (it._cur_it == _tail)
          _tail = _tail->_prev;
        else
        {
          it._cur_it->_next->_prev = it._cur_it->_prev;
          it._cur_it->_prev->_next = it._cur_it->_next;
        }

        delete it._cur_it;
        it._cur_it = 0;

        _size--;
      }
    }

    void clear()
    {
      ListItem *lit_cur = _head;
      ListItem *lit_del;

      while (lit_cur)
      {
        lit_del = lit_cur;
        lit_cur = lit_cur->_prev;

        delete lit_del;
      }

      _size = 0;
      _head = 0;
      _tail = 0;
    }

  private:
    ListItem *_head;
    ListItem *_tail;

    List &operator =(const List<A> &list)
    {
      return list;
    }


    List(const List<A> &list)
    {
      list;
    }
};

#endif /* !LIST_HH */
