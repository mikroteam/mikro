/**
 * \file list_embedded.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief handle embedded lists
 */

#ifndef LIST_EMBEDDED_HH_
#define LIST_EMBEDDED_HH_

#include <allocator.hh>
#include <container.hh>
#include <errors.hh>

// FIXME: some STL functions are missing, I guess I'm lazy.
// TODO: add some documentation

template<typename A>
class ListEmbedded;

template<typename A>
class ListEmbeddedItem
{
  public:
    ListEmbeddedItem()
      : _next(0), _prev(0)
    { }

  protected:
    A *_next;
    A *_prev;

    friend class ListEmbedded<A>;
};

template<typename A>
class ListEmbedded
  : public Container
{
  public:
    ListEmbedded()
      : Container(STANDARD), _head(0), _tail(0)
    { }

    ListEmbedded(u16 newFlags)
      : Container(newFlags), _head(0), _tail(0)
    { }

    ListEmbedded(const ListEmbedded<A> &list)
    {
      clear();
      _head = list._head;
      _tail = list._tail;
    }

    A *front()
    {
      return _head;
    }

    A *back()
    {
      return _tail;
    }

    void push_front(A *item)
    {
      if (!_head)
      {
        _head = item;
        _tail = item;
      }
      else
      {
        item->_next = _head;
        _head->_prev = item;
        _head = item;
      }

      _size++;
    }

    A *pop_front()
    {
      if (!_head)
        return 0;

      A *res = _head;

      if (_head == _tail)
      {
        _head = 0;
        _tail = 0;
      }
      else
        _head = _head->_next;

      _size--;
      return res;
    }

    void push_back(A *item)
    {
      if (!_tail)
      {
        _head = item;
        _tail = item;
      }
      else
      {
        item->_prev = _tail;
        _tail->_next = item;
        _tail = item;
      }

      _size++;
    }

    A *pop_back()
    {
      if (!_tail)
        return 0;

      A *res = _tail;

      if (_head == _tail)
      {
        _head = 0;
        _tail = 0;
      }
      else
        _tail = _tail->_prev;

      _size--;
      return res;
    }

    class Iterator
    {
      public:
        Iterator()
          : _cur_it(0)
        { }

        A *operator *()
        {
          return _cur_it;
        }

        bool operator ==(const Iterator &it)
        {
          return it._cur_it == _cur_it;
        }

        bool operator !=(const Iterator &it)
        {
          return it._cur_it != _cur_it;
        }

        Iterator &operator ++() // Prefix
        {
          if (_cur_it)
            _cur_it = _cur_it->_next;

          return *this;
        }

        Iterator &operator --() // Prefix
        {
          if (_cur_it)
            _cur_it = _cur_it->_prev;

          return *this;
        }


      private:
        A *_cur_it;

        Iterator(A *cur_it)
          : _cur_it(cur_it)
        { }

      friend class ListEmbedded;
    };

    Iterator begin()
    {
      return Iterator(_head);
    }

    Iterator end()
    {
      return Iterator();
    }

    void insert(const Iterator &it, A *new_it)
    {
      if (!it._cur_it)
      {
        push_back(new_it);
        return;
      }

      if (it._cur_it == _head)
        _head = new_it;

      new_it->_next = it._cur_it->_next;
      it._cur_it->_next = new_it;
      new_it->_prev = it._cur_it;

      _size++;
    }

    void insert(A *it, A *new_it)
    {
      if (it == _head)
        _head = new_it;

      new_it->_next = it->_next;
      it->_next = new_it;
      new_it->_prev = it;

      _size++;
    }

    void erase(Iterator &it)
    {
      if (it._cur_it)
      {
        if (it._cur_it == _head && it._cur_it == _tail)
        {
          _head = 0;
          _tail = 0;
        }
        else if (it._cur_it == _head)
          _head = _head->_next;
        else if (it._cur_it == _tail)
          _tail = _tail->_prev;
        else
        {
          it._cur_it->_next->_prev = it._cur_it->_prev;
          it._cur_it->_prev->_next = it._cur_it->_next;
        }

        it._cur_it->_next = 0;
        it._cur_it->_prev = 0;

        it._cur_it = 0;
        _size--;
      }
    }

    void erase(A *it)
    {
      if (it == _head && it == _tail)
      {
        _head = 0;
        _tail = 0;
      }
      else if (it == _head)
        _head = _head->_next;
      else if (it == _tail)
        _tail = _tail->_prev;
      else
      {
        it->_next->_prev = it->_prev;
        it->_prev->_next = it->_next;
      }

      it->_next = 0;
      it->_prev = 0;
      _size--;
    }

    void clear()
    {
      _size = 0;
      _head = 0;
      _tail = 0;
    }

  private:
    A *_head;
    A *_tail;

    ListEmbedded &operator =(const ListEmbedded<A> &list)
    {
      return list;
    }
};

#endif /* !LIST_EMBEDDED_HH_ */
