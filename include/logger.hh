/*
 * File: logger.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Handle logs in the kernel side
 *
 */

#ifndef LOGGER_HH_
# define LOGGER_HH_

# include <cstring.hh>

# include <utility.hh>
# include <display_manager.hh>
# include <spinlock.hh>

namespace log
{
  enum LogLevel
  {
    Raw = 0,
    Fatal = 1,
    Error = 2,
    Warning = 3,
    Info = 4,
    Verbose = 5,
    Debug = 6
  };

  extern LogLevel global_log_level;

# ifdef DEBUG
  extern const char* log_focus_module;
  extern const char* log_current_module;
# endif

  class LoggerTicket
  {
    public:
      LoggerTicket(enum LogLevel l)
        : _lvl (l),
          _has_lock (true)
      {
        _lock.lock();
      }

      ~LoggerTicket()
      {
        if (_has_lock)
        {
          if (global_log_level >= _lvl)
            dsp::kout << "\n";
          _lock.unlock();
        }
      }

      // Transfer ownership
      LoggerTicket(LoggerTicket&& t)
        : _lvl (t._lvl),
          _has_lock (t._has_lock)
      {
        t._has_lock = false;
      }

      template<typename T>
      LoggerTicket& operator<<(T t)
      {
    # ifdef DEBUG
        if (log_focus_module && log_current_module)
          if (strcmp(log_focus_module, log_current_module))
            return *this;
    # endif

        if (global_log_level < _lvl)
          return *this;

        dsp::kout << t;
        return *this;
      }


    private:
      static Spinlock _lock;
      enum LogLevel _lvl;
      bool _has_lock;
  };

  class RawLogger
  {
    public:

      LoggerTicket operator()()
      {
        return move(LoggerTicket(Raw));
      }
  };

  class RealLogger
  {
    public:
      RealLogger(enum LogLevel l, const char* name, unsigned char color)
        : lvl (l),
          _name (name),
          _attr (color)
      {
      }

      LoggerTicket operator()(const char* module)
      {
        LoggerTicket ticket(lvl);

# ifdef DEBUG
        log_current_module = module;

        if (log_focus_module && log_current_module)
          if (strcmp(log_focus_module, log_current_module))
            return move(ticket);
# endif

        ticket << "["
              << dsp::attr(_attr) << _name << dsp::attr(ATTR_DEFAULT)
              << "] ";

        if (module)
          ticket << dsp::attr(COLOR_BLACK | COLOR_LIGHT)
            << module << dsp::attr(ATTR_DEFAULT) << ": ";

        return move(ticket);
      }

      enum LogLevel lvl;

    private:
      const char* _name;
      unsigned char _attr;
  };

  class FakeLogger
  {
    public:
      FakeLogger(enum LogLevel, const char*) {};

      inline FakeLogger& operator()(const char*)
      {
        return *this;
      }

      template<typename T>
      inline FakeLogger& operator<<(T)
      {
        return *this;
      }
  };

  extern RawLogger raw;
  extern RealLogger fatal;
  extern RealLogger err;
  extern RealLogger warn;
  extern RealLogger info;
  extern RealLogger verbose;

# ifdef DEBUG
  extern RealLogger debug;
# else
  extern FakeLogger debug;
# endif /* DEBUG */

}

#endif /* !LOGGER_HH_ */
