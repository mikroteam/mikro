/**
 * \file map.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief handle maps
 */

#ifndef MAP_HH
#define MAP_HH

#include <pair.hh>
#include <rbtree.hh>

// FIXME: some STL functions are missing, I guess I'm lazy.
// TODO: add some documentation

template<typename A, typename B, typename Compare = Less<A> >
class Map
  : public RbTree<Pair<A, B> >
{
  public:
    Map()
      : RbTree<Pair<A, B> >(STANDARD)
    { }

    Map(u16 newFlags)
      : RbTree<Pair<A, B> >(newFlags)
    { }

    ~Map()
    {
      RbTree<Pair<A, B> >::clear();
    }

    bool copy(const Map<A, B, Compare> &map)
    {
      RbTree<Pair<A, B> >::clear();
      return copy_internal(map);
    }

    B &operator [](const A &key)
    {
      typename RbTree<Pair<A, B> >::Node *nit = find_internal(key);
      assert(nit);
      return nit->_item;
    }

    Pair<typename RbTree<Pair<A, B> >::Iterator, bool>
      insert(const Pair<A, B> &new_item)
    {
      typename RbTree<Pair<A, B> >::Node *nit = 0;

      if (insert_internal(new_item, &nit))
        return make_pair(RbTree<Pair<A, B> >::makeIterator(nit), false);
      else
        return make_pair(RbTree<Pair<A, B> >::makeIterator(nit), true);
    }

    typename RbTree<Pair<A, B> >::Iterator find(const A &key) const
    {
      return RbTree<Pair<A, B> >::makeIterator(find_internal(key));
    }

    typename RbTree<A>::Iterator lower_bound(const A &key) const
    {
      return RbTree<A>::makeIterator(lbound_internal(key));
    }

    typename RbTree<A>::Iterator upper_bound(const A &key) const
    {
      return RbTree<A>::makeIterator(ubound_internal(key));
    }

  private:
    Compare _comp;

    bool inf(const A &a, const Pair<A, B> &b) const
    {
      return _comp(a, b.first);
    }

    bool inf(const Pair<A, B> &a, const A &b) const
    {
      return _comp(a.first, b);
    }

    bool inf(const Pair<A, B> &a, const Pair<A, B> &b) const
    {
      return _comp(a.first, b.first);
    }

    bool eq(const A &a, const Pair<A, B> &b) const
    {
      return !inf(a, b) && !inf(b, a);
    }

    bool insert_internal(const Pair<A, B> &item,
        typename RbTree<Pair<A, B> >::Node **n)
    {
      typename RbTree<Pair<A, B> >::Node *nit = RbTree<Pair<A, B> >::_root;

      if (RbTree<Pair<A, B> >::_root)
      {
        while (true)
        {
          if (eq(item.first, nit->_item))
          {
            *n = nit;
            return false;
          }

          if (inf(item.first, nit->_item))
          {
            if (!nit->_left)
            {
              nit->_left = new(RbTree<Pair<A, B> >::_newFlags)
                typename RbTree<Pair<A, B> >::Node(item, nit);
              nit = nit->_left;
              if (!nit)
              {
                *n = 0;
                return false;
              }
              break;
            }

            nit = nit->_left;
          }
          else
          {
            if (!nit->_right)
            {
              nit->_right = new(RbTree<Pair<A, B> >::_newFlags)
                typename RbTree<Pair<A, B> >::Node(item, nit);
              nit = nit->_right;
              if (!nit)
              {
                *n = 0;
                return false;
              }
              break;
            }

            nit = nit->_right;
          }
        }
      }
      else
      {
        nit = new(RbTree<Pair<A, B> >::_newFlags)
          typename RbTree<Pair<A, B> >::Node(item, 0);
        if (!nit)
        {
          *n = 0;
          return false;
        }
        RbTree<Pair<A, B> >::_root = nit;
      }

      //RbTree<Pair<A, B> >::insert_balance(nit);
      RbTree<Pair<A, B> >::_size++;
      *n = nit;

      return true;
    }

    typename RbTree<Pair<A, B> >::Node *find_internal(const A &key) const
    {
      typename RbTree<Pair<A, B> >::Node *nit = RbTree<Pair<A, B> >::_root;

      while (nit)
      {
        if (eq(key, nit->_item))
          return nit;

        if (inf(key, nit->_item))
          nit = nit->_left;
        else
          nit = nit->_right;
      }

      return 0;
    }

    typename RbTree<Pair<A, B> >::Node *lbound_internal(const A &key) const
    {
      typename RbTree<Pair<A, B> >::Node *res = 0;
      typename RbTree<Pair<A, B> >::Node *nit = RbTree<A>::_root;

      while (nit)
      {
        if (eq(key, nit->_item))
          return nit;

        if (inf(key, nit->_item))
        {
          if (!res || inf(nit->_item, res->_item))
            res = nit;

          nit = nit->_left;
        }
        else
          nit = nit->_right;
      }

      return res;
    }

    typename RbTree<Pair<A, B> >::Node *ubound_internal(const A &key) const
    {
      typename RbTree<Pair<A, B> >::Node *res = 0;
      typename RbTree<Pair<A, B> >::Node *nit = RbTree<A>::_root;

      while (nit)
      {
        if (inf(key, nit->_item))
        {
          if (!res || inf(nit->_item, res->_item))
            res = nit;

          nit = nit->_left;
        }
        else
          nit = nit->_right;
      }

      return res;
    }

    Map &operator =(const Map<A, B, Compare> &map)
    {
      return *map;
    }

    Map (const Map<A, B, Compare> &map)
    {
      map;
    }
};

#endif /* !MAP_HH */
