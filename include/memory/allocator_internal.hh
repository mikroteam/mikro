/**
 * \file allocator_internal.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief internal functions needed to allocate memory
 */

#ifndef ALLOCATOR_INTERNAL_HH
#define ALLOCATOR_INTERNAL_HH

#define KALLOC_MAXBUCKET 7

#include "spinlock.hh"
#include "types.hh"

class KernelAllocator
{
  public:
    KernelAllocator()
    {
      _pageHandler = &_pHStart;
    }

    void init();

    void startParameter(unsigned long paddrStart, unsigned long paddrEnd)
    {
      _pHStart.init(paddrStart, paddrEnd);
    }

    unsigned int pagingOK()
    {
      _pageHandler = &_pHKernel;
      return _pHStart.pageUsed();
    }

  private:
    struct PageHandler
    {
      PageHandler(bool mustCheck)
        : mustCheck(mustCheck)
      { }

      virtual void *getPage() = 0;
      virtual void leavePage(void *) = 0;
      bool mustCheck;
    };

    class PageHandlerStart
      : public PageHandler
    {
      public:
        PageHandlerStart()
          : PageHandler(false), _paddrEnd(0), _curPage(0), _nr_used(0)
        { }

        void init(unsigned long paddrStart, unsigned long paddrEnd)
        {
          _curPage = paddrStart;
          _paddrEnd = paddrEnd;
        }

        unsigned int pageUsed()
        {
          return _nr_used;
        }

        virtual void *getPage();
        virtual void leavePage(void *p)
        {
          (void) p;
        }

      private:
        unsigned long _paddrEnd;
        unsigned long _curPage;
        unsigned int _nr_used;
    };

    class PageHandlerKernel
      : public PageHandler
    {
      public:
        PageHandlerKernel()
          : PageHandler(true)
        { }

        virtual void *getPage();
        virtual void leavePage(void *p);
    };

    struct MemData
    {
      u8 flags;
      MemData *nextData;
      MemData *prev;
    };

    PageHandlerStart _pHStart;
    PageHandlerKernel _pHKernel;
    PageHandler *_pageHandler;

    Spinlock _lock;

    void *allocate(size_t size, u16 flags);
    void deallocate(void *p);

    MemData *_buckets[KALLOC_MAXBUCKET];
    void *bucketAlloc(u8 order, u16 flags);
    void bucketDealloc(MemData *cur);

    friend void *operator new(size_t size, u16 flags);
    friend void *operator new(size_t size);
    friend void *operator new[](size_t size, u16 flags);
    friend void *operator new[](size_t size);
    friend void operator delete(void *p);
    friend void operator delete[](void *p);
};

extern KernelAllocator kAllocator;

#endif /* !ALLOCATOR_INTERNAL_HH */
