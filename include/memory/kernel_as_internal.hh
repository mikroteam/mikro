/**
 * \file kernel_as_internal.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Kernel address space internal
 */

#ifndef KERNEL_AS_INTERNAL_HH_
#define KERNEL_AS_INTERNAL_HH_

#include "memory/virtual_memory.hh"
#include "spinlock.hh"

namespace mem
{
  enum KmapFlags
  {
    KMAP_STD = 0x0,
    KMAP_TEMPORARY = 0x1,
    KMAP_NOLOCK = 0x2
  };

  class KernelASInternal
  {
    public:
      void init();

      bool kmap(void **paddr, void **vaddr, int flags = KMAP_STD);
      void kunmap(void *vaddr);

      void *temporaryMap(void *paddr);
      void temporaryUnmap(void *vaddr);

      VMemAreas kernelVMAS;
      Spinlock lock;

      /*
       * In order to reduce critical zone, an AS locked "pma->up();" must be
       * done prior to call mapIPC. Lock can then be released before calling
       * this function.
       */
      void *mapIPC(PMemArea *pma, size_t page_offset, bool sender,
          void *pfAddr);
      void unmapIPC();
      int checkPF(void *vaddr);


    private:
      struct IPCMap
      {
        PMemArea *pma;
        void *pfAddr;
      };

      void *_ipcVaddr;
      IPCMap *_ipcMaps;
  };
}

#endif /* !KERNEL_AS_INTERNAL_HH_ */
