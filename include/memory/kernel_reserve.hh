/**
 * \file kernel_reserve.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Kernel reserve
 */

#ifndef KERNEL_RESERVE_HH_
#define KERNEL_RESERVE_HH_

#include "memory/virtual_memory.hh"
#include "spinlock.hh"

/*
 * Well 42 is a great answer but somehow I feel like this is not the good
 * one to this particular question
 * FIXME: find a better setting
 */
#define KRESERVE_START 42
#define KRESERVE_MIN 40
#define KRESERVE_MAX 44

namespace mem
{
  class KernelReserve
  {
    public:
      KernelReserve()
        : _reserve(0), _nrReserved(0)
      { }

      void init(void *begin_vaddr, size_t nrPages);
      void check();

      void *get();
      void set(void *vaddr);

    private:
      struct ReservedPage
      {
        ReservedPage *next;
      };

      ReservedPage *_reserve;
      size_t _nrReserved;

      Spinlock _lock;
  };
}

#endif /* !KERNEL_RESERVE_HH_ */
