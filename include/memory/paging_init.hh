/**
 * \file paging_init.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging initialization class
 */

#ifndef PAGING_INIT_HH_
#define PAGING_INIT_HH_

#include "memory/phys_allocator.hh"
#include "paging.hh"

namespace mem
{
  struct Mapping
  {
    enum MappingFlags
    {
      NONE = 0x0,
      FREEABLE = 0x1,
      SYS_RESERVED = 0x2,
      KERNEL_RESERVE = 0x4,
      START_ALLOCATOR = 0x8
    };

    size_t paddr;
    size_t nrPages;
    u32 flags;

    size_t vaddr;
  };

  class PagingInit
  {
    public:
      PagingInit(Mapping *mappings, u32 nrMappings,
          size_t zoneNormalBegin = 0);

      /*
       * STEP 1
       * WARNING: NOT ANY NEW MUST BE DONE PRIOR THIS STEP
       */
      bool initStartAllocator();

      /*
       * STEP 2
       * 2 CASES:
       *  A) If reserved zones are in multiboot
       */
      bool setReserved(multiboot_info_t *info, size_t maxPhysical);
      /*
       * OR
       *  B) Else
       * Add reserved zones in mappings before constructing this class
       */
      bool setReserved(size_t maxAddr)
      {
        _maxAddr = maxAddr;
        _nrFreePages = (maxAddr >> 12) + 1;
        nrFreePages();
        return true;
      }

      /* STEP 3 */
      bool initialMapping();

      /*
       * STEP 4
       *
       * After this step preallocation for Physical Allocator must be mapped
       * and set to 0.
       * Its size in pages is in the Mapping returned. The virtual and
       * physical addresses must be properly set before calling the next step.
       *
       * WARNING: modules are mapped after the virtual address used for
       *          preallocation
       */
      Mapping *prealloc4PhysAllocator();

      /* STEP 5 */
      bool initPhysAllocators();

      /* STEP 6 */
      bool mapModules(multiboot_info_t *info);

      /* STEP 7 */
      void finalize();

    private:
      Set<PMemArea> _finalMapping;
      size_t _maxAddr;
      size_t _nrFreePages;
      size_t _zoneNormalBegin;

      Mapping *_mappings;
      Mapping _preallocMapping;
      u32 _nrMappings;

      Mapping *_startAllocator;
      Mapping *_kernelReserve;

      bool reverseSet(Set<PMemArea> &in, Set<PMemArea> &out, size_t max);
      void nrFreePages();
      bool doAllocation(Mapping *mapping);
      bool doAllocations(Mapping *mapping);
  };

}

#endif /* !PAGING_INIT_HH_ */
