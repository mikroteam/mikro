/**
 * \file paging_internal.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging internal class
 */

#ifndef PAGING_INTERNAL_HH_
#define PAGING_INTERNAL_HH_

#include "arch/paging_internal_i.hh"
#include "memory/kernel_as_internal.hh"
#include "memory/kernel_reserve.hh"
#include "memory/phys_allocator.hh"

namespace mem
{

  struct PagingInternal
  {
    KernelASInternal kASInternal;
    KernelReserve kReserve;
    PhysAllocator physAllocator;

    PagingInternalImpl<ARCH_GENERIC> impl;

    Spinlock shareLock;
  };

  extern PagingInternal pi;

}

#endif /* !PAGING_INTERNAL_HH_ */
