/**
 * \file paging_macro.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Generic paging macros
 */

#ifndef PAGING_MACRO_HH_
#define PAGING_MACRO_HH_

#ifndef MIKRO64
/* i386 case */

#define KERNEL_AS_BEGIN           0xC0000000
#define KERNEL_AS_END             0xFFFFFFFF

#define USER_AS_BEGIN             0x1000
#define USER_AS_ANON_BEGIN        0x100000
#define USER_AS_END               0xFFFFFFFF

#endif /* !MIKRO64 */
#endif /* !PAGING_MACRO_HH_ */
