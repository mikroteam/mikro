/**
 * \file paging_tools.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging internal tools
 */

#ifndef PAGING_TOOLS_HH_
#define PAGING_TOOLS_HH_

#include "memory/phys_allocator.hh"
#include "multiboot.h"
#include "set.hh"

namespace mem
{
  struct PagingTools
  {
    void *bestArea(multiboot_info_t *info, unsigned long after, size_t size);

    static unsigned long divUp(unsigned long dividend, unsigned long divisor)
    {
      return (1 + ((dividend - 1) / divisor));
    }
  };
}

#endif /* !PAGING_TOOLS_HH_ */
