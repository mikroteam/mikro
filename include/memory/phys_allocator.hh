/**
 * \file phys_allocator.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Page physical allocator
 */

#ifndef PHYS_ALLOCATOR_HH_
#define PHYS_ALLOCATOR_HH_

#include "arch/asm.hh"
#include "arch/paging_macro_i.hh"
#include "config.hh"
#include "list_embedded.hh"
#include "ref_count.hh"
#include "set.hh"
#include "spinlock.hh"

#define PALLOC_MAXBUCKET 11

namespace mem
{
  class PMemArea // Physical Memory Area
    : public RefCount
  {
    public:
      PMemArea(void *paddr_begin, unsigned long nrPages)
        : _paddr_begin(paddr_begin)
      {
        _paddr_end = (void *) ((unsigned long) paddr_begin +
            ((nrPages - 1) << 12) + 0xFFF);
      }

      PMemArea(void *paddr_begin, void *paddr_end)
        : _paddr_begin(paddr_begin), _paddr_end(paddr_end)
      { }

      bool operator <(const PMemArea &pma) const
      {
        return (unsigned long) _paddr_begin < (unsigned long) pma._paddr_begin;
      }

      void *begin() const
      {
        return _paddr_begin;
      }

      void *end() const
      {
        return _paddr_end;
      }

      size_t nrPages() const
      {
        return ((size_t) _paddr_end - (size_t) _paddr_begin + 1) >> 12;
      }

    protected:
      void *_paddr_begin;
      void *_paddr_end;
  };

  class StdAllocator
  {
    public:
      StdAllocator();

      bool init(size_t start, size_t nrPages, Set<PMemArea> &allocatableAreas,
          void *preAlloc);

      bool earlyAllocate(size_t paddr, size_t nrPages, List<PMemArea *> &pmas);
      bool allocate(size_t nrPages, u16 flags, List<PMemArea *> &pmas);
      void deallocate(List<PMemArea *> &pmas)
      {
        for (List<PMemArea *>::Iterator it = pmas.begin(); it != pmas.end();
            ++it)
          deallocateOrder(*it);
      }

      PMemArea *allocateOrder(int order);
      void deallocateOrder(PMemArea *pma);
      void deallocateOne(size_t paddr)
      {
        deallocateOrder(new AllocItem((void *) paddr, 1, getAreaID(paddr, 1)));
      }

      void *begin()
      {
        return (void *) _startAddr;
      }

      void *end()
      {
        return (void *) (_startAddr + (_nrPages << 12) - 1);
      }

      size_t nrPages()
      {
        return _nrPages;
      }

      size_t nrFreePages()
      {
        return _freePages;
      }

      size_t nrAllocPages()
      {
        return _allocPages;
      }

      size_t realNrPages()
      {
        return _realNrPages;
      }

      static u8 computeOrder(size_t nrPage);

    private:
      class AllocItem;

      struct Area
      {
        size_t begin;
        AllocItem **itemRefs;
        size_t maxRefs;
      };


      class AllocItem
        : public PMemArea, public ListEmbeddedItem<AllocItem>
      {
        public:
          AllocItem(void *paddr_begin, u8 order, size_t areaID)
            : PMemArea(paddr_begin, 1 << order), _order(order), _areaID(areaID)
          { }

          u8 order()
          {
            return _order;
          }

          void setOrder(u8 order)
          {
            _order = order;
            _paddr_end = (void *) ((size_t) _paddr_begin +
                ((1 << order) << 12));
          }

          size_t areaID()
          {
            return _areaID;
          }

          void check(Area *areas)
          {
            assert((size_t) _paddr_begin >= areas[_areaID].begin);
            assert((size_t) _paddr_end < areas[_areaID].begin +
                (areas[_areaID].maxRefs << 12));
          }

          AllocItem *split();
          void merge();

        private:
          u8 _order;
          size_t _areaID;
      };

      /*
       * Memory footprint of this allocator (only to find a buddy) is:
       *  - ~0.10% on a 32bit machine
       *  - ~0.20% on a 64bit machine
       */
      Area *_areas;
      size_t _nrAreas;
      ListEmbedded<AllocItem> _buckets[PALLOC_MAXBUCKET];

      AllocItem *findBuddy(AllocItem *item, bool &firstOne);
      AllocItem **getItemRef(AllocItem *item)
      {
        return getItemRef((size_t) item->begin(), item->areaID());
      }
      AllocItem **getItemRef(size_t paddr, size_t areaID);
      size_t getAreaID(size_t paddr, size_t nrPages);

      size_t _startAddr;
      size_t _nrPages;

      size_t _freePages;
      size_t _allocPages;
      size_t _realNrPages;


      Spinlock _lock;
  };

  class PhysAllocator
  {
    public:
      bool init(Set<PMemArea> &allocatableAreas, void *prealloc,
          size_t zoneNormalBegin, size_t maxAddr);

      bool earlyAllocate(size_t paddr, size_t nrPages, List<PMemArea *> &pmas)
      {
#ifdef MIKRO_ZONE_DMA
        if (paddr < KERNEL_ZONE_NORMAL_START)
          return _zoneDMA.earlyAllocate(paddr, nrPages, pmas);
#endif /* MIKRO_ZONE_DMA */
        return _zoneNormal.earlyAllocate(paddr, nrPages, pmas);
      }

      bool allocate(size_t nrPages, u16 flags, List<PMemArea *> &pmas)
      {
#ifdef MIKRO_ZONE_DMA
        if ((flags & ZONE_DMA))
          return _zoneDMA.allocate(nrPages, flags, pmas);
#endif /* MIKRO_ZONE_DMA */
        return _zoneNormal.allocate(nrPages, flags, pmas);
      }

      void deallocate(List<PMemArea *> &pmas)
      {
        if (pmas.empty())
          return;

#ifdef MIKRO_ZONE_DMA
        PMemArea *pma = *pmas.begin();

        if ((size_t) pma->begin() < KERNEL_ZONE_NORMAL_START)
          _zoneDMA.deallocate(pmas);
        else
#endif /* MIKRO_ZONE_DMA */
          _zoneNormal.deallocate(pmas);
      }

      PMemArea *allocateOrder(u8 order, u16 flags)
      {
#ifdef MIKRO_ZONE_DMA
        if ((flags & ZONE_DMA))
          return _zoneDMA.allocateOrder(order);
#endif /* MIKRO_ZONE_DMA */
        return _zoneNormal.allocateOrder(order);
      }

      void deallocateOrder(PMemArea *pma)
      {
#ifdef MIKRO_ZONE_DMA
        assert(pma);

        if ((size_t) pma->begin() < KERNEL_ZONE_NORMAL_START)
          _zoneDMA.deallocateOrder(pma);
        else
#endif /* MIKRO_ZONE_DMA */
          _zoneNormal.deallocateOrder(pma);
      }

      void deallocateOne(size_t paddr)
      {
#ifdef MIKRO_ZONE_DMA
        if (paddr < KERNEL_ZONE_NORMAL_START)
          _zoneDMA.deallocateOne(paddr);
        else
#endif /* MIKRO_ZONE_DMA */
          _zoneNormal.deallocateOne(paddr);
      }

#if 0
      PMemArea *getReserved(void *paddr, size_t nrPages)
      {
        return _zoneReserved.allocate(paddr, nrPages);
      }

      bool leaveReserved(PMemArea *pma)
      {
        return _zoneReserved.deallocate(pma);
      }
#endif

      size_t realNrPages()
      {
        return _zoneNormal.realNrPages()
#ifdef MIKRO_ZONE_DMA
          + _zoneDMA.realNrPages()
#endif /* MIKRO_ZONE_DMA */
        ;
      }

    private:
#ifdef MIKRO_ZONE_DMA
      StdAllocator _zoneDMA;
#endif /* MIKRO_ZONE_DMA */
      StdAllocator _zoneNormal;
  };
}

#endif /* !PHYS_ALLOCATOR_HH_ */
