/**
 * \file virtual_memory.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Virtual memory tools
 */

#ifndef VIRTUAL_MEMORY_HH_
#define VIRTUAL_MEMORY_HH_

#include "list.hh"
#include "memory/phys_allocator.hh"
#include "set.hh"

namespace mem
{
  class VMemArea // Virtual Memory Area
  {
    public:
      enum Flags
      {
        ZONE_RESERVED   = 0x1,
        KERNEL_RESERVE  = 0x2,
        SWAPPED         = 0x4
      };

      VMemArea(void *vaddr_begin, unsigned long nrPages)
        : pmas(CRITICAL), _flags(0), _vaddr_begin(vaddr_begin)
      {
        _vaddr_end = (void *)
          ((unsigned long) vaddr_begin + (nrPages << 12) - 1);
      }

      VMemArea(void *vaddr_begin, void *vaddr_end)
        : pmas(CRITICAL), _flags(0), _vaddr_begin(vaddr_begin),
        _vaddr_end(vaddr_end)
      { }

      bool operator <(const VMemArea &vma) const
      {
        return (unsigned long) _vaddr_begin < (unsigned long) vma._vaddr_begin;
      }

      bool getFlag(unsigned int flag)
      {
        return _flags & flag;
      }

      void setFlags(unsigned int flag)
      {
        _flags |= flag;
      }

      void unsetFlags(unsigned int flag)
      {
        _flags &= ~flag;
      }

      void *begin()
      {
        return _vaddr_begin;
      }

      void *end()
      {
        return _vaddr_end;
      }

      size_t nrPages()
      {
        return ((size_t) _vaddr_end - (size_t) _vaddr_begin + 1) >> 12;
      }

      /* Pmas MUST be sorted. Front is the smallest address */
      List<PMemArea *> pmas;

      bool getPMA(void *vaddr, PMemArea **pma, size_t *page_offset);

    private:
      unsigned int _flags;

      void *_vaddr_begin;
      void *_vaddr_end;
  };

  struct LessVMA
  {
    bool operator ()(const VMemArea *a, const VMemArea *b) const
    {
      assert(a && b);

      return *a < *b;
    }
  };

  class VMemAreas
    : public Set<VMemArea *, LessVMA>
  {
    public:
      VMemAreas()
        : Set(CRITICAL)
      { }

      bool fitIn(unsigned long addr, size_t nrPages, bool kernel);
      void *findRoom(size_t nrPages, bool kernel);

      Set<VMemArea *>::Iterator insideVMA(size_t vaddr, size_t size);
  };
}


#endif /* !VIRTUAL_MEMORY_HH_ */
