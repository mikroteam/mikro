/*
 * File: memory_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Arch independent class that manages memory
 *
 */

#ifndef MEMORY_MANAGER_HH_
# define MEMORY_MANAGER_HH_

# include <config.hh>
# include <arch/memory_manager_i.hh>

namespace mem
{

  class MemoryManager
  {
    public:
      inline void init()
      {
        arch.init();
      }

      MemoryManagerImpl<ARCH_GENERIC> arch;
  };

  extern MemoryManager manager;
}

#endif /* !MEMORY_MANAGER_HH_ */
