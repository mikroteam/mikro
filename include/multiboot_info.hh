/*
 * File: multiboot_info.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description:
 *
 */

#ifndef MULTIBOOT_INFO_H_
# define MULTIBOOT_INFO_H_

void dumpMultibootInfo(struct multiboot_info* info);

#endif /* !MULTIBOOT_INFO_H_ */
