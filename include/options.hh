#ifndef OPTIONS_HH_
# define OPTIONS_HH_

# include <cstring.hh>

namespace config {

  class Options {
    public:
      void init(char* cmdline);

      /*
      * Return NULL if name not found
      */
      char* getByName(const char* name);
  
    private:
      char* _cmdline;
  };

  extern Options options;
}


#endif /* !OPTIONS_HH_ */
