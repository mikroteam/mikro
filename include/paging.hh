/**
 * \file paging.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging entry point
 */

/*
 * Special thanks to Gabriel Laskar for his help and ideas to realise this
 * paging manager
 */

#ifndef PAGING
#define PAGING

#include "allocator.hh"
#include "list.hh"
#include "multiboot.h"
#include "memory/paging_internal.hh"
#include "set.hh"


# define PAGE_SIZE 0x1000

namespace mem
{
  enum PageFlags
  {
    READ            = 0x1,
    WRITE           = 0x2,
    EXEC            = 0x4,
    ZONE_DMA        = 0x8,
    CONTIGUOUS      = 0x10
  };

  /**
   * \class AddressSpace
   * \brief handle user land address space
   */
  class AddressSpace
  {
    public:
      /**
       * \fn ~AddressSpace()
       * \brief destructor
       */
      ~AddressSpace();

      /**
       * \fn init
       * \brief init empty address space
       */
      bool init();

      /**
       * \fn mmap
       * \brief anonymous mapping of pages
       *
       * \param vaddr wanted start virtual address
       * \param nrPages number of pages to map
       * \param flags page flags (see Paging::PageFlags)
       *
       * \return virtual address of mapped pages in this address space
       *
       */
      void *mmap(void *vaddr, const size_t nrPages, u16 flags);

      /**
       * \fn mmap
       * \brief mapping physical of pages
       *
       * \param vaddr wanted start virtual address
       * \param paddr physical address
       * \param nrPages number of pages to map
       * \param flags page flags (see Paging::PageFlags)
       *
       * \return virtual address of mapped pages in this address space
       *
       */
      void *mmap(void *vaddr, void *paddr, const size_t nrPages, u16 flags);

      /**
       * \fn mmap
       * \brief shared mapping of pages
       *
       * \param vaddr wanted start virtual address
       * \param ad address space where the pages to be shared reside
       * \param vaddrInAD virtual address in ad of the pages to be shared
       * \param nrPages number of pages to map
       * \param flags page flags
       *
       * \return virtual address of mapped pages in this address space
       *
       */
      void *mmap(void *vaddr, AddressSpace &ad, void *vaddrInAD,
          const size_t nrPages, u16 flags);

      /**
       * \fn changeRights
       * \brief change page rights
       *
       * \param vaddr virtual address of the pages
       * \param rights new rights
       *
       */
      bool changeRights(void *vaddr, u16 flags);

      /**
       * \fn munmap
       * \brief unmap pages from this address space
       *
       * \param vaddr virtual address of the pages
       *
       */
      bool munmap(void *vaddr);

      /**
       * \fn getPaddr
       * \brief return physical address of virtual address
       * Return 0 on error
       *
       * \param vaddr virtual address to convert
       *
       */
      void *getPaddr(void *vaddr)
      {
        return pi.impl.pageTableUser.getPaddr(_vaddr_pdt, vaddr);
      }

      /**
       * \fn getPDT
       * \brief return PDT physical address
       *
       */
      void *getPDT()
      {
        return _paddr_pdt;
      }

      /**
       * \fn mmapVM86
       * \brief map needed page for VM86
       *
       */
      void mmapVM86();

      /**
       * \fn munmapVM86
       * \brief unmap needed page for VM86
       *
       */
      void munmapVM86();

    private:
      AddressSpace()
      { }

      void *_paddr_pdt;
      void *_vaddr_pdt;

      VMemArea *mmapHandleVirtual(void *vaddr, const size_t nrPages,
          u16 flags);
      bool mmapHandlePhysical(VMemArea *vma, u16 flags);

      Spinlock _lock;
      VMemAreas _vmas;

      friend AddressSpace *newAD();
      friend class KernelAddressSpace;
  };

  /**
   * \class KernelAddressSpace
   * \brief handle kernel land address space (unique in class Paging)
   */
  class KernelAddressSpace
  {
    public:
      /**
       * \fn mmap
       * \brief anonymous mapping of pages
       *
       * \param vaddr wanted start virtual address
       * \param nrPages number of pages to map
       * \param flags page flags
       *
       * \return virtual address of mapped pages in the kernel address space
       *
       */
      void *mmap(void *vaddr, const size_t nrPages, u16 flags);

      /**
       * \fn mmap
       * \brief pysical mapping of pages
       *
       * \param vaddr wanted start virtual address
       * \param paddr physical address
       * \param nrPages number of pages to map
       * \param flags page flags
       *
       * \return virtual address of mapped pages in the kernel address space
       *
       */
      void *mmap(void *vaddr, void *paddr, const size_t nrPages, u16 flags);

      /**
       * \fn mmap
       * \brief shared with user land mapping of pages
       *
       * \param vaddr wanted start virtual address
       * \param ad address space where the pages to be shared reside
       * \param vaddrInAD virtual address in ad of the pages to be shared
       * \param nrPages number of pages to map
       * \param flags page flags
       *
       *
       * \return virtual address of mapped pages in the kernel address space
       *
       */
      void *mmap(void *vaddr, AddressSpace &ad, void *vaddrInAD,
          const size_t nrPages, u16 flags);

      /**
       * \fn mmapIPC
       * \brief map a userland page in kernel land for IPC
       *
       * \param as address space where the area to be mapped reside
       * \param vaddr virtual address of the area in as to map
       * \param sender is the address to map from sender
       * \param curVaddr virtual address used for IPC by current thread
       *
       *
       * \return virtual address of mapped area in the kernel address space
       *
       */
      void *mmapIPC(AddressSpace *as, void *vaddr, bool sender,
          void *curVaddr);

      /**
       * \fn munmapIPC
       * \brief unmap a userland page in kernel land for IPC
       *
       */
      void munmapIPC()
      {
          pi.kASInternal.unmapIPC();
      }

      /**
       * \fn changeRights
       * \brief change page rights
       *
       * \param vaddr virtual address of the pages
       * \param rights new rights
       *
       */
      bool changeRights(void *vaddr, u16 rights);

      /**
       * \fn munmap
       * \brief unmap pages from the kernel address space
       *
       * \param vaddr virtual address of the pages
       *
       */
      bool munmap(void *vaddr);

      /**
       * \fn getPaddr
       * \brief return physical address of virtual address
       * Return 0 on error
       *
       * \param vaddr virtual address to convert
       *
       */
      void *getPaddr(void *vaddr)
      {
        return pi.impl.pageTableKernel.getPaddr(vaddr);
      }

    private:
      VMemArea *mmapHandleVirtual(void *vaddr, const size_t nrPages,
          u16 flags);
      void mmapHandlePhysical(VMemArea *vma, u16 flags);
  };

  /*
   * kernel address space object
   */
  extern KernelAddressSpace kernelAD;

  /**
   * \fn init
   * \brief init paging
   *
   * \param info multiboot header from bootloader
   *
   */
  inline void initPaging(multiboot_info_t *info)
  {
    pi.impl.init(info);
  }

  /**
   * \fn initSMPPaging
   * \brief init SMP Paging
   *
   */
  inline void initSMPPaging()
  {
    pi.kASInternal.init();
  }

  /**
   * \fn newAD
   * \brief get a new user land address space
   *
   */
  inline AddressSpace *newAD()
  {
    AddressSpace *ad = new AddressSpace();
    if (!ad || !ad->init())
      return 0;

    return ad;
  }

  namespace tools
  {
    /**
     * \fn alignOnPage
     * \brief align address on page address
     *
     * \param addr address to align
     *
     */
    inline void* alignOnPage(void* addr)
    {
      return (void*)(~0xfff & (size_t)addr);
    }

    /**
     * \fn clearPage
     * \brief clear page (set all page to 0)
     *
     * \param page page address
     *
     */
    void clearPage(void *page);

    /**
     * \fn length2nrPages
     * \brief convert length to a number to pages
     *
     * \param length length to convert
     *
     */
    inline u32 length2nrPages(size_t length)
    {
      if (length < 0x1000)
        return 1;

      return PagingTools::divUp(length, 0x1000);
    }
  }
}

#endif /* !PAGING */
