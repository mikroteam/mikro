/**
 * \file pair.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief handle pairs
 */

#ifndef PAIR_HH
#define PAIR_HH

template<typename A, typename B>
class Pair
{
  public:
    Pair(const A &a, const B &b)
      : first(a), second(b)
    { }

    Pair &operator =(const Pair<A, B> &pair)
    {
      first = pair.first;
      second = pair.second;

      return *this;
    }

    void swap(Pair<A, B> &pair)
    {
      A first_tmp = pair.first;
      B second_tmp = pair.second;

      pair.first = first;
      pair.second = second;

      first = first_tmp;
      second = second_tmp;
    }

    A first;
    B second;
};


template<typename A, typename B>
bool operator ==(const Pair<A, B> &a, const Pair<A, B> &b)
{
  return (a.first == b.first && a.second == b.second);
}

template<typename A, typename B>
bool operator !=(const Pair<A, B> &a, const Pair<A, B> &b)
{
  return !(a == b);
}

template<typename A, typename B>
bool operator <(const Pair<A, B> &a, const Pair<A, B> &b)
{
  return ((a.first < b.first) ||
      (!(b.first < a.first) && a.second < b.second));
}

template<typename A, typename B>
bool operator <=(const Pair<A, B> &a, const Pair<A, B> &b)
{
  return !(b < a);
}

template<typename A, typename B>
bool operator >(const Pair<A, B> &a, const Pair<A, B> &b)
{
  return (b > a);
}

template<typename A, typename B>
bool operator >=(const Pair<A, B> &a, const Pair<A, B> &b)
{
  return !(a < b);
}

template<typename A, typename B>
Pair<A, B> make_pair(const A &a, const B &b)
{
  return Pair<A, B>(a, b);
}

template<typename A, typename B>
void swap(Pair<A, B> &a, Pair<A, B> &b)
{
  a.swap(b);
}

#endif /* !PAIR_HH */
