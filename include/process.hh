/**
 * \file process.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Process management (1 process = 1 or more threads)
 */

#ifndef PROCESS_HH_
#define PROCESS_HH_

#include "ipc/ipc_process.hh"
#include "paging.hh"
#include "ref_count.hh"

/*
 * NOTE: PID 0 is kernel!
 */


namespace task
{

  class Process
    : public RefCount
  {
    public:
      Process()
        : _pid(0), _as(0)
      { }

      ~Process()
      {
        // TODO: IPC close
        delete _as;
      }

      bool init(pid_t pid)
      {
        if (!ipcManager.init(MIKRO_DEFAULT_MHANDLE_NR))
          return false;

        _as = mem::newAD();

        if (!_as)
          return false;

        _pid = pid;
        return true;
      }

      /* General getters */
      pid_t getPid()
      {
        return _pid;
      }

      mem::AddressSpace* getAS()
      {
        return _as;
      }

      ipc::IPCProcessManager ipcManager;

    private:
      pid_t _pid;
      mem::AddressSpace *_as;
  };

}

#endif /* !PROCESS_HH_ */
