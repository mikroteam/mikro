/**
 * \file rbtree.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief generic red-black tree class
 */

#ifndef RBTREE_HH
#define RBTREE_HH

#include <allocator.hh>
#include <container.hh>
#include <errors.hh>
#include <list.hh>
#include <pair.hh>

#define BLACK true
#define RED   false


// FIXME: some STL functions are missing, I guess I'm lazy.
// TODO: add some documentation

template<typename A>
class Less
{
  public:
    bool operator ()(const A &a, const A &b) const
    {
      return a < b;
    }
};

template<typename A>
class RbTree
  : public Container
{
  protected:
    class Node
    {
      public:
        Node(const A &item, Node *father)
          : _item(item), _father(father), _left(0), _right(0)
        { }

        A _item;
        Node *_father;
        Node *_left;
        Node *_right;

        bool _color;
    };

  public:
    class Iterator
    {
      public:
        Iterator()
          : _cur_it(0)
        { }

        A &operator *()
        {
          assert(_cur_it);
          return _cur_it->_item;
        }

        bool operator ==(const Iterator &it)
        {
          return _cur_it == it._cur_it;
        }

        bool operator !=(const Iterator &it)
        {
          return _cur_it != it._cur_it;
        }

        Iterator &operator ++() // Prefix
        {
          if (!_cur_it)
            return *this;

          if (_cur_it->_right)
          {
            _cur_it = _cur_it->_right;
            while (_cur_it->_left)
              _cur_it = _cur_it->_left;
          }
          else
          {
            Node *nit;
            do
            {
              nit = _cur_it;
              _cur_it = _cur_it->_father;

              if (!_cur_it)
                break;
            }
            while (_cur_it->_right == nit);
          }

          return *this;
        }

        Iterator &operator --() // Prefix
        {
          if (!_cur_it)
            return *this;

          if (_cur_it->_left)
          {
            _cur_it = _cur_it->_left;
            while (_cur_it->_right)
              _cur_it = _cur_it->_right;
          }
          else
          {
            Node *nit;
            do
            {
              nit = _cur_it;
              _cur_it = _cur_it->_father;

              if (!_cur_it)
                break;
            }
            while (_cur_it->_left == nit);
          }

          return *this;
        }

      private:
        Node *_cur_it;

        Iterator(Node *cur_it)
          : _cur_it(cur_it)
        { }

      friend class RbTree;
    };

    Iterator begin()
    {
      if (!_root)
        return Iterator();

      Node *nit = _root;

      while (nit->_left)
        nit = nit->_left;

      return Iterator(nit);
    }

    Iterator end()
    {
      return Iterator();
    }

    void erase(Iterator &it)
    {
      if (!it._cur_it)
        return;

      erase_internal(it._cur_it);
      _size--;
    }

    void clear()
    {
      Node *cur = _root;
      Node *father;

      while (cur)
      {
        while (cur->_left)
          cur = cur->_left;
        while (cur->_right)
          cur = cur->_right;

        father = cur->_father;
        if (father)
        {
          if (father->_left == cur)
            father->_left = 0;
          else
            father->_right = 0;
        }

        delete cur;
        cur = father;
      }

      _root = 0;
      _size = 0;
    }

  protected:
    RbTree(u16 newFlags)
      : Container(newFlags), _root(0)
    { }

    Node *_root;

    Iterator makeIterator(Node *n) const
    {
      return Iterator(n);
    }

    void erase_internal(Node *n)
    {
      Node *child;

      if (n->_left && n->_right)
      {
        Node *pred = n->_left;
        while (pred->_right)
          pred = pred->_right;

        swapNode(n, pred);
      }

      if (n->_left)
        child = n->_left;
      else
        child = n->_right;

      if (n->_color == BLACK)
      {
        if (child)
          n->_color = child->_color;
        else
          n->_color = BLACK;

        //erase_balance(n);
      }

      // Swap child & n
      if (n->_father)
      {
        if (n->_father->_right == n)
          n->_father->_right = child;
        else
          n->_father->_left = child;
      }
      else
      {
        _root = child;
        if (child)
          child->_color = BLACK;
      }

      if (child)
        child->_father = n->_father;

      delete n;
    }

    bool copy_internal(const RbTree<A> &rbtree)
    {
      if (!rbtree._root)
        return true;

      List<bool> nodeStack;
      nodeStack.push_front(false);
      _root = new(_newFlags) Node(rbtree._root->_item, 0);
      if (!_root)
      {
        clear();
        return false;
      }

      Node *cur_node_remote = rbtree._root;
      Node *cur_node_local = _root;

      while (nodeStack.size())
      {
        if ((!cur_node_remote->_left && !cur_node_remote->_right) ||
            nodeStack.front())
        {
          nodeStack.pop_front();
          cur_node_remote = cur_node_remote->_father;
          cur_node_local = cur_node_local->_father;
        }
        else
        {
          if (cur_node_remote->_left)
          {
            cur_node_local->_left = new(_newFlags)
              Node(cur_node_remote->_left->_item, cur_node_local);
            if (!cur_node_local->_left)
            {
              clear();
              return false;
            }

            cur_node_remote = cur_node_remote->_left;
            cur_node_local = cur_node_local->_left;

            if (!nodeStack.push_front(false))
            {
              clear();
              return false;
            }
          }
          else
          {
            cur_node_local->_right = new(_newFlags)
              Node(cur_node_remote->_right->_item, cur_node_local);
            if (!cur_node_local->_right)
            {
              clear();
              return false;
            }

            cur_node_remote = cur_node_remote->_right;
            cur_node_local = cur_node_local->_right;

            if (!nodeStack.push_front(true))
            {
              clear();
              return false;
            }
          }
        }
      }

      _size = rbtree._size;
      return true;
    }

    void insert_balance(Node *n)
    {
      Node *u;
      Node *gf;

restart:
      // case 1:
      if (!n->_father)
        n->_color = BLACK;
      else
        return;

      // case 2:
      if (n->_father->_color == BLACK)
        return;

      // case 3:
      u = uncle(n);
      gf = grandfather(n);
      if (u && u->_color == RED)
      {
        n->_father->_color = BLACK;
        u->_color = BLACK;
        gf->_color = RED;
        n = gf;
        goto restart;
      }

      // case 4:
      if (n == n->_father->_right && n->_father == gf->_left)
      {
        rotate_left(n->_father);
        n = n->_left;
      }
      else
      {
        rotate_right(n->_father);
        n = n->_right;
      }

      // case 5:
      gf = grandfather(n);

      n->_father->_color = BLACK;
      gf->_color = RED;
      if (n == n->_father->_left)
        rotate_right(n->_father);
      else
        rotate_left(n->_father);
    }

    Node *sibling(Node *n)
    {
      if (n == n->_father->_left)
        return n->_father->_right;
      else
        return n->_father->_left;
    }

  private:
    void swapNode(Node *n1, Node *n2)
    {
      Node *tmp;

      tmp = n1->_father;
      n1->_father = n2->_father;
      n2->_father = tmp;

      if (!n1->_father)
        _root = n1;
      else if (!n2->_father)
        _root = n2;

      tmp = n1->_left;
      n1->_left = n2->_left;
      n2->_left = tmp;

      if (n1->_left)
        n1->_left->_father = n1;
      if (n2->_left)
        n2->_left->_father = n2;

      tmp = n1->_right;
      n1->_right = n2->_right;
      n2->_right = tmp;

      if (n1->_right)
        n1->_right->_father = n1;
      if (n2->_right)
        n2->_right->_father = n2;

      bool color = n1->_color;
      n1->_color = n2->_color;
      n2->_color = color;
    }

    Node *grandfather(Node *n)
    {
      if (n && n->_father)
        return n->_father->_father;

      return 0;
    }

    Node *uncle(Node *n)
    {
      Node *gf = grandfather(n);

      if (n)
      {
        if (n->_father == gf->_left)
          return gf->_right;

        return gf->_left;
      }

      return 0;
    }

    void rotate_right(Node *n)
    {
      Node *nit = n->_father;

      n->_father = nit->_father;
      if (nit->_father)
      {
        if (nit->_father->_left == nit)
          nit->_father->_left = n;
        else
          nit->_father->_right = n;
      }
      else
        _root = n;

      nit->_left = n->_right;
      if (n->_right)
        n->_right->_father = nit;

      nit->_father = n;

      n->_right = nit;
    }

    void rotate_left(Node *n)
    {
      Node *nit = n->_father;

      n->_father = nit->_father;
      if (nit->_father)
      {
        if (nit->_father->_left == nit)
          nit->_father->_left = n;
        else
          nit->_father->_right = n;
      }
      else
        _root = n;

      nit->_right = n->_left;
      if (n->_left)
        n->_left->_father = nit;

      nit->_father = n;

      n->_left = nit;
    }

    void erase_balance(Node *n)
    {
restart:
      // Case 1:
      if (!n->_father)
        return;

      // Case 2:
      Node *s = sibling(n);
      if (s && s->_color == RED)
      {
        n->_father->_color = RED;
        s->_color = BLACK;
        if (n == n->_father->_left)
          rotate_left(s);
        else
          rotate_right(s);

        s = sibling(n);
      }

      if (!s)
        return;

      // Case 3:
      if (n->_father->_color == BLACK &&
          s->_color == BLACK &&
          (!s->_left || s->_left->_color == BLACK) &&
          (!s->_right || s->_right->_color == BLACK))
      {
        s->_color = RED;
        n = n->_father;
        goto restart;
      }

      // Case 4:
      if (n->_father->_color == RED &&
          s->_color == BLACK &&
          (!s->_left || s->_left->_color == BLACK) &&
          (!s->_right || s->_right->_color == BLACK))
      {
        s->_color = RED;
        n->_father->_color = BLACK;
        return;
      }


      // Case 5:
      if (n == n->_father->_left &&
          s->_color == BLACK &&
          !(!s->_left && s->_left->_color != RED) &&
          (!s->_right || s->_right->_color == BLACK))
      {
        s->_color = RED;
        s->_left->_color = BLACK;
        rotate_right(s->_left);
      }
      else if (n == n->_father->_right &&
               s->_color == BLACK &&
               (s->_left && s->_left->_color == BLACK) &&
               !(!s->_right && s->_right != RED))
      {
        s->_color = RED;
        s->_right->_color = BLACK;
        rotate_left(s->_right);
      }

      // Case 6:
      s->_color = n->_father->_color;
      n->_father->_color = BLACK;
      if (n == n->_father->_left && s->_right)
      {
        s->_right->_color = BLACK;
        rotate_left(s);
      }
      else if (s->_left)
      {
        s->_left->_color = BLACK;
        rotate_right(s);
      }
    }
};

#endif /* !RBTREE_HH */
