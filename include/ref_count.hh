/**
 * \file ref_count.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Generic implementation of a reference counter
 */

#ifndef REF_COUNT_HH_
#define REF_COUNT_HH_

#include "arch/asm.hh"

class RefCount
{
  public:
    RefCount()
      : _count(0)
    { }

    void up()
    {
      asmf::Atomic<ARCH_GENERIC>::inc(&_count);
    }

    void down()
    {
      asmf::Atomic<ARCH_GENERIC>::dec(&_count);
    }

    bool isFree()
    {
      return !_count;
    }

  protected:
    size_t _count;
};

#endif /* !REF_COUNT_HH_ */
