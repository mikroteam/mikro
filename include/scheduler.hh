#ifndef SCHEDULER_HH_
# define SCHEDULER_HH_

# include <cpu.hh>
# include <map.hh>
# include <scheduler_dummy.hh>
# include <spinlock.hh>
# include <task_manager.hh>

namespace scheduler
{
  class Scheduler
  {
    public:
      void init()
      {
        _algo.initSchedule();
      }

      void add(task::Thread *thread)
      {
        _algo.addThread(thread);
      }

      void remove(task::Thread *thread)
      {
        _algo.removeThread(thread);
      }

      /* WARNING: call this function ONLY from syscall */
      void remove()
      {
        _algo.removeThread(irq::manager.getCurrentRegsDump());
      }

      /* WARNING: call this function ONLY from syscall */
      __noreturn void directSchedule(task::Thread *thread)
      {
        _algo.directSchedule(irq::manager.getCurrentRegsDump(), thread);
        thread->gotoTask(irq::manager.getCurrentRegsDump());
      }

      /* WARNING: call this function ONLY from syscall */
      __noreturn void schedule()
      {
        _algo.schedule(irq::manager.getCurrentRegsDump());
        if (cpu::myself->current_thread)
            cpu::myself->current_thread->gotoTask(
              irq::manager.getCurrentRegsDump());
        else
          task::manager.idle();
      }

      /* WARNING: call this function only from timer */
      void schedule(irq::RegsDump *curDump)
      {
        _algo.schedule(curDump);
      }

      /* WARNING: call this function from other Interrupts */
      __noreturn void getBackToTask(irq::RegsDump *curDump)
      {
        task::Thread *cThread = cpu::myself->current_thread;
        cThread->restore(curDump);
        cThread->gotoTask(curDump);
      }


    private:
      SchedulerImpl<SCHEDULER> _algo;
  };

  extern Scheduler scheduler;
}

#endif /* SCHEDULER_HH_ */
