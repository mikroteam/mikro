#ifndef SCHEDULER_RR_HH_
# define SCHEDULER_RR_HH_

/*
 * Dummy Scheduler
 */

# include <task_manager.hh>
# include <scheduler_common.hh>
# include <types.hh>
# include <list_embedded.hh>
# include <spinlock.hh>
# include <compiler_tools.hh>

namespace scheduler
{

  template<>
  class SchedulerImpl<Dummy>
  {
    public:
      SchedulerImpl<Dummy>();
      void addThread(task::Thread *thread);
      void removeThread(task::Thread *thread);
      void removeThread(irq::RegsDump *curDump);
      void initSchedule();
      void directSchedule(irq::RegsDump *curDump, task::Thread *thread);
      void schedule(irq::RegsDump *curDump);

    private:
      ListEmbedded<task::Thread> _threads;
      Spinlock          _slthreads;
  };
}

#endif /* SCHEDULER_RR_HH_ */
