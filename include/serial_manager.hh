#ifndef SERIAL_MANAGER_HH_
# define SERIAL_MANAGER_HH_

# include <types.hh>
# include <display_writer.hh>

namespace serial
{
  enum e_type
  {
    COM_1,
    COM_2,
    COM_3,
    COM_4
  };

  class SerialManager : public dsp::DisplayWriter
  {
    public:
      virtual void init() = 0;
      virtual void init(int com) = 0;
      virtual void setDriverWriterCom(int com) = 0;
      virtual void write(uint32_t port, const char* data, uint32_t size) = 0;
      virtual void read(uint32_t port, char* data, uint32_t size) = 0;
  };

  extern SerialManager* manager;
}

#endif /* SERIAL_MANAGER_HH_ */
