/**
 * \file set.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief handle sets
 */

#ifndef SET_HH
#define SET_HH

#include <rbtree.hh>

// FIXME: some STL functions are missing, I guess I'm lazy.
// TODO: add some documentation

template<typename A, typename Compare = Less<A> >
class Set
  : public RbTree<A>
{
  public:
    Set()
      : RbTree<A>::RbTree(STANDARD)
    { }

    Set(u16 newFlags)
      : RbTree<A>::RbTree(newFlags)
    { }

    ~Set()
    {
      RbTree<A>::clear();
    }

    bool copy(const Set<A, Compare> &set)
    {
      RbTree<A>::clear();
      return copy_internal(set);
    }

    Pair<typename RbTree<A>::Iterator, bool> insert(const A &new_item)
    {
      typename RbTree<A>::Node *nit = 0;

      if (insert_internal(new_item, &nit))
        return make_pair(RbTree<A>::makeIterator(nit), false);
      else
        return make_pair(RbTree<A>::makeIterator(nit), true);
    }

    typename RbTree<A>::Iterator find(const A &item) const
    {
      return RbTree<A>::makeIterator(find_internal(item));
    }

    typename RbTree<A>::Iterator lower_bound(const A &item) const
    {
      return RbTree<A>::makeIterator(lbound_internal(item));
    }

    typename RbTree<A>::Iterator upper_bound(const A &item) const
    {
      return RbTree<A>::makeIterator(ubound_internal(item));
    }

  private:
    Compare _comp;

    bool inf(const A &a, const A &b) const
    {
      return _comp(a, b);
    }

    bool eq(const A &a, const A &b) const
    {
      return !inf(a, b) && !inf(b, a);
    }

    bool insert_internal(const A &item, typename RbTree<A>::Node **n)
    {
      typename RbTree<A>::Node *nit = RbTree<A>::_root;

      if (RbTree<A>::_root)
      {
        while (true)
        {
          if (eq(nit->_item, item))
          {
            *n = nit;
            return false;
          }

          if (inf(item, nit->_item))
          {
            if (!nit->_left)
            {
              nit->_left = new(RbTree<A>::_newFlags)
                typename RbTree<A>::Node(item, nit);
              nit = nit->_left;
              if (!nit)
              {
                *n = 0;
                return false;
              }
              break;
            }

            nit = nit->_left;
          }
          else
          {
            if (!nit->_right)
            {
              nit->_right = new(RbTree<A>::_newFlags)
                typename RbTree<A>::Node(item, nit);
              nit = nit->_right;
              if (!nit)
              {
                *n = 0;
                return false;
              }
              break;
            }

            nit = nit->_right;
          }
        }
      }
      else
      {
        nit = new(RbTree<A>::_newFlags) typename RbTree<A>::Node(item, 0);
        if (!nit)
        {
          *n = 0;
          return false;
        }
        RbTree<A>::_root = nit;
      }

      //RbTree<A>::insert_balance(nit);
      RbTree<A>::_size++;
      *n = nit;

      return true;
    }

    typename RbTree<A>::Node *find_internal(const A &item) const
    {
      typename RbTree<A>::Node *nit = RbTree<A>::_root;

      while (nit)
      {
        if (eq(nit->_item, item))
          return nit;

        if (inf(item, nit->_item))
          nit = nit->_left;
        else
          nit = nit->_right;
      }

      return 0;
    }

    typename RbTree<A>::Node *lbound_internal(const A &item) const
    {
      typename RbTree<A>::Node *res = 0;
      typename RbTree<A>::Node *nit = RbTree<A>::_root;

      while (nit)
      {
        if (eq(nit->_item, item))
          return nit;

        if (inf(item, nit->_item))
        {
          if (!res || inf(nit->_item, res->_item))
            res = nit;

          nit = nit->_left;
        }
        else
          nit = nit->_right;
      }

      return res;
    }

    typename RbTree<A>::Node *ubound_internal(const A &item) const
    {
      typename RbTree<A>::Node *res = 0;
      typename RbTree<A>::Node *nit = RbTree<A>::_root;

      while (nit)
      {
        if (inf(item, nit->_item))
        {
          if (!res || inf(nit->_item, res->_item))
            res = nit;

          nit = nit->_left;
        }
        else
          nit = nit->_right;
      }

      return res;
    }

    Set (const Set<A, Compare> &set)
    {
      set;
    }

    Set &operator =(const Set<A, Compare> &set)
    {
      return set;
    }
};

#endif /* !SET_HH */
