/*
 * File: smp_manager.hh 
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Abstract SMP manager
 *
 */

#ifndef SMP_MANAGER_HH_
# define SMP_MANAGER_HH_

# include <config.hh>
# include <arch/smp_manager_i.hh>

namespace smp
{
  class SMPManager
  {
    public:
      void init();

      void wakeAPs()
      {
        arch.wakeAPs();
      }

      SMPManagerImpl<ARCH_GENERIC> arch;
  };

  extern SMPManager manager;
}

#endif /* !SMP_MANAGER_HH_ */
