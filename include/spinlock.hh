/**
 * \file spinlock.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Define anything needed to use spinlocks
 */

#ifndef SPINLOCK_HH_
#define SPINLOCK_HH_

#include "arch/spinlock_i.hh"
#include "config.hh"

class Spinlock
{
  public:
    void lock()
    {
      _spin.lock();
    }

    void unlock()
    {
      _spin.unlock();
    }

    bool isLocked()
    {
      return _spin.isLocked();
    }

  private:
    SpinlockImpl<x86> _spin;
};

#endif /* !SPINLOCK_HH_ */
