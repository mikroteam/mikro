/*
 * File: syscall.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Syscalls
 *
 */

#ifndef SYSCALL_HH_
# define SYSCALL_HH_

/* Memory */
# define SYS_MMAP                   0
# define SYS_MMAP_PHYS              1
# define SYS_MUNMAP                 2
# define SYS_MAP_CHANGE_RIGHTS      3

/* IPC */
# define SYS_CHANNEL_CREATE         4
# define SYS_CHANNEL_CLOSE          5
# define SYS_CHANNEL_OPEN           6
# define SYS_CHANNEL_SEND           7
# define SYS_CHANNEL_RECV           8
# define SYS_CHANNEL_SRECV          9
# define SYS_CHANNEL_REPLY_CLOSE    10
# define SYS_CHANNEL_SEND_EVENT     11
# define SYS_CHANNEL_CHANGE_PRIVATE 12

# define SYSCALL_COMMON_LAST SYS_CHANNEL_CHANGE_PRIVATE

# include <arch/syscall_i.hh>
# include <config.hh>

# ifdef DEBUG_SYSCALL
#  define SYS_DEBUG (SYSCALL_SPECIFIC_LAST + 1)
#  define SYSCALL_NUMBER (SYSCALL_SPECIFIC_LAST + 2)
# else /* DEBUG_SYSCALL */
#  define SYSCALL_NUMBER (SYSCALL_SPECIFIC_LAST + 1)
# endif /* DEBUG_SYSCALL */

# define MAX_STRING_BUF 1024

# include <types.hh>
# include <config.hh>
# include <compiler_tools.hh>
# include <ipc/syscall_ipc.hh>

namespace syscall
{
  class SyscallManager
  {
    public:
      void init();
      int add(size_t nb, void* syscall);

      bool copyFromUser(void *to, void *from, size_t size)
      {
        return _arch.copyFromUser(to, from, size);
      }

      bool copyToUser(void *to, void *from, size_t size)
      {
        return _arch.copyToUser(to, from, size);
      }

      size_t strncpyFromUser(void *to, void *from, size_t size)
      {
        return _arch.strncpyFromUser(to, from, size);
      }

      bool checkAddress(size_t addr)
      {
        return _arch.checkAddress(addr);
      }

    private:
      SyscallManagerImpl<ARCH> _arch;
  };

  extern SyscallManager manager;
  extern void* table[SYSCALL_NUMBER];
}

/* Begin common syscall list */
void* sys_mmap(void* vaddr, const size_t nrPages, u16 flags);
void* sys_mmap_phys(void* vaddr, void *paddr, const size_t nrPages, u16 flags);
int sys_munmap(void* vaddr);
int sys_map_change_rights(void* vaddr, u16 flags);
# ifdef DEBUG_SYSCALL
int sys_debug(const char *str);
# endif /* DEBUG_SYSCALL */

#endif /* !SYSCALL_HH_ */
