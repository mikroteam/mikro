#ifndef TASK_MANAGER_HH_
# define TASK_MANAGER_HH_

# include <arch/task_manager_i.hh>
# include <map.hh>
# include <paging.hh>
# include <thread.hh>
# include <types.hh>

namespace task
{

  class TaskManager
  {
    public:
      void init()
      {
        _arch.init();
      }

      /* Process Management */
      int createProcess(Process **p);
      int runProcess(Process *p, size_t entry)
      {
        return addThread(p, entry, p->getPid());
      }

      /* Thread management */
      int addThread(Process *process, size_t entry)
      {
        pid_t pid;

        if (!getNewPid(pid))
          return -ENOPID;

        return addThread(process, entry, pid);
      }

      void delThread(Thread *thread);

      task::Thread *getThread(size_t pid)
      {
        Map<size_t, task::Thread *>::Iterator it = _threads.find(pid);
        if (it == _threads.end())
          return 0;

        return (*it).second;
      }

      void updateCurrentTime(Thread *thread);

      __noreturn void idle()
      {
        _arch.idle();
      }

    private:
      TaskManagerImpl<ARCH> _arch;

      Map<pid_t, Thread *> _threads;
      Spinlock _slThreads;

      Map<pid_t, Process *> _processes;
      Spinlock _slProcesses;

      int addThread(Process *process, size_t entry, pid_t pid);

      /* PID */
      pid_t _lastPid;
      bool getNewPid(pid_t &newPid)
      {
        /*
         * TODO: fix the following issues
         *  - PID starvation
         *  - PID randomization
         */
        newPid = _lastPid++;
        return true;
      }

      bool setFreePid(pid_t pid)
      {
        (void) pid;
        return true;
      }
  };

  extern TaskManager manager;
}

#endif /* TASK_MANAGER_HH_ */
