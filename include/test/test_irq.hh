#ifndef TEST_IRQ_HH_
# define TEST_IRQ_HH_

# include <irq_manager.hh>
# include <test/test_tools.hh>

namespace test
{
  struct TestIRQ
  {
    static void run();
    static void easyPic();
    static void easyIrq();
  };
}

#endif /* TEST_IRQ_HH_ */
