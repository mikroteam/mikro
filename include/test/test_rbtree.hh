#ifndef TEST_RBTREE_HH_

# define TEST_RBTREE_HH_

# include <test/test_tools.hh>

namespace test
{
  struct TestRBTree
  {
    static void run();
    static void easySet();
    static void easyMap();
  };
}

#endif /* !TEST_RBTREE_HH_ */
