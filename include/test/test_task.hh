#ifndef TEST_TASK_HH_
# define TEST_TASK_HH_

# include <display_manager.hh>
# include <task_manager.hh>
# include <test/test_tools.hh>

namespace test
{
  struct TestTask
  {
    static void run();
    static void taskUser();
  };
}

#endif /* TEST_TASK_HH_ */
