#ifndef TEST_TOOLS_HH_
# define TEST_TOOLS_HH_

# include <display_manager.hh>

# define TEST_NAME(name)  dsp::kout << "{'" name "',"
# define TEST_TAB         dsp::kout << "'tests':["
# define TEST_MSG(msg)    dsp::kout << "'msg':'" msg "'},"
# define TEST_TAB_END     dsp::kout << "{'name':'null'}]"
# define TEST_END         dsp::kout << "}"

namespace test
{
  class Printer
  {
    public:
      virtual void name(const char *cmd) = 0;
      virtual void tab() = 0;
      virtual void tabEnd() = 0;
      virtual void msg(const char *cmd) = 0;
      virtual void end() = 0;
  };

  class PrinterSerial : public Printer
  {
    public:
      virtual void name(const char *name)
      {
        dsp::kout << "Test: " << name << "\n";
      }

      virtual void tab()
      {
        dsp::kout << "[begin]\n";
      }

      virtual void tabEnd()
      {
        dsp::kout << "[end]\n";
      }

      virtual void msg(const char *msg)
      {
        dsp::kout << "Result: " << msg << "\n";
      }

      virtual void end()
      {
      }
  };

  class PrinterJson : public Printer
  {
    public:
      virtual void name(const char *name)
      {
        dsp::kout << "{'" << name << "',";
      }

      virtual void tab()
      {
        dsp::kout << "'tests':[";
      }

      virtual void tabEnd()
      {
        dsp::kout << "{'name':'null'}]";
      }

      virtual void msg(const char *msg)
      {
        dsp::kout << "'msg':'" << msg << "'},";
      }

      virtual void end()
      {
        dsp::kout << "}";
      }
  };

  extern Printer *printer;
}

#endif /* TEST_TOOLS_HH_ */
