#ifndef TEST_MANAGER_HH_
# define TEST_MANAGER_HH_

# include <serial_manager.hh>
# include <display_manager.hh>
# include <cstring.hh>

# include <test/test_irq.hh>
# include <test/test_task.hh>
# include <test/test_rbtree.hh>

namespace test
{
  struct TestHanlde
  {
    const char  *name;
    void  (*func)(void);
  };

  class TestManager
  {
    public:
      void run();

    private:
      bool _parse(char *cmd);
      void _run_all();
  };

  extern TestManager *manager;
}

typedef void (test::TestManager::*handler)(void);

#endif /* TEST_MANAGER_HH_ */
