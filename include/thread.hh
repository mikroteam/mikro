/**
 * \file thread.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Thread management
 */

#ifndef THREAD_HH_
# define THREAD_HH_

# include <arch/asm.hh>
# include <arch/thread_i.hh>
# include <ipc/ipc_thread.hh>
# include <list_embedded.hh>
# include <process.hh>
# include <types.hh>

namespace task
{
  enum Status
  {
    RUNNING       = 0,
    UNSCHEDULE_ME = 1,
    ZOMBIE        = 2,
    WAITING       = 3
  };

  class Thread
    : public ListEmbeddedItem<Thread>
  {
    public:
      /* Constructor & init */
      Thread(pid_t pid, Process *father)
        : _pid(pid), _status(0), _process(father)
      {}

      bool init(size_t entry)
      {
        return (_arch.init(_process->getAS(), entry)
            && ipcManager.init(MIKRO_DEFAULT_CHANDLE_NR));
      }

      /* General getters */
      pid_t getPid()
      {
        return _pid;
      }

      mem::AddressSpace* getAS()
      {
        return _process->getAS();
      }

      Process *getProcess()
      {
        return _process;
      }

      /* Thread status management */
      bool getStatus(Status val)
      {
        return (_status & (1 << val));
      }

      void setStatus(Status val)
      {
        asmf::Atomic<ARCH_GENERIC>::setBit(&_status, val);
      }

      void unsetStatus(Status val)
      {
        asmf::Atomic<ARCH_GENERIC>::clearBit(&_status, val);
      }

      bool setAndCheckStatus(Status val)
      {
        return asmf::Atomic<ARCH_GENERIC>::testAndSetBit(&_status, val);
      }

      bool unsetAndCheckStatus(Status val)
      {
        return asmf::Atomic<ARCH_GENERIC>::testAndClearBit(&_status, val);
      }

      /* Scheduling management */
      void save(irq::RegsDump *curDump)
      {
        _arch.save(curDump);
      }

      void restore(irq::RegsDump *curDump)
      {
        _arch.restore(_process->getAS(), curDump);
      }

      __noreturn void gotoUser()
      {
        _arch.gotoUser(_process->getAS());
      }

      __noreturn void gotoTask(irq::RegsDump *curDump)
      {
        _arch.gotoTask(_process->getAS(), curDump);
      }

      /* Return value on sycall management */
      void setReturn(size_t val)
      {
        _arch.setReturn(val);
      }

      size_t getReturn()
      {
        return _arch.getReturn();
      }

      /* IPC Management */
      ipc::IPCThreadManager ipcManager;

      /* Time management */
      uint64_t run_start;
      uint32_t run_time;

    private:
      /* Arch dependant */
      ThreadImpl<ARCH> _arch;

      /* env */
      pid_t _pid;
      u32 _status;
      Process *_process;
  };

}

#endif /* !TASK_HH_ */
