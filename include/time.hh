/*
 * File: time.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Generic time management
 *
 */

#ifndef TIME_HH_
# define TIME_HH_

# include <config.hh>
# include <arch/time_i.hh>

# define TICK_HZ 1000

namespace time
{
  class TimeManager
  {
    public:
      TimeManager()
        : arch()
      {
      }

      // Return the number of ticks till startup
      inline u64 getCPUTickCount()
      {
        return arch.getCPUTickCount();
      }

      // Busy wait for the kernel
      inline void msleep(u32 msecs)
      {
        arch.msleep(msecs);
      }

    TimeManagerImpl<ARCH_GENERIC> arch;
  };

  extern volatile u64 jiffies;
  extern TimeManager manager;
}

#endif /* !TIME_HH_ */
