/*
 * File: timer.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Timer must provide this interface
 *
 */

#ifndef TIMER_HH_
# define TIMER_HH_

namespace time
{
  class PeriodicTimer
  {
    public:
      virtual ~PeriodicTimer() {};

      virtual void initPeriodic() = 0;
  };

  extern PeriodicTimer* periodic_timer;
}

#endif /* !TIMER_HH_ */
