/**
 * \file types.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Basic integer types
 */

#ifndef TYPES_HH
#define TYPES_HH

#ifdef MIKRO64

typedef unsigned char u8;
typedef signed char i8;

typedef unsigned short u16;
typedef unsigned short le16;
typedef unsigned short be16;
typedef short i16;

typedef unsigned int u32;
typedef unsigned int le32;
typedef unsigned int be32;
typedef int i32;

typedef unsigned long u64;
typedef unsigned long le64;
typedef unsigned long be64;
typedef long i64;

typedef unsigned long size_t;

#else /* MIKRO64 */

typedef unsigned char u8;
typedef signed char i8;

typedef unsigned short u16;
typedef unsigned short le16;
typedef unsigned short be16;
typedef short i16;

typedef unsigned int u32;
typedef unsigned int le32;
typedef unsigned int be32;
typedef int i32;

typedef unsigned long long u64;
typedef unsigned long long le64;
typedef unsigned long long be64;
typedef long long i64;

typedef unsigned int size_t;

#endif /* MIKRO64 */

typedef u16 uint16_t;
typedef i16 int16_t;

typedef u32 uint32_t;
typedef i32 int32_t;

typedef u64 uint64_t;
typedef i64 int64_t;

typedef size_t uint_ptr;
typedef size_t pid_t;

#endif /* !TYPES_HH */
