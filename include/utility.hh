/*
 * File: utility.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Utility basic implementation
 *
 */

#ifndef UTILITY_HH_
# define UTILITY_HH_

template <typename T>
struct remove_reference
{
  typedef T type;
};

template <typename T>
struct remove_reference<T&>
{
  typedef T type;
};

template <typename T>
struct remove_reference<T&&>
{
  typedef T type;
};

template<class T>
typename remove_reference<T>::type&& move(T&& a) noexcept
{
  typedef typename remove_reference<T>::type&& RvalRef;
  return static_cast<RvalRef>(a);
}

#endif /* !UTILITY_HH_ */
