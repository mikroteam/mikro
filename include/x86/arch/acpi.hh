/*
 * File: acpi.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ACPI operations
 *
 */

#ifndef ACPI_HH_
# define ACPI_HH_

# include <arch/acpi_rsdp.hh>
# include <arch/acpi_rsdt.hh>
# include <arch/acpi_madt.hh>

namespace acpi
{
  class ACPIManager
  {
    public:
      ACPIManager();
      bool init();
      void release();

      RSDPManager* rsdp;
      RSDTManager* rsdt;
  };

  extern ACPIManager manager;
}

#endif /* !ACPI_HH_ */
