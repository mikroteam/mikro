/*
 * File: acpi_common.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Common struct and functions for ACPI
 *
 */


#ifndef ACPI_COMMON_HH_
# define ACPI_COMMON_HH_

# include <types.hh>

# define ACPI_TABLE_SIGN_STR_LEN 4

namespace acpi
{
  class RSDTManager;

  struct ACPIHeader
  {
    char sig[ACPI_TABLE_SIGN_STR_LEN];
    u32 len;
    u8 rev;
    u8 cksum;
    char oemid[6];
    char oemtable_id[8];
    u32 oemrev;
    u32 creator_id;
    u32 creator_rev;
  };

  class ACPITable
  {
    public:
      virtual ~ACPITable();

      int findACPITable(RSDTManager* rsdt, const char* sign);

      template<typename T>
      inline T getTableAddr()
      {
        return (T)_table_addr;
      }

      void release();

    private:
      void* _table_addr;
      void* _page_addr;
  };

  u8 checksum(const void* st, int len);
}

#endif /* !ACPI_COMMON_HH_ */
