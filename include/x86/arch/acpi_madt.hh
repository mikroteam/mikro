/*
 * File: acpi_madt.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Multiple APIC Description Table
 *
 */

#ifndef ACPI_MADT_HH_
# define ACPI_MADT_HH_

# include <arch/acpi_common.hh>
# include <arch/smp_info.hh>

# define MAX_CPU_NUMBER 256

# define ACPI_MADT_SIGN_STR "APIC"

# define ACPI_MADT_CTRL_LAPIC 0x0
# define ACPI_MADT_CTRL_IOAPIC 0x1
# define ACPI_MADT_CTRL_ISO 0x2
# define ACPI_MADT_CTRL_NMI 0x3
# define ACPI_MADT_CTRL_LAPIC_NMI 0x4
# define ACPI_MADT_CTRL_LAPIC_OVERRIDE 0x5
# define ACPI_MADT_CPU_ENABLE 0x1

namespace acpi
{
  class RSDTManager;

  class MADTManager : public smp::SMPInfo, public ACPITable
  {
    public:
      MADTManager();
      ~MADTManager();
      bool init(RSDTManager* rsdt);
      void parse();

      virtual u8 getIOAPICCount()
      {
        return _ioapic_count;
      }

      virtual uint_ptr getAPICAddr()
      {
        return _lapic_addr;
      }

      virtual u32 getCpuCount()
      {
        return _cpu_count;
      }

      virtual u8 getAPICId(u8 cpu)
      {
        return _apic_ids[cpu];
      }

      virtual bool configureIOAPICs(smp::IOAPICManager* ioapic);
      virtual bool configureIOAPICsLines(smp::IOAPICManager* ioapic);
      virtual bool configureLAPICLines();

    private:
      struct Madt
      {
        struct ACPIHeader header;
        u32 local_interrupt_controller_addr;
        u32 flags;
        char interrupt_controllers[];
      }__attribute__((packed));

      struct MadtController
      {
        u8 type;
        u8 len;
      }__attribute__((packed));

      #define ACPI_ALL_CPU 255

      struct MadtLAPICController
      {
        struct MadtController header;
        u8 acpi_id;
        u8 apic_id;
        u32 flags;
      }__attribute__((packed));

      struct MadtIOAPICController
      {
        struct MadtController header;
        u8 ioapic_id;
        u8 reserved;
        u32 ioapic_addr;
        u32 gsib;
      }__attribute__((packed));

      #define ISO_POLARITY_CONFORM 0
      #define ISO_POLARITY_HIGH 1
      #define ISO_POLARITY_LOW 3

      #define ISO_TRIGGER_CONFORM 0
      #define ISO_TRIGGER_EDGE 1
      #define ISO_TRIGGER_LEVEL 3

      struct MadtISOController
      {
        struct MadtController header;
        u8 bus;
        u8 source;
        u32 gsi;
        u16 polarity: 2;
        u16 trigger: 2;
        u16 flags : 12;
      }__attribute__((packed));

      struct MadtNMIController
      {
        struct MadtController header;
        u16 polarity: 2;
        u16 trigger: 2;
        u16 flags : 12;
        u32 gsi;
      }__attribute__((packed));

      struct MadtLAPICNMIController
      {
        struct MadtController header;
        u8 acpi_proc_id;
        u16 polarity: 2;
        u16 trigger: 2;
        u16 flags : 12;
        u8 lapic_int;
      }__attribute__((packed));

      struct MadtLAPICOverrideController
      {
        struct MadtController header;
        u16 reserved;
        u64 addr;
      }__attribute__((packed));

      bool _parseLAPIC(struct MadtController* ctrl);
      bool _parseIOAPIC(struct MadtController* ctrl);
      bool _parseISO(struct MadtController* ctrl);
      bool _parseNMI(struct MadtController* ctrl);
      bool _parseLAPICNMI(struct MadtController* ctrl);
      bool _parseLAPICOverride(struct MadtController* ctrl);

      bool _configureIOAPIC(struct MadtIOAPICController* ctrl,
                            smp::IOAPICManager*);
      bool _configureISO(struct MadtISOController* iso,
                         smp::IOAPICManager* ioapic_m);
      bool _configureNMI(struct MadtNMIController* nmi,
                         smp::IOAPICManager* ioapic_m);
      bool _configureLAPICNMI(struct MadtLAPICNMIController* nmi,
                              u8 cpu_acpi_id);

      u32 _cpu_count;
      u8 _ioapic_count;
      u8* _apic_ids;
      u8* _cpu_acpi_ids;
      uint_ptr _lapic_addr;

      typedef bool (MADTManager::*parse_function)(struct MadtController*);
      static parse_function _parse_entry_table[];
  };
}

#endif /* !ACPI_MADT_HH_ */
