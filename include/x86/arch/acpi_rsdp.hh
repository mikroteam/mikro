/*
 * File: acpi_rsdp.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Root System Descriptor Pointer
 *
 */

#ifndef ACPI_RSDP_HH_
# define ACPI_RSDP_HH_

# define ACPI_RSDP_SIGN_STR "RSD PTR "
# define ACPI_RSDP_SIGN_STR_LEN 8

# include <types.hh>

namespace acpi
{
  struct Rsdp
  {
    char sig[ACPI_RSDP_SIGN_STR_LEN];
    u8 cksum;
    char oemid[6];
    u8 rev;
    u32 rsdt_addr;

    /* 2.0 specification */
    u32 len;
    u64 xsdt_addr;
    u8 extended_cksum;
    u8 reserved[3];
  };

  class RSDPManager
  {
    public:
      RSDPManager();
      ~RSDPManager();

      void release();
      bool init();

      void* getRSDTAddr()
      {
        if (!_rsdp_ptr)
          return 0;
        return (void*)_rsdp_ptr->rsdt_addr;
      }

    private:
      struct Rsdp* _searchRSDPinZone(void* addr, size_t len);

      void* _rsdp_page_addr;
      struct Rsdp* _rsdp_ptr;
  };

}

#endif /* !ACPI_RSDP_HH_ */
