/*
 * File: acpi_rsdt.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Root System Descriptor Table
 *
 */

#ifndef ACPI_RSDT_HH_
# define ACPI_RSDT_HH_

# include <arch/acpi_common.hh>

# define ACPI_RSDT_SIGN_STR "RSDT"

namespace acpi
{
  struct Rsdt
  {
    struct ACPIHeader header;
    uint32_t pointers_to_others_sdt[];
  };

  class RSDTManager
  {
    public:
      ~RSDTManager();
      bool init(void* addr);
      void release();

      inline u32 getEntryNumber()
      {
        return (_rsdt_ptr->header.len - sizeof (struct ACPIHeader)) / sizeof (u32);
      }

      inline void* getPointerToEntry(u32 n)
      {
        return (void*)_rsdt_ptr->pointers_to_others_sdt[n];
      }

    private:
      void* _page_addr;
      struct Rsdt* _rsdt_ptr;
  };
}

#endif /* !ACPI_RSDT_HH_ */
