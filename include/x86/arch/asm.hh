/*
 * File: asm.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ASM inline functions
 *
 */

#ifndef ASM_HH_
# define ASM_HH_

# include <config.hh>
# include <arch_template.hh>
# include <compiler_tools.hh>
# include <types.hh>

# define CPUID_REGS_EAX 0
# define CPUID_REGS_EBX 1
# define CPUID_REGS_ECX 2
# define CPUID_REGS_EDX 3

namespace asmf
{

  inline void hlt()
  {
    __asm__ __volatile__ ("hlt" : : );
  }

namespace io
{
  inline void outb(u16 port, u8 val)
  {
    __asm__ __volatile__ ("outb %0, %1"
                          :
                          : "a" (val), "Nd" (port));
  }

  inline void outw(u16 port, u16 val)
  {
    __asm__ __volatile__ ("outw %0, %1"
                          :
                          : "a" (val), "Nd" (port));
  }

  inline void outl(u16 port, u32 val)
  {
    __asm__ __volatile__ ("outl %0, %1"
                          :
                          : "a" (val), "Nd" (port));
  }

  inline u8 inb(u16 port)
  {
    u8 ret;
    __asm__ __volatile__ ("inb %1, %0"
                          : "=a" (ret)
                          : "Nd" (port));
    return ret;
  }

  inline u16 inw(u16 port)
  {
    u16 ret;
    __asm__ __volatile__ ("inw %1, %0"
                          : "=a" (ret)
                          : "Nd" (port));
    return ret;
  }

  inline u32 inl(u16 port)
  {
    u32 ret;
    __asm__ __volatile__ ("inl %1, %0"
                          : "=a" (ret)
                          : "Nd" (port));
    return ret;
  }
}

namespace cpu
{

#ifdef MIKRO32
  inline u64 rdtsc()
  {
    u64 x;
    __asm__ __volatile__ ("rdtsc" : "=A" (x));
    return x;
  }
#else
  inline u64 rdtsc()
  {
    u64 a, d;
    __asm__ __volatile__ ("rdtsc" : "=a" (a), "=d" (d));
    return (d << 32) | a;
  }
#endif

  bool hasCpuID();

  inline void cpuid(u32 eax, u32 ecx, u32 regs[4])
  {
    __asm__ __volatile__ ("cpuid"
                          : "=a" (regs[CPUID_REGS_EAX]),
                            "=b" (regs[CPUID_REGS_EBX]),
                            "=c" (regs[CPUID_REGS_ECX]),
                            "=d" (regs[CPUID_REGS_EDX])
                          : "a" (eax), "c" (ecx));
  }

  inline void cpuid(u32 eax, u32 regs[4])
  {
    cpuid(eax, 0, regs);
  }

  inline void getMSR(u32 msr, u32* lo, u32* hi)
  {
    __asm__ __volatile__ ("rdmsr"
                          : "=a" (*lo),
                            "=d" (*hi)
                          : "c" (msr));
  }

  inline void setMSR(u32 msr, u32 lo, u32 hi)
  {
    __asm__ __volatile__ ("wrmsr"
                          :
                          : "a" (lo),
                            "d" (hi),
                            "c" (msr));
  }
}

  inline void ltrw(uint16_t tss_selector)
  {
    __asm__ __volatile__ ("ltrw %w0" : : "r" (tss_selector));
  }

namespace mem
{
  inline void lgdt(void* addr)
  {
    __asm__ __volatile__ ("lgdt %0" : : "m" (*(void**)addr));
  }

  inline void* sgdt(void* addr)
  {
    __asm__ __volatile__ ("sgdt %0" : "=m" (*(void**)addr));
    return addr;
  }

  inline void invlpg(void* area)
  {
    __asm__ __volatile__ (
      "invlpg (%0) \n"
    :
    : "r" (area)
    :
    );
  }

  inline void reloadCR3()
  {
    __asm__ __volatile__ (
      "mov %%cr3, %%eax \n"
      "mov %%eax, %%cr3 \n"
    :
    :
    : "eax"
    );
  }

}

namespace irq
{
  inline void sti()
  {
    __asm__ __volatile__ ("sti" : : );
  }

  inline void cli()
  {
    __asm__ __volatile__ ("cli" : : );
  }

  inline void lidt(void* addr)
  {
    __asm__ __volatile__ ("lidt %0" : : "m" (*(void**)addr));
  }

}

namespace regs
{

# define GET_REG(x)                     \
  inline size_t get_##x (void)          \
  {                                     \
    size_t ret;                         \
    __asm__("mov %%" #x ", %0\n\t"      \
            : "=&r" (ret));             \
    return ret;                         \
  }

GET_REG(cr0)
GET_REG(cr2)
GET_REG(cr3)
GET_REG(cr4)
GET_REG(ds)
GET_REG(ss)
GET_REG(es)
GET_REG(fs)
GET_REG(gs)
GET_REG(eax)
GET_REG(ebx)
GET_REG(ecx)
GET_REG(edx)
GET_REG(esp)
GET_REG(ebp)
GET_REG(esi)
GET_REG(edi)

# undef GET_CRX

# define SET_REG(x)                       \
  inline void set_##x (size_t v)          \
  {                                       \
    __asm__("mov %0, %%" #x "\n\t"        \
            : /* No output */             \
            : "r" (v)                     \
            : "memory");                  \
  }

SET_REG(cr0)
SET_REG(cr1)
SET_REG(cr2)
SET_REG(cr3)
SET_REG(cr4)
SET_REG(ds)
SET_REG(ss)
SET_REG(es)
SET_REG(fs)
SET_REG(gs)

# undef SET_CRX

  inline u32 get_eflags()
  {
    u32 res;
    __asm__ __volatile__ (
      "pushf \n"
      "popl %0 \n"
    : "=r" (res)
    :
    :
    );

    return res;
  }

  inline void set_cs(size_t val)
  {
    __asm__ __volatile__ ("pushl %0; pushl $1f; lret; 1:" : : "r" (val));
  }


}

template<typename T>
class Atomic
{
};

/**
 * \class Atomic<x86>
 * \brief Every atomic assembly operations that can be useful for x86
 *	  architecture
 */
template<>
struct Atomic<x86>
{
  /**
   * \fn inc
   * \brief compute an atomic increment (a <- a + 1)
   *
   * \param a input and output
   */
  static void inc(volatile u32 *a)
  {
    __asm__ __volatile__ ( \
      "lock incl %0\n" \
      : "+m" (*a) \
      );
  }

  /**
   * \fn dec
   * \brief compute an atomic decrement (a <- a + 1)
   *
   * \param a input and output
   */
  static void dec(volatile u32 *a)
  {
    __asm__ __volatile__ ( \
      "lock decl %0\n" \
      : "+m" (*a) \
      );
  }

  /**
   * \fn add
   * \brief compute an atomic add (b <- a + b)
   *
   * \param a input
   * \param b input and output
   */
  static void add(u32 a, u32 *b)
  {
    __asm__ __volatile__ ( \
      "lock addl %1, %0\n" \
      : "+m" (*b) \
      : "ir" (a) \
      );
  }

  /**
   * \fn sub
   * \brief compute an atomic sub (b <- a - b)
   *
   * \param a input
   * \param b input and output
   */
  static void sub(u32 a, u32 *b)
  {
    __asm__ __volatile__ ( \
      "lock subl %1, %0\n" \
      : "+m" (*b) \
      : "ir" (a) \
      );
  }

  /**
   * \fn setBit
   * \brief set bit
   *
   * \param a input
   * \param b bit
   */
  static void setBit(volatile u32 *a, u32 b)
  {
    if (isConstant(b))
    {
      __asm__ __volatile__ (
          "lock orb %0, (%1)\n"
          :
          : "ir" ((1 << b)), "r" (a)
          );
    }
    else
    {
      __asm__ __volatile__ (
          "lock btsl %0, (%1)\n"
          :
          : "Ir" (b), "r" (a)
          );
    }
  }

  /**
   * \fn testAndSetBit
   * \brief set bit and get old value
   *
   * \param a input
   * \param b bit
   */
  static bool testAndSetBit(volatile u32 *a, u32 b)
  {
    u32 res;

    __asm__ __volatile__ (
      "lock btsl %1, (%2)\n"
      "sbb %0, %0\n"
      : "=r" (res)
      : "Ir" (b), "r" (a)
      );

    return res;
  }

  /**
   * \fn clearBit
   * \brief clear bit
   *
   * \param a input
   * \param b bit
   */
  static void clearBit(volatile u32 *a, u32 b)
  {
    if (isConstant(b))
    {
      __asm__ __volatile__ (
          "lock andb %0, (%1)\n"
          :
          : "r" (~(1 << b)), "r" (a)
          );
    }
    else
    {
      __asm__ __volatile__ (
          "lock btrl %0, (%1)\n"
          :
          : "Ir" (b), "r" (a)
          );
    }
  }

  /**
   * \fn testAndClearBit
   * \brief clear bit and get old value
   *
   * \param a input
   * \param b bit
   */
  static bool testAndClearBit(volatile u32 *a, u32 b)
  {
    u32 res;

    __asm__ __volatile__ (
      "lock btrl %1, (%2)\n"
      "sbb %0, %0\n"
      : "=r" (res)
      : "Ir" (b), "r" (a)
      );

    return res;
  }

  /**
   * \fn xchg
   * \brief exchange a and memory
   *
   * \param a register
   * \param b memory
   */
  static void xchg(u32 a, u32 *b)
  {
    __asm__ __volatile__ ( \
      "xchgl %0, %1\n" \
      : "+r" (a), "+m" (*b) \
      : \
      );
  }

  /**
   * \fn cas8
   * \brief compare and swap (8b version)
   *
   * \param a register
   * \param dst register
   * \param src memory
   */
  static u8 cas8(u8 a, u8 src, volatile u8 *dst)
  {
    u8 accu = a;

    __asm__ __volatile__ ( \
      "lock cmpxchgb %2, %0\n" \
      : "=m" (dst), "=a" (accu) \
      : "r" (src) \
      );

    return accu;
  }

  /**
   * \fn cas16
   * \brief compare and swap (16b version)
   *
   * \param a register
   * \param src register
   * \param dst memory
   */
  static u16 cas16(u16 a, u16 src, volatile u16 *dst)
  {
    u16 accu = a;

    __asm__ __volatile__ ( \
      "lock cmpxchgw %2, %0\n" \
      : "=m" (dst), "=a" (accu) \
      : "r" (src) \
      );

    return accu;
  }

  /**
   * \fn cas32
   * \brief compare and swap (32b version)
   *
   * \param a register
   * \param src register
   * \param dst memory
   */
  static u32 cas32(u32 a, u32 src, volatile u32 *dst)
  {
    u32 accu = a;

    __asm__ __volatile__ ( \
      "lock cmpxchgl %2, %0\n" \
      : "=m" (dst), "=a" (accu) \
      : "r" (src) \
      );

    return accu;
  }

  /**
   * \fn cas64
   * \brief compare and swap (64b version)
   *
   * \param a register
   * \param src register
   * \param dst memory
   */
  static u64 cas64(u64 a, u64 src, volatile u64 *dst)
  {
    u64 accu = a;

    __asm__ __volatile__ ( \
      "lock cmpxchgq %2, %0\n" \
      : "=m" (dst), "=a" (accu) \
      : "r" (src) \
      );

    return accu;
  }
};

namespace atomic
{
  /**
   * \fn xadd
   * \brief compute an atomic exchanged add (cf Intel Manual)
   *
   * \param src source
   * \param dest destination
   */
  inline u32 xadd(u32 src, u32 *dest)
  {
    u32 res = src;
    u32 *d = dest;
    __asm__ __volatile__ ( \
      "lock xadd %0, (%1)\n" \
      : "+r" (res) \
      : "r" (d) \
      );

    return res;
  }

  /**
   * \fn inc16
   * \brief compute an atomic increment (a <- a + 1) (16 bits)
   *
   * \param a input and output
   */
  inline void inc16(u16 *a)
  {
    __asm__ __volatile__ ( \
      "lock incw (%0)\n" \
      : "+r" (a) \
      );
  }

  /**
   * \fn asm_pause
   * \brief pause instruction used in spinlock (cf Intel Manual)
   */
  inline void asm_pause()
  {
    __asm__ __volatile__ ( \
      "pause\n" \
      : \
      : \
      );
  }

  /**
   * \fn asm_mbarrier
   * \brief memory barrier instruction (cf Intel Manual)
   */
  inline void asm_mbarrier()
  {
    __asm__ __volatile__ (\
      "" \
      : \
      : \
      : "memory" );
  }
}

}

#endif /* !ASM_HH_ */
