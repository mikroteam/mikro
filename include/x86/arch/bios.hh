/*
 * File: bios.hh 
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Functions to deal with the bios
 *
 */

#ifndef BIOS_HH_
# define BIOS_HH_

# include <types.hh>

# define BDA_PADDR 0x400
# define BDA_EBDA_BASE_ADDR 0x40E

namespace bios
{

size_t getEBDAPhysAddr();

}

#endif /* !BIOS_HH_ */
