/*
 * File: compat_checker_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Compatibility check for x86
 *
 */

#ifndef COMPAT_CHECKER_I_HH_
# define COMPAT_CHECKER_I_HH_

# include <arch_template.hh>

template<typename T>
class CompatibilityCheckerImpl
{
};

template<>
class CompatibilityCheckerImpl<x86>
{
  public:
    void check();
};

#endif /* !COMPAT_CHECKER_I_HH_ */
