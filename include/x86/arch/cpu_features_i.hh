/*
 * File: cpu_features_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Enable/Disable Cpu features on x86 CPU
 *
 */

#ifndef CPU_FEATURES_I_HH_
# define CPU_FEATURES_I_HH_

# include <arch_template.hh>

namespace cpu
{
  template <typename T>
  class CpuFeaturesImpl
  {
  };

  template<>
  class CpuFeaturesImpl<x86>
  {
    public:
      void init();

    private:
      bool _enableSSE();
  };
}

#endif /* !CPU_FEATURES_I_HH_ */
