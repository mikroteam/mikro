/*
 * File: cpu_info_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Informations about x86 CPUs
 *
 */

#ifndef CPU_INFO_I_HH_
# define CPU_INFO_I_HH_

# define VENDOR_INTEL 0
# define VENDOR_AMD 1
# define VENDOR_UNKNOWN 2
# define VENDOR_INTEL_STR "GenuineIntel"
# define VENDOR_AMD_STR "AuthenticAMD"
# define VENDOR_STR_LEN 12

# define CPUID_FUN_VENDOR_ID 0x0
# define CPUID_FUN_STD_FEATURES 0x1
# define CPUID_FUN_INTEL_CACHE 0x4
# define CPUID_FUN_INTEL_PROC_TOPO 0xb

# define CPUID_FEATURE_HTT (1 << 28)
# define CPUID_FEATURE_SSE (1 << 25)
# define CPUID_FEATURE_LAPIC (1 << 9)
# define CPUID_FEATURE_MSR (1 << 5)

# include <arch_template.hh>

namespace cpu
{
  template<typename T>
  class CpuInfoImpl
  {
  };

  template<>
  class CpuInfoImpl<x86>
  {
    public:
      CpuInfoImpl();

      void init();

      inline u32 getLogicals()
      {
        return _logicals;
      }

      inline u32 getCores()
      {
        return _cores;
      }

      inline bool isHyperThreaded()
      {
        return _hthread;
      }

      bool hasSSE();
      bool hasAPIC();
      bool hasMSR();
      void printInfos();

    private:
      void _detectVendor();
      void _detectLogicals(u32 regs[4]);
      void _detectCores();
      void _detectAPICID();
      u32 _extractField(u32 n, u32 begin, u32 end);

      u8 _vendor;
      u32 _logicals;
      u8 _logicalsBits;
      u32 _cores;
      u8 _coresBits;
      bool _hthread;
  };
}

#endif /* !CPU_INFO_I_HH_ */
