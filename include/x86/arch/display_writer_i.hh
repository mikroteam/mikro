/*
 * File: display_writer_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Print characters on screen
 *
 */

#ifndef DISPLAY_WRITER_I_H_
# define DISPLAY_WRITER_I_H_

# include <display_writer.hh>

# define DEFAULT_ATTR 0x07
# define LINE_NUMBER 25
# define COLUMN_NUMBER 80
# define VIDEOMEM_VADDR 0xc00b8000

namespace dsp
{

  class DisplayWriterX86 : public DisplayWriter
  {
    public:
      DisplayWriterX86();

      virtual void init();
      virtual void writeChar(char c);
      virtual void setAttr(unsigned char attr);

    private:
      void _updateCursor();
      void _moveCursor();
      void _displayTab();
      void _clearScreen();
      void _scroll();

      char* _video_mem;
      unsigned char _cursor_c;
      unsigned char _cursor_l;
      unsigned char _attr;
  };

}

#endif /* !DISPLAY_WRITER_I_H_ */
