/**
 * \file errors_i.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief All error handling
 */

#ifndef ERRORS_I_HH_
#define ERRORS_I_HH_

#include "arch/asm.hh"
#include "arch_template.hh"

template<typename T>
struct ErrorsImpl
{
};

template<>
struct ErrorsImpl<x86>
{
  void stopInterrupts()
  {
    asmf::irq::cli();
  }

  void funnyDisplay();
  void dumpRegisters();
  void dumpStack();
};

#endif /* !ERRORS_I_HH_ */
