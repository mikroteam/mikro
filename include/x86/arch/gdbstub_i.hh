/**
 * \file gdbstub_i.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Gdb remote stub implementation for x86
 */

#ifndef GDBSTUB_I_HH_
#define GDBSTUB_I_HH_

#include "arch_template.hh"
#include "gdbstub_internal.hh"
#include "irq_manager.hh"
#include "serial_manager.hh"
#include "types.hh"

namespace debug
{
  template<typename T>
  struct GdbStubImpl
  {
  };

  template<>
  struct GdbStubImpl<x86>
  {
    GdbStubImpl<x86>()
      : internal(serial::COM_2)
    { }

    void init();

    bool cont(irq::RegsDump *regs, u32 size);
    bool step(irq::RegsDump *regs, u32 size);
    bool readRegs(irq::RegsDump *regs, u32 size);
    bool writeRegs(irq::RegsDump *regs, u32 size);

    void haltReason();
    void detach(irq::RegsDump *regs);

    GdbStubInternal internal;
  };
}

#endif /* !GDBSTUB_I_HH_ */
