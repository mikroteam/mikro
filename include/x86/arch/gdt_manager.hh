/*
 * File: gdt_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class that manages the gdt
 *
 */

#ifndef GDT_MANAGER_HH_
# define GDT_MANAGER_HH_

# define SEG_SELECTOR(Index, Table, RPL)  \
  ((Index << 3) | (Table << 2) | RPL)

# define KERNEL_CODE 1
# define KERNEL_CODE_SEL SEG_SELECTOR(KERNEL_CODE, 0, 0)
# define KERNEL_DATA 2
# define KERNEL_DATA_SEL SEG_SELECTOR(KERNEL_DATA, 0, 0)
# define KERNEL_CPU_DATA 3
# define KERNEL_CPU_DATA_SEL SEG_SELECTOR(KERNEL_CPU_DATA, 0, 0)
# define USER_CODE 4
# define USER_CODE_SEL SEG_SELECTOR(USER_CODE, 0, 3)
# define USER_DATA 5
# define USER_DATA_SEL SEG_SELECTOR(USER_DATA, 0, 3)
# define TSS 6

# define GDT_LAST_ENTRY TSS

# define GDT_TYPE_CODE 0x8
# define GDT_TYPE_DATA_WRITE 0x2
# define GDT_DPL_0 0x0
# define GDT_DPL_3 0x60
# define GDT_PRESENT 0x80
# define GDT_NOT_SYSTEM 0x10

# define GDT_GRANULARITY_BYTE 0
# define GDT_GRANULARITY_PAGE 1

# define GDT_CODE_ACCESS (GDT_TYPE_CODE | \
                          GDT_DPL_0 | \
                          GDT_NOT_SYSTEM)
# define GDT_DATA_ACCESS (GDT_TYPE_DATA_WRITE | \
                          GDT_DPL_0 | \
                          GDT_NOT_SYSTEM)

# define GDT_SIZE (GDT_LAST_ENTRY + 1)

# ifndef ASM_FILE

# include <types.hh>

struct GDTPtr
{
  u16 limit;
  size_t base_addr;
} __attribute__((packed));

struct GDTEntry
{
  u16 seg_limit_0_15;
  u16 base_addr_0_15;
  u8 base_addr_16_23;
  u8 access:7;
  u8 present:1;
  u8 seg_limit_16_19:4;
  u8 avl:1;
  u8 l:1;
  u8 granularity:1;
  u8 op_size:1;
  u8 base_addr_24_31;
} __attribute__((packed));

class GDTManager
{
  public:
    virtual void init() = 0;

  protected:
    void applyGDT(struct GDTEntry*);
    void setGate(struct GDTEntry* gdt,
                 u32 index, u32 base, u32 limit,
                 u8 access, u8 granularity);
};

class GDTDynamicManager: public GDTManager
{
  public:
    virtual void init();
    void setGate(u32 index, u32 base, u32 limit, u8 access, u8 granularity);
};

class GDTStaticManager: public GDTManager
{
  public:
    virtual void init();

  private:
    struct GDTEntry _gdt[GDT_SIZE];
};

# endif /* ASM_FILE */

#endif /* !GDT_MANAGER_HH_ */
