/*
 * File: ioapic_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manage multiple IOAPICs
 *
 */

#ifndef IOAPIC_MANAGER_HH_
# define IOAPIC_MANAGER_HH_

# include <types.hh>
# include <interrupt_controller.hh>
# include <arch/irq_manager_i.hh>

# define MAX_ISA_IRQ 16
# define IOAPIC_MAX_IRQ 24

# define IOAPIC_IOREGSEL 0x0
# define IOAPIC_IOWIN 0x10

# define IOAPIC_REG_IOAPICID 0x0
# define IOAPIC_REG_IOAPICVER 0x1
# define IOAPIC_REG_IOAPICARB 0x2
# define IOAPIC_REG_IOREDTBL 0x10

# define IOAPIC_REDIR_OFFSET 16

# define IOAPIC_MASK_IRQ (1 << 16)

# define IOAPIC_IRQ_VECTOR_0 IRQ_VECTOR_IOAPIC_BASE

namespace smp
{
  class IOAPIC: public irq::InterruptController
  {
    public:
      IOAPIC(u8 id, uint_ptr addr, u8 irq_base);
      ~IOAPIC();

      virtual void init();

      u8 getLastIRQ()
      {
        return _last_irq;
      }
      u8 getFirstIRQ()
      {
        return _first_irq;
      }
      u8 getId()
      {
        return _id;
      }

      virtual void mask(u8 intr);
      virtual void unmask(u8 intr);
      virtual void acknowledge(u8 intr);

      enum DeliveryMode
      {
        Fixed = 0,
        Low = 1,
        SMI = 2,
        // Reserved
        NMI = 4,
        INIT = 5,
        // Reserved
        ExtINT = 7
      };

      enum DestinationMode
      {
        Physical = 0,
        Logical = 1,
      };

      enum Polarity
      {
        HighActive = 0,
        LowActive = 1
      };

      enum TriggerMode
      {
        Edge = 0,
        Level = 1
      };

      void configureLine(u8 intr,
                         enum IOAPIC::DeliveryMode,
                         enum IOAPIC::Polarity,
                         enum IOAPIC::TriggerMode);
      void redirectIRQtoCPU(u8 intr, enum IOAPIC::DestinationMode, u8 dest);

    private:
      void _setReg(u32 reg, u32 value);
      u32 _getReg(u32 reg);

      u8 _getMaxRedirectionEntry();

      u8 _id;
      u8 _first_irq;
      u8 _last_irq;
      volatile char* _addr;
  };

  class IOAPICManager: public irq::InterruptController
  {
    public:
      IOAPICManager();
      ~IOAPICManager();

      virtual void init();
      virtual void mask(u8 intr);
      virtual void unmask(u8 intr);
      virtual void acknowledge(u8 intr);
      void setIOAPICnumber(u8 nb);
      // Take ownership of the IOAPIC
      void addIOAPIC(IOAPIC* ioapic);
      void configureLine(u8 intr,
                         enum IOAPIC::DeliveryMode,
                         enum IOAPIC::Polarity,
                         enum IOAPIC::TriggerMode);
      void redirectIRQtoCPU(u8 intr, enum IOAPIC::DestinationMode, u8 dest);
      void addInterruptRedirection(u32 line, u8 redir);
      u8 getMaxIRQ()
      {
        return _max_irq;
      }
      IOAPIC* getIOAPICbyId(u8 id);
      IOAPIC* getIOAPICbyIRQOrder(u8 id);

    private:
      u8 _max_irq;
      u8 _ioapic_nb;
      IOAPIC** _ioapics;
  };
}

#endif /* !IOAPIC_MANAGER_HH_ */
