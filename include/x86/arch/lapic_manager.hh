/*
 * File: lapic_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manage the local apic for each CPU
 *
 */

#ifndef LAPIC_MANAGER_HH_
# define LAPIC_MANAGER_HH_

# include <types.hh>
# include <interrupt_controller.hh>
# include <arch/irq_manager_i.hh>
# include <timer.hh>

# define IA32_APIC_BASE_MSR 0x1b

# define LAPIC_PENDING_IPI (1 << 12)

# define LAPIC_ICR_LOW 0x300
# define LAPIC_ICR_HIGH 0x310
# define LAPIC_LVT_TIMER 0x320
# define LAPIC_LVT_THERMAL 0x330
# define LAPIC_LVT_PERF 0x340
# define LAPIC_LVT_LINT0 0x350
# define LAPIC_LVT_LINT1 0x360
# define LAPIC_LVT_ERR 0x370

# define LAPIC_REG_TIMER_INITIAL 0x380
# define LAPIC_REG_TIMER_CURRENT 0x390
# define LAPIC_REG_TIMER_DIV 0x3e0

# define LAPIC_SPURIOUS 0xf0

# define LAPIC_TPR 0x80

# define LAPIC_LDR 0xd0
# define LAPIC_DFR 0xe0
# define LAPIC_FLAT (0xf << 28)
# define LAPIC_CLUSTER (0 << 28)

# define LAPIC_ID 0x20

# define LAPIC_MASK_IRQ (1 << 16)

# define LAPIC_VECTOR_IRQ_BASE IRQ_VECTOR_LAPIC_BASE
# define LAPIC_VECTOR_TIMER (LAPIC_VECTOR_IRQ_BASE + 0)
# define LAPIC_VECTOR_THERM (LAPIC_VECTOR_IRQ_BASE + 1)
# define LAPIC_VECTOR_PERF (LAPIC_VECTOR_IRQ_BASE + 2)
# define LAPIC_VECTOR_LINT0 (LAPIC_VECTOR_IRQ_BASE + 3)
# define LAPIC_VECTOR_LINT1 (LAPIC_VECTOR_IRQ_BASE + 4)
# define LAPIC_VECTOR_ERR (LAPIC_VECTOR_IRQ_BASE + 5)
# define LAPIC_VECTOR_SPURIOUS (LAPIC_VECTOR_IRQ_BASE + 6)

# define LAPIC_ENABLE 0x100

# define LAPIC_EOI 0xb0

# define LAPIC_TIMER_ONESHOT 0x0
# define LAPIC_TIMER_PERIODIC 0x1
# define LAPIC_TIMER_TSC 0x2

# define LAPIC_DIVIDER 16

# define LAPIC_TIMER_DIV_16 0x3

namespace smp
{
  class LAPICManager: public irq::InterruptController,
                      public time::PeriodicTimer
  {
    public:
      enum DeliveryMode
      {
        Fixed = 0,
        Low = 1,
        SMI = 2,
        // Entry 3 is reserved
        NMI = 4,
        INIT = 5,
        StartUP = 6,
        ExtINT = 7
      };

      enum DestinationMode
      {
        Physical = 0,
        Virtual = 1
      };

      enum Level
      {
        Deassert = 0,
        Assert = 1
      };

      enum Polarity
      {
        HighActive = 0,
        LowActive = 1
      };

      enum TriggerMode
      {
        Edge = 0,
        Level = 1
      };

      enum DestinationShortand
      {
        NoShorthand = 0,
        Self = 1,
        All = 2,
        AllButSelf = 3
      };

      LAPICManager(void* addr);
      ~LAPICManager();
      virtual void init();
      // We just need to call this method once on BSP
      void initHandlers();
      void sendIPI(u8 cpu_apic_id,
                   enum DestinationShortand destination_shorthand,
                   enum DestinationMode destination_mode,
                   enum DeliveryMode delivery_mode,
                   u8 vector);
      u8 getAPICId()
      {
        return (_getReg(LAPIC_ID) >> 24);
      }

      static uint_ptr getLAPICAddr();
      virtual void mask(u8 reg);
      virtual void unmask(u8 reg);
      virtual void acknowledge(u8 intr);
      void setLocalInterruptVector(u32 reg, u8 val);
      void configureLine(u8 line, enum DeliveryMode,
                         enum Polarity, enum TriggerMode);
      void setPriority(u8 prio);

      virtual void initPeriodic();

    private:
      void _maskReg(u32 reg);
      void _unmaskReg(u32 reg);
      void _setReg(u32 reg, u32 value);
      u32 _getReg(u32 reg);

      volatile char* _addr;
  };

  extern LAPICManager* lapic;
};

#endif /* !LAPIC_MANAGER_HH_ */
