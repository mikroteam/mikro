/*
 * File: memory_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manage memory on x86
 *
 */

#ifndef MEMORY_MANAGER_I_HH_
# define MEMORY_MANAGER_I_HH_

# include <arch_template.hh>
# include <arch/gdt_manager.hh>

namespace mem
{
  template<typename T>
  class MemoryManagerImpl
  {
  };

  template<>
  class MemoryManagerImpl<x86>
  {
    public:
      void init();
      void initEarly();

      GDTStaticManager gdtsm;
      GDTDynamicManager gdtm;
  };
}

#endif /* !MEMORY_MANAGER_I_HH_ */
