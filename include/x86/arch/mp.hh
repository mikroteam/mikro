/*
 * File: mp.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manipulate the Multi processor struct defined by Intel
 *
 */

#ifndef MP_HH_
# define MP_HH_

# include <types.hh>
# include <errors.hh>
# include <arch/lapic_manager.hh>
# include <arch/smp_info.hh>

# define MP_FLOATING_SIGN "_MP_"
# define MP_TABLE_HEADER_SIGN "PCMP"

namespace smp
{

# define MP_CPU_ENTRY_TYPE 0
# define MP_BUS_ENTRY_TYPE 1
# define MP_IOAPIC_ENTRY_TYPE 2
# define MP_IO_INT_ENTRY_TYPE 3
# define MP_LO_INT_ENTRY_TYPE 4

# define MP_CPU_ENTRY_SIZE 20
# define MP_BUS_ENTRY_SIZE 8
# define MP_IOAPIC_ENTRY_SIZE 8
# define MP_IO_INT_ENTRY_SIZE 8
# define MP_LO_INT_ENTRY_SIZE 8

# define CPU_MAX 256
# define BUS_MAX 256
# define ALL_APIC 255

  class MPManager : public SMPInfo
  {
    public:
      MPManager();
      virtual ~MPManager();

      void release();
      bool init();

      virtual u8 getIOAPICCount()
      {
        return _ioapic_count;
      }

      virtual u32 getCpuCount()
      {
        return _cpu_count;
      }

      u8 getAPICId(u8 cpu)
      {
        return _cpu_apic_ids[cpu];
      }

      virtual uint_ptr getAPICAddr()
      {
        return _lapic_addr;
      }

      virtual bool configureIOAPICs(IOAPICManager*);
      virtual bool configureIOAPICsLines(IOAPICManager*);
      virtual bool configureLAPICLines();

    private:
      struct MPFloating
      {
        char signature[4];
        u32 phys_addr;
        u8 length;
        u8 spec_rev;
        u8 checksum;
        u8 mp_features[5];
      }__attribute__((packed));

      struct MPConfigTableHeader
      {
        char signature[4];
        u16 base_table_len;
        u8 spec_rev;
        u8 checksum;
        char oem_id_str[8];
        char product_id_str[12];
        u32 oem_table_ptr;
        u16 oem_table_size;
        u16 entry_count;
        u32 lapic_addr;
        u16 extended_table_len;
        u8 extended_table_checksum;
        u8 reserved;
      }__attribute__((packed));

      struct MPCpuEntry
      {
        u8 entry_type;
        u8 local_apic_id;
        u8 local_apic_version;
        u8 cpu_usable : 1;
        u8 cpu_bsp : 1;
        u8 cpu_flags : 6;
        u32 cpu_sign;
        u32 cpu_features;
        u32 reserved[2];
      }__attribute__((packed));

      struct MPBusEntry
      {
        u8 entry_type;
        u8 bus_id;
        char bus_type_str[6];
      }__attribute__((packed));

      struct MPIOApicEntry
      {
        u8 entry_type;
        u8 ioapic_id;
        u8 ioapic_version;
        u8 ioapic_enable : 1;
        u8 ioapic_flags : 7;
        u32 ioapic_addr;
      }__attribute__((packed));

      /* Used for IO and local interrupts */
      struct MPInterrupt
      {
        u8 entry_type;
        u8 interrupt_type;
        u8 polarity: 2;
        u8 trigger_mode: 2;
        u16 reserved: 12;
        u8 source_bus_id;
        u8 source_bus_irq;
        u8 apic_id;
        u8 apic_int;
      }__attribute__((packed));

      enum MPInterruptType
      {
        INT = 0,
        NMI = 1,
        SMI = 2,
        ExtINT = 3
      };

      enum MPInterruptPolarity
      {
        POLARITY_CONFORMS = 0,
        HIGH = 1,
        POLARITY_RESERVED = 2,
        LOW = 3
      };

      enum MPInterruptTriggerMode
      {
        MODE_CONFORMS = 0,
        EDGE = 1,
        MODE_RESERVED = 2,
        LEVEL = 3
      };

      enum MPEntryType
      {
        CPU = 0,
        BUS = 1,
        IOAPIC = 2,
        IO_INT_ASSIGN = 3,
        LO_INT_ASSIGN = 4
      };


      bool _searchFloating();
      bool _searchFloatingInZone(void* addr, size_t len);
      bool _parseConfigTableHeader();
      bool _parseBaseTable();
      bool _parseCPU(char*);
      bool _parseBUS(char*);
      bool _parseIOAPIC(char*);
      bool _parseIOInt(char*);
      bool _parseLOInt(char*);
      bool _parseInt(struct MPInterrupt* interrupt);
      void _configureIOAPIC(struct MPIOApicEntry* ioapic_entry,
                            IOAPICManager*);
      void _configureIOAPICLine(struct MPInterrupt* intr,
                                IOAPICManager* ioapicm);
      void _configureLAPICLine(struct MPInterrupt* intr, u8 apic_id);
      void _configureIOAPIC(struct MPInterrupt* intr,
                            IOAPICManager* ioapicm,
                            class IOAPIC* ioapic);
      enum smp::IOAPIC::Polarity _convertPolarityForIOAPIC(u16 polarity);
      enum smp::IOAPIC::TriggerMode _convertTriggerModeForIOAPIC(u16 trigger);
      enum smp::IOAPIC::DeliveryMode _convertTypeForIOAPIC(u16 type);
      enum smp::LAPICManager::Polarity _convertPolarityForLAPIC(u16 polarity);
      enum smp::LAPICManager::TriggerMode _convertTriggerModeForLAPIC(u16 trigger);
      enum smp::LAPICManager::DeliveryMode _convertTypeForLAPIC(u16 type);
      u8 _getDefaultPolarityForBus(u8 bus_id);
      u8 _getDefaultTriggerModeForBus(u8 bus_id);

      struct MPFloating* _mpf;
      struct MPConfigTableHeader* _mcth;

      u32 _lapic_addr;

      u32 _cpu_count;
      u8 _bsp_apic_id;
      void* _mpf_page_addr;
      void* _mcth_page_addr;
      u8 _ioapic_count;

      u8* _cpu_apic_ids;
      char** _bus_names;

      struct MPEntry
      {
        u8 size;
        bool (MPManager::*parse_function)(char*);
      };

      static struct MPEntry _parse_entry_table[];
  };

}

#endif /* !MP_HH_ */
