/**
 * \file paging_internal_i.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging entry point for x86
 */

#ifndef PAGING_INTERNAL_I_HH_
#define PAGING_INTERNAL_I_HH_

#include "arch/paging_macro_i.hh"
#include "arch_template.hh"
#include "memory/paging_tools.hh"
#include "multiboot.h"
#include "types.hh"

namespace mem
{

#ifndef MIKRO64
/* i386 case */

  struct Pde
  {
    unsigned p    : 1;
    unsigned rw   : 1;
    unsigned us   : 1;
    unsigned pwt  : 1;
    unsigned pcd  : 1;
    unsigned a    : 1;
    unsigned ign1 : 1;
    unsigned bit7 : 1;
    unsigned ign2 : 4;
    unsigned addr : 20;
  };

  struct Pte
  {
    unsigned p    : 1;
    unsigned rw   : 1;
    unsigned us   : 1;
    unsigned pwt  : 1;
    unsigned pcd  : 1;
    unsigned a    : 1;
    unsigned d    : 1;
    unsigned pat  : 1;
    unsigned g    : 1;
    unsigned ign  : 3;
    unsigned addr : 20;
  };

  struct KernelMapping
  {
    u32 paddr;
    u32 binSize;
    u32 dataSize;
  };

  struct PagingBasics
  {
    void pdt_change(Pde *pde, void *paddr_pt, u16 rights);
    void pdt_delete(Pde *pde)
    {
      pde->p = 0;
    }
    unsigned int pdt_offset(void *vaddr)
    {
      return ((unsigned int) vaddr) >> 22;
    }

    /* Page Table functions */
    void pt_change(Pte *pte, void *paddr_page, u16 rights);
    void pt_delete(Pte *pte)
    {
      pte->p = 0;
    }
    unsigned int pt_offset(void *vaddr)
    {
      return (((unsigned int) vaddr) >> 12) & 0x3FF;
    }
  };

  class PageTableKernel
  {
    public:
      PageTableKernel()
        : _pts_paddr(0), _pts_vaddr(0)
      { }

      void init(u32 paddr, u32 vaddr)
      {
        _pts_paddr = paddr;
        _pts_vaddr = vaddr;
      }

      void add(void *paddr, void *vaddr, size_t nrPages, u16 flags);
      void change(void *vaddr, size_t nrPages, u16 rights);
      void free(void *vaddr, size_t nrPages);
      void *getPaddr(void *vaddr);

      u32 getPtsPaddr()
      {
        return _pts_paddr;
      }

      u32 getPtsVaddr()
      {
        return _pts_vaddr;
      }

    private:
      u32 _pts_paddr;
      u32 _pts_vaddr;

      PagingBasics _basics;
  };

  class PageTableUser
  {
    public:
      bool create(void **paddr, void **vaddr);
      bool add(void *pd_vaddr, void *paddr, void *vaddr, size_t nrPages,
          u16 flags);
      bool change(void *pd_vaddr, void *vaddr, size_t nrPages, u16 rights);
      void attachKernel(void *pd_vaddr);
      bool free(void *pd_vaddr, void *vaddr, size_t nrPages);
      void *getPaddr(void *pd_vaddr, void *vaddr);
      void clear(void *pd_vaddr);

    private:
      PagingBasics _basics;
  };

  template<typename T>
  class PagingInternalImpl
  {
  };

  template<>
  class PagingInternalImpl<x86>
  {
    public:
      PagingInternalImpl()
        : dmaEnd(KERNEL_ZONE_DMA_START + (KERNEL_ZONE_DMA_PAGES << 12))
      { }

      void init(multiboot_info_t *info);

      PageTableKernel pageTableKernel;
      PageTableUser pageTableUser;

      const u32 dmaEnd;

    private:
      bool moveKernel(multiboot_info_t *info, KernelMapping &kmap);
      void addInitialMapping(size_t pt_paddr, size_t *paddr, size_t vaddr,
          size_t nrPages, u16 flags);

      PagingBasics _basics;
      PagingTools _tools;
  };

#endif /* !MIKRO64 */
}
#endif /* !PAGING_INTERNAL_I_HH_ */
