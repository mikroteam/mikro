/**
 * \file paging_macro_i.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging macros for x86
 */

#ifndef PAGING_MACRO_I_HH_
#define PAGING_MACRO_I_HH_

#include "memory/paging_macro.hh"

#ifndef MIKRO64
/* i386 case */

#define KERNEL_LOAD_ADDR          0x10000
#define KERNEL_VIRTPHYS_OFFSET    0x100000
#define VIRT2PHYS(addr)           ((addr) - \
                                  (KERNEL_AS_BEGIN - KERNEL_VIRTPHYS_OFFSET))
#define KERNEL_NR_PT              256

#define KERNEL_ZONE_DMA_START     0
#define KERNEL_ZONE_DMA_PAGES     0x1000    // 16MB
#define KERNEL_ZONE_NORMAL_START  0x1000000

#define KERNEL_MIN_MEMORY         0x4000000 // 64MB

#define KERNEL_COPY_OFFSET        (0x400 * 0x1000)
#define KERNEL_COPY_VADDR         (KERNEL_AS_BEGIN + KERNEL_COPY_OFFSET)

#define KERNEL_START_NR_PAGES     20

#define KERNEL_VIDEO_MEM          0xB0000
#define KERNEL_VIDEO_MEM_SIZE     0x10000
#define KERNEL_VIDEO_MEM_END      (KERNEL_VIDEO_MEM + KERNEL_VIDEO_MEM_SIZE)

#define KERNEL_SMP_VECT           0x1
#define KERNEL_SMP_PAGE           (KERNEL_SMP_VECT << 12)

#endif /* !MIKRO64 */
#endif /* !PAGING_MACRO_I_HH_ */
