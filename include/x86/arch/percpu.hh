/*
 * File: percpu.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Per cpu variable
 *
 */

#ifndef PERCPU_HH_
# define PERCPU_HH_

namespace smp
{
  void initCpuVar(u32 id);
}

#endif /* !PERCPU_HH_ */
