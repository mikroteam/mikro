/*
 * File: pic_manager.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class that manages the 8259PIC
 *
 */

#ifndef PIC_MANAGER_HH_
# define PIC_MANAGER_HH_

# include <types.hh>
# include <irq_common.hh>
# include <interrupt_controller.hh>

# define PIC_MASTER_PA 0x20
# define PIC_MASTER_PB 0x21
# define PIC_SLAVE_PA 0xA0
# define PIC_SLAVE_PB 0xA1

# define PIC_INPUTS 8

# define PIC_ACK 0x20

# define PIC_ICW1 (0x10)
# define PIC_ICW1_ICW4_PRESENT 0x1
# define PIC_ICW1_CASCADE_MODE (0x0 << 1)
# define PIC_ICW1_SINGLE_MODE (0x1 << 1)
# define PIC_ICW1_EDGE_TRIGGER (0x0 << 3)
# define PIC_ICW1_LEVEL_TRIGGER (0x1 << 3)

# define PIC_ICW3_MASTER_SLAVE_INPUT (0x4)
# define PIC_ICW3_SLAVE_ID (0x2)

# define PIC_ICW4_MODE_8086 (0x1)
# define PIC_ICW4_EOI_NORMAL (0x0 << 1)
# define PIC_ICW4_EOI_AUTO (0x1 << 1)
# define PIC_ICW4_SLAVE (0x0 << 2)
# define PIC_ICW4_MASTER (0x1 << 2)
# define PIC_ICW4_BUFFER (0x1 << 3)
# define PIC_ICW4_SPECIAL_FULLY_NESTED (0x1 << 4)

namespace irq
{
  class PicManager: public InterruptController
  {
    public:
      virtual void init();
      virtual void mask(u8 num);
      virtual void unmask(u8 num);
      virtual void disable();
      virtual void acknowledge(u8 intr);
  };
}

#endif /* !PIC_MANAGER_HH_ */
