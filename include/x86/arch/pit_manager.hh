#ifndef PIT_MANAGER_HH_
# define PIT_MANAGER_HH_

# include <timer.hh>
# include <arch/irq_manager_i.hh>

# define PIT_CMD_PORT   0x43
# define PIT_CNT1_PORT  0x40
# define PIT_CNT2_PORT  0x40
# define PIT_CNT3_PORT  0x40
# define PIT_ORG_FREQ   0x1234DE

# define PIT_CNT_BINARY 0x0
# define PIT_CNT_BCD 0x1
# define PIT_MODE_ONESHOT (0x0 << 1)
# define PIT_MODE_ONESHOT_RETRIG (0x1 << 1)
# define PIT_MODE_RATE_GEN (0x2 << 1)
# define PIT_MODE_SQUARE_GEN (0x3 << 1)
# define PIT_MODE_SOFT_TRIGGER_PROBE (0x4 << 1)
# define PIT_MODE_HARD_TRIGGER_PROBE (0x5 << 1)
# define PIT_CNT_READ (0x0 << 4)
# define PIT_CNT_SET_LSB (0x1 << 4)
# define PIT_CNT_SET_MSB (0x2 << 4)
# define PIT_CNT_SET_LSB_MSB (0x3 << 4)
# define PIT_CNT1 (0x0 << 6)
# define PIT_CNT2 (0x1 << 6)
# define PIT_CNT3 (0x2 << 6)

namespace time
{
  class PitManager: public PeriodicTimer
  {
    public:
      virtual void initPeriodic();
  };
}

#endif /* PIT_MANAGER_HH_ */
