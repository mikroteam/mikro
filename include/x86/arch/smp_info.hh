/*
 * File: smp_info.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Deliver informations on a SMP system
 *
 */

#ifndef SMP_INFO_HH_
# define SMP_INFO_HH_

# include <types.hh>
# include <arch/ioapic_manager.hh>

namespace smp
{
  class SMPInfo
  {
    public:
      virtual ~SMPInfo() {};

      static bool init();
      static void release();

      virtual u8 getIOAPICCount() = 0;
      virtual u32 getCpuCount() = 0;
      virtual uint_ptr getAPICAddr() = 0;
      virtual u8 getAPICId(u8 cpu) = 0;

      virtual bool configureIOAPICs(IOAPICManager* ioapic) = 0;
      virtual bool configureIOAPICsLines(IOAPICManager* ioapic) = 0;
      virtual bool configureLAPICLines() = 0;
  };

  extern SMPInfo* info;
}

#endif /* !SMP_INFO_HH_ */
