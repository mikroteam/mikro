/*
 * File: smp_manager_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Handle SMP on x86 arch
 *
 */

#ifndef SMP_MANAGER_I_HH_
# define SMP_MANAGER_I_HH_

# include <cpu.hh>
# include <arch/mp.hh>
# include <arch/lapic_manager.hh>
# include <arch_template.hh>

namespace smp
{
  template<typename T>
  class SMPManagerImpl
  {
  };

  template<>
  class SMPManagerImpl<x86>
  {
    public:
      SMPManagerImpl()
        : _bsp_apic_id(0)
      { }
      ~SMPManagerImpl();
      void init();

      void wakeAPs()
      {
        if (cpu::number > 1)
          _wakeAllAPs(cpu::number, _bsp_apic_id);
      }

    private:
      void _wakeAllAPs(u32 cpu_count, u8 bsp_id);
      void _wakeAP(u8 local_apic_id);

      u32 _bsp_apic_id;
  };
}

#endif /* !SMP_MANAGER_I_HH_ */
