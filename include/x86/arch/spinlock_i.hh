/**
 * \file spinlock_i.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Define anything needed to use spinlocks
 */

#ifndef SPINLOCK_I_HH_
#define SPINLOCK_I_HH_

#include "arch_template.hh"
#include "arch/asm.hh"
#include "compiler_tools.hh"
#include "types.hh"

template<typename T>
class SpinlockImpl
{
};

template<>
class SpinlockImpl<x86>
{
  public:
    SpinlockImpl<x86>()
    {
      _lock.val = 0;
    }

    void lock()
    {
      ticket_lock buff;
      buff.queue.head = 0;
      buff.queue.tail = 1;

      buff.val = asmf::atomic::xadd(buff.val, &_lock.val);

      while (true)
      {
        if (buff.queue.head == buff.queue.tail)
          break;

        asmf::atomic::asm_pause();

        buff.queue.head = alwaysFromMem(_lock.queue.head);
      }

      asmf::atomic::asm_mbarrier();
    }

    void unlock()
    {
      asmf::atomic::inc16(&_lock.queue.head);
    }

    bool isLocked()
    {
      return (_lock.queue.head != _lock.queue.tail);
    }

  protected:
    union ticket_lock
    {
      u32 val;
      struct
      {
        u16 head;
        u16 tail;
      } queue;
    };

    ticket_lock _lock;
};

#endif /* !SPINLOCK_I_HH_ */
