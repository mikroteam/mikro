/*
 * File: time_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Time management implementation on x86
 *
 */

#ifndef TIME_I_HH_
# define TIME_I_HH_

# include <arch_template.hh>
# include <arch/asm.hh>

namespace time
{
  template<typename T>
  class TimeManagerImpl
  {
  };

  template<>
  class TimeManagerImpl<x86>
  {
    public:
      inline u64 getCPUTickCount()
      {
        return asmf::cpu::rdtsc();
      }

      void msleep(u32 msecs);
  };
}

#endif /* !TIME_I_HH_ */
