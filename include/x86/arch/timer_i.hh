/*
 * File: timer_i.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Timer on x86
 *
 */

#ifndef TIMER_I_HH_
# define TIMER_I_HH_

namespace time
{
  void init();
}

#endif /* !TIMER_I_HH_ */
