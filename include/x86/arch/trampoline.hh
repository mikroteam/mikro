/*
 * File: trampoline.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Macros for the trampoline code that will boot APs
 *
 */

#ifndef TRAMPOLINE_HH_
# define TRAMPOLINE_HH_

# include <arch/paging_macro_i.hh>

# define TR_START_AP_VECTOR KERNEL_SMP_VECT
# define TR_START_AP_ADDR KERNEL_SMP_PAGE
# define TR_START_AP_SIZE 0x1000

#endif /* !TRAMPOLINE_HH_ */
