/**
 * \file vm86_macro.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Virtual Mode 86 management macros
 */

#ifndef VM86_MACRO_HH
#define VM86_MACRO_HH

#define VM86_TASK_EIP     0x1000
#define VM86_TASK_ESP_OFF (0x1000 - (2 * 7))
#define VM86_TASK_ESP     (VM86_TASK_EIP + VM86_TASK_ESP_OFF)
#define VM86_RETURN_INT   77

#endif /* !VM86_MACRO_HH */
