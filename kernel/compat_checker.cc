/*
 * File: compat_checker.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Check platform compatibility
 *
 */

#include <compat_checker.hh>

CompatibilityChecker compat_checker;
