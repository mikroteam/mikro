/*
 * File: __cxa_guard.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Guard that init static variables in C++
 *
 */

#include <logger.hh>
#include <errors.hh>
#include <__cxa_guard.hh>

static Spinlock __guard_slock;

extern "C" int __cxa_guard_acquire(__guard* g)
{
  char* guard = (char*)g;

  // First check if init is required
  if (guard[0] != 0)
    return 0;

  // Block extra threads at this point
  __guard_slock.lock();
  // Check if another thread has init the object
  if (guard[0] != 0)
    return 0;

  return 1;
}

extern "C" void __cxa_guard_release(__guard* g)
{
  char* guard = (char*)g;

  // Mark object has initialized
  guard[0] = 1;
  __guard_slock.unlock();
}

extern "C" void __cxa_guard_abort(__guard*)
{
  log::fatal("PANIC") << "Error while initializing a local static variable";
  panic();
}
