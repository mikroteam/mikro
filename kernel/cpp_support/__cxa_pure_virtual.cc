/*
 * File: __cxa_pure_virtual.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Pure virtual call function for the compiler
 *
 */

#include <errors.hh>
#include <logger.hh>

/* 
 * This function is called when a call to a virtual function happen
 * This should hopefully never happen...
 */
extern "C" void __cxa_pure_virtual()
{
  log::fatal("PANIC") << "Call to a pure virtual function";
  panic();
}
