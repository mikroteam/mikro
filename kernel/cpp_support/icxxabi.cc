#include <cstring.hh>

#include <icxxabi.hh>

# define ATEXIT_MAX_FUNC_ENTRY 128u

void* __dso_handle = 0;

struct atexit_func_entry
{
  void (*destructor) (void *);
  void* arg;
  void* dso;
};

static struct atexit_func_entry atexit_func_table[ATEXIT_MAX_FUNC_ENTRY];
static int atexit_entry_nb = 0;

int __cxa_atexit(void (*destructor) (void *), void* arg, void* dso)
{
  if (!destructor)
    return -1;

  /* No more space left */
  if (atexit_entry_nb == ATEXIT_MAX_FUNC_ENTRY)
    return -1;

  atexit_func_table[atexit_entry_nb].destructor = destructor;
  atexit_func_table[atexit_entry_nb].arg = arg;
  atexit_func_table[atexit_entry_nb].dso = dso;

  ++atexit_entry_nb;

  return 0;
}

static void fillGap(int gap)
{
  /* No need to fill the gap, only one element */
  if (atexit_entry_nb == 1)
    return;

  /* Last entry no need to fill a gap */
  if (gap == (atexit_entry_nb - 1))
    return;

  size_t size = sizeof (struct atexit_func_entry) * ((atexit_entry_nb - 1) - gap);
  memcpy(&(atexit_func_table[gap]), &(atexit_func_table[gap + 1]), size);
}

void __asmcall __cxa_finalize(void* f)
{
  /* We must call every destructor (in reverse order) */
  if (f == 0)
  {
    --atexit_entry_nb;
    for ( ; atexit_entry_nb >= 0; atexit_entry_nb--)
    {
      void* arg = atexit_func_table[atexit_entry_nb].arg;
      atexit_func_table[atexit_entry_nb].destructor(arg);
    }
    atexit_entry_nb = 0;
  }
  /* We must call only the specified desctructor */
  else
  {
    for (int i = atexit_entry_nb - 1 ; i >= 0; i--)
    {
      if (f == atexit_func_table[i].destructor)
      {
        atexit_func_table[i].destructor(atexit_func_table[i].arg);
        fillGap(i);
        --atexit_entry_nb;
      }
    }
  }
}
