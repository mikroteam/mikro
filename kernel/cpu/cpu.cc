/*
 * File: cpu.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Useful informations about a CPU
 *
 */

#include <cpu.hh>

namespace cpu
{

CPU* __percpu myself;
u8 number = 1;
CPU** cpus = 0;

CPU::CPU(u32 i)
  : id (i),
    current_thread (0)
{
}

CPU::CPU()
  : CPU(0)
{
}

}
