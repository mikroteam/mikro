/**
 * \file gdbstub.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Gdb remote stub
 */

#include "logger.hh"
#include "gdbstub.hh"

namespace debug
{

GdbStub gdbStub;

bool GdbStub::readMemory(u32 size)
{
  u32 addr, memSize, nrChars;

  nrChars = _impl.internal.hex2int(&_impl.internal.buff[1], addr, size);
  _impl.internal.hex2int(&_impl.internal.buff[2 + nrChars], memSize, size);

  _impl.internal.mem2hex((char *) addr, _impl.internal.buff, memSize);
  _impl.internal.send(_impl.internal.buff, memSize << 1);

  return true;
}

bool GdbStub::writeMemory(u32 size)
{
  u32 addr, memSize, nrChars;

  nrChars = _impl.internal.hex2int(&_impl.internal.buff[1], addr, size);
  nrChars += _impl.internal.hex2int(&_impl.internal.buff[2 + nrChars], memSize,
      size);

  _impl.internal.hex2mem(&_impl.internal.buff[3 + nrChars], (char *) addr,
      memSize << 1);
  _impl.internal.send("OK", 2);

  return true;
}

bool GdbStub::handleRequest(u32 size, irq::RegsDump *regs)
{
  switch (_impl.internal.buff[0])
  {
    /*
     * c [addr]
     * Continue. addr is address to resume.
     * If addr is omitted, resume at current address
     */
    case 'c':
      if (!_impl.cont(regs, size))
        break;
      return true;

    /*
     * s [addr]
     * Single step. addr is the address at which to resume.
     * If addr is omitted, resume at same address.
     */
    case 's':
      if (!_impl.step(regs, size))
        break;
      return true;

    /*
     * m addr,length
     * Read length bytes of memory starting at address addr.
     * Note that addr may not be aligned to any particular boundary.
     */
    case 'm':
      if (!readMemory(size))
        break;
      return false;

    /*
     * M addr,length:XX...
     * Write length bytes of memory starting at address addr. XX...
     * is the data; each byte is transmitted as a two-digit hexadecimal number.
     */
    case 'M':
      if (!writeMemory(size))
        break;
      return false;

    /*
     * g
     * Read general registers.
          */
    case 'g':
      if (!_impl.readRegs(regs, size))
        break;
      return false;

    /*
     * G XX...
     * Write general registers
     */
    case 'G':
      if (_impl.writeRegs(regs, size))
        break;
      return false;

    /*
     * ?
     * Indicate the reason the target halted. The reply is the same as for step
     * and continue. This packet has a special interpretation when the target
     * is in non-stop mode.
     */
    case '?':
      _impl.haltReason();
      break;

    /*
     * D
     * Used to detach gdb from the remote system.
     */
    case 'D':
    case 'k':
      _impl.detach(regs);
      return true;
  }

  _impl.internal.send("", 0);
  return false;
}

void irqHandler(irq::RegsDump *regs)
{
  u32 size;

  gdbStub._impl.haltReason();

  do
    size = gdbStub._impl.internal.receive();
  while (!gdbStub.handleRequest(size, regs));
}

}
