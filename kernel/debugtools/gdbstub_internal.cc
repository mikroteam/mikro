/**
 * \file gdbstub_internal.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Gdb remote stub internal
 */

#include "gdbstub_internal.hh"
#include "serial_manager.hh"

namespace debug
{

char hexchars[] = "0123456789abcdef";

u8 GdbStubInternal::hex(char in)
{
  if (in >= '0' && in <= '9')
    return in - '0';
  else if (in >= 'a' && in <= 'f')
    return in - 'a' + 10;
  else if (in >= 'A' && in <= 'F')
    return in - 'A' + 10;

  return 42;
}

void GdbStubInternal::hex2mem(const char *buff, char *to, u32 size)
{
  u8 *mem = (u8 *) to;

  for (u32 i = 0; i < (size >> 1); i++)
  {
    *mem = (hex(*buff) << 4);
    buff++;
    *mem += hex(*buff);
    buff++;

    mem++;
  }
}

u32 GdbStubInternal::hex2int(const char *buff, u32 &res, u32 size)
{
  res = 0;
  u8 hexVal;
  u32 nrChars = 0;

  for (u32 i = 0; i < size; i++)
  {
    hexVal = hex(*buff);
    buff++;
    if (hexVal > 0xF)
      break;

    res = (res << 4) | hexVal;
    nrChars++;
  }

  return nrChars;
}

void GdbStubInternal::mem2hex(const char *in, char *buff, u32 size)
{
  u8 *mem = (u8 *) in;

  for (u32 i = 0; i < size; i++)
  {
    *buff = hexchars[*mem >> 4];
    buff++;
    *buff = hexchars[*mem & 0xF];
    buff++;

    mem++;
  }
}

u8 GdbStubInternal::checksum(const char *buff, u32 size)
{
  u32 res = 0;

  for (u32 i = 0; i < size; i++)
    res += buff[i];

  res %= 256;

  return res;
}

void GdbStubInternal::send(const char *buff, u32 size)
{
  u8 cs = checksum(buff, size);
  char cs_char[2];
  char res;

  mem2hex((char *) &cs, cs_char, 1);

  do
  {
    serial::manager->write(_port, "$", 1);
    if (size)
      serial::manager->write(_port, buff, size);
    serial::manager->write(_port, "#", 1);
    serial::manager->write(_port, cs_char, 2);

    serial::manager->read(_port, &res, 1);
  }
  while(res != '+');
}

u32 GdbStubInternal::receive()
{
  u32 size = 0;
  char temp_buff[2];

  while (true)
  {
    do
      serial::manager->read(_port, temp_buff, 1);
    while (temp_buff[0] != '$');

    do
      serial::manager->read(_port, &buff[size], 1);
    while (buff[size++] != '#');

    size--;

    u8 cs_computed = checksum(buff, size);
    u8 cs_sent;

    serial::manager->read(_port, temp_buff, 2);
    hex2mem(temp_buff, (char *) &cs_sent, 2);

    if (cs_computed == cs_sent)
    {
      serial::manager->write(_port, "+", 1);
      break;
    }
    else
      serial::manager->write(_port, "-", 1);
  }

  return size;
}

}
