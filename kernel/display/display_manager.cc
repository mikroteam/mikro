/*
 * File: display_manager.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class that defines stream operators to write on screen
 *
 */

#include <display_manager.hh>
#include <display_writer.hh>

namespace dsp
{

DisplayManager kout;
StreamBaseHelper hex(16);
StreamBaseHelper dec(10);
StreamBaseHelper bin(2);

StreamAttributes attr(ATTR_DEFAULT);

DisplayManager::DisplayManager()
  : _base(10)
{
}

void DisplayManager::init()
{
  dwriter->init();
}

DisplayManager& DisplayManager::operator<<(const char *s)
{
  unsigned int i = 0;
  for ( ; s[i]; i++)
    dwriter->writeChar(s[i]);

  return *this;
}

DisplayManager& DisplayManager::operator<<(char c)
{
  dwriter->writeChar(c);
  return *this;
}

DisplayManager& DisplayManager::operator<<(StreamBaseHelper& sbh)
{
  _base = sbh.base;
  return *this;
}

DisplayManager& DisplayManager::operator<<(StreamAttributes& attr)
{
  dwriter->setAttr(attr.attr);
  return *this;
}

DisplayManager& DisplayManager::operator<<(void* ptr)
{
  *this << (size_t)ptr;
  return *this;
}

DisplayManager& DisplayManager::operator<<(unsigned long val)
{
  char buff[65];
  int i = 64;

  if (!val)
  {
    *this << '0';
    return *this;
  }

  buff[i--] = 0;

  for( ; val && i ; --i, val /= _base)
    buff[i] = "0123456789abcdef"[val % _base];

  *this << &buff[i+1];

  return *this;
}

DisplayManager& DisplayManager::operator<<(int val)
{
  if (val < 0)
  {
    val = -val;
    dwriter->writeChar('-');
  }
  *this << (size_t)val;
  return *this;
}

DisplayManager& DisplayManager::operator<<(unsigned int val)
{
  *this << (unsigned long)val;
  return *this;
}

}
