/*
 * File: logger.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Handler logs in the kernel side
 *
 */

#include <display_manager.hh>

#include <logger.hh>

namespace log
{

RawLogger raw;
RealLogger fatal(Fatal, "FTL", COLOR_RED | COLOR_BG(COLOR_WHITE));
RealLogger err(Error, "ERR", COLOR_RED);
RealLogger warn(Warning, "WRN", COLOR_YELLOW);
RealLogger info(Info, "INF", COLOR_GREEN);
RealLogger verbose(Verbose, "VRB", ATTR_DEFAULT);

#ifdef DEBUG

const char* log_focus_module = 0;
const char* log_current_module = 0;

RealLogger debug(Debug, "DBG", COLOR_BLUE);
LogLevel global_log_level = Debug;
#else
FakeLogger debug(Debug, "DBG");
LogLevel global_log_level = Info;
#endif /* DEBUG */

Spinlock LoggerTicket::_lock = Spinlock();

}
