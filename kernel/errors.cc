/**
 * \file errors.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief All error handling
 */

#include "arch/errors_i.hh"
#include "config.hh"
#include "errors.hh"
#include "spinlock.hh"

ErrorsImpl<ARCH_GENERIC> impl;

void panic()
{
  static Spinlock spin;

  impl.stopInterrupts();

  // That way we ensure only one CPU trigger the panic
  spin.lock();

  // TODO: stop others CPUs

  dsp::kout << "\n***    Mikro - Panic    ***\n";

  dsp::kout << "Registers:\n";
  impl.dumpRegisters();
  dsp::kout << "Stack:\n";
  impl.dumpStack();

  dsp::kout << "*** Mikro Rest In Peace ***";
  impl.funnyDisplay();

  while (true)
    ;
}

__noreturn void earlyAbort(const char* err)
{
  dsp::kout << "Mikro has encountered a problem while loading:\n\n";
  dsp::kout << err;
  dsp::kout << "\n\nAbort boot sequence.";

  while (true)
    ;
}
