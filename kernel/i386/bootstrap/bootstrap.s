#include <arch/paging_macro_i.hh>

.global k_entry

/* we suppose that kernel has a maximum size of 256 KB */
k_entry:
/* multiboot info: ebx, magic: eax */
  cli
  mov %eax, %ecx

/* clean .bss */
  mov $0x0, %eax
  mov $(VIRT2PHYS(_sbss)), %edi

1:
  stosl
  cmp $(VIRT2PHYS(_ebss)), %edi
  jne 1b

/* init page directory */
  mov $0x0, %eax
  mov $(VIRT2PHYS(_bootpdt + 0x4)), %edi

2:
  stosl
  cmp $(VIRT2PHYS(_bootpdt + 0x1000)), %edi
  jne 2b

  movl $(VIRT2PHYS(_bootpdt)), %eax
  movl $(VIRT2PHYS(_bootpt1_entry)), (%eax)
  movl $(VIRT2PHYS(_bootpdt + (KERNEL_AS_BEGIN >> 20))), %eax
  movl $(VIRT2PHYS(_bootpt2_entry)), (%eax)

/* init page table 1: 1:1 */
  mov $0x7, %eax
  mov $(VIRT2PHYS(_bootpt1)), %edi

3:
  stosl
  add $0x1000, %eax
  cmp $(VIRT2PHYS(_bootpt1 + 0x1000)), %edi
  jne 3b

/*
 * init page table 2: 0xC0000000
 * up to video memory + video memory size
 */
  mov $(KERNEL_VIRTPHYS_OFFSET + 0x7), %eax
  mov $(VIRT2PHYS(_bootpt2)), %edi

4:
  stosl
  add $0x1000, %eax
  cmp $(VIRT2PHYS(_bootpt2 + (KERNEL_VIDEO_MEM >> 12) * 4)), %edi
  jne 4b

  mov $(KERNEL_VIDEO_MEM + 0x7), %eax
5:
  stosl
  add $0x1000, %eax
  cmp $(VIRT2PHYS(_bootpt2 + (KERNEL_VIDEO_MEM_END >> 12) * 4)), %edi
  jne 5b

  mov $0x0, %eax
6:
  stosl
  cmp $(VIRT2PHYS(_bootpt2 + 0x1000)), %edi
  jne 6b

/* enable paging */
  movl $(VIRT2PHYS(_bootpdt)), %eax
  movl %eax, %cr3
  mov %cr0, %eax
  orl $0x80000000, %eax
  movl %eax, %cr0
  mov $upper, %eax
  jmp *%eax
upper:

/* set the new stack */
  mov $_bootstack, %esp
  push %ebx
  push %ecx

  mov  $start_ctors, %ebx             # call the constructors
  jmp  2f
1:
  call *(%ebx)
  add  $4, %ebx
2:
  cmp  $end_ctors, %ebx
  jb   1b

	call k_main	# kernel entry point

  mov  $end_dtors, %ebx               # call the destructors
  jmp  4f
3:
  sub  $4, %ebx
  call *(%ebx)
4:
  cmp  $start_dtors, %ebx
  ja   3b

/* call finalize to execute every callbacks */
  push $0
  call __cxa_finalize
  add $4, %esp

  cli
end:
  hlt
  jmp end

