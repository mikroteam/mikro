/*
 * File: irq_asm_handler.s
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Asm code that handle irqs
 *
 */

#define ASM_FILE
#include <arch/gdt_manager.hh>

.macro dump_regs
  pushal
  push %gs
  push %fs
  push %es
  push %ds
.endm

.macro restore_regs
  pop %ds
  pop %es
  pop %fs
  pop %gs
  popal
.endm

asm_common_stub:
  dump_regs

  push %eax
  movw $(KERNEL_DATA_SEL), %ax
  movw %ax, %ds
  movw $(KERNEL_CPU_DATA_SEL), %ax
  movw %ax, %fs
  pop %eax

  push %esp
  call idtDispatcher
  add $4, %esp
  restore_regs
  add $8, %esp
  iret

#define IRQ_ASM_HANDLER(Name, Num, Instr, Dpl)  \
.globl Name;                                    \
Name:;                                          \
  Instr;                                        \
  push $(Num);                                  \
  jmp asm_common_stub
#include "irq_asm_handler.def"
#undef IRQ_ASM_HANDLER
