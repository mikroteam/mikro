/*
 * File: irq_manager_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Irq manager for i386
 *
 */

#include <compiler_tools.hh>
#include <logger.hh>
#include <errors.hh>
#include <arch/gdt_manager.hh>
#include <arch/smp_info.hh>
#include <arch/ioapic_manager.hh>

#include <arch/asm.hh>
#include <arch/irq_manager_i.hh>
#include <irq_manager.hh>
#include <syscall.hh>

static irq::Interrupt _interrupt_table[IRQ_VECTOR_SIZE];

#define IRQ_ASM_HANDLER(Name, Num, Instr, Dpl) \
extern "C" void Name (void);
#include "irq_asm_handler.def"
#undef IRQ_ASM_HANDLER

void __noreturn generalFault(struct irq::RegsDump*)
{
  log::fatal("IRQ") << "General Protection Fault";
  panic();
}

void __noreturn doubleFault(struct irq::RegsDump*)
{
  log::fatal("IRQ") << "Double Fault";
  panic();
}

void pageFault(struct irq::RegsDump* regs)
{
  irq::ErrorCodePageFault *error;
  irq::RegsDumpI386 *regsi;

  regsi = static_cast<irq::RegsDumpI386*>(regs);
  error = reinterpret_cast<irq::ErrorCodePageFault*>(&regsi->err_code);

  log::err("IRQ") << "Page Fault at: 0x" << dsp::hex << asmf::regs::get_cr2() << dsp::dec;
  log::err("IRQ") << "\tPresent: " << (error->present? "Yes" : "No");
  log::err("IRQ") << "\tAccess mode: " << (error->wr ? "write" : "read");
  log::err("IRQ") << "\tCaused by: " << (error->us ? "user" : "system");

  /* FIXME: page fault reparation */
  log::fatal("IRQ") << "Page fault recovery not implemented";
  panic();
}

namespace irq
{

static RegsDump* __percpu _cur_regs;

void IRQManagerImpl<i386>::init()
{
#define IRQ_ASM_HANDLER(Name, Num, Instr, Dpl) \
  _fillIdt(Num, (size_t)Name, Dpl);
#include "irq_asm_handler.def"
#undef IRQ_ASM_HANDLER

  _idt_ptr.limit = (sizeof (struct Idt) * IRQ_VECTOR_SIZE) - 1;
  _idt_ptr.base = (u32)&_idt_table;
  asmf::irq::lidt(reinterpret_cast<void*>(&_idt_ptr));

  /* set default exceptions */
  setHandler(IRQ_VECTOR_DOUBLEFAULT, doubleFault);
  setHandler(IRQ_VECTOR_GPE, generalFault);
  setHandler(IRQ_VECTOR_PAGEFAULT, pageFault);

  log::info("IRQ") << "init";
}

void IRQManagerImpl<i386>::initGlobalController()
{
  // Need to init the pic before disable
  PicManager* pic = new PicManager();
  assert(pic);
  pic->init();
  global_controller = pic;

  if (smp::info && smp::info->getIOAPICCount())
  {
    pic->disable();
    smp::IOAPICManager* ioapicm = new smp::IOAPICManager();
    assert(ioapicm);

    ioapicm->setIOAPICnumber(smp::info->getIOAPICCount());
    smp::info->configureIOAPICs(ioapicm);
    ioapicm->init();
    smp::info->configureIOAPICsLines(ioapicm);

    delete pic;
    global_controller = ioapicm;
  }
}

void IRQManagerImpl<i386>::initAp()
{
  asmf::irq::lidt(reinterpret_cast<void*>(&_idt_ptr));
}

bool IRQManagerImpl<i386>::setHandler(u32 num, irq_handler h)
{
  if (num >= IRQ_VECTOR_SIZE)
    return false;
  _interrupt_table[num].handler = h;
  return true;
}

void IRQManagerImpl<i386>::maskInterrupt(u32 num)
{
  if (num >= IRQ_VECTOR_SIZE)
    return;
  _interrupt_table[num].mask(num);
}

void IRQManagerImpl<i386>::unmaskInterrupt(u32 num)
{
  if (num >= IRQ_VECTOR_SIZE)
    return;
  _interrupt_table[num].unmask(num);
}

bool IRQManagerImpl<i386>::setInterruptController(u32 num, InterruptController* c)
{
  if (num >= IRQ_VECTOR_SIZE)
    return false;
  else
  {
    _interrupt_table[num].controller = c;
    return true;
  }
}

InterruptController* IRQManagerImpl<i386>::getInterruptController(u32 num)
{
  if (num >= IRQ_VECTOR_SIZE)
    return 0;
  else
    return _interrupt_table[num].controller;
}

void IRQManagerImpl<i386>::_fillIdt(u32 idx, size_t offset, u8 dpl)
{
  if (idx >= IRQ_VECTOR_SIZE)
    return;

  _idt_table[idx].off_0_15 = offset & 0xffff;
  _idt_table[idx].seg_select = KERNEL_CODE_SEL;
  _idt_table[idx].constant1 = 0;
  _idt_table[idx].t = 0; // Interruption
  _idt_table[idx].constant2 = 0x3;
  _idt_table[idx].d = 1; // 32 bits
  _idt_table[idx].constant3 = 0;
  _idt_table[idx].dpl = dpl; // Privilege
  _idt_table[idx].present = 1;
  _idt_table[idx].off_16_31 = (offset >> 16) & 0xffff;
}

void IRQManagerImpl<i386>::_zeroIdt(u32 idx)
{
  if (idx > IRQ_VECTOR_SIZE)
    return;

  _idt_table[idx].off_0_15 = 0;
  _idt_table[idx].seg_select = 0;
  _idt_table[idx].constant1 = 0;
  _idt_table[idx].t = 0;
  _idt_table[idx].constant2 = 0;
  _idt_table[idx].d = 0;
  _idt_table[idx].constant3 = 0;
  _idt_table[idx].dpl = 0;
  _idt_table[idx].present = 0;
  _idt_table[idx].off_16_31 = 0;
}

RegsDump* IRQManagerImpl<i386>::getCurrentRegsDump()
{
  return _cur_regs;
}

void IRQManagerImpl<i386>::setCurrentRegsDump(RegsDump *regs)
{
  _cur_regs = regs;
}

}

extern "C" __asmcall void idtDispatcher(struct irq::RegsDumpI386* dump)
{
  u8 intr = dump->int_num;

  if (!_interrupt_table[intr].handle(dump))
    log::warn("IRQ") << "Unhandled Interruption : " << intr;
  _interrupt_table[intr].eoi(intr);
}
