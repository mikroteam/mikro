#include <arch/serial_manager_i.hh>
#include <options.hh>
#include <display_manager.hh>

namespace serial
{
  SerialManagerI386 manager_i386;
  SerialManager* manager = &manager_i386;

  SerialManagerI386::SerialManagerI386()
    : _port_driver_writer(SERIAL_COM_1)
  {
  }

#if 0 /* next */
  char SerialManagerI386::readChar()
  {
    char res;
    uint8_t status = 0;

    while(!(status & 0x1))
      INB(_port + 5, status);
    INB(_port, res);

    return res;
  }
#endif

  void SerialManagerI386::writeChar(char c)
  {
    u8 status = 0;

    while(!(status & 0x20 ))
      status = asmf::io::inb(_port_driver_writer + 5);
    asmf::io::outb(_port_driver_writer, c);
  }

  void SerialManagerI386::setDriverWriterCom(int com)
  {
      _port_driver_writer = _get_real_com(com);
  }

  void SerialManagerI386::init()
  {
    char  *output = config::options.getByName("console");
    u16   port;

    if (!output)
      return;

    if (!strncmp(output, "com", 3))
    {
      port = output[3] - '0';
      if (port < 5 && port > 0)
        init(port);
      dsp::dwriter = manager;
    }

  }

  void SerialManagerI386::init(int com)
  {
    uint16_t port = _get_real_com(com);

    asmf::io::outb(port + 1, 0x00);
    asmf::io::outb(port + 3, 0x80);
    asmf::io::outb(port + 0, SERIAL_BR57600);
    asmf::io::outb(port + 1, 0x00);
    asmf::io::outb(port + 3, SERIAL_8N1);
    asmf::io::outb(port + 2, SERIAL_FIFO_8);
    asmf::io::outb(port + 4, 0x08);
  }

  int SerialManagerI386::_get_real_com(int com)
  {
    switch (com)
    {
      case serial::COM_1:
        return SERIAL_COM_1;
      case serial::COM_2:
        return SERIAL_COM_2;
      case serial::COM_3:
        return SERIAL_COM_3;
      case serial::COM_4:
        return SERIAL_COM_4;
    }
    return SERIAL_COM_1;
  }

  void SerialManagerI386::read(uint32_t port, char* data, uint32_t size)
  {
    u8 status;

    port = _get_real_com(port);
    while (size)
    {
      status = asmf::io::inb(port + 5);

      if (status & 1)
      {
        *data++ = asmf::io::inb(port);
        size--;
      }
    }
  }

  void SerialManagerI386::write(uint32_t port, const char* data, uint32_t size)
  {
    u8 status;

    port = _get_real_com(port);
    while(size)
    {
      status = asmf::io::inb(port + 5);

      if (status & 0x20)
      {
        asmf::io::outb(port, *data++);
        size--;
      }
    }
  }

}
