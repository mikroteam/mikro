#include <arch/trampoline.hh>

#define RELOC_ADDR(x) (x - trampoline_start + TR_START_AP_ADDR)

.code16
.global trampoline_start
trampoline_start:

  /* Load the gdt */
  cli
  lgdt RELOC_ADDR(.gdtp)

  /* Init protected mode */
  mov %cr0, %eax
  or $1, %ax
  mov %eax, %cr0
  jmp 2f
2:

  mov $0x10, %ax
  mov %ax, %ds
  mov %ax, %fs
  mov %ax, %gs
  mov %ax, %es
  mov %ax, %ss

  ljmp $0x8, $(RELOC_ADDR(protected))

.align 16

.gdt:
 .byte 0, 0, 0, 0, 0, 0, 0, 0
gdt_cs:
 .byte 0xFF, 0xFF, 0x0, 0x0, 0x0, 0b10011011, 0b11011111, 0x0
gdt_ds:
 .byte 0xFF, 0xFF, 0x0, 0x0, 0x0, 0b10010011, 0b11011111, 0x0
gdt_end:

.gdtp:
 .word 3 * 8 - 1 /* Hard coded, won't change */
 .long RELOC_ADDR(.gdt)

.align 16

.code32
protected:

  /* init paging */
  movl RELOC_ADDR(trampolineCR3), %eax
  movl %eax, %cr3

  mov %cr0, %eax
  orl $0x80000000, %eax
  movl %eax, %cr0

  /* init stack */
  mov RELOC_ADDR(trampolineStack), %esp

  /* call C++ code */
  mov RELOC_ADDR(trampolineJmpAddr), %ebx
  call *%ebx

.global trampolineCR3
trampolineCR3:
  .long 0

.global trampolineStack
trampolineStack:
  .long 0

.global trampolineJmpAddr
trampolineJmpAddr:
  .long 0

.global trampoline_end
trampoline_end:
