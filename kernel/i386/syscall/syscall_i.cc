/*
 * File: sycall_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Syscall implementation on i386
 *
 */

#include <syscall.hh>
#include <arch/syscall_i.hh>
#include <irq_manager.hh>
#include <arch/irq_manager_i.hh>

namespace syscall
{

void syscallHandler(struct irq::RegsDump* regs)
{
  irq::RegsDumpI386* regsi = reinterpret_cast<struct irq::RegsDumpI386*>(regs);
  size_t syscall_nb = regsi->eax;
  size_t (*syscall)(...) = 0;

  if (syscall_nb >= SYSCALL_NUMBER)
  {
    regsi->eax = -1;
    return;
  }

  irq::manager.arch.setCurrentRegsDump(regsi);
  syscall = (size_t (*)(...))syscall::table[syscall_nb];

  if (!syscall)
  {
    regsi->eax = -1;
    return;
  }

  regsi->eax = syscall(regsi->ebx, regsi->ecx, regsi->edx, regsi->esi,
                       regsi->edi, regsi->ebp);

  irq::manager.arch.setCurrentRegsDump(0);
  return;
}

void SyscallManagerImpl<i386>::init()
{
  irq::manager.setHandler(SYSCALL_GATE, syscallHandler);
  syscall::manager.add(SYS_IN, (void*)sys_in);
  syscall::manager.add(SYS_OUT, (void*)sys_out);
  syscall::manager.add(SYS_VM86, (void*)sys_vm86);
}

size_t SyscallManagerImpl<i386>::strncpyFromUser(
    void *to, void *from, size_t size)
{
  char *cfrom = (char *) from;
  char *cto = (char *) to;
  size_t i, res = 0;

  if (!checkAddress((size_t) from))
    return 0;

  if (!checkAddress((size_t) from + size))
    return 0;

  for (i = 0; i < size; i++)
  {
    if (*cfrom == '\0')
    {
      res = i + 1;
      break;
    }

    *cto = *cfrom;

    cto++;
    cfrom++;
  }

  for (; i < size; i++)
    *cto = '\0';

  return res;
}

}
