#include <arch/task_manager_i.hh>
#include <cpu.hh>

namespace task
{

static tss* __percpu _tss;

void TaskManagerImpl<i386>::init()
{
  mem::MemoryManager *memx;
  memx = &mem::manager;

  /* Set tss */
  _tss = new tss;
  assert(_tss);
  memset(_tss, 0x0, sizeof (tss));

  _tss->ss0 = KERNEL_DATA_SEL;
  _tss->esp0 = (u32) mem::kernelAD.mmap(0, KERNEL_STACK_PAGE_SIZE,
      mem::READ | mem::WRITE);
  assert(_tss->esp0 != 0);
  _tss->esp0 += (KERNEL_STACK_PAGE_SIZE << 12);
  memx->arch.gdtm.setGate(TSS, (u32)_tss, sizeof (tss),
                          GDT_DPL_0 | 0x9,
                          GDT_GRANULARITY_BYTE);
  asmf::ltrw(TSS << 3);

  /* Set user code */
  memx->arch.gdtm.setGate(USER_CODE, 0x0, 0xFFFFFFFF,
                          GDT_TYPE_CODE | GDT_DPL_3 | GDT_NOT_SYSTEM,
                          GDT_GRANULARITY_PAGE);

  /* Set user data */
  memx->arch.gdtm.setGate(USER_DATA, 0x0, 0xFFFFFFFF,
                          GDT_TYPE_DATA_WRITE | GDT_DPL_3 | GDT_NOT_SYSTEM,
                          GDT_GRANULARITY_PAGE);

  /* Set v8086 tss field */
  _tss->io = (uint16_t)(((uint32_t)&_tss->io_bitmap - (uint32_t)_tss) & 0xFFFF);
  _tss->io_end = 0xFF;
  _tss->v8086_int_mask[9] = (1 << 5);

  log::info("Task") << "init";
}

__noreturn void TaskManagerImpl<i386>::idle()
{
  asmf::irq::sti();
  while (1)
    asmf::hlt();
}

}
