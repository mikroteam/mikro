/**
 * \file thread_i.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Thread implementation for i386
 */

#include "arch/asm.hh"
#include "arch/gdt_manager.hh"
#include "arch/thread_i.hh"
#include "cstring.hh"
#include "logger.hh"
#include "memory_manager.hh"

namespace task
{

ThreadImpl<i386>::ThreadImpl()
{
  /* Set general registers to 0 */
  regs.edi = 0;
  regs.esi = 0;
  regs.ebp = 0;

  regs.ebx = 0;
  regs.edx = 0;
  regs.ecx = 0;
  regs.eax = 0;

  /* Set selector */
  regs.cs = USER_CODE_SEL;
  regs.ds = USER_DATA_SEL;
  regs.fs = USER_DATA_SEL;
  regs.ss = USER_DATA_SEL;
  regs.es = USER_DATA_SEL;
  regs.gs = USER_DATA_SEL;

  /* Set eflags */
  regs.eflags = (1 << 1) | (1 << 9);
}

bool ThreadImpl<i386>::init(mem::AddressSpace *as, uint32_t entry)
{

  /* Set entry point */
  regs.eip = entry;

  /* Set stack */
  regs.esp_prev = (u32) as->mmap(0, 2, mem::READ | mem::WRITE);
  if (!regs.esp_prev)
    return false;

  regs.esp_prev += 4096 * 2;

  return true;
}

void ThreadImpl<i386>::save(irq::RegsDump *curDump)
{
  irq::RegsDumpI386 *cur_regs = (irq::RegsDumpI386 *) curDump;

  /* Save current registers from stack */
  memcpy(&regs, cur_regs, sizeof (irq::RegsDumpI386));
}

void ThreadImpl<i386>::restore(mem::AddressSpace* as,
    irq::RegsDump *curDump)
{
  irq::RegsDumpI386 *cur_regs = (irq::RegsDumpI386 *) curDump;

  /* Restore registers to stack */
  memcpy(cur_regs, &regs, sizeof (irq::RegsDumpI386));

  /* Set CR3 */
  asmf::regs::set_cr3((size_t) as->getPDT());
}

void ThreadImpl<i386>::gotoUser(mem::AddressSpace* as)
{
  /* Set CR3 */
  asmf::regs::set_cr3((size_t) as->getPDT());

  /*
   * Push needed registers on stack
   * Reset general purpose registers
   * Switch to user
   */
  __asm__ (
      "pushl 0x48(%%eax)      \n"
      "pushl 0x44(%%eax)      \n"
      "pushl 0x40(%%eax)      \n"
      "pushl 0x3C(%%eax)      \n"
      "pushl 0x38(%%eax)      \n"
      "movl 0x10(%%eax), %%gs \n"
      "movl 0xC(%%eax), %%fs  \n"
      "movl 0x4(%%eax), %%es  \n"
      "movl (%%eax), %%ds     \n"
      "movl $0x0, %%ecx       \n"
      "movl $0x0, %%edx       \n"
      "movl $0x0, %%ebx       \n"
      "movl $0x0, %%ebp       \n"
      "movl $0x0, %%esi       \n"
      "movl $0x0, %%edi       \n"
      "movl $0x0, %%eax       \n"
      "iret                   \n"
      :
      : "a" (&regs)
      );
  __builtin_unreachable();
}

void ThreadImpl<i386>::gotoTask(mem::AddressSpace* as, irq::RegsDump *curDump)
{
  /* Set CR3 */
  asmf::regs::set_cr3((size_t) as->getPDT());

  /*
   * Set ESP to curDump
   * Restore general purpose registers
   * Switch to user
   */
  __asm__ (
      "mov %0, %%esp  \n"
      "pop %%ds       \n"
      "pop %%es       \n"
      "pop %%fs       \n"
      "pop %%gs       \n"
      "popal          \n"
      "add $8, %%esp  \n"
      "iret           \n"
      :
      : "r" (curDump)
      );
  __builtin_unreachable();
}

}
