/**
 * \file action.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC actions
 */

#include "ipc/action.hh"
#include "scheduler.hh"
#include "thread.hh"

namespace ipc
{

int Action::doCopyFromSender(Message &msg, task::Thread *rcver)
{
  void *vaddr = mem::kernelAD.mmapIPC(rcver->getAS(), _data, false,
      msg.payload());

  if (!vaddr)
    return -EFAULT;

  msg.write(vaddr, msg.payload());
  mem::kernelAD.munmapIPC();

  return 0;
}

int Action::doCopyFromRcver(Message *msg)
{
  void *vaddr = mem::kernelAD.mmapIPC(msg->sender()->getAS(), msg->payload(),
      true, _data);

  if (!vaddr)
    return -EFAULT;

  msg->write(_data, vaddr);
  mem::kernelAD.munmapIPC();

  return 0;
}

}
