/**
 * \file chandle.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Channel handle for IPC
 */

#include "ipc/chandle.hh"
#include "scheduler.hh"

namespace ipc
{

Message *Chandle::getMessage()
{
  Message *res;

  _lock.lock();
  if (_msgQueue.empty())
  {
    scheduler::scheduler.remove();
    asmf::Atomic<ARCH_GENERIC>::setBit(&_flags, RECEIVING_BIT);
    _lock.unlock();
    return 0;
  }

  res = _msgQueue.pop_front();
  _lock.unlock();

  return res;
}

int Chandle::putMessage(Message &msg)
{
  if (asmf::Atomic<ARCH_GENERIC>::testAndClearBit(&_flags, RECEIVING_BIT))
    return 1;

  _lock.lock();

  if (asmf::Atomic<ARCH_GENERIC>::testAndClearBit(&_flags, RECEIVING_BIT))
  {
    _lock.unlock();
    return 1;
  }

  Message *msg_store = new Message(msg);
  if (!msg_store)
    return -ENOMEM;

  if (!_msgQueue.push_back(msg_store))
  {
    _lock.unlock();
    delete msg_store;
    return -ENOMEM;
  }

  scheduler::scheduler.remove();
  _lock.unlock();
  return 0;
}

}
