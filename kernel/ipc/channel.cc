/**
 * \file channel.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC channel management
 */

#include "ipc/channel.hh"
#include "thread.hh"

namespace ipc
{

int Channel::addConnection(Connection &connection)
{
  _conLock.lock();
  Pair<Map<pid_t, Connection>::Iterator, bool> insert_res =
    _connections.insert(make_pair(connection.thread()->getPid(), connection));

  if (insert_res.first == _connections.end())
  {
    _conLock.unlock();
    return -ENOMEM;
  }

  _conLock.unlock();
  return !insert_res.second;
}

int Channel::removeConnection(pid_t pid)
{
  _conLock.lock();
  Map<pid_t, Connection>::Iterator el = _connections.find(pid);

  if (el == _connections.end())
  {
    _conLock.unlock();
    return -ECHAN;
  }

  _connections.erase(el);
  _conLock.unlock();

  return 0;
}

}
