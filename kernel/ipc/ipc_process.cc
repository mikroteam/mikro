/**
 * \file ipc_process.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Process Manager
 */

#include "cpu.hh"
#include "ipc/ipc_process.hh"

namespace ipc
{

bool IPCProcessManager::init(size_t nrMhandles)
{
  if (_mhandles || !nrMhandles)
    return false;

  _mhandles = new Mhandle[nrMhandles];

  if (!_mhandles)
    return false;

  _mhandleSize = nrMhandles;
  return true;
}

IpcData *IPCProcessManager::getPrivate(pid_t pid_server)
{
  IpcData *res;
  Map<pid_t, IpcData>::Iterator it_data;

  _dataLock.lock();
  it_data = _privateData.find(pid_server);

  if (it_data == _privateData.end())
  {
    pid_t pid = cpu::myself->current_thread->getPid();
    Pair<Map<pid_t, IpcData>::Iterator, bool> res_insert =
      _privateData.insert(make_pair(pid_server, IpcData(pid)));

    if (res_insert.first == _privateData.end())
    {
      _dataLock.unlock();
      return 0;
    }

    res = &((*res_insert.first).second);
  }
  else
    res = &((*it_data).second);

  res->up();
  _dataLock.unlock();
  return res;
}

void IPCProcessManager::clearPrivate(pid_t pid_server)
{
  Map<pid_t, IpcData>::Iterator it_data;

  _dataLock.lock();
  it_data = _privateData.find(pid_server);

  assert(it_data != _privateData.end());
  _privateData.erase(it_data);

  _dataLock.unlock();
}

int IPCProcessManager::getFreeMhandle()
{
  int id = -1;

  _mhandleLock.lock();

  for (int i = 0; i < _mhandleSize; i++)
    if (!_mhandles[i].isUsed())
    {
      id = i;
      break;
    }

  if (id == -1)
  {
    _mhandleLock.unlock();
    return -1;
  }

  _mhandles[id].use();
  _mhandleLock.unlock();

  return id;
}

void IPCProcessManager::setMhandleFree(int mid)
{
  assert(mid < _mhandleSize);

  _mhandleLock.lock();
  _mhandles[mid].unuse();
  _mhandleLock.unlock();
}

bool IPCProcessManager::replyMhandle(int mid, Connection &conn)
{
  if (mid >= _mhandleSize)
    return false;

  _mhandleLock.lock();
  bool res = _mhandles[mid].reply(conn);
  _mhandleLock.unlock();

  return res;
}

bool IPCProcessManager::getMhandle(int mid, Connection &conn)
{
  if (mid >= _mhandleSize)
    return false;

  _mhandleLock.lock();
  bool res = _mhandles[mid].getConn(conn);
  _mhandleLock.unlock();

  return res;
}

}
