/**
 * \file ipc_thread.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Thread Manager
 */

#include "cpu.hh"
#include "ipc/ipc_thread.hh"
#include "scheduler.hh"
#include "thread.hh"

#define DEFAULT_NR_MHANDLES 12

namespace ipc
{

Map<char *, Channel*, LessString> IPCThreadManager::_channels;
Spinlock IPCThreadManager::_channelLock;

int IPCThreadManager::createChannel(char *name, Channel::Type type)
{
  int errCode;
  size_t cid;
  Channel *channel = new Channel(name, type);
  Chandle *chandle;

  if (!channel)
    return -ENOMEM;

  _channelLock.lock();
  Pair<Map<char *, Channel*>::Iterator, bool> res_insert =
    _channels.insert(make_pair(name, channel));

  if (res_insert.first == _channels.end())
  {
    errCode = -ENOMEM;
    goto error;
  }

  if (res_insert.second)
  {
    errCode = -ECHAN;
    goto error;
  }

  chandle = getFreeChandle(cid);

  if (!chandle)
  {
    errCode = -ECHAND;
    goto error;
  }

  channel->init(Connection(chandle, cpu::myself->current_thread));

  if (type == Channel::ONE_TO_ONE)
    chandle->online(Chandle::ONE_TO_ONE, channel, 0);
  else
    chandle->online(Chandle::ONE_TO_N_SERVER, channel, 0);

  _channelLock.unlock();

  return cid;

error:
  _channels.erase(res_insert.first);
  _channelLock.unlock();
  delete channel;

  return errCode;
}

int IPCThreadManager::openChannel(char *name)
{
  int res;
  size_t cid;
  IpcData *data;

  _channelLock.lock();
  Map<char *, Channel*>::Iterator it_chan = _channels.find(name);

  if (it_chan == _channels.end())
  {
    _channelLock.unlock();
    return -ECHAN;
  }

  Channel *channel = (*it_chan).second;
  Chandle *chandle = getFreeChandle(cid);

  if (!chandle)
  {
    _channelLock.unlock();
    return -ECHAND;
  }

  Connection myConn(chandle, cpu::myself->current_thread);

  if (channel->getType() != Channel::ONE_TO_ONE)
  {
    task::Process *serverProcess = channel->getOwner().thread()->getProcess();
    task::Process *myProcess = cpu::myself->current_thread->getProcess();
    data = myProcess->ipcManager.getPrivate(serverProcess->getPid());

    if (!data)
    {
      _channelLock.unlock();
      return -ENOMEM;
    }
  }

  res = channel->addConnection(myConn);

  if (res < 0)
  {
    _channelLock.unlock();
    return res;
  }

  chandle->setConnection(channel->getOwner());
  if (channel->getType() == Channel::ONE_TO_ONE)
  {
    chandle->online(Chandle::ONE_TO_ONE, channel, 0);
    channel->getOwner().chandle()->setConnection(myConn);
  }
  else
    chandle->online(Chandle::ONE_TO_N_CLIENT, channel, data);

  _channelLock.unlock();

  return cid;
}

int IPCThreadManager::closeChannel(size_t cid)
{
  Chandle *chandle = getChandle(cid);
  if (!chandle)
    return -ECHAND;

  if (!chandle->isOnline())
    return -ECHAND;

  Channel *channel = chandle->channel();
  chandle->offline();

  _channelLock.lock();

  Map<char *, Channel*>::Iterator it_chan = _channels.find(channel->getName());
  assert(it_chan != _channels.end());
  delete (*it_chan).second;
  _channels.erase(it_chan);

  _channelLock.unlock();

  return 0;
}

int IPCThreadManager::send(size_t cid, Message &msg)
{
  Chandle *chandle = getChandle(cid);

  if (!chandle || !chandle->isOnline() ||
      chandle->mode() == Chandle::ONE_TO_N_SERVER)
    return -ECHAND;

  msg.setChandle(chandle);

  Connection conn;
  if (!chandle->getConnection(conn))
    return -ECHAN;

  return doSend(msg, conn);
}

int IPCThreadManager::send(size_t cid, pid_t pid, Message &msg)
{
  Chandle *chandle = getChandle(cid);

  if (!chandle || !chandle->isOnline() ||
      chandle->mode() != Chandle::ONE_TO_N_SERVER)
    return -ECHAND;

  msg.setChandle(chandle);

  Channel *channel = chandle->channel();
  Connection conn;

  if (!channel->getConnection(pid, conn))
    return -ECHAN;

  return doSend(msg, conn);
}

int IPCThreadManager::send(size_t cid, Message &msg, size_t mid)
{
  Chandle *chandle = getChandle(cid);

  if (!chandle || !chandle->isOnline() ||
      chandle->mode() != Chandle::ONE_TO_N_SERVER)
    return -ECHAND;

  msg.setChandle(chandle);

  Connection conn;
  task::Process *process = cpu::myself->current_thread->getProcess();
  if (!process->ipcManager.replyMhandle(mid, conn))
    return -EMHAND;

  return doSend(msg, conn);
}

int IPCThreadManager::receive(size_t cid, void *recvData, size_t recvSize)
{
  Chandle *chandle = getChandle(cid);

  if (!chandle || !chandle->isOnline())
    return -ECHAND;

  return doReceive(chandle, recvData, recvSize);
}

int IPCThreadManager::srcv(size_t cid, Message &msg, void *recvData,
    size_t recvSize)
{
  Chandle *chandle = getChandle(cid);

  if (!chandle || !chandle->isOnline() ||
      chandle->mode() == Chandle::ONE_TO_N_SERVER)
    return -ECHAND;

  Connection conn;
  if (!chandle->getConnection(conn))
    return -ECHAN;

  switch (conn.chandle()->putMessage(msg))
  {
    case 1:
      break;

    case 0:
      return doReceive(chandle, recvData, recvSize);

    case -ENOMEM:
      return -ENOMEM;

    default:
      panic();
  }

  Action &action = conn.thread()->ipcManager._action;
  int err = action.doCopyFromSender(msg, conn.thread());

  switch (err)
  {
    case 0:
      _action.setReceiving(recvData, recvSize);
      scheduler::scheduler.remove();
      chandle->setReceiving();
      conn.thread()->setReturn(msg.payloadSize());
      scheduler::scheduler.directSchedule(conn.thread());

    case -EMSIZE:
    case -EFAULT:
      break;

    default:
      panic();
  }

  conn.thread()->setReturn(err);
  scheduler::scheduler.add(conn.thread());
  return err;
}

int IPCThreadManager::setPrivate(size_t cid, size_t mid, size_t data)
{
  Chandle *chandle = getChandle(cid);

  if (!chandle || !chandle->isOnline() ||
      chandle->mode() != Chandle::ONE_TO_N_SERVER)
    return -ECHAND;

  Connection conn;
  task::Process *process = cpu::myself->current_thread->getProcess();
  if (!process->ipcManager.getMhandle(mid, conn))
    return -EMHAND;

  IpcData *ipcData = conn.chandle()->getData();
  assert(ipcData);
  ipcData->setData(data, false);

  return 0;
}

int IPCThreadManager::mhandleSetup(task::Process *process, Chandle *chandle,
    int &mid)
{
  if (chandle->mode() == Chandle::ONE_TO_N_SERVER)
  {
    mid = process->ipcManager.getFreeMhandle();
    if (mid < 0)
      return -EMHAND;
  }

  return 0;
}

void IPCThreadManager::mhandleAccept(task::Process *process, Message *msg,
    int mid)
{
  if (mid >= 0)
  {
    Connection conn = msg->connection();
    process->ipcManager.setMhandleConn(mid, conn);
    msg->setClient(mid);
  }
}

void IPCThreadManager::mhandleRelease(task::Process *process, int mid)
{
  if (mid >= 0)
    process->ipcManager.setMhandleFree(mid);
}

int IPCThreadManager::doSend(Message &msg, Connection &conn)
{
  int mid = -1;
  int res =
    mhandleSetup(conn.thread()->getProcess(), conn.chandle(), mid);
  if (res < 0)
    return res;

  switch (conn.chandle()->putMessage(msg))
  {
    case 1:
      break;

    case 0:
      mhandleRelease(conn.thread()->getProcess(), mid);
      scheduler::scheduler.schedule();

    case -ENOMEM:
      return -ENOMEM;

    default:
      panic();
  }

  Action &action = conn.thread()->ipcManager._action;
  mhandleAccept(conn.thread()->getProcess(), &msg, mid);
  int err = action.doCopyFromSender(msg, conn.thread());

  switch (err)
  {
    case 0:
      conn.thread()->setReturn(msg.payloadSize());
      scheduler::scheduler.directSchedule(conn.thread());

    case -EMSIZE:
    case -EFAULT:
      break;

    default:
      panic();
  }

  mhandleRelease(conn.thread()->getProcess(), mid);
  conn.thread()->setReturn(err);
  scheduler::scheduler.add(conn.thread());
  return err;
}

int IPCThreadManager::doReceive(Chandle *chandle, void *recvData,
    size_t recvSize)
{
  task::Process *process = cpu::myself->current_thread->getProcess();
  int mid = -1;
  int res = mhandleSetup(process, chandle, mid);
  if (res < 0)
    return res;

  _action.setReceiving(recvData, recvSize);

  Message *msg = chandle->getMessage();

  if (!msg)
  {
    mhandleRelease(process, mid);
    scheduler::scheduler.schedule();
  }
  else
  {
    mhandleAccept(process, msg, mid);
    int err = _action.doCopyFromRcver(msg);

    switch (err)
    {
      case 0:
        msg->sender()->setReturn(0);
        err = msg->payloadSize();
        break;

      case -EMSIZE:
      case -EFAULT:
        mhandleRelease(process, mid);
        msg->sender()->setReturn(err);
        break;

      default:
        panic();
    }

    scheduler::scheduler.add(msg->sender());
    delete msg;

    return err;
  }
}

}
