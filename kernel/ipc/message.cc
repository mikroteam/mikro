/**
 * \file message.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief IPC Message abstraction
 */

#include "cstring.hh"
#include "ipc/chandle.hh"
#include "ipc/message.hh"

#define MSG_PID_BIT (1 << (sizeof(size_t) * 8 - 1))
#define MSG_DEL_BIT (1 << (sizeof(size_t) * 8 - 2))

namespace ipc
{

void Message::setClient(size_t mhandleId, bool del)
{
  if (del)
    _mhandleId = mhandleId | MSG_DEL_BIT;
  else
    _mhandleId = mhandleId;

  int pad = _payloadSize % sizeof(size_t);

  if (pad)
    _msgSize += 2 * sizeof(size_t) + sizeof(size_t) - pad;
  else
    _msgSize += 2 * sizeof(size_t);
}

void Message::write(void *to, void *payload)
{
  /* Write Payload */
  memcpy(to, payload, _payloadSize);

  /* Write client meta data if needed */
  if (_mhandleId >= 0)
  {
    assert(_chandle);
    size_t *userData = (size_t *) ((char *) to + _msgSize);
    userData -= 2;

    /* Write Mhandle id + flags */
    if (_chandle->getData()->isPID())
      *userData = _mhandleId | MSG_PID_BIT;
    else
      *userData = _mhandleId;
    userData++;

    /* Write private data */
    *userData = _chandle->getData()->getData();
  }
}

}
