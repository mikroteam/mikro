/*
 * File: interrupt_controller.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Global interrupt controller
 *
 */

#include <interrupt_controller.hh>

namespace irq
{

InterruptController* global_controller = 0;

}
