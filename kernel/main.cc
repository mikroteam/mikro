/*
 * File: main.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: First C++ code executed
 *
 */

#include <compat_checker.hh>
#include <compiler_tools.hh>
#include <errors.hh>
#include <init.hh>
#include <init_daemon.hh>
#include <logger.hh>
#include <multiboot.h>
#include <multiboot_info.hh>
#include <options.hh>
#include <scheduler.hh>
#include <smp_manager.hh>
#include <task_manager.hh>
#include <test_manager.hh>

char *k_cmd;

extern "C" __asmcall void k_main(unsigned long magic,
    multiboot_info_t* info)
{
  if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
    earlyAbort("Incorrect Multiboot bootloader magic");
  compat_checker.check();

  // Console init - Use screen by default
  dsp::kout.init();

  log::info("Mikro") << "Commande line: " << (info->cmdline ? (char*)info->cmdline : "");
  log::info("Mikro") << "Kernel is booting...";

  initBsp(info);

  log::info("Mikro") << "Launch daemons...";

  u32 daemonID = 1;
  task::Process *p;
  for (auto &daemon : task::initDaemons)
  {
    if (task::manager.createProcess(&p) < 0 ||
        task::manager.runProcess(p, (size_t) daemon.load(p->getAS())) < 0)
    {
      log::fatal("Daemon") << "Cannot init daemon task #" << daemonID;
      panic();
    }

    daemonID++;
  }

  task::initDaemons.clear();

  test::TestManager().run();

  scheduler::scheduler.init();
}
