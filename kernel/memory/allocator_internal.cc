/**
 * \file allocator_internal.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief internal functions needed to allocate memory
 */

#include "allocator.hh"
#include "errors.hh"
#include "memory/allocator_internal.hh"
#include "paging.hh"

#define FREE_BIT 0x40
#define MULTIPLE_PAGE_BIT 0x80

KernelAllocator kAllocator;

u32 bucketSize[] =
{
  32 - 1,
  64 - 1,
  128 - 1,
  256 - 1,
  512 - 1,
  1024 - 1,
  2048 - 1
};

void KernelAllocator::init()
{
  for (u32 i = 0; i < KALLOC_MAXBUCKET; i++)
    _buckets[i] = 0;
}

void *KernelAllocator::PageHandlerStart::getPage()
{
  if (_curPage > _paddrEnd)
    return 0;

  size_t addr = _curPage;
  _curPage += 0x1000;
  _nr_used++;

  return (void *) addr;
}

void *KernelAllocator::PageHandlerKernel::getPage()
{
  return mem::pi.kReserve.get();
}

void KernelAllocator::PageHandlerKernel::leavePage(void *vaddr)
{
  mem::pi.kReserve.set(vaddr);
}

// Alloc return address is always aligned on 32 bits
void *KernelAllocator::allocate(size_t size, u16 flags)
{
  const size_t maxAlloc = ((1 << (PALLOC_MAXBUCKET - 1)) << 12) - 1;
  assert(size <= maxAlloc);

  void *res;

  if (size > 2048 - 1)
  {
    /* >= 1 page => directly allocate pages */
    res = mem::kernelAD.mmap(0, mem::tools::length2nrPages(size + 1),
        mem::READ | mem::WRITE);
    if (res)
    {
      MemData *newData = (MemData *) res;
      newData->flags = MULTIPLE_PAGE_BIT;

      res = (void *) &newData->nextData;
    }
  }
  else
  {
    /* < 1 page => try to find some room */
    u8 order = 0;

    for (int i = KALLOC_MAXBUCKET - 2; i >= 0; i--)
      if (size > bucketSize[i])
      {
        order = i + 1;
        break;
      }

    res = bucketAlloc(order, flags);
  }

  return res;
}

void KernelAllocator::deallocate(void *p)
{
  if (!p)
    return;

  MemData *curData =
    (MemData *) ((char *) p - (sizeof (MemData) - 2 * sizeof (void *)));

  if (curData->flags & FREE_BIT)
    return;

  if (curData->flags & MULTIPLE_PAGE_BIT)
    /* >= 1 page => directly deallocate pages */
    mem::kernelAD.munmap((void *) curData);
  else
    /* < 1 page => set free */
    bucketDealloc(curData);
}

void *KernelAllocator::bucketAlloc(u8 order, u16 flags)
{
  void *res;

  if (!(flags & CRITICAL) && _pageHandler->mustCheck)
    mem::pi.kReserve.check();

  _lock.lock();
  if (_buckets[order])
  {
    res = (void *) &_buckets[order]->nextData;
    _buckets[order]->flags &= ~FREE_BIT;

    _buckets[order] = _buckets[order]->nextData;

    if (_buckets[order])
      _buckets[order]->prev = 0;
  }
  else
  {
    int curOrder = order + 1;
    MemData *cur;

    while (curOrder < KALLOC_MAXBUCKET && !_buckets[curOrder])
      curOrder++;

    if (curOrder == KALLOC_MAXBUCKET)
    {
      cur = (MemData *) _pageHandler->getPage();
      MemData *split =
        (MemData *) ((char *) cur + bucketSize[KALLOC_MAXBUCKET - 1] + 1);

      split->flags = (KALLOC_MAXBUCKET - 1) & FREE_BIT;
      split->nextData = 0;
      split->prev = 0;

      _buckets[KALLOC_MAXBUCKET - 1] = split;
      curOrder--;
    }
    else
    {
      cur = _buckets[curOrder];
      _buckets[curOrder] = _buckets[curOrder]->nextData;
      if (_buckets[curOrder])
        _buckets[curOrder]->prev = 0;

      MemData *split = (MemData *) ((char *) cur + bucketSize[--curOrder] + 1);

      split->flags = curOrder & FREE_BIT;
      split->nextData = 0;
      split->prev = 0;

      _buckets[curOrder] = split;
    }
    curOrder--;

    while (curOrder >= order)
    {
      MemData *split = (MemData *) ((char *) cur + bucketSize[curOrder] + 1);

      split->flags = curOrder & FREE_BIT;
      split->nextData = 0;
      split->prev = 0;

      _buckets[curOrder] = split;
      curOrder--;
    }

    res = (void *) &cur->nextData;
    cur->flags = order;
  }

  _lock.unlock();

  return res;
}

void KernelAllocator::bucketDealloc(MemData *cur)
{
  _lock.lock();
  while (true)
  {
    if (cur->flags == KALLOC_MAXBUCKET)
    {
      _pageHandler->leavePage((void *) cur);
      _lock.unlock();
      return;
    }

    MemData *buddy = (MemData *) ((size_t) cur ^ bucketSize[cur->flags]);
    if ((buddy->flags & FREE_BIT) && (buddy->flags & ~FREE_BIT) == cur->flags)
    {
      if ((size_t) buddy < (size_t) cur)
      {
        if (buddy->prev)
          buddy->prev->nextData = cur->nextData;
        else
          _buckets[cur->flags] = cur->nextData;

        if (cur->nextData)
          cur->nextData->prev = buddy->prev;

        buddy->flags &= ~FREE_BIT;
        cur = buddy;
      }
      else
      {
        if (cur->prev)
          cur->prev->nextData = buddy->nextData;
        else
          _buckets[cur->flags] = buddy->nextData;

        if (buddy->nextData)
          buddy->nextData->prev = cur->prev;
      }

      cur->flags++;
    }
    else
      break;
  }

  cur->nextData = _buckets[cur->flags];
  cur->prev = 0;

  _buckets[cur->flags] = cur;
  cur->flags |= FREE_BIT;

  _lock.unlock();
}

void *operator new(size_t size, u16 flags)
{
  return kAllocator.allocate(size, flags);
}

void *operator new(size_t size)
{
  return kAllocator.allocate(size, STANDARD);
}

void *operator new[](size_t size, u16 flags)
{
  return kAllocator.allocate(size, flags);
}

void *operator new[](size_t size)
{
  return kAllocator.allocate(size, STANDARD);
}

void operator delete(void *p)
{
  kAllocator.deallocate(p);
}

void operator delete[](void *p)
{
  kAllocator.deallocate(p);
}
