/**
 * \file kernel_as_internal.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Kernel address space internal
 */

#include "allocator.hh"
#include "cpu.hh"
#include "memory/kernel_as_internal.hh"
#include "memory/paging_internal.hh"
#include "paging.hh"

namespace mem
{

bool KernelASInternal::kmap(void **paddr, void **vaddr, int flags)
{
  PMemArea *pma;

  if (!(flags & KMAP_NOLOCK))
    lock.lock();

  *vaddr = kernelVMAS.findRoom(1, true);
  if (!*vaddr)
    goto error;

  pma = pi.physAllocator.allocateOrder(0, STANDARD);
  if (!pma)
    goto error;
  *paddr = pma->begin();

  if (!(flags & KMAP_TEMPORARY))
  {
    VMemArea *vma = new(CRITICAL) VMemArea(*vaddr, 1);
    if (!vma)
    {
      pi.physAllocator.deallocateOrder(pma);
      goto error;
    }

    assert(vma->pmas.push_front(pma));

    Pair<Set<VMemArea *>::Iterator, bool> ins_res = kernelVMAS.insert(vma);

    assert(ins_res.first != kernelVMAS.end());
  }

  pi.impl.pageTableKernel.add(*paddr, *vaddr, 1, READ | WRITE);

  if (!(flags & KMAP_NOLOCK))
    lock.unlock();

  return true;

error:
  if (!(flags & KMAP_NOLOCK))
    lock.unlock();
  return false;
}

void KernelASInternal::kunmap(void *vaddr)
{
  if (!vaddr)
    return;

  VMemArea findVMA(vaddr, 0x1000);

  lock.lock();
  Set<VMemArea *>::Iterator it = kernelVMAS.find(&findVMA);

  assert(it != kernelVMAS.end());
  assert(!(*it)->getFlag(VMemArea::ZONE_RESERVED));
  assert(!(*it)->pmas.empty());
  assert((*(*it)->pmas.begin())->nrPages() == 1);

  pi.impl.pageTableKernel.free(vaddr, 1);

  PMemArea *pma = *(*it)->pmas.begin();
  kernelVMAS.erase(it);

  pi.physAllocator.deallocateOrder(pma);
  lock.unlock();
}

void *KernelASInternal::temporaryMap(void *paddr)
{
  void *vaddr = kernelVMAS.findRoom(1, true);
  if (!vaddr)
    return 0;

  pi.impl.pageTableKernel.add(paddr, vaddr, 1, READ | WRITE);
  return vaddr;
}

void KernelASInternal::temporaryUnmap(void *vaddr)
{
  pi.impl.pageTableKernel.free(vaddr, 1);
}

void KernelASInternal::init()
{
  _ipcVaddr = pi.kASInternal.kernelVMAS.findRoom(cpu::number, true);
  assert(_ipcVaddr);

  VMemArea *vma = new(CRITICAL) VMemArea(_ipcVaddr, cpu::number);
  Pair<Set<VMemArea *>::Iterator, bool> ins_res =
    pi.kASInternal.kernelVMAS.insert(vma);
  assert(ins_res.first != pi.kASInternal.kernelVMAS.end());

  _ipcMaps = new(CRITICAL) IPCMap[cpu::number];
  assert(_ipcMaps);
}

void *KernelASInternal::mapIPC(PMemArea *pma, size_t page_offset, bool sender,
    void *pfAddr)
{
  void *vaddr = (void *) ((size_t) _ipcVaddr + (cpu::myself->id << 12));
  void *paddr = (void *) ((size_t) pma->begin() + (page_offset << 12));

  _ipcMaps[cpu::myself->id].pma = pma;
  _ipcMaps[cpu::myself->id].pfAddr = pfAddr;

  if (sender)
    pi.impl.pageTableKernel.add(paddr, vaddr, 1, mem::READ);
  else
    pi.impl.pageTableKernel.add(paddr, vaddr, 1, mem::READ | mem::WRITE);

  return vaddr;
}

void KernelASInternal::unmapIPC()
{
  void *vaddr = (void *) ((size_t) _ipcVaddr + (cpu::myself->id << 12));
  PMemArea *pma = _ipcMaps[cpu::myself->id].pma;
  _ipcMaps[cpu::myself->id].pfAddr = 0;

  pi.impl.pageTableKernel.free(vaddr, 1);
  pi.physAllocator.deallocateOrder(pma);
}

int KernelASInternal::checkPF(void *vaddr)
{
  size_t pfAddr = (size_t) vaddr;
  size_t pfAddr_b = (size_t) _ipcMaps[cpu::myself->id].pfAddr;

  return (pfAddr >= pfAddr_b && pfAddr < pfAddr_b + 0x1000);
}

}
