/**
 * \file kernel_reserve.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Kernel reserve
 */

#include "allocator.hh"
#include "arch/asm.hh"
#include "memory/kernel_reserve.hh"
#include "paging.hh"

namespace mem
{

void KernelReserve::init(void *begin_vaddr, size_t nrPages)
{
  void *vaddr = begin_vaddr;
  ReservedPage *page;

  for (u32 i = 0; i < nrPages; i++)
  {
    page = (ReservedPage *) vaddr;
    page->next = _reserve;
    _reserve = page;

    _nrReserved++;
    vaddr = (void *) ((size_t) vaddr + 0x1000);
  }
}

void KernelReserve::check()
{
  ReservedPage *reserved;
  size_t curNrReserved = _nrReserved;

  if (curNrReserved < KRESERVE_MIN)
  {
    void *vaddr;
    void *paddr;

    for (size_t i = 0; i <  KRESERVE_START - curNrReserved; i++)
    {
      if (!pi.kASInternal.kmap(&paddr, &vaddr))
      {
        // TODO: handle low memory case instead of panicking
        log::fatal("Paging") << "Kernel run out of memory";
        panic();
      }

      reserved = (ReservedPage *) vaddr;

      _lock.lock();
      reserved->next = _reserve;
      _reserve = reserved;
      _lock.unlock();

      asmf::Atomic<ARCH_GENERIC>::inc(&_nrReserved);
    }
  }

  if (curNrReserved > KRESERVE_MAX)
  {
    for (size_t i = 0; i < curNrReserved - KRESERVE_START; i++)
    {
      _lock.lock();
      reserved = _reserve;
      _reserve = reserved->next;
      _lock.unlock();

      asmf::Atomic<ARCH_GENERIC>::dec(&_nrReserved);

      pi.kASInternal.kunmap((void *) reserved);
    }
  }
}

void *KernelReserve::get()
{
  if (!_nrReserved)
  {
    // TODO: handle low memory case instead of panicking
    log::fatal("Paging") << "Kernel run out of memory";
    panic();
  }

  ReservedPage *reserved;

  _lock.lock();
  reserved = _reserve;
  _reserve = reserved->next;
  _lock.unlock();

  asmf::Atomic<ARCH_GENERIC>::dec(&_nrReserved);

  return (void *) reserved;
}

void KernelReserve::set(void *vaddr)
{
  ReservedPage *reserved = (ReservedPage *) vaddr;

  _lock.lock();
  reserved->next = _reserve;
  _reserve = reserved;
  _lock.unlock();

  asmf::Atomic<ARCH_GENERIC>::inc(&_nrReserved);
}

}
