/**
 * \file paging.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Generic Paging utilities
 */

#include "memory/paging_tools.hh"
#include "paging.hh"

namespace mem
{

KernelAddressSpace kernelAD;

/*
 * USER ADDRESS SPACE
 * ==================
 */

AddressSpace::~AddressSpace()
{
  pi.impl.pageTableUser.clear(_vaddr_pdt);
}

bool AddressSpace::init()
{
  if (!pi.impl.pageTableUser.create(&_paddr_pdt, &_vaddr_pdt))
    return false;

  pi.impl.pageTableUser.attachKernel(_vaddr_pdt);
  return true;
}

void *AddressSpace::mmap(void *vaddr, const size_t nrPages, u16 flags)
{
  if (!nrPages)
    return 0;

  pi.kReserve.check();

  _lock.lock();
  VMemArea *vma = mmapHandleVirtual(vaddr, nrPages, 0);
  if (!vma)
  {
    _lock.unlock();
    return 0;
  }

  if (!pi.physAllocator.allocate(nrPages, flags, vma->pmas))
  {
    delete vma;
    _lock.unlock();
    return 0;
  }

  Pair<Set<VMemArea *>::Iterator, bool> ins_res = _vmas.insert(vma);

  if (ins_res.first == _vmas.end())
  {
    pi.physAllocator.deallocate(vma->pmas);
    delete vma;
    _lock.unlock();
    return 0;
  }

  assert(mmapHandlePhysical(vma, flags));
  _lock.unlock();

  return vma->begin();
}

void *AddressSpace::mmap(void *vaddr, void *paddr, const size_t nrPages,
    u16 flags)
{
  if (!nrPages)
    return 0;

  pi.kReserve.check();
  paddr = tools::alignOnPage(paddr);

  _lock.lock();
  VMemArea *vma = mmapHandleVirtual(vaddr, nrPages, VMemArea::ZONE_RESERVED);
  if (!vma)
  {
    _lock.unlock();
    return 0;
  }

  // TODO: add kernel physical allocator mechanism

  Pair<Set<VMemArea *>::Iterator, bool> ins_res = _vmas.insert(vma);

  if (ins_res.first == _vmas.end())
  {
    delete vma;
    _lock.unlock();
    return 0;
  }

  assert(pi.impl.pageTableUser.add(_vaddr_pdt, paddr, vma->begin(), nrPages,
        flags));

  _lock.unlock();

  return vma->begin();
}

// TODO: find a solution to map only partially an area
void *AddressSpace::mmap(void *vaddr, AddressSpace &ad, void *vaddrInAD,
          const size_t nrPages, u16 flags)
{
  if (!nrPages)
    return 0;

  VMemArea *vma;
  vaddrInAD = tools::alignOnPage(vaddrInAD);
  pi.kReserve.check();

  pi.shareLock.lock();
  _lock.lock();
  ad._lock.lock();

  VMemArea vmaFind(vaddrInAD, 1);
  Set<VMemArea *>::Iterator vma_it = ad._vmas.find(&vmaFind);

  if (vma_it == ad._vmas.end() || (*vma_it)->nrPages() != nrPages)
    goto error;

  vma = mmapHandleVirtual(vaddr, nrPages, 0);
  if (!vma)
    goto error;

  for (List<PMemArea *>::Iterator pma_it = (*vma_it)->pmas.begin();
      pma_it != (*vma_it)->pmas.end(); ++pma_it)
    if (!vma->pmas.push_back(*pma_it))
    {
      delete vma;
      goto error;
    }

  if (_vmas.insert(vma).first == _vmas.end())
  {
    delete vma;
    goto error;
  }

  for (List<PMemArea *>::Iterator pma_it = vma->pmas.begin();
      pma_it != vma->pmas.end(); ++pma_it)
    (*pma_it)->up();

  assert(mmapHandlePhysical(vma, flags));

  ad._lock.unlock();
  _lock.unlock();
  pi.shareLock.unlock();

  return vma->begin();

error:
  ad._lock.unlock();
  _lock.unlock();
  pi.shareLock.unlock();

  return 0;
}

bool AddressSpace::changeRights(void *vaddr, u16 flags)
{
  if (!vaddr)
    return false;

  vaddr = tools::alignOnPage(vaddr);

  _lock.lock();

  VMemArea vmaFind(vaddr, 1);
  Set<VMemArea *>::Iterator vma_it = _vmas.find(&vmaFind);

  if (vma_it == _vmas.end())
  {
    _lock.unlock();
    return false;
  }

  if (!pi.impl.pageTableUser.change(_vaddr_pdt, vaddr,
        (*vma_it)->nrPages(), flags))
  {
    _lock.unlock();
    return false;
  }

  _lock.unlock();
  return true;
}

bool AddressSpace::munmap(void *vaddr)
{
  if (!vaddr)
    return true;

  vaddr = tools::alignOnPage(vaddr);

  _lock.lock();

  VMemArea vmaFind(vaddr, 1);
  Set<VMemArea *>::Iterator vma_it = _vmas.find(&vmaFind);

  if (vma_it == _vmas.end())
  {
    _lock.unlock();
    return false;
  }

  if ((*vma_it)->getFlag(VMemArea::ZONE_RESERVED))
  {
    // TODO
  }
  else
    pi.physAllocator.deallocate((*vma_it)->pmas);

  pi.impl.pageTableUser.free(_vaddr_pdt, vaddr, (*vma_it)->nrPages());

  delete *vma_it;
  _vmas.erase(vma_it);

  _lock.unlock();

  return true;
}

void AddressSpace::mmapVM86()
{
  _lock.lock();

  assert(pi.impl.pageTableUser.add(_vaddr_pdt, (void *) 0x0, (void *) 0x0, 1,
        READ | WRITE | EXEC));

  _lock.unlock();
}

void AddressSpace::munmapVM86()
{
  _lock.lock();
  assert(pi.impl.pageTableUser.free(_vaddr_pdt, (void *) 0x0, 1));
  _lock.unlock();
}

VMemArea *AddressSpace::mmapHandleVirtual(void *vaddr, const size_t nrPages,
    u16 flags)
{
  void *realVaddr = 0;

  vaddr = tools::alignOnPage(vaddr);

  if (vaddr && _vmas.fitIn((size_t) vaddr, nrPages, false))
      realVaddr = vaddr;

  if (!realVaddr)
    realVaddr = _vmas.findRoom(nrPages, false);

  if (!realVaddr)
    return 0;

  VMemArea *vma = new(CRITICAL) VMemArea(realVaddr, nrPages);
  if (!vma)
    return 0;

  vma->setFlags(flags);
  return vma;
}

bool AddressSpace::mmapHandlePhysical(VMemArea *vma, u16 flags)
{
  size_t curVaddr = (size_t) vma->begin();

  for (List<PMemArea *>::Iterator it = vma->pmas.begin();
      it != vma->pmas.end(); ++it)
  {
    if (!pi.impl.pageTableUser.add(_vaddr_pdt, (*it)->begin(),
          (void *) curVaddr, (*it)->nrPages(), flags))
      return false;

    curVaddr += ((*it)->nrPages() << 12);
  }

  return true;
}

/*
 * KERNEL ADDRESS SPACE
 * ====================
 */

void *KernelAddressSpace::mmap(void *vaddr, const size_t nrPages, u16 flags)
{
  if (!nrPages)
    return 0;

  pi.kReserve.check();

  pi.kASInternal.lock.lock();
  VMemArea *vma = mmapHandleVirtual(vaddr, nrPages, 0);
  if (!vma)
  {
    pi.kASInternal.lock.unlock();
    return 0;
  }

  if (!pi.physAllocator.allocate(nrPages, flags, vma->pmas))
  {
    pi.kASInternal.lock.unlock();
    return 0;
  }

  Pair<Set<VMemArea *>::Iterator, bool> ins_res =
    pi.kASInternal.kernelVMAS.insert(vma);

  if (ins_res.first == pi.kASInternal.kernelVMAS.end())
  {
    pi.physAllocator.deallocate(vma->pmas);
    delete vma;
    pi.kASInternal.lock.unlock();
    return 0;
  }

  mmapHandlePhysical(vma, flags);
  pi.kASInternal.lock.unlock();

  return vma->begin();
}

void *KernelAddressSpace::mmap(void *vaddr, void *paddr,
    const size_t nrPages, u16 flags)
{
  if (!nrPages)
    return 0;

  pi.kReserve.check();
  paddr = tools::alignOnPage(paddr);

  pi.kASInternal.lock.lock();
  VMemArea *vma = mmapHandleVirtual(vaddr, nrPages, VMemArea::ZONE_RESERVED);
  if (!vma)
  {
    pi.kASInternal.lock.unlock();
    return 0;
  }

  // TODO: add kernel physical allocator mechanism

  Pair<Set<VMemArea *>::Iterator, bool> ins_res =
    pi.kASInternal.kernelVMAS.insert(vma);

  if (ins_res.first == pi.kASInternal.kernelVMAS.end())
  {
    delete vma;
    pi.kASInternal.lock.unlock();
    return 0;
  }

  pi.impl.pageTableKernel.add(paddr, vma->begin(), nrPages, flags);

  pi.kASInternal.lock.unlock();

  return vma->begin();
}

void *KernelAddressSpace::mmap(void *vaddr, AddressSpace &ad,
    void *vaddrInAD, const size_t nrPages, u16 flags)
{
  if (!nrPages)
    return 0;

  VMemArea *vma;
  vaddrInAD = tools::alignOnPage(vaddrInAD);
  pi.kReserve.check();

  ad._lock.lock();
  pi.kASInternal.lock.lock();

  VMemArea vmaFind(vaddrInAD, 1);
  Set<VMemArea *>::Iterator vma_it = ad._vmas.find(&vmaFind);

  if (vma_it == ad._vmas.end() || (*vma_it)->nrPages() != nrPages)
    goto error;

  vma = mmapHandleVirtual(vaddr, nrPages, 0);
  if (!vma)
    goto error;

  for (List<PMemArea *>::Iterator pma_it = (*vma_it)->pmas.begin();
      pma_it != (*vma_it)->pmas.end(); ++pma_it)
    if (!vma->pmas.push_back(*pma_it))
    {
      delete vma;
      goto error;
    }

  if (pi.kASInternal.kernelVMAS.insert(vma).first ==
      pi.kASInternal.kernelVMAS.end())
  {
    delete vma;
    goto error;
  }

  for (List<PMemArea *>::Iterator pma_it = vma->pmas.begin();
      pma_it != vma->pmas.end(); ++pma_it)
    (*pma_it)->up();

  mmapHandlePhysical(vma, flags);

  pi.kASInternal.lock.unlock();
  ad._lock.unlock();

  return vma->begin();

error:
  pi.kASInternal.lock.unlock();
  ad._lock.unlock();

  return 0;
}

void *KernelAddressSpace::mmapIPC(AddressSpace *as, void *vaddr, bool sender,
    void *curVaddr)
{
  PMemArea *pma;
  size_t page_offset;
  void *res;

  void *page_vaddr = tools::alignOnPage(vaddr);
  curVaddr = tools::alignOnPage(curVaddr);
  assert(as);

  as->_lock.lock();
  Set<VMemArea *>::Iterator vma_it = as->_vmas.insideVMA((size_t) page_vaddr,
      1);

  if (vma_it == as->_vmas.end() || !(*vma_it)->getPMA(page_vaddr, &pma,
        &page_offset))
  {
    as->_lock.unlock();
    return 0;
  }

  pma->up();
  as->_lock.unlock();

  res = pi.kASInternal.mapIPC(pma, page_offset, sender, curVaddr);
  return (void *) ((size_t) res + (0xFFF & (size_t) vaddr));
}

bool KernelAddressSpace::changeRights(void *vaddr, u16 flags)
{
  if (!vaddr)
    return false;

  vaddr = tools::alignOnPage(vaddr);

  pi.kASInternal.lock.lock();

  VMemArea vmaFind(vaddr, 1);
  Set<VMemArea *>::Iterator vma_it = pi.kASInternal.kernelVMAS.find(&vmaFind);

  if (vma_it == pi.kASInternal.kernelVMAS.end())
  {
    pi.kASInternal.lock.unlock();
    return false;
  }

  pi.impl.pageTableKernel.change(vaddr, (*vma_it)->nrPages(), flags);
  pi.kASInternal.lock.unlock();

  return true;
}

bool KernelAddressSpace::munmap(void *vaddr)
{
  if (!vaddr)
    return true;

  vaddr = tools::alignOnPage(vaddr);

  pi.kASInternal.lock.lock();

  VMemArea vmaFind(vaddr, 1);
  Set<VMemArea *>::Iterator vma_it = pi.kASInternal.kernelVMAS.find(&vmaFind);

  if (vma_it == pi.kASInternal.kernelVMAS.end())
  {
    pi.kASInternal.lock.unlock();
    return false;
  }

  if ((*vma_it)->getFlag(VMemArea::ZONE_RESERVED))
  {
    // TODO
  }
  else
    pi.physAllocator.deallocate((*vma_it)->pmas);

  pi.impl.pageTableKernel.free(vaddr, (*vma_it)->nrPages());

  delete *vma_it;
  pi.kASInternal.kernelVMAS.erase(vma_it);

  pi.kASInternal.lock.unlock();

  return true;
}

VMemArea *KernelAddressSpace::mmapHandleVirtual(void *vaddr,
    const size_t nrPages, u16 flags)
{
  void *realVaddr = 0;

  vaddr = tools::alignOnPage(vaddr);

  if (vaddr &&
      pi.kASInternal.kernelVMAS.fitIn((size_t) vaddr, nrPages, true))
    realVaddr = vaddr;

  if (!realVaddr)
    realVaddr = pi.kASInternal.kernelVMAS.findRoom(nrPages, true);

  if (!realVaddr)
    return 0;

  VMemArea *vma = new(CRITICAL) VMemArea(realVaddr, nrPages);
  if (!vma)
    return 0;

  vma->setFlags(flags);
  return vma;
}

void KernelAddressSpace::mmapHandlePhysical(VMemArea *vma, u16 flags)
{
  size_t curVaddr = (size_t) vma->begin();

  for (List<PMemArea *>::Iterator it = vma->pmas.begin();
      it != vma->pmas.end(); ++it)
  {
    pi.impl.pageTableKernel.add((*it)->begin(), (void *) curVaddr,
        (*it)->nrPages(), flags);

    curVaddr += ((*it)->nrPages() << 12);
  }
}

void tools::clearPage(void *page)
{
#ifdef MIKRO64
  u64 *data = (u64 *) page;

  // In compiler, we trust
  for (u32 i = 0; i < 0x200; i++, data++)
    *data = 0;
#else /* MIKRO64 */
  u32 *data = (u32 *) page;

  // In compiler, we trust
  for (u32 i = 0; i < 0x400; i++, data++)
    *data = 0;
#endif /* MIKRO64 */
}

}
