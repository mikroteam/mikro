/**
 * \file paging_init.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging initialization class
 */

#include "errors.hh"
#include "init_daemon.hh"
#include "memory/allocator_internal.hh"
#include "memory/paging_init.hh"

namespace mem
{

PagingInit::PagingInit(Mapping *mappings, u32 nrMappings,
    size_t zoneNormalBegin)
  : _maxAddr(0), _nrFreePages(0), _zoneNormalBegin(zoneNormalBegin),
    _mappings(mappings), _nrMappings(nrMappings)
{
  _startAllocator = 0;
  _kernelReserve = 0;

  for (u32 i = 0; i < _nrMappings; i++)
  {
    if (_mappings[i].flags & Mapping::START_ALLOCATOR)
      _startAllocator = &_mappings[i];

    if (_mappings[i].flags & Mapping::KERNEL_RESERVE)
      _kernelReserve = &_mappings[i];

    if (_startAllocator && _kernelReserve)
      break;
  }
}

bool PagingInit::initStartAllocator()
{
  if (!_startAllocator || !_kernelReserve)
    return false;

  kAllocator.startParameter(_startAllocator->vaddr,
      _startAllocator->vaddr + (_startAllocator->nrPages << 12));
  kAllocator.init();

  return true;
}

bool PagingInit::setReserved(multiboot_info_t *info, size_t maxPhysical)
{
  multiboot_memory_map_t *map_entry =
    (multiboot_memory_map_t *) info->mmap_addr;
  u32 nr_entries = info->mmap_length / sizeof (struct multiboot_mmap_entry);

  size_t max = 0;
  size_t nrPages;
  void *startAddr;
  Set<PMemArea> free;

  if (!nr_entries || !(info->flags & MULTIBOOT_INFO_MEM_MAP))
    return false;

  for (unsigned int i = 0; i < nr_entries; i++)
  {
    if (map_entry->type == MULTIBOOT_MEMORY_AVAILABLE)
    {
      startAddr = (void *) (map_entry->addr & ~0xFFF);
      nrPages = map_entry->len >> 12;

      Pair<Set<PMemArea>::Iterator, bool> ins_res =
        free.insert(PMemArea(startAddr, nrPages));

      if (ins_res.first == free.end())
        return false;

      max = (size_t) startAddr + (map_entry->len & ~0xFFF) - 1;
      if (max > _maxAddr)
        _maxAddr = max;

      _nrFreePages += ((map_entry->len & ~0xFFF) >> 12);
    }

    map_entry++;
  }

  if (!reverseSet(free, _finalMapping, maxPhysical))
    return false;

  nrFreePages();

  return true;
}

bool PagingInit::initialMapping()
{
  for (u32 i = 0; i < _nrMappings; i++)
  {
    if (!(_mappings[i].flags & Mapping::FREEABLE))
    {
      Pair<Set<PMemArea>::Iterator, bool> ins_res = _finalMapping.insert(
          PMemArea((void *) _mappings[i].paddr, _mappings[i].nrPages)
          );

      if (ins_res.first == _finalMapping.end())
        return false;
    }
  }

  return true;
}

Mapping *PagingInit::prealloc4PhysAllocator()
{
  if (_nrFreePages <= (sizeof(void *) + 0x1000))
    return 0;

  // This is only pure math and not magic :D
  size_t realFreePages = _nrFreePages / (sizeof(void *) + 0x1000);
  realFreePages *= 0x1000;

  if (_nrFreePages <= realFreePages)
    return 0;

  _preallocMapping.nrPages = _nrFreePages - realFreePages;
  _preallocMapping.flags = Mapping::NONE;
  _nrFreePages = realFreePages;

  return &_preallocMapping;
}

bool PagingInit::initPhysAllocators()
{
  Set<PMemArea> allocatableAreas;

  size_t toClear = _preallocMapping.vaddr;
  for (u32 i = 0; i < _preallocMapping.nrPages; i++)
  {
    tools::clearPage((void *) toClear);
    toClear += 0x1000;
  }

  Pair<Set<PMemArea>::Iterator, bool> ins_res = _finalMapping.insert(
      PMemArea((void *) _preallocMapping.paddr, _preallocMapping.nrPages)
      );

  if (ins_res.first == _finalMapping.end())
    return false;

  if (!reverseSet(_finalMapping, allocatableAreas, _maxAddr))
    return false;

  if (!pi.physAllocator.init(allocatableAreas, (void *) _preallocMapping.vaddr,
        _zoneNormalBegin, _maxAddr))
    return false;

  if (pi.physAllocator.realNrPages() > _preallocMapping.nrPages *
      (0x1000 / sizeof(void *)))
    return false;

  for (u32 i = 0; i < _nrMappings; i++)
    if (!(_mappings[i].flags & Mapping::KERNEL_RESERVE) &&
        !(_mappings[i].flags & Mapping::START_ALLOCATOR))
      if (!doAllocation(&_mappings[i]))
        return false;

  if (!doAllocation(&_preallocMapping))
      return false;

  if (!doAllocations(_startAllocator) || !doAllocations(_kernelReserve))
    return false;

  return true;
}

bool PagingInit::mapModules(multiboot_info_t *info)
{
  multiboot_module_t *mbModule = (multiboot_module_t *) info->mods_addr;

  Mapping map_temp;
  map_temp.vaddr = _preallocMapping.vaddr + (_preallocMapping.nrPages << 12);
  map_temp.flags = Mapping::FREEABLE;

  for (u32 i = 0; i < info->mods_count; i++)
  {
    map_temp.nrPages =
      tools::length2nrPages(mbModule->mod_end - mbModule->mod_start + 1);
    map_temp.paddr = mbModule->mod_start;

    if (!task::initDaemons.push_front(task::InitDaemon((void *) map_temp.vaddr,
            map_temp.nrPages)))
      return false;

    pi.impl.pageTableKernel.add((void *) map_temp.paddr,
        (void *) map_temp.vaddr,
        map_temp.nrPages,
        READ);

    if (!doAllocation(&map_temp))
      return false;

    mbModule++;
    map_temp.vaddr += (map_temp.nrPages << 12);
  }

  return true;
}

void PagingInit::finalize()
{
  pi.kReserve.init((void *) _kernelReserve->vaddr, _kernelReserve->nrPages);

  u32 nrPageUsed = kAllocator.pagingOK();

  pi.kReserve.init((void *) (_startAllocator->vaddr + (nrPageUsed << 12)),
        _startAllocator->nrPages - nrPageUsed);
}

bool PagingInit::reverseSet(Set<PMemArea> &in, Set<PMemArea> &out, size_t max)
{
  Set<PMemArea>::Iterator itNext = in.begin();
  ++itNext;

  for (Set<PMemArea>::Iterator it = in.begin(); it != in.end(); ++it, ++itNext)
  {
    if ((size_t) (*it).begin() > max)
        break;

    if (it == in.begin() && (*it).begin() != (void *) 0x0)
    {
      Pair<Set<PMemArea>::Iterator, bool> ins_res = out.insert(PMemArea(0,
              (void *) ((size_t) (*it).begin() - 1)));

      if (ins_res.first == out.end())
        return false;
    }

    if (itNext != in.end() && (size_t) (*itNext).begin() < max)
    {
      if ((size_t) (*it).end() + 1 != (size_t) (*itNext).begin())
      {
        Pair<Set<PMemArea>::Iterator, bool> ins_res =
          out.insert(PMemArea((void *) ((size_t) (*it).end() + 1),
                (void *) ((size_t) (*itNext).begin() - 1)));

        if (ins_res.first == out.end())
          return false;
      }
    }
    else if ((*it).end() != (void *) max)
    {
      Pair<Set<PMemArea>::Iterator, bool> ins_res =
        out.insert(PMemArea((void *) ((size_t) (*it).end() + 1),
              (void *) max));

      if (ins_res.first == out.end())
        return false;
    }
  }

  return true;
}

void PagingInit::nrFreePages()
{
  for (u32 i = 0; i < _nrMappings; i++)
  {
    if (!(_mappings[i].flags & Mapping::FREEABLE) ||
        (_mappings[i].flags & Mapping::SYS_RESERVED))
      _nrFreePages -= _mappings[i].nrPages;
  }
}

bool PagingInit::doAllocation(Mapping *mapping)
{
  VMemArea *vma;

  if (!(mapping->flags & Mapping::SYS_RESERVED))
  {
    vma = new VMemArea((void *) mapping->vaddr, mapping->nrPages);

    if (!vma)
      return false;

    if (mapping->flags & Mapping::FREEABLE)
    {
      if (!pi.physAllocator.earlyAllocate(
          mapping->paddr,
          mapping->nrPages,
          vma->pmas
          ))
      {
        log::fatal("Paging") << "Early mapping at P: 0x" << dsp::hex
          << mapping->paddr << " V: 0x" << mapping->vaddr << " of "
          << dsp::dec << mapping->nrPages << " page(s) failed";
        return false;
      }
    }

    Pair<Set<VMemArea *>::Iterator, bool> ins_res =
      pi.kASInternal.kernelVMAS.insert(vma);

    if (ins_res.first == pi.kASInternal.kernelVMAS.end())
      return false;
  }

  return true;
}

bool PagingInit::doAllocations(Mapping *mapping)
{
  Mapping map_temp;
  map_temp.paddr = mapping->paddr;
  map_temp.vaddr = mapping->vaddr;
  map_temp.flags = mapping->flags;
  map_temp.nrPages = 1;

  for (u32 i = 0; i < mapping->nrPages; i++)
  {
    if (!doAllocation(&map_temp))
      return false;

    map_temp.paddr += 0x1000;
    map_temp.vaddr += 0x1000;
  }

  return true;
}


}
