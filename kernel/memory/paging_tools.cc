/**
 * \file paging_tools.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging internal tools
 */

#include "memory/paging_internal.hh"
#include "memory/paging_macro.hh"
#include "memory/paging_tools.hh"

namespace mem
{

static size_t checkModuleAddr(multiboot_info_t *info, size_t addr,
    size_t sizeArea, size_t sizeData)
{
  if (!(info->flags & MULTIBOOT_INFO_MODS))
    return addr;

  multiboot_mod_list *map_entry = (multiboot_mod_list *) info->mods_addr;
  u32 nr_entries = info->mods_count;

  if (!nr_entries)
    return addr;

  sizeData = (sizeData & ~0xFFF) + 0x1000;
  size_t addrEnd = addr + sizeData;

  for (unsigned int i = 0; i < nr_entries; i++)
  {
    size_t mod_end = (map_entry->mod_end & ~0xFFF) + 0x1000;

    if ((map_entry->mod_start >= addr && map_entry->mod_start <= addrEnd) ||
        (mod_end >= addr && mod_end <= addrEnd))
    {
      if (mod_end + sizeData <= addr + sizeArea)
      {
        i = 0;
        addr = mod_end + sizeData;
      }
      else
        return 0;
    }

    map_entry++;
  }

  return addr;
}

void *PagingTools::bestArea(multiboot_info_t *info, unsigned long after,
    size_t size)
{
  size_t res;
  if (!(info->flags & MULTIBOOT_INFO_MEM_MAP))
    return 0;

  multiboot_memory_map_t *map_entry =
    (multiboot_memory_map_t *) info->mmap_addr;
  u32 nr_entries = info->mmap_length / sizeof (struct multiboot_mmap_entry);

  if (!nr_entries)
    return 0;

  for (unsigned int i = 0; i < nr_entries; i++)
  {
    if (map_entry->type == MULTIBOOT_MEMORY_AVAILABLE)
    {
      if (map_entry->addr >= after && map_entry->len >= size)
      {
        res = checkModuleAddr(info, map_entry->addr, map_entry->len, size);
        if (res)
          return (void *) res;
      }

      if (map_entry->addr < after &&
          map_entry->addr + map_entry->len > after &&
          map_entry->len >= (after - map_entry->addr) + size)
      {
        res = checkModuleAddr(info, after, map_entry->len, size);
        if (res)
          return (void *) res;
      }
    }

    map_entry++;
  }

  return 0;
}

}
