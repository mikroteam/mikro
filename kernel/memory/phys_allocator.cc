/**
 * \file phys_allocator.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Page physical allocator
 */

#include "arch/asm.hh"
#include "paging.hh"
#include "memory/phys_allocator.hh"
#include "memory/paging_internal.hh"

namespace mem
{

StdAllocator::AllocItem *StdAllocator::AllocItem::split()
{
  assert(_order);
  size_t newNrPages = nrPages() >> 1;

  _order--;
  _paddr_end = (void *) ((size_t) _paddr_begin + (newNrPages << 12) - 1);

  return new(CRITICAL) AllocItem((void *) ((size_t) _paddr_begin +
        (newNrPages << 12)), _order, _areaID);
}

void StdAllocator::AllocItem::merge()
{
  size_t newNrPages = nrPages() << 1;

  _order++;
  _paddr_end = (void *) ((size_t) _paddr_begin + (newNrPages << 12) - 1);
}

u8 StdAllocator::computeOrder(u32 nrPage)
{
  size_t bucketSize = 1 << (PALLOC_MAXBUCKET - 1);

  if (nrPage > bucketSize)
    return PALLOC_MAXBUCKET;

  for (int i = PALLOC_MAXBUCKET - 2; i >= 0; i--)
  {
    bucketSize >>= 1;

    if (nrPage > bucketSize)
      return i + 1;
  }

  return 0;
}

StdAllocator::StdAllocator()
  : _areas(0), _nrAreas(0), _startAddr(0), _nrPages(0), _freePages(0),
    _allocPages(0), _realNrPages(0)
{ }

bool StdAllocator::init(size_t start, u32 nrPages,
    Set<PMemArea> &allocatableAreas, void *preAlloc)
{
  _startAddr = start;
  _nrPages = nrPages;

  size_t endZone = start + (nrPages << 12) - 1;

  size_t mapStart;
  size_t mapNrPages;

  AllocItem *allocIt;

  for (Set<PMemArea>::Iterator it = allocatableAreas.begin();
      it != allocatableAreas.end(); ++it)
  {
    if ((size_t) (*it).end() < start ||
        (size_t) (*it).begin() > endZone)
      continue;

    _nrAreas++;
  }

  _areas = new Area[_nrAreas];
  size_t curArea = 0;

  for (Set<PMemArea>::Iterator it = allocatableAreas.begin();
      it != allocatableAreas.end(); ++it)
  {
    if ((size_t) (*it).end() < start || (size_t) (*it).begin() > endZone)
      continue;

    if ((size_t) (*it).begin() < start)
      mapStart = start;
    else
      mapStart = (size_t) (*it).begin();

    if ((size_t) (*it).end() > endZone)
      mapNrPages = (endZone - mapStart + 1) >> 12;
    else
      mapNrPages = ((size_t) (*it).end() - mapStart + 1) >> 12;

    _areas[curArea].begin = mapStart;
    _areas[curArea].itemRefs = (AllocItem **) preAlloc;
    _areas[curArea].maxRefs = mapNrPages;
    _realNrPages += mapNrPages;
    preAlloc = (void *) ((size_t) preAlloc + mapNrPages * sizeof(void *));

    size_t refOff = 0;
    int order = PALLOC_MAXBUCKET - 1;
    size_t nrPagesOrder = (1 << order);

    while (mapNrPages)
    {
      if (nrPagesOrder > mapNrPages)
      {
        nrPagesOrder = nrPagesOrder >> 1;
        order--;
        continue;
      }

      allocIt = new AllocItem((void *) mapStart, order, curArea);

      if (!allocIt)
        return false;

      _buckets[order].push_front(allocIt);
      _areas[curArea].itemRefs[refOff] = allocIt;

      mapNrPages -= nrPagesOrder;
      mapStart += (nrPagesOrder << 12);
      refOff += nrPagesOrder;
    }

    curArea++;
  }

  _freePages = _realNrPages;

  return true;
}

bool StdAllocator::earlyAllocate(size_t paddr, size_t nrPages,
    List<PMemArea *> &pmas)
{
  Area *area = &_areas[getAreaID(paddr, nrPages)];
  size_t itemAAddr = ((size_t) paddr - area->begin) >> 12;

  /* Determine max bucket order in this area */
  int maxOrderArea = PALLOC_MAXBUCKET - 1;
  size_t maxOrderAreaNrPages = (1 << maxOrderArea);
  while (maxOrderAreaNrPages > area->maxRefs)
  {
    maxOrderArea--;
    maxOrderAreaNrPages = maxOrderAreaNrPages >> 1;
  }

  while (nrPages)
  {
    /* Determine max bucket order at this AAddr */
    int maxOrder = 0;
    size_t maxOrderNrPages = 1;

    if (!itemAAddr)
    {
      maxOrder = maxOrderArea;
      maxOrderNrPages = maxOrderAreaNrPages;
    }
    else
    {
      maxOrder = 0;
      maxOrderNrPages = 1;

      while (!(itemAAddr & maxOrderNrPages))
      {
        maxOrder++;
        maxOrderNrPages = maxOrderNrPages << 1;
      }
    }

    /* Determine max number of allocatable pages */
    int targetOrder = PALLOC_MAXBUCKET - 1;
    size_t targetNrPages = 1 << targetOrder;
    do
    {
      targetOrder--;
      targetNrPages >>= 1;
    }
    while (nrPages < targetNrPages || targetOrder > maxOrder);

    /* Walk bucket tree until finding a bucket item */
    int curOrder = targetOrder;
    size_t curAAddr = itemAAddr;
    size_t buddyAAddr;
    while (!area->itemRefs[curAAddr])
    {
      if (curOrder > maxOrderArea)
        return false;
      buddyAAddr = curAAddr ^ (1 << curOrder);

      if (buddyAAddr < area->maxRefs)
      {
        if (buddyAAddr < curAAddr)
          curAAddr = buddyAAddr;
        else if(area->itemRefs[buddyAAddr])
          return false;
      }

      curOrder++;
    }

    /* Rebuild bucket tree to insert new item */
    AllocItem *item = area->itemRefs[curAAddr];
    curOrder = area->itemRefs[curAAddr]->order();
    size_t splitedAAddr;
    while (curOrder > targetOrder)
    {
      curOrder--;
      splitedAAddr = curAAddr + (1 << curOrder);

      if (splitedAAddr >= area->maxRefs)
      {
        item->setOrder(curOrder);
        item->check(_areas);
      }
      else
      {
        AllocItem *splited = item->split();
        splited->check(_areas);
        area->itemRefs[splitedAAddr] = splited;
        _buckets[curOrder + 1].erase(item);

        _buckets[curOrder].push_front(item);
        _buckets[curOrder].push_front(splited);

        if (itemAAddr >= splitedAAddr)
        {
          item = splited;
          curAAddr = splitedAAddr;
        }
      }
    }

    if (curAAddr != itemAAddr)
      return false;
    /* Remove item from bucket tree because it isn't free */
    _buckets[targetOrder].erase(item);
    area->itemRefs[itemAAddr] = 0;

    /* Update necessary information */
    _freePages -= targetNrPages;
    _allocPages += targetNrPages;

    if (targetNrPages > nrPages)
      break;

    nrPages -= targetNrPages;
    itemAAddr += targetNrPages;

    assert(pmas.push_front(item));
  }

  return true;
}

bool StdAllocator::allocate(size_t nrPages, u16 flags, List<PMemArea *> &pmas)
{
  PMemArea *pma;
  int order = PALLOC_MAXBUCKET - 1;
  size_t nrPagesOrder = 1 << order;

  if ((flags & CONTIGUOUS) && nrPages > (1 << (PALLOC_MAXBUCKET - 1)))
    return false;

  while (nrPages)
  {
    assert(order >= 0);

    if (nrPages >= nrPagesOrder)
    {
      pma = allocateOrder(order);
      if (!pma)
      {
        deallocate(pmas);
        return false;
      }

      if (!pmas.push_front(pma))
      {
        deallocateOrder(pma);
        deallocate(pmas);
        return false;
      }

      nrPages -= nrPagesOrder;
    }
    else
    {
      order--;
      nrPagesOrder >>= 1;
    }
  }

  return true;
}

PMemArea *StdAllocator::allocateOrder(int order)
{
  AllocItem *res = 0;
  AllocItem **itemRef;

  _lock.lock();

  if (!_buckets[order].empty())
  {
    res = _buckets[order].pop_front();
    res->check(_areas);

    itemRef = getItemRef(res);
    *itemRef = 0;
  }
  else
  {
    int curOrder = order + 1;

    while (curOrder < PALLOC_MAXBUCKET && _buckets[curOrder].empty())
      curOrder++;

    if (curOrder >= PALLOC_MAXBUCKET)
    {
      _lock.unlock();
      return 0;
    }

    res = _buckets[curOrder].pop_front();
    res->check(_areas);
    curOrder--;

    while (curOrder >= order)
    {
      AllocItem *splited = res->split();
      assert(splited);
      splited->check(_areas);

      itemRef = getItemRef(splited);
      *itemRef = splited;

      _buckets[curOrder].push_front(splited);
      curOrder--;
    }

    itemRef = getItemRef(res);
    *itemRef = 0;
  }

  // FIXME: check lock or atomic?
  asmf::Atomic<ARCH_GENERIC>::add(1 << order, &_allocPages);
  asmf::Atomic<ARCH_GENERIC>::sub(1 << order, &_freePages);

  res->up();
  _lock.unlock();

  return res;
}

void StdAllocator::deallocateOrder(PMemArea *pma)
{
  assert(pma);

  pma->down();
  if (!pma->isFree())
    return;

  AllocItem *item = (AllocItem *) pma;
  AllocItem *buddy;
  AllocItem **itemRef;

  int curOrder = item->order();
  bool buddyFirst;

  _lock.lock();

  while (curOrder < PALLOC_MAXBUCKET - 1)
  {
    item->check(_areas);
    buddy = findBuddy(item, buddyFirst);

    if (!buddy || buddy->order() != curOrder)
      break;
    else
    {
      _buckets[curOrder].erase(buddy);
      itemRef = getItemRef(buddy);
      *itemRef = 0;

      if (buddyFirst)
      {
        buddy->merge();
        buddy->check(_areas);
        delete item;

        item = buddy;
      }
      else
      {
        item->merge();
        item->check(_areas);
        delete buddy;
      }

      curOrder++;
    }
  }


  _buckets[curOrder].push_front(item);
  itemRef = getItemRef(item);
  *itemRef = item;

  asmf::Atomic<ARCH_GENERIC>::sub(1 << curOrder, &_allocPages);
  asmf::Atomic<ARCH_GENERIC>::add(1 << curOrder, &_freePages);

  _lock.unlock();
}

StdAllocator::AllocItem**
  StdAllocator::getItemRef(size_t paddr, size_t areaID)
{
  Area *area = &_areas[areaID];
  assert(paddr >= area->begin);

  size_t itemAAddr = (paddr - area->begin) >> 12;
  assert(itemAAddr < area->maxRefs);

  return &area->itemRefs[itemAAddr];
}

StdAllocator::AllocItem *StdAllocator::findBuddy(StdAllocator::AllocItem *item,
    bool &firstOne)
{
  Area *area = &_areas[item->areaID()];
  assert((size_t) item->begin() >= area->begin);

  size_t itemAAddr = ((size_t) item->begin() - area->begin) >> 12;
  assert(itemAAddr < area->maxRefs);
  size_t buddyAAddr = itemAAddr ^ (1 << item->order());

  if (buddyAAddr >= area->maxRefs)
    return 0;

  firstOne = (buddyAAddr < itemAAddr);
  return area->itemRefs[itemAAddr];
}

size_t StdAllocator::getAreaID(size_t paddr, size_t nrPages)
{
  size_t res = _nrAreas;

  for (size_t i = 0; i < _nrAreas; i++)
    if (paddr >= _areas[i].begin &&
        paddr + (nrPages << 12) <= _areas[i].begin + (_areas[i].maxRefs << 12))
    {
      res = i;
      break;
    }

  assert(res != _nrAreas);
  return res;
}

bool PhysAllocator::init(Set<PMemArea> &allocatableAreas, void *prealloc,
    size_t zoneNormalBegin, size_t maxAddr)
{
#ifdef MIKRO_ZONE_DMA
  if (!_zoneDMA.init(0x0, (zoneNormalBegin >> 12), allocatableAreas, prealloc))
    return false;

  prealloc = ((size_t *) prealloc) + _zoneDMA.realNrPages();
#endif /* MIKRO_ZONE_DMA */

  if (!_zoneNormal.init(zoneNormalBegin, (maxAddr >> 12) + 1, allocatableAreas,
        prealloc))
    return false;

  return true;
}

}
