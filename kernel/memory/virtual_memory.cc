/**
 * \file virtual_memory.hh
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Virtual memory tools
 */

#include "memory/paging_macro.hh"
#include "memory/virtual_memory.hh"

namespace mem
{

bool VMemArea::getPMA(void *vaddr, PMemArea **pma, size_t *page_offset)
{
  size_t off = ((size_t) vaddr - (size_t) _vaddr_begin) >> 12;
  size_t cur_off = 0;

  for (List<PMemArea *>::Iterator it_pma = pmas.begin(); it_pma != pmas.end();
      ++it_pma)
  {
    if (off >= cur_off && off < cur_off + (*it_pma)->nrPages())
    {
      *page_offset = off - cur_off;
      *pma = *it_pma;
      return true;
    }
    cur_off += (*it_pma)->nrPages();
  }

  return false;
}

bool VMemAreas::fitIn(unsigned long addr, size_t nrPages, bool kernel)
{
  unsigned long beginAddr;
  unsigned long endAddr;

  if (empty())
    return true;

  if (kernel)
  {
    beginAddr = KERNEL_AS_BEGIN;
    endAddr = KERNEL_AS_END;
  }
  else
  {
    beginAddr = USER_AS_BEGIN;
    endAddr = USER_AS_END;
  }

  Set<VMemArea *>::Iterator itNext = begin();
  ++itNext;

  for (Set<VMemArea *>::Iterator it = begin(); it != end(); ++it,
      ++itNext)
  {
    if (it == begin() && (unsigned long) (*it)->begin() != beginAddr)
    {
      if (addr >= beginAddr && itNext != end() &&
          addr + (nrPages << 12) <= (unsigned long) (*itNext)->begin())
        return true;
    }

    if (itNext != end())
    {
      if ((unsigned long) (*it)->end() + 1 !=
          (unsigned long) (*itNext)->begin())
      {
        if (addr > (unsigned long) (*it)->end() &&
            addr + (nrPages << 12) <= (unsigned long) (*itNext)->begin())
          return true;
      }
    }
    else if ((unsigned long) (*it)->end() != endAddr)
    {
      if (addr > (unsigned long) (*it)->end() &&
          (addr - 1) + (nrPages << 12) <= endAddr)
        return true;
    }
  }

  return false;
}

void *VMemAreas::findRoom(size_t nrPages, bool kernel)
{
  unsigned long beginAddr;
  unsigned long endAddr;

  if (kernel)
  {
    beginAddr = KERNEL_AS_BEGIN;
    endAddr = KERNEL_AS_END;
  }
  else
  {
    beginAddr = USER_AS_ANON_BEGIN;
    endAddr = USER_AS_END;
  }

  if (empty())
    return (void *) beginAddr;

  Set<VMemArea *>::Iterator itNext = begin();
  ++itNext;

  for (Set<VMemArea *>::Iterator it = begin(); it != end(); ++it,
      ++itNext)
  {
    if (it == begin() && (unsigned long) (*it)->begin() > beginAddr)
    {
      if (((unsigned long) (*it)->begin() - beginAddr) >> 12 >= nrPages)
        return (void *) beginAddr;
    }

    if (itNext != end())
    {
      if ((unsigned long) (*it)->end() + 1 !=
          (unsigned long) (*itNext)->begin())
      {
        if (((unsigned long) (*itNext)->begin() - (unsigned long) (*it)->end()
              - 1) >> 12 >= nrPages)
          return (void *) ((unsigned long) (*it)->end() + 1);
      }
    }
    else if ((unsigned long) (*it)->end() != endAddr)
    {
      if ((endAddr - (unsigned long) (*it)->end()) >> 12 >= nrPages)
        return (void *) ((unsigned long) (*it)->end() + 1);
    }
  }

  return 0;
}

Set<VMemArea *>::Iterator VMemAreas::insideVMA(size_t vaddr, size_t size)
{
  size_t vaddrEnd = size + vaddr;

  for (Set<VMemArea *>::Iterator it = begin(); it != end(); ++it)
  {
    if ((size_t) (*it)->begin() <= vaddr && (size_t) (*it)->end() >= vaddrEnd)
      return it;

    if ((size_t) (*it)->end() > vaddrEnd)
      break;
  }

  return end();
}

}
