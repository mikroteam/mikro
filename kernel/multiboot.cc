/*
 * File: multiboot.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Multiboot header for the kernel
 *
 */

#include <multiboot.h>

# define MULTIBOOT_FLAGS (MULTIBOOT_PAGE_ALIGN | MULTIBOOT_MEMORY_INFO) 
# define MULTIBOOT_CKSUM -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_FLAGS)

multiboot_header MultibootKernelHeader 
                   __attribute__((section(".multiboot"), aligned(4))) = 
  {
      .magic = MULTIBOOT_HEADER_MAGIC,
      .flags = MULTIBOOT_FLAGS,
      .checksum = (multiboot_uint32_t)(MULTIBOOT_CKSUM),
      .header_addr = 0,
      .load_addr = 0,
      .load_end_addr = 0,
      .bss_end_addr = 0,
      .entry_addr = 0,
      .mode_type = 0,
      .width = 0,
      .height = 0,
      .depth = 0
  };
