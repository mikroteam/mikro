/*
 * File: multiboot_info.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Dump informations in the multiboot info struct
 *
 */

#include <display_manager.hh>
#include <multiboot.h>
#include <types.hh>

#include <multiboot_info.hh>

void dumpMultibootInfo(struct multiboot_info* info)
{
  dsp::kout << "Multiboot info :\n\n";

  if ((info->flags & MULTIBOOT_INFO_MEMORY) > 0)
  {
    dsp::kout << dsp::hex << "Memory info :\n"
      << "Lower memory available: " << info->mem_lower
      << "\nUpper memory available: " << info->mem_upper
      << "\n\n" << dsp::dec;
  }
  if ((info->flags & MULTIBOOT_INFO_MEM_MAP) > 0)
  {
    dsp::kout << "Memory Map :\n";

    u32 entries = info->mmap_length / sizeof (struct multiboot_mmap_entry);
    char* mmapc = (char*)info->mmap_addr;
    struct multiboot_mmap_entry* mmap = (struct multiboot_mmap_entry*)(mmapc);

    for (u32 i = 0; i < entries; i++)
    {
      dsp::kout << dsp::hex <<  i << " : " << (u32)mmap->addr << " -- "
                << (u32)mmap->len << " -- ";
      if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE)
        dsp::kout << "Free\n";
      else
        dsp::kout << "Reserved\n";
      mmapc += mmap->size + 4; // Add the size of the size field
      mmap = (struct multiboot_mmap_entry*)mmapc;
    }
    dsp::kout << dsp::dec;

  }
}
