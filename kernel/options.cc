#include <options.hh>

namespace config
{

  Options options;

  void Options::init(char *cmdline)
  {
    char *tmp = cmdline;

    _cmdline = cmdline;

    if (!tmp)
      return;
    while (*tmp)
      if (*tmp++ == ' ')
        *(tmp - 1) = '\0';
  }

  char* Options::getByName(const char* name)
  {
    char*   tmp = _cmdline;
    u32     size;

#if 0 // SEGFAULT QEMU
    size = strlen(name);
    while (*tmp)
    {
      if (!strncmp(tmp, name, size))
        return tmp + size + 1;
      tmp += size;
    }
#endif

    if (!tmp)
      return 0;

    size = strlen(name);
    while (*tmp)
    {
      if (!strncmp(tmp, name, size))
        return tmp + size + 1;
      while (*tmp++ != '\0');
    }

    return 0;
  }

}
