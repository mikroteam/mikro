#include <scheduler.hh>

extern "C" void schedule(struct irq::RegsDump *regs)
{
  scheduler::scheduler.schedule(regs);
}

namespace scheduler
{

Scheduler scheduler;

}
