#include <cpu.hh>
#include <scheduler_dummy.hh>

namespace scheduler
{
  SchedulerImpl<Dummy>::SchedulerImpl()
  {
  }

  void SchedulerImpl<Dummy>::addThread(task::Thread *thread)
  {
    _slthreads.lock();
    assert(!thread->setAndCheckStatus(task::RUNNING));

    if (!thread->unsetAndCheckStatus(task::UNSCHEDULE_ME))
      _threads.push_back(thread);
    _slthreads.unlock();
  }

  void SchedulerImpl<Dummy>::removeThread(task::Thread *thread)
  {
    _slthreads.lock();
    assert(thread->unsetAndCheckStatus(task::RUNNING));

    for (u32 i = 0; i < cpu::number; i++)
      if (cpu::cpus[i]->current_thread == thread)
      {
        thread->setStatus(task::UNSCHEDULE_ME);

        _slthreads.unlock();
        return;
      }

    _threads.erase(thread);
    _slthreads.unlock();
  }

  void SchedulerImpl<Dummy>::removeThread(irq::RegsDump *curDump)
  {
    cpu::myself->current_thread->unsetStatus(task::RUNNING);
    cpu::myself->current_thread->save(curDump);
    cpu::myself->current_thread = 0;
  }

  void SchedulerImpl<Dummy>::initSchedule()
  {
    _slthreads.lock();

    if (_threads.empty())
    {
      cpu::myself->current_thread = 0;
      _slthreads.unlock();

      task::manager.idle();
    }

    task::Thread* thread_to = _threads.pop_front();
    cpu::myself->current_thread = thread_to;
    thread_to->setStatus(task::RUNNING);
    _slthreads.unlock();

    thread_to->gotoUser();
  }

  void SchedulerImpl<Dummy>::directSchedule(irq::RegsDump *curDump,
      task::Thread *thread)
  {
    task::Thread* thread_from = cpu::myself->current_thread;
    task::Thread* thread_to = thread;

    _slthreads.lock();

    if (thread_from)
    {
      thread_from->save(curDump);
      _threads.push_back(thread_from);
    }

    cpu::myself->current_thread = thread_to;
    thread_to->setStatus(task::RUNNING);
    _slthreads.unlock();

    thread_to->restore(curDump);
  }

  void SchedulerImpl<Dummy>::schedule(irq::RegsDump *curDump)
  {
    task::Thread* thread_from = cpu::myself->current_thread;
    task::Thread* thread_to;

    _slthreads.lock();
    if (_threads.empty())
    {
      /* Idle case */
      if (thread_from && thread_from->unsetAndCheckStatus(task::UNSCHEDULE_ME))
      {
        cpu::myself->current_thread = 0;
        _slthreads.unlock();

        thread_from->save(curDump);
        task::manager.idle();
      }

      _slthreads.unlock();
      return;
    }

    if (thread_from)
    {
      thread_from->save(curDump);

      if (!thread_from->unsetAndCheckStatus(task::UNSCHEDULE_ME))
        _threads.push_back(thread_from);

      thread_from->unsetStatus(task::RUNNING);
    }

    thread_to = _threads.pop_front();
    cpu::myself->current_thread = thread_to;
    thread_to->setStatus(task::RUNNING);
    _slthreads.unlock();

    thread_to->restore(curDump);
  }
}
