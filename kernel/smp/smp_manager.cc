/*
 * File: smp_manager.cc 
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Generic SMP manager
 *
 */

#include <logger.hh>

#include <smp_manager.hh>

namespace smp
{

SMPManager manager;

void SMPManager::init()
{
  /* Common Stuff */
  log::info("SMP") << "init";
  
  /* Now we can do some arch specific stuff */
  arch.init();
}

}
