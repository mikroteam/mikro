/*
 * File: sycall.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Syscalls
 *
 */

#include <cpu.hh>
#include <logger.hh>
#include <syscall.hh>

namespace syscall
{

void* table[SYSCALL_NUMBER];

SyscallManager manager;

int SyscallManager::add(size_t nb, void* syscall)
{
  if (nb >= SYSCALL_NUMBER)
    return -1;

  table[nb] = syscall;
  return 1;
}

void SyscallManager::init()
{
  for (int i = 0; i < SYSCALL_NUMBER; i++)
    table[i] = 0;
  add(SYS_MMAP, (void*)sys_mmap);
  add(SYS_MMAP_PHYS, (void*)sys_mmap_phys);
  add(SYS_MUNMAP, (void*)sys_munmap);
  add(SYS_MAP_CHANGE_RIGHTS, (void*)sys_map_change_rights);
  add(SYS_MAP_CHANGE_RIGHTS, (void*)sys_map_change_rights);

  add(SYS_CHANNEL_CREATE, (void*)sys_channel_create);
  add(SYS_CHANNEL_CLOSE, (void*)sys_channel_close);
  add(SYS_CHANNEL_OPEN, (void*)sys_channel_open);
  add(SYS_CHANNEL_SEND, (void*)sys_channel_send);
  add(SYS_CHANNEL_RECV, (void*)sys_channel_recv);
  add(SYS_CHANNEL_SRECV, (void*)sys_channel_srecv);
  add(SYS_CHANNEL_REPLY_CLOSE, (void*)sys_channel_reply_close);
  add(SYS_CHANNEL_SEND_EVENT, (void*)sys_channel_send_event);
  add(SYS_CHANNEL_CHANGE_PRIVATE, (void*)sys_channel_change_private);

#ifdef DEBUG_SYSCALL
  add(SYS_DEBUG, (void*)sys_debug);
#endif /* DEBUG_SYSCALL */

  _arch.init();
}

}

#ifdef DEBUG_SYSCALL
int sys_debug(const char *str)
{
  log::debug("USER") << "PID " << cpu::myself->current_thread->getPid()
    << " says: " << str;

  return 0;
}
#endif /* DEBUG_SYSCALL */


