/**
 * \file syscall_ipc.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief System calls for inter process communication
 */

#include "cpu.hh"
#include "ipc/ipc_process.hh"
#include "ipc/ipc_thread.hh"
#include "ipc/syscall_ipc.hh"
#include "syscall.hh"

int sys_channel_create(const char* name, u32 flags)
{
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel create syscall";
  int res;
  char *kName = new char[MAX_STRING_BUF];

  if (!kName)
    return -ENOMEM;

  if (!syscall::manager.strncpyFromUser(kName, (void *) name, MAX_STRING_BUF))
  {
    delete kName;
    return -EINVAL;
  }

  if (flags & CHANNEL_SERVER)
    res = cpu::myself->current_thread->ipcManager.createChannel(kName,
        ipc::Channel::ONE_TO_N);
  else
    res = cpu::myself->current_thread->ipcManager.createChannel(kName,
        ipc::Channel::ONE_TO_ONE);

  if (res < 0)
    delete kName;

  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel create syscall returns " << res;
  return res;
}

int sys_channel_close(int ch)
{
  return cpu::myself->current_thread->ipcManager.closeChannel(ch);
}

int sys_channel_open(const char* name, u32 flags)
{
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel open syscall";
  (void) flags; // No flags available at the moment
  int res;
  char *kName = new char[MAX_STRING_BUF];

  if (!kName)
    return -ENOMEM;

  if (!syscall::manager.strncpyFromUser(kName, (void *) name, MAX_STRING_BUF))
  {
    delete kName;
    return -EINVAL;
  }

  res = cpu::myself->current_thread->ipcManager.openChannel(kName);

  delete kName;
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel open syscall returns " << res;
  return res;
}

int sys_channel_send(int ch, void* msg, size_t size, u32 flags)
{
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel send syscall";
  int res;
  (void) flags; // No flags available at the moment

  if (!syscall::manager.checkAddress((size_t) msg) ||
      !syscall::manager.checkAddress((size_t) msg + size))
    return -EINVAL;

  ipc::Message msgObj(cpu::myself->current_thread, msg, size);

  res = cpu::myself->current_thread->ipcManager.send(ch, msgObj);
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel send syscall returns " << res;
  return res;
}

int sys_channel_recv(int ch, void* msg, size_t size, u32 flags)
{
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel recv syscall";
  (void) flags; // No flags available at the moment
  int res;

  if (!syscall::manager.checkAddress((size_t) msg) ||
      !syscall::manager.checkAddress((size_t) msg + size))
    return -EFAULT;

  res = cpu::myself->current_thread->ipcManager.receive(ch, msg, size);
  log::debug("CHAN") << cpu::myself->id << ":" <<cpu::myself->current_thread->getPid() << " Channel recv syscall returns " << res;
  return res;
}

int sys_channel_srecv(int ch, void* msg_out, size_t size_out, void *msg_in,
    size_t size_in, u32 flags)
{
  (void) flags; // No flags available at the moment

  if (!syscall::manager.checkAddress((size_t) msg_out) ||
      !syscall::manager.checkAddress((size_t) msg_out + size_out) ||
      !syscall::manager.checkAddress((size_t) msg_in) ||
      !syscall::manager.checkAddress((size_t) msg_in + size_in))
    return -EINVAL;

  ipc::Message msgObj(cpu::myself->current_thread, msg_out, size_out);

  return cpu::myself->current_thread->ipcManager.srcv(ch, msgObj, msg_in,
      size_in);
}

int sys_channel_reply_close(int ch, int mh, void* msg, size_t size, u32 flags)
{
  (void) flags; // No flags available at the moment

  if (!syscall::manager.checkAddress((size_t) msg) ||
      !syscall::manager.checkAddress((size_t) msg + size))
    return -EFAULT;

  ipc::Message msgObj(cpu::myself->current_thread, msg, size);

  return cpu::myself->current_thread->ipcManager.send(ch, msgObj, mh);
}

int sys_channel_send_event(int ch, pid_t pid, void* msg, size_t size,
    u32 flags)
{
  (void) flags; // No flags available at the moment

  if (!syscall::manager.checkAddress((size_t) msg) ||
      !syscall::manager.checkAddress((size_t) msg + size))
    return -EFAULT;

  ipc::Message msgObj(cpu::myself->current_thread, msg, size);

  return cpu::myself->current_thread->ipcManager.send(ch, pid, msgObj);
}

int sys_channel_change_private(int ch, int mh, size_t dprivate)
{
  return cpu::myself->current_thread->ipcManager.setPrivate(ch, mh, dprivate);
}
