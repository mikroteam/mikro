/*
 * File: syscall_mem.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Memory Related syscalls
 *
 */

#include <scheduler.hh>
#include <syscall.hh>
#include <cpu.hh>

void* sys_mmap(void* vaddr, const size_t nrPages, u16 flags)
{
  task::Thread* thread = cpu::myself->current_thread;

  assert(thread != 0);
  return thread->getAS()->mmap(vaddr, nrPages, flags);
}

void* sys_mmap_phys(void* vaddr, void *paddr, const size_t nrPages, u16 flags)
{
  task::Thread* thread = cpu::myself->current_thread;

  assert(thread != 0);
  return thread->getAS()->mmap(vaddr, paddr, nrPages, flags);
}

int sys_munmap(void* vaddr)
{
  task::Thread* thread = cpu::myself->current_thread;

  assert(thread != 0);
  return thread->getAS()->munmap(vaddr);
}

int sys_map_change_rights(void* vaddr, u16 flags)
{
  task::Thread* thread = cpu::myself->current_thread;

  assert(thread != 0);
  return thread->getAS()->changeRights(vaddr, flags);
}
