/**
 * \file int_daemon.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Handle init daemons
 */

#include "cstring.hh"
#include "init_daemon.hh"

namespace task
{

List<InitDaemon> initDaemons;

bool InitDaemon::checkElfValid(ElfN_Ehdr* t)
{
  return (strncmp((char*)t->e_ident, ELFMAG, SELFMAG) == 0);
}

u16 InitDaemon::loadRights(ElfN_Phdr* ph)
{
  u16 flags = 0;

  if (ph->p_flags & PF_X)
    flags |= mem::EXEC;
  if (ph->p_flags & PF_R)
    flags |= mem::READ;
  if (ph->p_flags & PF_W)
    flags |= mem::WRITE;

  return flags;
}

int InitDaemon::loadProgramHeader(void* elf, ElfN_Phdr* ph,
    mem::AddressSpace* as)
{
  if (!ph->p_memsz)
    return 0;

  size_t memSize = mem::tools::length2nrPages(ph->p_memsz +
      ((size_t) ph->p_vaddr & 0xFFF));

  log::debug("Loader") << "Load program header to vaddr: " << dsp::hex
                       << ph->p_vaddr << " with size: " << ph->p_memsz
                       << dsp::dec;

  u16 rights = loadRights(ph);
  void *vaddr_ph = mem::tools::alignOnPage((void*)ph->p_vaddr);

  void *vaddr_res = as->mmap(vaddr_ph, memSize, rights);

  if (vaddr_ph != vaddr_res)
  {
    as->munmap(vaddr_ph);
    return -1;
  }

  void* kaddr = mem::kernelAD.mmap(0, *as, vaddr_ph, memSize,
      mem::READ | mem::WRITE);

  assert(kaddr);

  void *kaddr_ph = (char *) kaddr + ((size_t) ph->p_vaddr & 0xFFF);

  memcpy(kaddr_ph, (char*)elf + ph->p_offset, ph->p_filesz);
  memset((char*)kaddr_ph + ph->p_filesz, 0, ph->p_memsz - ph->p_filesz);
  mem::kernelAD.munmap(kaddr);

  return 0;
}

void* InitDaemon::load(mem::AddressSpace* as)
{
  // FIXME: do better assertions
  assert(_nrPages);
  ElfN_Ehdr* header = reinterpret_cast<ElfN_Ehdr*>(_vaddr);
  void *res;

  if (!checkElfValid(header))
  {
    log::err("Loader") << "Not a valid ELF file";
    return 0;
  }

  u16 entries = header->e_phnum;

  for (u16 i = 0; i < entries; i++)
  {
    ElfN_Phdr* ph = (ElfN_Phdr*)((char*) _vaddr + header->e_phoff
        + i * header->e_phentsize);

    if (ph->p_type & PT_LOAD)
    {
      if (loadProgramHeader(_vaddr, ph, as) != 0)
        return 0;
    }
  }

  res = (void *) header->e_entry;
  mem::kernelAD.munmap(_vaddr);

  log::debug("Loader") << "Sucessfull load, entry point is: " << dsp::hex
                       << res << dsp::dec;

  return res;
}

}
