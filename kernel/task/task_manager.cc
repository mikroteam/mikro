#include <scheduler.hh>
#include <task_manager.hh>
#include <thread.hh>

namespace task
{

  TaskManager manager;

  int TaskManager::createProcess(Process **p)
  {
    pid_t pid;
    *p = new Process();

    if (!*p)
      return -ENOMEM;

    if (!getNewPid(pid))
      return -ENOPID;

    if (!(*p)->init(pid))
      return -ENOMEM;

    _slProcesses.lock();
    Pair<Map<pid_t, Process *>::Iterator, bool> insert_res =
      _processes.insert(make_pair((*p)->getPid(), *p));

    if (insert_res.first == _processes.end())
    {
      _slProcesses.unlock();
      return -ENOMEM;
    }
    _slProcesses.unlock();

    return 0;
  }

  int TaskManager::addThread(Process *process, size_t entry, pid_t pid)
  {
    Thread *thread = new Thread(pid, process);

    if (!thread || !thread->init(entry))
      return -ENOMEM;

    _slThreads.lock();
    Pair<Map<pid_t, Thread *>::Iterator, bool> insert_res =
      _threads.insert(make_pair(thread->getPid(), thread));

    if (insert_res.first == _threads.end())
    {
      _slThreads.unlock();
      return -ENOMEM;
    }
    _slThreads.unlock();

    process->up();
    scheduler::scheduler.add(thread);

    return 0;
  }

  void TaskManager::delThread(Thread *thread)
  {
    Map<pid_t, Thread *>::Iterator itThread =
      _threads.find(thread->getPid());

    if (itThread != _threads.end())
      _threads.erase(itThread);

    Process *p = thread->getProcess();
    delete thread;

    p->down();

    if (p->isFree())
    {
      Map<pid_t, Process *>::Iterator itProc =
        _processes.find(p->getPid());

      if (itProc != _processes.end())
        _processes.erase(itProc);

      delete p;
    }
  }

  void TaskManager::updateCurrentTime(Thread *thread)
  {
    if (!thread)
      return;

    /* FIXME update time */
  }
}
