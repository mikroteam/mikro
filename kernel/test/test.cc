#include <test_manager.hh>
#include <errors.hh>
#include <options.hh>
#include <logger.hh>

namespace test
{
  PrinterSerial printer_serial;
  PrinterJson   printer_json;
  TestManager   manager_all;
  TestManager   *manager = &manager_all;

  TestHanlde    tests [] = {
    {"irq", &TestIRQ::run},
    {"task", &TestTask::run},
    {"rbtree", &TestRBTree::run},
    {0, 0}
  };

  Printer *printer;

  void TestManager::run()
  {
    char line[512];
    int  i = 0;

    /* Passe real params */
    if (!config::options.getByName("test"))
      return;

    serial::manager->init(serial::COM_1);
    dsp::dwriter = serial::manager;
 
    printer = &printer_serial;

    while (1)
    {
      i = 0;
      line[0] = 0;
#if 0
      dsp::kout << ">";
#endif

      while (1)
      {
        serial::manager->read(serial::COM_1, line + i++, 1);
        if (line[i - 1] == '\n')
          break;
        if (i == 511)
        {
          log::fatal("Test") << "Commande Buffer Overflow";
          panic();
        }
      }
      line[i - 1] = 0;
    
      if (!_parse(line))
        dsp::kout << "Bad cmd: [" << line << "]\n";
    }

  }

  bool TestManager::_parse(char *cmd)
  {
    u8 i;

    if (!strcmp("all", cmd))
    {
      _run_all();
      return true;
    }

    for (i = 0; tests[i].name; i++)
      if (!strcmp(tests[i].name, cmd))
      {
        tests[i].func();
        return true;
      }

    return false;
  }

  void TestManager::_run_all()
  {
    u8 i;

    for (i = 0; tests[i].name; i++)
      tests[i].func();
  }
}
