#include <test/test_irq.hh>

namespace test
{
  uint32_t __lock_test_irq = 0;

}

extern "C" void test_irq_handler(struct irq::RegsDump* regs)
{
  (void)regs;
  test::__lock_test_irq = 1;
}

namespace test
{
  void TestIRQ::run()
  {
    printer->name("IRQ");
    printer->tab();

    easyIrq();

    easyPic();

    printer->tabEnd();
    printer->end();
  }

  void TestIRQ::easyIrq()
  {
    printer->name("Easy irq");
    __lock_test_irq = 0;
    irq::manager.setHandler(0, &test_irq_handler);

    asm volatile ("int $0");

    if (__lock_test_irq)
      printer->msg("True");
    else
      printer->msg("False");

    irq::manager.setHandler(0, 0);
  }

  void TestIRQ::easyPic()
  {
    printer->name("Easy pic");
    __lock_test_irq = 0;
    irq::manager.setHandler(0X40, &test_irq_handler);

    asm volatile ("hlt");

    if (__lock_test_irq)
      printer->msg("True");
    else
      printer->msg("False");

    // FIXME: restor old Handler
    irq::manager.setHandler(0X40, 0);
  }
}
