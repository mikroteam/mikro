#include <test/test_rbtree.hh>

namespace test
{
  void TestRBTree::run()
  {
    printer->name("RBTree");
    printer->tab();

    easySet();
    easyMap();

    printer->tabEnd();
    printer->end();
  }

  void TestRBTree::easySet()
  {
    printer->name("Set");
    printer->msg("True");
  }

  void TestRBTree::easyMap()
  {
    printer->name("Map");
    printer->msg("True");
  }
}
