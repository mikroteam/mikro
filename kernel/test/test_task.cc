#include <test/test_task.hh>
#include <scheduler.hh>

extern "C" void userland()
{
  /* FIXME: put real exit but message */
  asm volatile ("int $14");
  while (1)
    ;
}

namespace test
{

  void TestTask::run()
  {
    printer->name("Task");
    printer->tab();

    taskUser();

    printer->tabEnd();
    printer->end();
  }

  void TestTask::taskUser()
  {
    printer->name("task user");
    /* TODO: rebuild a test */
    printer->msg("True");
  }

}
