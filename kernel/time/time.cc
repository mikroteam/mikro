/*
 * File: time.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Generic time management
 *
 */

#include <time.hh>

namespace time
{
  TimeManager manager;
  volatile u64 jiffies;
}
