/*
 * File: string.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr> 
 *
 * Description: Useful functions of string.h
 *
 */

#include <cstring.hh>

void* memcpy(void* s1, const void* s2, size_t n)
{
  char* s1c = (char*)s1;
  char* s2c = (char*)s2;

  for ( ; n; s1c++, s2c++, n--)
    s1c[0] = s2c[0];

  return s1;
}

void* memset(void* s1, int c, size_t n)
{
  char* s1c = (char*)s1;

  for ( ; n; s1c++, n--)
    s1c[0] = (unsigned char)c;

  return s1;
}

size_t strlen(const char* str)
{
  size_t i = 0;

  for ( ; str[i]; i++)
    ;

  return i;
}

int strcmp(const char* a, const char* b)
{
  size_t i = 0;

  for ( ; a[i] && b[i]; i++)
    if (a[i] != b[i])
      return b[i] - a[i];

  return b[i] - a[i];
}

int strncmp(const char* a, const char* b, size_t len)
{
  size_t i = 0;

  for ( ; a[i] && b[i] && len; i++, len--)
    if (a[i] != b[i])
      return b[i] - a[i];

  if (len == 0)
    return 0;

  return b[i] - a[i];
}
