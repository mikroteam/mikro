/*
 * File: acpi.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: ACPI operations
 *
 */

#include <errors.hh>
#include <arch/acpi.hh>
#include <logger.hh>

namespace acpi
{

ACPIManager manager;

ACPIManager::ACPIManager()
  : rsdp(0),
    rsdt(0)
{
}

bool ACPIManager::init()
{
  rsdp = new RSDPManager();
  assert(rsdp);

  if (!rsdp->init())
  {
    log::info("ACPI") << "No ACPI Table found";
    release();
    return false;
  }

  rsdt = new RSDTManager();
  assert(rsdt);

  if (!rsdt->init(rsdp->getRSDTAddr()))
  {
    log::err("ACPI") << "Unable to find RSDT";
    release();
    return false;
  }

  return true;
}

void ACPIManager::release()
{
  if (rsdt)
  {
    delete rsdt;
    rsdt = 0;
  }

  if (rsdp)
  {
    delete rsdp;
    rsdp = 0;
  }
}

}
