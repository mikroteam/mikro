/*
 * File: acpi_common.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Common struct and functions for ACPI
 *
 */

#include <cstring.hh>
#include <paging.hh>
#include <arch/acpi_rsdt.hh>

#include <arch/acpi_common.hh>

namespace acpi
{

ACPITable::~ACPITable()
{
  release();
}

int ACPITable::findACPITable(RSDTManager* rsdt, const char* sign)
{
  int entries = rsdt->getEntryNumber();

  for (int i = 0; i < entries; i++)
  {
    void* table_addr = rsdt->getPointerToEntry(i);
    _page_addr = mem::kernelAD.mmap(0, mem::tools::alignOnPage(table_addr), 1, mem::READ);
    u32 offset = (char*)table_addr - (char*)mem::tools::alignOnPage(table_addr);
    _table_addr = (char*)_page_addr + offset;
    struct ACPIHeader* h = getTableAddr<struct ACPIHeader*>();

    if (!strncmp(h->sig, sign, ACPI_TABLE_SIGN_STR_LEN))
    {
      // Check that one page is enough
      if (h->len > PAGE_SIZE)
      {
        mem::kernelAD.munmap(_page_addr);
        _page_addr = mem::kernelAD.mmap(0, mem::tools::alignOnPage(table_addr),
                                           mem::tools::length2nrPages(h->len),
                                           mem::READ);
        _table_addr = (char*)_page_addr + offset;
      }

      // Checksum verification
      if (acpi::checksum(h, h->len) != 0)
      {
        mem::kernelAD.munmap(_page_addr);
        return 0;
      }

      return 1;
    }

    mem::kernelAD.munmap(_page_addr);
  }

  return 0;
}

void ACPITable::release()
{
  mem::kernelAD.munmap(_page_addr);
  _page_addr = 0;
  _table_addr = 0;
}

u8 checksum(const void* st, int len)
{
  const u8* data = (const u8*)st;
  int sum = 0;

  for(int i = 0; i < len; i++)
    sum += data[i];

  return sum & 0xff;
}

}
