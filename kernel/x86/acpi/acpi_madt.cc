/*
 * File: acpi_madt.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Multiple APIC Description Table
 *
 */

#include <logger.hh>
#include <errors.hh>
#include <arch/lapic_manager.hh>
#include <arch/acpi_rsdt.hh>

#include <arch/acpi_madt.hh>

namespace acpi
{

MADTManager madt;

MADTManager::parse_function MADTManager::_parse_entry_table[] =
{
  &MADTManager::_parseLAPIC,
  &MADTManager::_parseIOAPIC,
  &MADTManager::_parseISO,
  &MADTManager::_parseNMI,
  &MADTManager::_parseLAPICNMI,
  &MADTManager::_parseLAPICOverride
};

MADTManager::MADTManager()
  : _cpu_count (0),
    _ioapic_count (0),
    _apic_ids (0),
    _lapic_addr (0)
{
}

MADTManager::~MADTManager()
{
  if (_apic_ids)
    delete[] _apic_ids;
  if (_cpu_acpi_ids)
    delete[] _cpu_acpi_ids;
}

bool MADTManager::init(RSDTManager* rsdp)
{
  _apic_ids = new u8[MAX_CPU_NUMBER];
  assert(_apic_ids);
  _cpu_acpi_ids = new u8[MAX_CPU_NUMBER];
  assert(_cpu_acpi_ids);
  return findACPITable(rsdp, ACPI_MADT_SIGN_STR);
}

bool MADTManager::_parseLAPIC(struct MadtController* ctrl)
{
  struct MadtLAPICController* ctrl_cpu =
         reinterpret_cast<struct MadtLAPICController*>(ctrl);

  log::debug("MADT") << "Found a CPU with ID : " << (unsigned int)ctrl_cpu->apic_id
                     << " enabled : " << (u32)(ctrl_cpu->flags & ACPI_MADT_CPU_ENABLE);
  if (ctrl_cpu->flags & ACPI_MADT_CPU_ENABLE)
  {
    if (_cpu_count == MAX_CPU_NUMBER)
      return false;
    _apic_ids[_cpu_count++] = ctrl_cpu->apic_id;
    _cpu_acpi_ids[ctrl_cpu->apic_id] = ctrl_cpu->acpi_id;
  }

  return true;
}

bool MADTManager::_parseIOAPIC(struct MadtController* ctrl)
{
  struct MadtIOAPICController* ctrl_ioapic =
         reinterpret_cast<struct MadtIOAPICController*>(ctrl);

  log::debug("MADT") << "Found an IOAPIC with ID: " << (unsigned int)ctrl_ioapic->ioapic_id
                     << " at addr : " << dsp::hex << ctrl_ioapic->ioapic_addr << dsp::dec
                     << " base IRQ : " << ctrl_ioapic->gsib;
  _ioapic_count++;
  return true;
}

bool MADTManager::_parseISO(struct MadtController* ctrl)
{
  struct MadtISOController* ctrl_iso =
         reinterpret_cast<struct MadtISOController*>(ctrl);

  log::debug("MADT") << "Found an ISO from " << ctrl_iso->source
                     << " to " << ctrl_iso->gsi;

  return true;
}

bool MADTManager::_parseNMI(struct MadtController* ctrl)
{
  struct MadtNMIController* ctrl_nmi =
         reinterpret_cast<struct MadtNMIController*>(ctrl);

  log::debug("MADT") << "Found an NMI at " << ctrl_nmi->gsi;
  return true;
}

bool MADTManager::_parseLAPICNMI(struct MadtController* ctrl)
{
  struct MadtLAPICNMIController* ctrl_lapic_nmi =
         reinterpret_cast<struct MadtLAPICNMIController*>(ctrl);

  log::debug("MADT") << "Found a LAPIC NMI at " << ctrl_lapic_nmi->lapic_int
                     << " for processor " << ctrl_lapic_nmi->acpi_proc_id;
  return true;
}

bool MADTManager::_parseLAPICOverride(struct MadtController* ctrl)
{
  struct MadtLAPICOverrideController* ctrl_lapic_override =
         reinterpret_cast<struct MadtLAPICOverrideController*>(ctrl);

  log::debug("MADT") << "Found a LAPIC Override for addr " << (u32)ctrl_lapic_override->addr;
  _lapic_addr = ctrl_lapic_override->addr;
  return true;
}

void MADTManager::parse()
{
  struct Madt* madt_ptr = getTableAddr<struct Madt*>();
  int len = madt_ptr->header.len - sizeof (struct ACPIHeader) - 2 * sizeof (u32);
  char* ctrl_ptr = &(madt_ptr->interrupt_controllers[0]);

  _lapic_addr = madt_ptr->local_interrupt_controller_addr;
  while (len)
  {
    struct MadtController* ctrl = (struct MadtController*)(ctrl_ptr);

    if (ctrl->type <= ACPI_MADT_CTRL_LAPIC_OVERRIDE)
      (this->*_parse_entry_table[ctrl->type])(ctrl);

    len -= ctrl->len;
    ctrl_ptr += ctrl->len;
  }
}

bool MADTManager::_configureIOAPIC(struct MadtIOAPICController* ctrl_ioapic,
                                   smp::IOAPICManager* ioapic_m)
{
  smp::IOAPIC* ioapic = new smp::IOAPIC(ctrl_ioapic->ioapic_id,
                                         ctrl_ioapic->ioapic_addr,
                                         ctrl_ioapic->gsib);
  assert(ioapic);
  ioapic_m->addIOAPIC(ioapic);
  return true;
}

enum smp::IOAPIC::Polarity convertPolarityForIOAPIC(u16 polarity)
{
  switch (polarity)
  {
    case ISO_POLARITY_HIGH:
      return smp::IOAPIC::HighActive;
    case ISO_POLARITY_LOW:
      return smp::IOAPIC::LowActive;
    default:
      log::err("MADT") << "Unknown polarity, default to HIGH";
      return smp::IOAPIC::HighActive;
  }
}

enum smp::IOAPIC::TriggerMode convertTriggerModeForIOAPIC(u16 trigger)
{
  switch (trigger)
  {
    case ISO_TRIGGER_EDGE:
      return smp::IOAPIC::Edge;
    case ISO_TRIGGER_LEVEL:
      return smp::IOAPIC::Level;
    default:
      log::err("MADT") << "Unknown Trigger Mode, default to edge";
      return smp::IOAPIC::Edge;
  }
}

bool MADTManager::_configureISO(struct MadtISOController* iso,
                                smp::IOAPICManager* ioapic_m)
{
  u16 polarity = iso->polarity;
  u16 trigger = iso->trigger;

  // Default settings for ISA
  if (polarity == ISO_POLARITY_CONFORM)
    polarity = ISO_POLARITY_HIGH;
  if (trigger == ISO_TRIGGER_CONFORM)
    trigger = ISO_TRIGGER_EDGE;

  ioapic_m->addInterruptRedirection(iso->gsi, iso->source);
  ioapic_m->configureLine(iso->gsi,
                          smp::IOAPIC::Low,
                          convertPolarityForIOAPIC(polarity),
                          convertTriggerModeForIOAPIC(trigger));
  return true;
}

bool MADTManager::_configureNMI(struct MadtNMIController* nmi,
                                smp::IOAPICManager* ioapic_m)
{
  u16 polarity = nmi->polarity;
  u16 trigger = nmi->trigger;

  if (polarity == ISO_POLARITY_CONFORM)
  {
    log::warn("MADT") << "Unknown polarity for NMI, default to HIGH";
    polarity = ISO_POLARITY_HIGH;
  }
  if (trigger == ISO_TRIGGER_CONFORM)
  {
    log::warn("MADT") << "Unknown trigger mode for NMI, default to EDGE";
    trigger = ISO_TRIGGER_EDGE;
  }

  ioapic_m->configureLine(nmi->gsi,
                          smp::IOAPIC::NMI,
                          convertPolarityForIOAPIC(polarity),
                          convertTriggerModeForIOAPIC(trigger));
  return true;
}

bool MADTManager::configureIOAPICs(smp::IOAPICManager* ioapic_m)
{
  struct Madt* madt_ptr = getTableAddr<struct Madt*>();
  int len = madt_ptr->header.len - sizeof (struct ACPIHeader) - 2 * sizeof (u32);
  char* ctrl_ptr = &(madt_ptr->interrupt_controllers[0]);

  while (len)
  {
    struct MadtController* ctrl = (struct MadtController*)(ctrl_ptr);

    if (ctrl->type == ACPI_MADT_CTRL_IOAPIC)
      if (!_configureIOAPIC(reinterpret_cast<struct MadtIOAPICController*>(ctrl),
                            ioapic_m))
        return false;

    len -= ctrl->len;
    ctrl_ptr += ctrl->len;
  }

  return true;
}

bool MADTManager::configureIOAPICsLines(smp::IOAPICManager* ioapic_m)
{
  struct Madt* madt_ptr = getTableAddr<struct Madt*>();
  int len = madt_ptr->header.len - sizeof (struct ACPIHeader) - 2 * sizeof (u32);
  char* ctrl_ptr = &(madt_ptr->interrupt_controllers[0]);

  while (len)
  {
    struct MadtController* ctrl = (struct MadtController*)(ctrl_ptr);

    if (ctrl->type == ACPI_MADT_CTRL_ISO)
    {
      if (!_configureISO(reinterpret_cast<struct MadtISOController*>(ctrl),
                         ioapic_m))
        return false;
    }
    else if (ctrl->type == ACPI_MADT_CTRL_NMI)
    {
      if (!_configureNMI(reinterpret_cast<struct MadtNMIController*>(ctrl),
                         ioapic_m))
        return false;
    }

    len -= ctrl->len;
    ctrl_ptr += ctrl->len;
  }

  return true;
}

enum smp::LAPICManager::Polarity convertPolarityForLAPIC(u16 polarity)
{
  switch (polarity)
  {
    case ISO_POLARITY_HIGH:
      return smp::LAPICManager::HighActive;
    case ISO_POLARITY_LOW:
      return smp::LAPICManager::LowActive;
    default:
      log::err("MADT") << "Unknown polarity, default to HIGH";
      return smp::LAPICManager::HighActive;
  }
}

enum smp::LAPICManager::TriggerMode convertTriggerModeForLAPIC(u16 trigger)
{
  switch (trigger)
  {
    case ISO_TRIGGER_EDGE:
      return smp::LAPICManager::Edge;
    case ISO_TRIGGER_LEVEL:
      return smp::LAPICManager::Level;
    default:
      log::err("MADT") << "Unknown Trigger Mode, default to edge";
      return smp::LAPICManager::Edge;
  }
}
bool MADTManager::_configureLAPICNMI(struct MadtLAPICNMIController* nmi,
                                     u8 cpu_acpi_id)
{
  if (nmi->acpi_proc_id != ACPI_ALL_CPU && cpu_acpi_id != nmi->acpi_proc_id)
    return true;

  u16 polarity = nmi->polarity;
  u16 trigger = nmi->trigger;

  // FIXME: Apparently this has to be treated as ISA bus ?
  if (polarity == ISO_POLARITY_CONFORM)
    polarity = ISO_POLARITY_HIGH;
  if (trigger == ISO_TRIGGER_CONFORM)
    trigger = ISO_TRIGGER_EDGE;

  smp::lapic->configureLine(nmi->lapic_int,
                            smp::LAPICManager::Fixed,
                            convertPolarityForLAPIC(polarity),
                            convertTriggerModeForLAPIC(trigger));
  return true;
}

bool MADTManager::configureLAPICLines()
{
  struct Madt* madt_ptr = getTableAddr<struct Madt*>();
  int len = madt_ptr->header.len - sizeof (struct ACPIHeader) - 2 * sizeof (u32);
  char* ctrl_ptr = &(madt_ptr->interrupt_controllers[0]);

  // Retrieve acpi id of the cpu
  u8 cpu_acpi_id = _cpu_acpi_ids[smp::lapic->getAPICId()];

  while (len)
  {
    struct MadtController* ctrl = (struct MadtController*)(ctrl_ptr);

    if (ctrl->type == ACPI_MADT_CTRL_LAPIC_NMI)
    {
      if (!_configureLAPICNMI(reinterpret_cast<struct MadtLAPICNMIController*>(ctrl),
                              cpu_acpi_id))
        return false;
    }

    len -= ctrl->len;
    ctrl_ptr += ctrl->len;
  }

  return true;
}

}
