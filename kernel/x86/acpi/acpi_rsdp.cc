/*
 * File: acpi_rsdp.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Root System Descriptor Pointer
 *
 */

#include <cstring.hh>
#include <paging.hh>
#include <arch/bios.hh>

#include <arch/acpi_rsdp.hh>
#include <arch/acpi_common.hh>

namespace acpi
{

RSDPManager rsdp;

RSDPManager::RSDPManager()
  : _rsdp_page_addr (0),
    _rsdp_ptr (0)
{
}

RSDPManager::~RSDPManager()
{
  release();
}

void RSDPManager::release()
{
  if (_rsdp_page_addr)
    mem::kernelAD.munmap(_rsdp_page_addr);
}

bool RSDPManager::init()
{
  /*
   * According to spec, search in:
   * 1/ the first 1K of the ebda
   * 2/ 0xe0000 -> 0xfffff
   */

  size_t ebda_addr = bios::getEBDAPhysAddr();

  if (ebda_addr)
  {
    _rsdp_page_addr = mem::kernelAD.mmap(0, (void*)ebda_addr, 1, mem::READ);
    if (_rsdp_page_addr == 0)
      return false;

    _rsdp_ptr = _searchRSDPinZone(_rsdp_page_addr, 0x400);
    if (_rsdp_ptr)
      return true;

    mem::kernelAD.munmap(_rsdp_page_addr);
  }

  _rsdp_page_addr = mem::kernelAD.mmap(0, (void*)0xe0000, 0x20000 / PAGE_SIZE, mem::READ);
  if (_rsdp_page_addr == 0)
    return false;

  _rsdp_ptr = _searchRSDPinZone(_rsdp_page_addr, 0x20000);
  if (_rsdp_ptr)
  {
    void* phys_rsdp = (void*)((char*)_rsdp_ptr - (char*)_rsdp_page_addr + 0xe0000);
    void* phys_aligned_addr = mem::tools::alignOnPage(phys_rsdp);
    u32 offset = (char*)phys_rsdp - (char*)phys_aligned_addr;

    mem::kernelAD.munmap(_rsdp_page_addr);
    _rsdp_page_addr = mem::kernelAD.mmap(0, phys_aligned_addr, 1, mem::READ);
    _rsdp_ptr = (struct Rsdp*)((char*)_rsdp_page_addr + offset);
    return true;
  }

  mem::kernelAD.munmap(_rsdp_page_addr);
  return false;
}

struct Rsdp* RSDPManager::_searchRSDPinZone(void* addr, size_t len)
{
  char* addr_ptr = (char*)addr;
  char* limit = addr_ptr + len;

  for ( ; addr_ptr < limit; addr_ptr += 16)
  {
    if (addr_ptr[0] == ACPI_RSDP_SIGN_STR[0])
    {
      if (strncmp(addr_ptr, ACPI_RSDP_SIGN_STR, ACPI_RSDP_SIGN_STR_LEN) == 0
          && acpi::checksum(addr_ptr, 20) == 0)
        return (struct Rsdp*)addr_ptr;
    }
  }
  return 0;
}
}
