/*
 * File: acpi_rsdt.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Root System Descriptor Table
 *
 */

#include <paging.hh>

#include <arch/acpi_rsdt.hh>

namespace acpi
{

RSDTManager rsdt;

RSDTManager::~RSDTManager()
{
  release();
}

bool RSDTManager::init(void* addr)
{
  _page_addr = mem::kernelAD.mmap(0, mem::tools::alignOnPage(addr), 1,
      mem::READ);
  if (_page_addr == 0)
    return false;

  u32 offset = (char*)addr - (char*)mem::tools::alignOnPage(addr);
  _rsdt_ptr = reinterpret_cast<struct Rsdt*>((char*)_page_addr + offset);

  if (strncmp(_rsdt_ptr->header.sig, ACPI_RSDT_SIGN_STR,
                                     ACPI_TABLE_SIGN_STR_LEN) != 0
      || (acpi::checksum(_rsdt_ptr, _rsdt_ptr->header.len) != 0))
  {
    mem::kernelAD.munmap(_page_addr);
    return false;
  }

  return true;
}

void RSDTManager::release()
{
  mem::kernelAD.munmap(_page_addr);
  _page_addr = 0;
  _rsdt_ptr = 0;
}

}
