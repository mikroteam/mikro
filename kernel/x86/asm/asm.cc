/*
 * File: asm.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Assembler functions
 *
 */

#include <arch/asm.hh>

namespace asmf
{

namespace cpu
{

bool hasCpuID()
{
  bool ret;

  __asm__ __volatile__
  (
    "pushf\t\n" // Push flags
    "pop %%eax\t\n" // Copy EFLAGS into EAX.
    "mov %%eax, %%ebx\t\n" // Then make a backup copy in EBX.
    "xorl $0x00200000, %%eax\t\n" // Flip bit 21.
    "push %%eax\t\n"
    "popf\t\n" // Copy the modified EAX back to EFLAGS.
    "pushf\t\n"
    "pop %%eax\t\n" // Copy EFLAGS into EAX again.
    "push %%ebx\t\n"
    "popf\t\n" // Reset EFLAGS
    "cmp %%eax, %%ebx\t\n"
    "jz cpuid_not_supported\t\n"
    "mov $1, %0\t\n"
    "jmp end\t\n"
    "cpuid_not_supported:\t\n"
    "mov $0, %0\t\n"
    "end:\t\n"
    : "=r" (ret) : : "%eax", "%ebx"
  );

  return ret;
}

}
}
