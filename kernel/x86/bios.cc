/*
 * File: bios.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Functions useful with a bios
 *
 */

#include <paging.hh>

#include <arch/bios.hh>

namespace bios
{

size_t getEBDAPhysAddr()
{
  void* aligned_addr = mem::tools::alignOnPage((void*)BDA_EBDA_BASE_ADDR);
  u32 offset = (char*)BDA_EBDA_BASE_ADDR - (char*)aligned_addr;
  void* virtual_addr = mem::kernelAD.mmap(0, aligned_addr, 1, mem::READ);
  u16* ebda_base_addr = (u16*)((char*)virtual_addr + offset);
  size_t res = ebda_base_addr[0] << 4;

  mem::kernelAD.munmap(virtual_addr);
  return res;
}

}
