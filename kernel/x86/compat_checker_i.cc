/*
 * File: compat_checker.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <errors.hh>
#include <arch/asm.hh>
#include <arch/compat_checker_i.hh>

void CompatibilityCheckerImpl<x86>::check()
{
  if (!asmf::cpu::hasCpuID())
    earlyAbort("CPU is too old for running Mikro, no cpuid instruction.");
}
