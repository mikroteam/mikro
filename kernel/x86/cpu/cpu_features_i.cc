/*
 * File: cpu_features.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Enable/Disable Cpu features on x86 CPU
 *
 */

#include <logger.hh>
#include <cpu_info.hh>
#include <arch/cpu_features_i.hh>
#include <arch/asm.hh>

namespace cpu
{

void CpuFeaturesImpl<x86>::init()
{
// FIXME: Enable this feature when SSE registers are saved/restored on context switch
#if 0
  if (_enableSSE())
    log::info("CPU") << "SSE enabled";
#endif
}

bool CpuFeaturesImpl<x86>::_enableSSE()
{
  if (!cpu::info.arch.hasSSE())
    return false;

  size_t cr0 = asmf::regs::get_cr0();
  cr0 &= ~(1 << 2); // Clear EM flag
  cr0 |= (1 << 1); // Set MP flag
  asmf::regs::set_cr0(cr0);

  size_t cr4 = asmf::regs::get_cr4();
  cr4 |= (1 << 9); // Enable OSFXSR
  cr4 |= (1 << 10); // Enable OSXMMEXCPT
  asmf::regs::set_cr4(cr4);

  return true;
}

}
