/*
 * File: cpu_info_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <logger.hh>
#include <cstring.hh>

#include <arch/cpu_info_i.hh>

namespace cpu
{

CpuInfoImpl<x86>::CpuInfoImpl()
  : _vendor (VENDOR_UNKNOWN),
    _logicals (0),
    _logicalsBits (0),
    _cores (0),
    _coresBits (0),
    _hthread (false)
{
}

void CpuInfoImpl<x86>::init()
{
  u32 regs[4];

  _detectVendor();
  asmf::cpu::cpuid(CPUID_FUN_STD_FEATURES, regs);

  /* No logical CPUs, no detection needed */
  if (! (CPUID_FEATURE_HTT & regs[CPUID_REGS_EDX]))
  {
    _logicals = 1;
    _cores = 1;
    return;
  }

  _detectLogicals(regs);
  _detectCores();
  _detectAPICID();

  if (_cores < _logicals)
    _hthread = true;
}

void CpuInfoImpl<x86>::printInfos()
{
  log::info("CPU") << "Logical : " << _logicals
                   << ", Cores : " << _cores
                   << ", HyperThreading : " << (_hthread ? "true" : "false");

  log::debug("CPU") << "APIC ID -> Core bits : " << _coresBits
                    << ", Logical bits : " << _logicalsBits;
}

void CpuInfoImpl<x86>::_detectVendor()
{
  u32 regs[4];
  u32 buff_str[4];

  asmf::cpu::cpuid(CPUID_FUN_VENDOR_ID, regs);

  /* Make order in the string */
  buff_str[0] = regs[CPUID_REGS_EBX];
  buff_str[1] = regs[CPUID_REGS_EDX];
  buff_str[2] = regs[CPUID_REGS_ECX];

  if (strncmp((char*)buff_str, VENDOR_INTEL_STR, VENDOR_STR_LEN) == 0)
    _vendor = VENDOR_INTEL;
  else if (strncmp((char*)buff_str, VENDOR_AMD_STR, VENDOR_STR_LEN) == 0)
    _vendor = VENDOR_AMD;
  else
  {
    log::warn("CPU") << "Unknown vendor";
    _vendor = VENDOR_UNKNOWN;
  }
}

void CpuInfoImpl<x86>::_detectLogicals(u32 regs[4])
{
  _logicals = _extractField(regs[CPUID_REGS_EBX], 16, 23);
}

void CpuInfoImpl<x86>::_detectCores()
{
  if (_vendor == VENDOR_INTEL)
  {
    u32 regs[4];

    asmf::cpu::cpuid(CPUID_FUN_INTEL_CACHE, regs);
    _cores = _extractField(regs[CPUID_REGS_EAX], 26, 31) + 1;
  }
  // FIXME: handle AMD processors
  else
    _cores = _logicals;
}

void CpuInfoImpl<x86>::_detectAPICID()
{
  if (_vendor == VENDOR_INTEL)
  {
    u32 regs[4];

    asmf::cpu::cpuid(CPUID_FUN_INTEL_PROC_TOPO, 0, regs);
    _logicalsBits = _extractField(regs[CPUID_REGS_EAX], 0, 4);
    asmf::cpu::cpuid(CPUID_FUN_INTEL_PROC_TOPO, 1, regs);
    _coresBits = _extractField(regs[CPUID_REGS_EAX], 0, 4);
  }
  // FIXME: handler AMD Processors
}

u32 CpuInfoImpl<x86>::_extractField(u32 n, u32 begin, u32 end)
{
  u32 hmask = 0xffffffff;
  u32 lmask = 0xffffffff;

  if (end < 32)
    hmask = ~(hmask << end);
  lmask = lmask << begin;

  return ((n & hmask & lmask) >> begin);
}

bool CpuInfoImpl<x86>::hasSSE()
{
  u32 regs[4];

  asmf::cpu::cpuid(CPUID_FUN_STD_FEATURES, regs);

  if (regs[CPUID_REGS_EDX] & CPUID_FEATURE_SSE)
    return true;

  return false;
}

bool CpuInfoImpl<x86>::hasAPIC()
{
  u32 regs[4];

  asmf::cpu::cpuid(CPUID_FUN_STD_FEATURES, regs);
  if (regs[CPUID_REGS_EDX] & CPUID_FEATURE_LAPIC)
    return true;

  return false;
}

bool CpuInfoImpl<x86>::hasMSR()
{
  u32 regs[4];

  asmf::cpu::cpuid(CPUID_FUN_STD_FEATURES, regs);
  if (regs[CPUID_REGS_EDX] & CPUID_FEATURE_MSR)
    return true;

  return false;
}

}
