/**
 * \file gdbstub_i.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Gdb remote stub implementation for x86
 */

#include "arch/gdbstub_i.hh"
#include "arch/irq_manager_i.hh"
#include "gdbstub.hh"

namespace debug
{

void GdbStubImpl<x86>::init()
{
  serial::manager->init(internal.getPort());

  irq::manager.setHandler(1, &irqHandler);
  irq::manager.setHandler(3, &irqHandler);

  __asm__ ("int $3");
}

bool GdbStubImpl<x86>::cont(irq::RegsDump *regs, u32 size)
{
  irq::RegsDumpI386 *archRegs = (irq::RegsDumpI386 *) regs;
  (void) size;
  archRegs->eflags &= ~0x100;

  return true;
}

bool GdbStubImpl<x86>::step(irq::RegsDump *regs, u32 size)
{
  irq::RegsDumpI386 *archRegs = (irq::RegsDumpI386 *) regs;
  (void) size;
  archRegs->eflags |= 0x100;

  return true;
}

/*
 * Register order: eax, ecx, edx, ebx, esp, ebp, esi, edi, eip, eflags, cs, ss,
 * ds, es, fs, gs
 */
bool GdbStubImpl<x86>::readRegs(irq::RegsDump *regs, u32 size)
{
  (void) size;
  irq::RegsDumpI386 *archRegs = (irq::RegsDumpI386 *) regs;
  char *cBuff = internal.buff;
  size_t realEsp = archRegs->esp + 5 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->eax, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->ecx, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->edx, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->ebx, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &realEsp, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->ebp, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->esi, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->edi, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->eip, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->eflags, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->cs, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->ss, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->ds, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->es, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->fs, cBuff, sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.mem2hex((char *) &archRegs->gs, cBuff, sizeof(size_t));

  internal.send(internal.buff, 2 * 16 * sizeof(size_t));

  return true;
}

bool GdbStubImpl<x86>::writeRegs(irq::RegsDump *regs, u32 size)
{
  if (size != 2 * 16 * sizeof(size_t) + 1)
    return false;

  irq::RegsDumpI386 *archRegs = (irq::RegsDumpI386 *) regs;
  char *cBuff = &internal.buff[1];

  internal.hex2mem(cBuff, (char *) &archRegs->eax, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->ecx, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->edx, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->ebx, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  size_t fakeEsp;
  internal.hex2mem(cBuff, (char *) &fakeEsp, 2 * sizeof(size_t));
  archRegs->esp = fakeEsp - 5 * sizeof(size_t);
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->ebp, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->esi, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->edi, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->eip, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->eflags, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->cs, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->ss, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->ds, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->es, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->fs, 2 * sizeof(size_t));
  cBuff += 2 * sizeof(size_t);

  internal.hex2mem(cBuff, (char *) &archRegs->gs, 2 * sizeof(size_t));

  internal.send("OK", 2);
  return true;
}

void GdbStubImpl<x86>::haltReason()
{
  // FIXME: this is a quite simple answer, maybe too simple
  internal.send("S05", 3);
}

void GdbStubImpl<x86>::detach(irq::RegsDump *regs)
{
  irq::RegsDumpI386 *archRegs = (irq::RegsDumpI386 *) regs;
  archRegs->eflags &= ~0x100;

  internal.send("OK", 2);
}

}
