/*
 * File: display_writer_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class that displays objects on screen
 *
 */

#include <cstring.hh>
#include <arch/asm.hh>

#include <arch/display_writer_i.hh>

namespace dsp
{

DisplayWriterX86 dwriter_x86;
DisplayWriter* dwriter = &dwriter_x86;

DisplayWriterX86::DisplayWriterX86()
  : DisplayWriter(),
    _video_mem ((char*) VIDEOMEM_VADDR),
    _cursor_c (0),
    _cursor_l (0),
    _attr (DEFAULT_ATTR)
{
}

void DisplayWriterX86::init()
{
  _moveCursor();
  _clearScreen();
}

void DisplayWriterX86::setAttr(unsigned char attr)
{
  _attr = attr;
}

void DisplayWriterX86::_moveCursor()
{
  unsigned short position = (_cursor_l * COLUMN_NUMBER) + _cursor_c;

  asmf::io::outb(0x3D4, 14);
  asmf::io::outb(0x3D5, (position >> 8));
  asmf::io::outb(0x3D4, 15);
  asmf::io::outb(0x3D5, position);
}

void DisplayWriterX86::_clearScreen()
{
  unsigned int i = 0, j = 0;

  for ( ; j < LINE_NUMBER; j++)
    for (i = 0; i < COLUMN_NUMBER; i++)
    {
      _video_mem[(j * COLUMN_NUMBER + i) * 2] = 0;
      _video_mem[(j * COLUMN_NUMBER + i) * 2 + 1] = _attr;
    }
}

void DisplayWriterX86::_displayTab()
{
  unsigned char i = 0;

  for ( ; i < 8; i++)
  {
    _video_mem[(_cursor_l * COLUMN_NUMBER + _cursor_c) * 2] = ' ';
    _video_mem[(_cursor_l * COLUMN_NUMBER + _cursor_c) * 2 + 1] = _attr;
    _cursor_c++;
    if (_cursor_c >= 80)
    {
      _cursor_l++;
      _cursor_c = 0;
    }
    if (_cursor_l >= 25)
      _cursor_l = 0;
    _moveCursor();
  }
}

void DisplayWriterX86::_scroll()
{
  memcpy(_video_mem, _video_mem + COLUMN_NUMBER * 2,
          COLUMN_NUMBER * (LINE_NUMBER - 1) * 2);

  for (unsigned int i = 0; i < COLUMN_NUMBER; i++)
  {
    _video_mem[((LINE_NUMBER - 1) * COLUMN_NUMBER + i) * 2] = 0;
    _video_mem[((LINE_NUMBER - 1) * COLUMN_NUMBER + i) * 2 + 1] = _attr;
  }
}

void DisplayWriterX86::_updateCursor()
{
  if (_cursor_c >= COLUMN_NUMBER)
  {
    _cursor_l++;
    _cursor_c = 0;
  }
  if (_cursor_l >= LINE_NUMBER)
  {
    _cursor_l = LINE_NUMBER - 1;
    _scroll();
  }
  _moveCursor();
}

void DisplayWriterX86::writeChar(char c)
{
  if (c == '\n')
  {
    _cursor_l++;
    _cursor_c = 0;
    _updateCursor();
    return;
  }
  else if (c == '\r')
  {
    _updateCursor();
    _cursor_c = 0;
    _updateCursor();
    return;
  }
  else if (c == '\t')
  {
    _displayTab();
    _updateCursor();
    return;
  }
  else
  {
    _video_mem[(_cursor_l * COLUMN_NUMBER + _cursor_c) * 2] = c;
    _video_mem[(_cursor_l * COLUMN_NUMBER + _cursor_c) * 2 + 1] = _attr;
  }

  _cursor_c++;
  _updateCursor();
}

}
