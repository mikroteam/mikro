/**
 * \file errors_i.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief All error handling
 */

#include "arch/errors_i.hh"
#include "arch/display_writer_i.hh"
#include "display_manager.hh"

extern void * _stext;
extern void * _etext;

void ErrorsImpl<x86>::funnyDisplay()
{
  u16 *curChar = (u16 *) VIDEOMEM_VADDR;

  for (u32 i = 0; i < LINE_NUMBER * COLUMN_NUMBER; i++)
  {
    *curChar &= ~0xFF00;
    *curChar |= 0x7000;
    curChar++;
  }
}

void ErrorsImpl<x86>::dumpRegisters()
{
  dsp::kout << "EAX: 0x" << dsp::hex << asmf::regs::get_eax();
  dsp::kout << " | EBX: 0x" << dsp::hex << asmf::regs::get_ebx();
  dsp::kout << " | ECX: 0x" << dsp::hex << asmf::regs::get_ecx() << "\n";
  dsp::kout << "EDX: 0x" << dsp::hex << asmf::regs::get_edx();
  dsp::kout << " | ESP: 0x" << dsp::hex << asmf::regs::get_esp();
  dsp::kout << " | EBP: 0x" << dsp::hex << asmf::regs::get_ebp() << "\n";
  dsp::kout << "ESI: 0x" << dsp::hex << asmf::regs::get_esi();
  dsp::kout << " | EDI: 0x" << dsp::hex << asmf::regs::get_edi() << "\n";

  dsp::kout << "DS: 0x" << dsp::hex << asmf::regs::get_ds();
  dsp::kout << " | SS: 0x" << dsp::hex << asmf::regs::get_ss();
  dsp::kout << " | ES: 0x" << dsp::hex << asmf::regs::get_es();
  dsp::kout << " | FS: 0x" << dsp::hex << asmf::regs::get_fs();
  dsp::kout << " | GS: 0x" << dsp::hex << asmf::regs::get_gs() << "\n";

  dsp::kout << "CR0: 0x" << dsp::hex << asmf::regs::get_cr0();
  dsp::kout << " | CR3: 0x" << dsp::hex << asmf::regs::get_cr3() << "\n";
}

void ErrorsImpl<x86>::dumpStack()
{
  int nrDump = 0;
  size_t stackMin = asmf::regs::get_esp() & ~0xFFF;
  size_t stackMax = stackMin | 0xFFF;

  size_t curEBP = asmf::regs::get_ebp();
  size_t *EIP;
  size_t *EBP;

  while (curEBP >= stackMin && curEBP <= stackMax &&
      curEBP + sizeof(size_t) <= stackMax)
  {
    EIP = (size_t *) (curEBP + sizeof(size_t));
    if (*EIP < (size_t) &_stext || *EIP >= (size_t) &_etext)
      break;

    dsp::kout << "0x" << dsp::hex << *EIP << " ";
    if (++nrDump == 7)
    {
     dsp::kout << "\n";
     nrDump = 0;
    }

    EBP = (size_t *) curEBP;
    curEBP = *EBP;
  }

  if (nrDump)
    dsp::kout << "\n";
}
