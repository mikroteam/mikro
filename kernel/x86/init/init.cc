/*
 * File: init.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Init functions for x86
 *
 */

#include <serial_manager.hh>
#include <display_manager.hh>
#include <memory_manager.hh>
#include <irq_manager.hh>
#include <smp_manager.hh>
#include <arch/percpu.hh>
#include <cpu_info.hh>
#include <cpu.hh>
#include <cpu_features.hh>
#include <task_manager.hh>
#include <paging.hh>
#include <errors.hh>
#include <logger.hh>
#include <arch/acpi.hh>
#include <gdbstub.hh>
#include <syscall.hh>
#include <options.hh>
#include <arch/timer_i.hh>

#include <init.hh>

int initBsp(multiboot_info_t* info)
{
  config::options.init((char*)info->cmdline);

  serial::manager->init();

  cpu::info.init();
  mem::manager.arch.initEarly();
  irq::manager.init();

  if (config::options.getByName("gdb"))
    debug::gdbStub.init();

  mem::initPaging(info);
  mem::manager.init();

  acpi::manager.init();
  smp::info->init();
  irq::manager.initGlobalController();
  time::init();
  syscall::manager.init();

  smp::manager.init();
  mem::initSMPPaging();
  smp::manager.wakeAPs();

  cpu::features.init();
  task::manager.init();

  return 1;
}

int initAp(u32 id)
{
  cpu::info.init();
  irq::manager.initAp();
  mem::manager.init();
  smp::initCpuVar(id);
  smp::lapic->init();
  smp::lapic->initPeriodic();
  smp::info->configureLAPICLines();
  cpu::features.init();
  time::init();
  task::manager.init();

  return 1;
}
