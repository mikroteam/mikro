/*
 * File: pic_manager.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class that manages the 8259PIC
 *
 */

#include <logger.hh>
#include <arch/asm.hh>
#include <arch/irq_manager_i.hh>
#include <arch/pic_manager.hh>

#include <irq_manager.hh>

namespace irq
{

void PicManager::init()
{
  for (u8 i = 0; i < PIC_INPUTS * 2; i++)
  {
    irq::manager.setInterruptController(IRQ_VECTOR_PIC_MASTER_BASE + i, this);
  }

  /* ICW1 */
  asmf::io::outb(PIC_MASTER_PA, PIC_ICW1 | PIC_ICW1_ICW4_PRESENT
                                | PIC_ICW1_CASCADE_MODE
                                | PIC_ICW1_EDGE_TRIGGER);
  asmf::io::outb(PIC_SLAVE_PA, PIC_ICW1 | PIC_ICW1_ICW4_PRESENT
                                | PIC_ICW1_CASCADE_MODE
                                | PIC_ICW1_EDGE_TRIGGER);
  /* ICW2 */
  asmf::io::outb(PIC_MASTER_PB, IRQ_VECTOR_PIC_MASTER_BASE);
  asmf::io::outb(PIC_SLAVE_PB, IRQ_VECTOR_PIC_SLAVE_BASE);
  /* ICW3 */
  asmf::io::outb(PIC_MASTER_PB, PIC_ICW3_MASTER_SLAVE_INPUT);
  asmf::io::outb(PIC_SLAVE_PB, PIC_ICW3_SLAVE_ID);
  /* ICW4 */
  asmf::io::outb(PIC_MASTER_PB, PIC_ICW4_MODE_8086 | PIC_ICW4_MASTER);
  asmf::io::outb(PIC_SLAVE_PB, PIC_ICW4_MODE_8086 | PIC_ICW4_SLAVE);

  /* Mask all IRQs */
  asmf::io::outb(PIC_MASTER_PB, 0xff);
  asmf::io::outb(PIC_SLAVE_PB, 0xff);
}

void PicManager::disable()
{
  /* Mask all IRQs */
  asmf::io::outb(PIC_MASTER_PB, 0xff);
  asmf::io::outb(PIC_SLAVE_PB, 0xff);
}

void PicManager::unmask(u8 num)
{
  unsigned char actual = 0;

  num -= IRQ_VECTOR_PIC_MASTER_BASE;
  if (num < 8)
  {
    actual = asmf::io::inb(PIC_MASTER_PB);
    asmf::io::outb(PIC_MASTER_PB, actual & ~(1 << num));
  }
  else if (num < 16)
  {
    actual = asmf::io::inb(PIC_SLAVE_PB);
    asmf::io::outb(PIC_SLAVE_PB, actual & ~(1 << (num - 8)));
  }
  else
  {
    log::err("PIC") << "Incorrect IRQ to mask";
  }
}

void PicManager::mask(u8 num)
{
  unsigned char actual = 0;

  num -= IRQ_VECTOR_PIC_MASTER_BASE;
  if (num < 8)
  {
    actual = asmf::io::inb(PIC_MASTER_PB);
    asmf::io::outb(PIC_MASTER_PB, actual | (1 << num));
  }
  else if (num < 16)
  {
    actual = asmf::io::inb(PIC_SLAVE_PB);
    asmf::io::outb(PIC_SLAVE_PB, actual | (1 << (num - 8)));
  }
  else
  {
    log::err("PIC") << "Incorrect IRQ to mask";
  }

}

void PicManager::acknowledge(u8 intr)
{
  intr -= IRQ_VECTOR_PIC_MASTER_BASE;

  if (intr < 8)
  {
    asmf::io::outb(PIC_MASTER_PA, PIC_ACK);
  }
  else if (intr < 16)
  {
    asmf::io::outb(PIC_MASTER_PA, PIC_ACK);
    asmf::io::outb(PIC_SLAVE_PA, PIC_ACK);
  }
  else
  {
    log::err("PIC") << "Bad EOI";
  }
}

}
