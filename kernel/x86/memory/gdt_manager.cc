/*
 * File: gdt_manager.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Class implementation that manages the gdt
 *
 */

#include <logger.hh>
#include <arch/asm.hh>
#include <arch/gdt_manager.hh>

void GDTManager::setGate(struct GDTEntry* gdt, u32 index, u32 base,
                         u32 limit, u8 access, u8 granularity)
{
  if (index >= GDT_SIZE)
    return;

  gdt[index].base_addr_0_15 = (base & 0xFFFF);
  gdt[index].base_addr_16_23 = (base >> 16) & 0xFF;
  gdt[index].base_addr_24_31 = (base >> 24) & 0xFF;
  gdt[index].seg_limit_0_15 = (limit & 0xFFFF);
  gdt[index].seg_limit_16_19 = (limit >> 16);
  gdt[index].granularity = granularity;
  gdt[index].op_size = 1;
  gdt[index].access = access;
  gdt[index].present = 1;
  gdt[index].avl = 0;
  gdt[index].l = 0;
}

void GDTDynamicManager::setGate(u32 index, u32 base, u32 limit, u8 access, u8 granularity)
{
  struct GDTPtr gdtptr;
  struct GDTEntry* gdt;

  asmf::mem::sgdt(&gdtptr);
  gdt = (struct GDTEntry*)(gdtptr.base_addr);

  GDTManager::setGate(gdt, index, base, limit, access, granularity);
}

void GDTManager::applyGDT(struct GDTEntry* gdt)
{
  struct GDTPtr gdtptr;

  gdtptr.base_addr = (size_t)gdt;
  gdtptr.limit = (sizeof (struct GDTEntry) * GDT_SIZE) - 1;

  asmf::mem::lgdt(reinterpret_cast<void*>(&gdtptr));

  asmf::regs::set_ds(KERNEL_DATA_SEL);
  asmf::regs::set_ss(KERNEL_DATA_SEL);
  asmf::regs::set_es(KERNEL_DATA_SEL);
  asmf::regs::set_fs(KERNEL_DATA_SEL);
  asmf::regs::set_gs(KERNEL_DATA_SEL);

  asmf::regs::set_cs(KERNEL_CODE_SEL);
}

void GDTDynamicManager::init()
{
  struct GDTEntry* gdt = new struct GDTEntry[GDT_SIZE];

  GDTManager::setGate(gdt, 0, 0, 0, 0, 0);
  GDTManager::setGate(gdt, KERNEL_CODE, 0, 0xFFFFFFFF, GDT_CODE_ACCESS, 0x1);
  GDTManager::setGate(gdt, KERNEL_DATA, 0, 0xFFFFFFFF, GDT_DATA_ACCESS, 0x1);
  GDTManager::setGate(gdt, KERNEL_CPU_DATA, 0, 0xFFFFFFFF, GDT_DATA_ACCESS, 0x1);
  applyGDT(gdt);
}

void GDTStaticManager::init()
{
  GDTManager::setGate(_gdt, 0, 0, 0, 0, 0);
  GDTManager::setGate(_gdt, KERNEL_CODE, 0, 0xFFFFFFFF, GDT_CODE_ACCESS, 0x1);
  GDTManager::setGate(_gdt, KERNEL_DATA, 0, 0xFFFFFFFF, GDT_DATA_ACCESS, 0x1);
  GDTManager::setGate(_gdt, KERNEL_CPU_DATA, 0, 0xFFFFFFFF, GDT_DATA_ACCESS, 0x1);
  applyGDT(_gdt);
}
