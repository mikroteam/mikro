/*
 * File: memory_manager_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manage memory on x86
 *
 */

#include <logger.hh>

#include <arch/memory_manager_i.hh>

namespace mem
{

void MemoryManagerImpl<x86>::initEarly()
{
  gdtsm.init();
}

void MemoryManagerImpl<x86>::init()
{
  gdtm.init();
  log::info("Memory") << "init";
}

}
