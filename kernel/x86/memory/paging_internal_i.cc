/**
 * \file paging_internal_i.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief Paging entry point for x86
 */

#include "arch/asm.hh"
#include "arch/paging_internal_i.hh"
#include "config.hh"
#include "cstring.hh"
#include "logger.hh"
#include "memory/paging_init.hh"
#include "paging.hh"
#include <options.hh>

extern void * _stext;
extern void * _etext;
extern void * _srodata;
extern void * _erodata;
extern void * _sdata;
extern void * _ebss;
extern void * _bootpdt;
extern void * _bootpt1;
extern void * _extrapt;
extern void * _bootstack;

extern char *k_cmd;

#define KERNEL 0x80
#define NO_CHANGE 0x100

namespace mem
{

#ifndef MIKRO64
/* i386 case */
Mapping maps[] = {
  /* SMP init page */
  {
    KERNEL_SMP_PAGE,
    1,
    Mapping::FREEABLE | Mapping::SYS_RESERVED,
    0x0
  },

  /* Stack */
  {
    VIRT2PHYS((size_t) &_bootstack) - 0x1000,
    1,
    Mapping::FREEABLE,
    (size_t) &_bootstack - 0x1000
  },

  /* Kernel Binary */
  {
    0x0,
    0,
    Mapping::NONE,
    (size_t) &_stext
  },

  /* Page Tables */
  {
    0x0,
    KERNEL_NR_PT,
    Mapping::NONE,
    KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END
  },

  /* Page Table Directory */
  {
    KERNEL_NR_PT << 12,
    1,
    Mapping::FREEABLE,
    KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END + (KERNEL_NR_PT << 12)
  },

  /* Reserved kernel pages */
  {
    (KERNEL_NR_PT + 1) << 12,
    KRESERVE_START,
    Mapping::FREEABLE | Mapping::KERNEL_RESERVE,
    KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END + ((KERNEL_NR_PT + 1) << 12)
  },

  /* Start allocator kernel pages */
  {
    (KERNEL_NR_PT + KRESERVE_START + 1) << 12,
    KERNEL_START_NR_PAGES,
    Mapping::FREEABLE | Mapping::START_ALLOCATOR,
    KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END +
      ((KERNEL_NR_PT + KRESERVE_START + 1) << 12)
  }
};


void PagingBasics::pdt_change(Pde *pde, void *paddr_pt, u16 rights)
{
  if (rights & KERNEL)
  {
    if (rights & WRITE)
      pde->rw = 1;
    else
      pde->rw = 0;

    pde->us = 0;
  }
  else
  {
    pde->us = 1;

    if (rights & WRITE)
      pde->rw = 1;
    else
      pde->rw = 0;
  }

  // FIXME: check this
  pde->pwt = 0;
  pde->pcd = 0;
  pde->a = 0;

  // bit 7 for 4K pages
  pde->bit7 = 0;
  pde->addr = ((unsigned int) paddr_pt) >> 12;
  pde->p = 1;
}

void PagingBasics::pt_change(Pte *pte, void *paddr_page, u16 rights)
{
  if (rights & KERNEL)
  {
    if (rights & WRITE)
      pte->rw = 1;
    else
      pte->rw = 0;

    pte->us = 0;
  }
  else
  {
    pte->us = 1;

    if (rights & WRITE)
      pte->rw = 1;
    else
      pte->rw = 0;
  }

  // FIXME: check this
  pte->pwt = 0;
  pte->pcd = 0;
  pte->a = 0;
  pte->d = 0;
  pte->pat = 0;
  pte->g = 0;

  if (!(rights & NO_CHANGE))
  {
    pte->addr = ((unsigned int) paddr_page) >> 12;
    pte->p = 1;
  }
}

// TODO: invalidate issue + free all

void PageTableKernel::add(void *paddr, void *vaddr, size_t nrPages, u16 flags)
{
  Pte *pte;

  assert((u32) vaddr >= KERNEL_AS_BEGIN);
  pte = (Pte *) _pts_vaddr;
  pte += ((u32) vaddr - KERNEL_AS_BEGIN) >> 12;

  for (size_t i = 0; i < nrPages; i++)
  {
    _basics.pt_change(pte, paddr, flags | KERNEL);

    paddr = (void *) ((u32) paddr + 0x1000);
    pte++;
  }
}

void PageTableKernel::change(void *vaddr, size_t nrPages, u16 rights)
{
  Pte *pte;

  assert((u32) vaddr >= KERNEL_AS_BEGIN);
  pte = (Pte *) _pts_vaddr;
  pte += ((u32) vaddr - KERNEL_AS_BEGIN) >> 12;

  for (size_t i = 0; i < nrPages; i++)
  {
    _basics.pt_change(pte, 0, rights | KERNEL | NO_CHANGE);
    asmf::mem::invlpg(vaddr);

    pte++;
  }
}

void PageTableKernel::free(void *vaddr, size_t nrPages)
{
  Pte *pte;

  assert((u32) vaddr >= KERNEL_AS_BEGIN);
  pte = (Pte *) _pts_vaddr;
  pte += ((u32) vaddr - KERNEL_AS_BEGIN) >> 12;

  for (size_t i = 0; i < nrPages; i++)
  {
    _basics.pt_delete(pte);
    asmf::mem::invlpg(vaddr);

    pte++;
    vaddr = (void *) ((u32) vaddr + 0x1000);
  }
}


void *PageTableKernel::getPaddr(void *vaddr)
{
  assert((u32) vaddr >= KERNEL_AS_BEGIN);

  Pte *pte = (Pte *) _pts_vaddr;
  pte += ((u32) vaddr - KERNEL_AS_BEGIN) >> 12;

  if (!pte->p)
    return 0;

  return (void *) ((pte->addr << 12) | ((size_t) vaddr & 0xFFF));
}

bool PageTableUser::create(void **paddr, void **vaddr)
{
  if (!pi.kASInternal.kmap(paddr, vaddr))
    return false;

  tools::clearPage(*vaddr);

  return true;
}

bool PageTableUser::add(void *pd_vaddr, void *paddr, void *vaddr,
    size_t nrPages, u16 flags)
{
  unsigned int off_tab = _basics.pt_offset(vaddr);
  Pte *pte;
  void *pt_vaddr;
  u16 flags_safe = flags & ~KERNEL;

  Pde *pde = (Pde *) pd_vaddr;
  pde += _basics.pdt_offset(vaddr);

  u32 bound_pt;

  while (nrPages)
  {
    if (nrPages + off_tab <= 0x400)
      bound_pt = nrPages;
    else
      bound_pt = 0x400 - off_tab;

    nrPages -= bound_pt;

    if (pde->p)
    {
      pt_vaddr = pi.kASInternal.temporaryMap((void *) (pde->addr << 12));
      if (!pt_vaddr)
        return false;
    }
    else
    {
      void *pt_paddr;

      if (!pi.kASInternal.kmap(&pt_paddr, &pt_vaddr,
            KMAP_TEMPORARY | KMAP_NOLOCK))
        return false;

      tools::clearPage(pt_vaddr);

      _basics.pdt_change(pde, pt_paddr, READ | WRITE | EXEC);
    }

    pte = (Pte *) pt_vaddr;
    if (off_tab)
    {
      pte += off_tab;
      off_tab = 0;
    }

    for (u32 j = 0; j < bound_pt; j++)
    {
      _basics.pt_change(pte, paddr, flags_safe);

      paddr = (void *) ((u32) paddr + 0x1000);
      pte++;
    }

    pi.kASInternal.temporaryUnmap(pt_vaddr);
    pde++;
  }

  return true;
}

bool PageTableUser::change(void *pd_vaddr, void *vaddr, size_t nrPages,
    u16 flags)
{
  unsigned int off_tab = _basics.pt_offset(vaddr);
  Pte *pte;
  void *pt_vaddr;
  u16 flags_safe = flags & ~KERNEL;

  Pde *pde = (Pde *) pd_vaddr;
  pde += _basics.pdt_offset(vaddr);

  u32 bound_pt;

  while (nrPages)
  {
    if (nrPages + off_tab <= 0x400)
      bound_pt = nrPages;
    else
      bound_pt = 0x400 - off_tab;

    nrPages -= bound_pt;

    if (pde->p)
    {
      pt_vaddr = pi.kASInternal.temporaryMap((void *) (pde->addr << 12));
      if (!pt_vaddr)
        return false;
    }
    else
      return false;

    pte = (Pte *) pt_vaddr;
    if (off_tab)
    {
      pte += off_tab;
      off_tab = 0;
    }

    for (u32 j = 0; j < bound_pt; j++)
    {
      _basics.pt_change(pte, 0, flags_safe | NO_CHANGE);
      pte++;
    }

    pi.kASInternal.temporaryUnmap(pt_vaddr);
    pde++;
  }

  return true;
}

void PageTableUser::attachKernel(void *pd_vaddr)
{
  Pde *pde = (Pde *) pd_vaddr;
  pde += (KERNEL_AS_BEGIN >> 22);

  for (u32 i = 0; i < KERNEL_NR_PT; i++)
  {
    _basics.pdt_change(pde,
        (void *) (pi.impl.pageTableKernel.getPtsPaddr() + (i << 12)),
        READ | WRITE | EXEC | KERNEL);

    pde++;
  }
}

bool PageTableUser::free(void *pd_vaddr, void *vaddr,
    size_t nrPages)
{
  unsigned int off_tab = _basics.pt_offset(vaddr);
  Pte *pte;
  void *pt_vaddr;

  Pde *pde = (Pde *) pd_vaddr;
  pde += _basics.pdt_offset(vaddr);

  u32 bound_pt;

  while (nrPages)
  {
    if (nrPages + off_tab <= 0x400)
      bound_pt = nrPages;
    else
      bound_pt = 0x400 - off_tab;

    nrPages -= bound_pt;

    if (pde->p)
    {
      if (bound_pt == 0x400)
      {
        pi.physAllocator.deallocateOne(pde->addr);

        _basics.pdt_delete(pde);
        continue;
      }
      else
      {
        pt_vaddr = pi.kASInternal.temporaryMap((void *) (pde->addr << 12));
        if (!pt_vaddr)
          return false;
      }
    }
    else
    {
      if (off_tab)
        off_tab = 0;
      continue;
    }

    pte = (Pte *) pt_vaddr;
    if (off_tab)
    {
      pte += off_tab;
      off_tab = 0;
    }

    for (u32 j = 0; j < bound_pt; j++)
    {
      _basics.pt_delete(pte);
      asmf::mem::invlpg(vaddr);
      pte++;
    }

    pi.kASInternal.temporaryUnmap(pt_vaddr);
    pde++;
  }

  return true;
}

void *PageTableUser::getPaddr(void *pd_vaddr, void *vaddr)
{
  Pde *pde = (Pde *) pd_vaddr;
  pde += _basics.pdt_offset(vaddr);

  if (!pde->p)
    return 0;

  void *pt_vaddr = pi.kASInternal.temporaryMap((void *) (pde->addr << 12));
  if (!pt_vaddr)
    return 0;

  Pte *pte = (Pte *) pt_vaddr;
  pte += _basics.pt_offset(vaddr);

  pi.kASInternal.temporaryUnmap(pt_vaddr);

  return (void *) ((pte->addr << 12) | ((size_t) vaddr & 0xFFF));
}

void PageTableUser::clear(void *pd_vaddr)
{
  Pde *pde = (Pde *) pd_vaddr;

  for (u32 i = 0; i < 0x400; i++)
    if (pde->p)
      pi.physAllocator.deallocateOne(pde->addr);

  pi.kASInternal.kunmap(pd_vaddr);
}

void PagingInternalImpl<x86>::init(multiboot_info_t *info)
{
  /* Move kernel somewhere better */
  KernelMapping kmap;
  if (!moveKernel(info, kmap))
  {
    log::fatal("Paging") << "Error cannot move kernel.";
    panic();
  }

  /* Init mappings */
  maps[2].paddr = kmap.paddr;
  maps[2].nrPages = kmap.binSize >> 12;
  for (int i = 3; i < 7; i++)
    maps[i].paddr += kmap.paddr + kmap.binSize;

  PagingInit pagingInit(maps, 7, KERNEL_ZONE_NORMAL_START);

  /* Other init */
  if (!pagingInit.initStartAllocator())
  {
    log::fatal("Paging") << "Error while initializing kernel start allocator.";
    panic();
  }

  if (!pagingInit.setReserved(info, 0xFFFFFFFF))
  {
    log::fatal("Paging") << "Error while getting system reserved areas.";
    panic();
  }

  if (!pagingInit.initialMapping())
  {
    log::fatal("Paging") << "Error while setting initial mapping.";
    panic();
  }

  /* Preallocation for physical allocator */
  Mapping *preallocMap = pagingInit.prealloc4PhysAllocator();
  preallocMap->paddr = (size_t) _tools.bestArea(info,
      kmap.paddr + kmap.binSize + kmap.dataSize, preallocMap->nrPages << 12);
  if (!preallocMap->paddr)
  {
    log::fatal("Paging") << "No room for paging medatas";
    panic();
  }
  preallocMap->vaddr = KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END +
    ((KERNEL_NR_PT + KRESERVE_START + KERNEL_START_NR_PAGES + 1) << 12);

  pageTableKernel.add((void *) preallocMap->paddr, (void *) preallocMap->vaddr,
      preallocMap->nrPages, READ | WRITE);

  /* Final init */
  if (!pagingInit.initPhysAllocators())
  {
    log::fatal("Paging") << "Error while initializing physical allocator.";
    panic();
  }

  if (!pagingInit.mapModules(info))
  {
    log::fatal("Paging") << "Error while mapping bootloader modules.";
    panic();
  }

  pagingInit.finalize();

  /* Map command line */
  if (info->flags & MULTIBOOT_INFO_CMDLINE)
  {
    size_t len = strlen((char *) info->cmdline) + 1 + 1; // +1 for double zero for end table
    k_cmd = new char[len];
    memcpy(k_cmd, (char *) info->cmdline, len);
    config::options.init(k_cmd);
  }
  else
    k_cmd = 0;

  /* Remove low memory mapping */
  // TODO: keep the address to free it later
  void *smpPt = kernelAD.mmap(0x0, 1, READ | WRITE);
  tools::clearPage(smpPt);
  Pte *pte = (Pte *) smpPt;
  pte++;
  _basics.pt_change(pte, (void *) KERNEL_SMP_PAGE, READ | WRITE | KERNEL);
  Pde *pde = (Pde *) (KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END +
    KERNEL_NR_PT * 0x1000);
  _basics.pdt_change(pde, (void *) kernelAD.getPaddr(smpPt),
      READ | WRITE | KERNEL);
  asmf::mem::reloadCR3();

  // PAGING IS INITIATED ENJOY :)
  log::info("Paging") << "init";
}

bool PagingInternalImpl<x86>::moveKernel(multiboot_info_t *info,
    KernelMapping &kmap)
{
  /*
   * KERNEL MAPPING
   * **************
   *
   * PHYSICAL MAPPING (from low to high addresses)
   * > Low memory overlapping:
   *  - SMP Page
   *  - Kernel stack
   *  - Video Memory
   * > Beginning at kmap.paddr:
   *  - Section .text
   *  - Section .rodata
   *  - Section .data
   *  - Section .bss
   *  - Kernel page tables
   *  - Kernel (temporary) page directory
   *  - Kernel reserve start pages
   *  - Start allocator pages
   *
   * VIRTUAL MEMORY (from low to high addresses, new lines are holes)
   *  - Low memory
   *
   *  - Section .text
   *  - Section .rodata
   *  - Section .data
   *  - Section .bss
   *
   *  - Kernel stack dead page
   *  - Kernel stack
   *
   *  - Video memory
   *  - Kernel page tables
   *  - Kernel (temporary) page directory
   *  - Kernel reserve start pages
   *  - Start allocator pages
   *
   */

  /* compute and find where to put kernel in memory */
  u32 textSize = ((unsigned int) &_etext - (unsigned int) &_stext + 1);
  u32 textSizeAligned = textSize + (0x1000 - (textSize & 0xFFF));
  u32 rodataSize = ((unsigned int) &_erodata - (unsigned int) &_srodata + 1);
  u32 rodataSizeAligned = rodataSize + (0x1000 - (rodataSize & 0xFFF));
  u32 dataSize = ((unsigned int) &_ebss - (unsigned int) &_sdata + 1);
  u32 dataSizeAligned = dataSize + (0x1000 - (dataSize & 0xFFF));

  kmap.binSize = textSizeAligned + rodataSizeAligned + dataSizeAligned;

  kmap.dataSize = KERNEL_NR_PT * 0x1000; // Kernel Page Tables
  kmap.dataSize += 0x1000; // Kernel Page Directory
  kmap.dataSize += KRESERVE_START * 0x1000; // Minimal reserved pages
  kmap.dataSize += KERNEL_START_NR_PAGES * 0x1000; // Start allocator pages

  u32 totSize = kmap.binSize + kmap.dataSize;

  kmap.paddr = (u32) _tools.bestArea(info, KERNEL_ZONE_NORMAL_START, totSize);
  if (!kmap.paddr)
    return false;

  /* add page table to map the final location */
  Pte *pte = (Pte *) VIRT2PHYS((u32) &_extrapt);
  u32 paddr = kmap.paddr;
  u32 j = 0;
  for (; j < (totSize >> 12); j++)
  {
    _basics.pt_change(pte, (void *) paddr, READ | WRITE | KERNEL);
    paddr += 0x1000;
    pte++;
  }

  for (; j < 0x400; j++)
  {
    _basics.pt_delete(pte);
    pte++;
  }

  Pde *pde = (Pde *) &_bootpdt;
  pde += _basics.pdt_offset((void *) KERNEL_COPY_VADDR);
  _basics.pdt_change(pde, (void *) VIRT2PHYS((u32) &_extrapt), READ |
      WRITE | KERNEL);

  /* init pd entries for kernel mapping */
  u32 dest = KERNEL_COPY_VADDR + kmap.binSize;
  u32 pt_paddr = (u32) kmap.paddr + kmap.binSize;
  pageTableKernel.init(pt_paddr, KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM_END);

  u32 pdt_paddr = pt_paddr + KERNEL_NR_PT * 0x1000;
  u32 pdt_vaddr = dest + KERNEL_NR_PT * 0x1000;
  // Add low mem start mapping
  _basics.pdt_change((Pde *) pdt_vaddr, (void *) VIRT2PHYS((u32) &_bootpt1),
      READ | WRITE | KERNEL);

  u32 *nullPde = (u32 *) pdt_vaddr;
  nullPde++;
  for (u32 i = 1; i < (KERNEL_AS_BEGIN >> 22); i++, nullPde++)
    *nullPde = 0;

  pde = (Pde *) nullPde;
  for (u32 i = 0; i < KERNEL_NR_PT; i++)
  {
    tools::clearPage((void *) dest);

    _basics.pdt_change(pde, (void *) pt_paddr, READ | WRITE |
        EXEC | KERNEL);

    pde++;
    pt_paddr += 0x1000;
    dest += 0x1000;
  }

  /* add kernel binary mapping */
  size_t page_paddr = kmap.paddr;
  addInitialMapping(
      KERNEL_COPY_VADDR + kmap.binSize,
      &page_paddr,
      (size_t) &_stext,
      (textSizeAligned >> 12),
      READ | EXEC | KERNEL
      );

  addInitialMapping(
      KERNEL_COPY_VADDR + kmap.binSize,
      &page_paddr,
      (size_t) &_srodata,
      (rodataSizeAligned >> 12),
      READ | KERNEL
      );

  addInitialMapping(
      KERNEL_COPY_VADDR + kmap.binSize,
      &page_paddr,
      (size_t) &_sdata,
      (dataSizeAligned >> 12),
      READ | WRITE | KERNEL
      );

  /* add needed page for kernel start mapping */
  page_paddr = kmap.paddr + kmap.binSize;
  addInitialMapping(
      KERNEL_COPY_VADDR + kmap.binSize,
      &page_paddr,
      pageTableKernel.getPtsVaddr(),
      (kmap.dataSize >> 12),
      READ | WRITE | KERNEL
      );

  /* add stack mapping */
  page_paddr = VIRT2PHYS((u32) &_bootstack) - 0x1000;
  addInitialMapping(
      KERNEL_COPY_VADDR + kmap.binSize,
      &page_paddr,
      (size_t) &_bootstack - 0x1000,
      1,
      READ | WRITE | KERNEL
      );

  /* add video memory mapping */
  page_paddr = KERNEL_VIDEO_MEM;
  addInitialMapping(
      KERNEL_COPY_VADDR + kmap.binSize,
      &page_paddr,
      KERNEL_AS_BEGIN + KERNEL_VIDEO_MEM,
      KERNEL_VIDEO_MEM_SIZE >> 12,
      READ | WRITE | KERNEL
      );

  /* copy kernel sections */
  dest = KERNEL_COPY_VADDR;

  memcpy((void *) dest, &_stext, textSize);
  dest += textSizeAligned;

  memcpy((void *) dest, &_srodata, rodataSize);
  dest += rodataSizeAligned;

  memcpy((void *) dest, &_sdata, dataSize);
  dest += dataSizeAligned;

  /* load new page directory */
  asmf::regs::set_cr3(pdt_paddr);

  return true;
}

void PagingInternalImpl<x86>::addInitialMapping(size_t pt_paddr, size_t *paddr,
    size_t vaddr, size_t nrPages, u16 flags)
{
  Pte *pte = (Pte *) pt_paddr;
  pte += (vaddr - KERNEL_AS_BEGIN) >> 12;
  for (u32 i = 0; i < nrPages; i++)
  {
    _basics.pt_change(pte, (void *) *paddr, flags);

    pte++;
    *paddr += 0x1000;
  }
}

#endif /* !MIKRO64 */

}
