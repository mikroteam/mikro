/*
 * File: ioapic_manager.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manage multiple IOAPICs
 *
 */

#include <errors.hh>
#include <paging.hh>
#include <irq_manager.hh>

#include <arch/lapic_manager.hh>
#include <arch/ioapic_manager.hh>

namespace smp
{

static u8* _interrupt_redirection = 0;

IOAPIC::IOAPIC(u8 id, uint_ptr addr, u8 irq_base)
  : _id (id),
    _first_irq (irq_base)
{
  assert(addr);
  _addr = (volatile char*)mem::kernelAD.mmap(0, (void*)addr, 1, mem::READ | mem::WRITE);
  assert(_addr);

  _last_irq = irq_base + _getMaxRedirectionEntry();
}

IOAPIC::~IOAPIC()
{
  // _addr cannot be zero at this point
  mem::kernelAD.munmap((void*)_addr);
}

void IOAPIC::init()
{
  for (u8 i = 0; i < _last_irq - _first_irq; i++)
  {
    mask(IOAPIC_IRQ_VECTOR_0 + _first_irq + i);
    irq::manager.setInterruptController(IOAPIC_IRQ_VECTOR_0 + _first_irq + i, this);
  }
}

void IOAPIC::mask(u8 intr)
{
  intr -= (IOAPIC_IRQ_VECTOR_0 + _first_irq);
  assert(intr < 24);

  u32 reg = _getReg(IOAPIC_REG_IOREDTBL + 2 * intr);
  reg |= IOAPIC_MASK_IRQ;
  _setReg(IOAPIC_REG_IOREDTBL + 2 * intr, reg);
}

void IOAPIC::unmask(u8 intr)
{
  intr -= (IOAPIC_IRQ_VECTOR_0 + _first_irq);
  assert(intr < 24);

  u32 reg = _getReg(IOAPIC_REG_IOREDTBL + 2 * intr);
  reg &= ~IOAPIC_MASK_IRQ;
  _setReg(IOAPIC_REG_IOREDTBL + 2 * intr, reg);
}

void IOAPIC::acknowledge(u8 intr)
{
  // Acknowledgment is done via lapic
  smp::lapic->acknowledge(intr);
}

void IOAPIC::configureLine(u8 intr,
                           enum DeliveryMode d,
                           enum Polarity p,
                           enum TriggerMode t)
{
  u64 reg = 0;
  u8 vect = _interrupt_redirection[intr] + IOAPIC_IRQ_VECTOR_0;

  if (d == SMI || d == NMI)
    // Must be set to zero according to the spec
    vect = 0;

  if (d == SMI || d == NMI || d == INIT || d == ExtINT)
  {
    if (t != Edge)
    {
      t = Edge;
      log::warn("IOAPIC") << "Trigger mode with this delivery mode must be Edge";
    }
  }

  reg = vect | (d << 8) | (Logical << 11) | (p << 13)
             | IOAPIC_MASK_IRQ | (t << 15) | (0xffLL << 56);

  _setReg(IOAPIC_REG_IOREDTBL + (intr - _first_irq) * 2, (u32)reg);
  _setReg(IOAPIC_REG_IOREDTBL + (intr - _first_irq) * 2 + 1, (u32)(reg >> 32));
  unmask(intr + IOAPIC_IRQ_VECTOR_0);
}

void IOAPIC::redirectIRQtoCPU(u8 intr, enum IOAPIC::DestinationMode d, u8 dest)
{
  intr -= _first_irq;
  u32 low = _getReg(IOAPIC_REG_IOREDTBL + intr * 2);
  u32 high = _getReg(IOAPIC_REG_IOREDTBL + intr * 2 + 1);

  low = (low & ~(1 << 11)) | (d << 11);
  high = (high & ~(0xff << (56 - 32))) | (dest << (56 - 32));
  _setReg(IOAPIC_REG_IOREDTBL + intr * 2, low);
  _setReg(IOAPIC_REG_IOREDTBL + intr * 2 + 1, high);
}

u8 IOAPIC::_getMaxRedirectionEntry()
{
  return (_getReg(IOAPIC_REG_IOAPICVER) >> IOAPIC_REDIR_OFFSET) & 0xff;
}

void IOAPIC::_setReg(u32 reg, u32 value)
{
  *(volatile u32*)(_addr + IOAPIC_IOREGSEL) = reg;
  *(volatile u32*)(_addr + IOAPIC_IOWIN) = value;
}

u32 IOAPIC::_getReg(u32 reg)
{
  *(volatile u32*)(_addr + IOAPIC_IOREGSEL) = reg;
  return *(volatile u32*)(_addr + IOAPIC_IOWIN);
}

IOAPICManager::IOAPICManager()
  : _max_irq (0),
    _ioapic_nb (0),
    _ioapics (0)
{
}

IOAPICManager::~IOAPICManager()
{
  for (u8 i = 0; i < _ioapic_nb; i++)
    delete _ioapics[i];
}

void IOAPICManager::init()
{
  for (u8 i = 0; i < _ioapic_nb; i++)
    _ioapics[i]->init();
}

void IOAPICManager::setIOAPICnumber(u8 ioapic_number)
{
  _ioapic_nb = ioapic_number;

  _ioapics = new IOAPIC*[ioapic_number];
  assert(_ioapics);
  _interrupt_redirection = new u8[IOAPIC_MASK_IRQ * ioapic_number];
  assert(_interrupt_redirection);
  for (u32 i = 0; i < IOAPIC_MASK_IRQ * ioapic_number; i++)
    _interrupt_redirection[i] = i;
}

void IOAPICManager::mask(u8 intr)
{
  u8 local_intr = intr - IOAPIC_IRQ_VECTOR_0;
  assert(local_intr < _max_irq);

  for (u8 i = 0; i < _ioapic_nb; i++)
  {
    if (local_intr <= _ioapics[i]->getLastIRQ())
      _ioapics[i]->mask(intr);
  }
}

void IOAPICManager::unmask(u8 intr)
{
  u8 local_intr = intr - IOAPIC_IRQ_VECTOR_0;
  assert(local_intr < _max_irq);

  for (u8 i = 0; i < _ioapic_nb; i++)
  {
    if (local_intr <= _ioapics[i]->getLastIRQ())
      _ioapics[i]->unmask(intr);
  }
}

void IOAPICManager::addIOAPIC(IOAPIC* ioapic)
{
  static u8 current_ioapic = 0;

  assert(current_ioapic < _ioapic_nb);
  // This case seems to be impossible according to the specification
  // but IOAPIC must be added in the right order
  assert(ioapic->getFirstIRQ() == _max_irq);

  _max_irq += ioapic->getLastIRQ() + 1;
  _ioapics[current_ioapic++] = ioapic;
}

void IOAPICManager::configureLine(u8 intr,
                                  enum IOAPIC::DeliveryMode d,
                                  enum IOAPIC::Polarity p,
                                  enum IOAPIC::TriggerMode t)
{
  assert(intr < _max_irq);

  for (u8 i = 0; i < _ioapic_nb; i++)
  {
    if (intr <= _ioapics[i]->getLastIRQ())
      _ioapics[i]->configureLine(intr, d, p, t);
  }
}

void IOAPICManager::redirectIRQtoCPU(u8 intr,
                                     enum IOAPIC::DestinationMode mode,
                                     u8 dest)
{
  assert(intr < _max_irq);

  for (u8 i = 0; i < _ioapic_nb; i++)
  {
    if (intr <= _ioapics[i]->getLastIRQ())
      _ioapics[i]->redirectIRQtoCPU(intr, mode, dest);
  }
}

void IOAPICManager::addInterruptRedirection(u32 line, u8 redir)
{
  assert(redir < MAX_ISA_IRQ);
  assert(line < IOAPIC_MAX_IRQ * _ioapic_nb);
  _interrupt_redirection[line] = redir;
}

void IOAPICManager::acknowledge(u8 intr)
{
  // Acknowledgment is done via lapic
  smp::lapic->acknowledge(intr);
}

IOAPIC* IOAPICManager::getIOAPICbyId(u8 id)
{
  for (u8 i = 0; i < _ioapic_nb; i++)
  {
    if (_ioapics[i]->getId() == id)
      return _ioapics[i];
  }

  return 0;
}

IOAPIC* IOAPICManager::getIOAPICbyIRQOrder(u8 id)
{
  for (u8 i = 0; i < _ioapic_nb; i++)
  {
    if (_ioapics[i]->getId() == id)
      return _ioapics[i];
  }

  return 0;
}

}
