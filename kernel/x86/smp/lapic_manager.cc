/*
 * File: lapic_manager.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manage the local apic for each CPU
 *
 */

#include <paging.hh>
#include <logger.hh>
#include <time.hh>
#include <scheduler.hh>

#include <irq_manager.hh>
#include <arch/lapic_manager.hh>

namespace smp
{

LAPICManager* lapic = 0;

static void __noreturn spurious_interrupt_handler(struct irq::RegsDump*)
{
  log::fatal("LAPIC") << "Spurious interrupt - IRQ error";
  panic();
}

static void __noreturn error_interrupt_handler(struct irq::RegsDump*)
{
  log::fatal("LAPIC") << "Error interrupt - IRQ error";
  panic();
}

LAPICManager::LAPICManager(void* addr)
{
  assert(addr);
  _addr = (volatile char*)mem::kernelAD.mmap(0, addr, 1, mem::READ | mem::WRITE);
  assert(_addr);
}

LAPICManager::~LAPICManager()
{
  // _addr cannot be zero at this point
  mem::kernelAD.munmap((void*)_addr);
}

void LAPICManager::acknowledge(u8)
{
  _setReg(LAPIC_EOI, 0);
}

void LAPICManager::mask(u8 intr)
{
  intr -= LAPIC_VECTOR_IRQ_BASE;
  assert(intr < 6);
  _maskReg(LAPIC_LVT_TIMER + intr * 0x10);
}

void LAPICManager::unmask(u8 intr)
{
  intr -= LAPIC_VECTOR_IRQ_BASE;
  assert(intr < 6);
  _unmaskReg(LAPIC_LVT_TIMER + intr * 0x10);
}

void LAPICManager::_maskReg(u32 reg)
{
  u32 val = _getReg(reg);
  val |= LAPIC_MASK_IRQ;
  _setReg(reg, val);
}

void LAPICManager::_unmaskReg(u32 reg)
{
  u32 val = _getReg(reg);
  val &= ~LAPIC_MASK_IRQ;
  _setReg(reg, val);
}

void LAPICManager::configureLine(u8 line, enum DeliveryMode d,
                                 enum Polarity p, enum TriggerMode t)
{
  // You can only configure LINT0 and LINT1
  assert(line < 2);

  u8 vect = LAPIC_VECTOR_LINT0 + line;

  if (d == SMI || d == NMI || d == INIT)
    vect = 0;

  if (d == SMI || d == NMI || d == INIT || d == ExtINT)
  {
    // Do not set the controller since we don't want to ack
  }

  u32 val = vect | (d << 8) | (p << 13) | (t << 15) | LAPIC_MASK_IRQ;
  _setReg(LAPIC_LVT_LINT0 + line * 0x10, val);
  _unmaskReg(LAPIC_LVT_LINT0 + line * 0x10);
}

void LAPICManager::setLocalInterruptVector(u32 reg, u8 val)
{
  u32 prev = _getReg(reg);
  prev = (prev & 0xffffff00) | val;
  _setReg(reg, prev);
}

void LAPICManager::initHandlers()
{
  for (u32 i = LAPIC_VECTOR_TIMER; i <= LAPIC_VECTOR_SPURIOUS; i++)
    irq::manager.setInterruptController(i, this);
}

void LAPICManager::init()
{
  u32 val;

  // First we make sure that all local interrupts are masked and inactive
  _maskReg(LAPIC_LVT_TIMER);
  setLocalInterruptVector(LAPIC_LVT_TIMER, 0);
  _maskReg(LAPIC_LVT_THERMAL);
  setLocalInterruptVector(LAPIC_LVT_THERMAL, 0);
  _maskReg(LAPIC_LVT_PERF);
  setLocalInterruptVector(LAPIC_LVT_PERF, 0);
  _maskReg(LAPIC_LVT_LINT0);
  setLocalInterruptVector(LAPIC_LVT_LINT0, 0);
  _maskReg(LAPIC_LVT_LINT1);
  setLocalInterruptVector(LAPIC_LVT_LINT1, 0);
  _maskReg(LAPIC_LVT_ERR);
  setLocalInterruptVector(LAPIC_LVT_ERR, 0);

  // Set Destinations registers
  _setReg(LAPIC_DFR, LAPIC_FLAT | 0x0fffffff);
  val = _getReg(LAPIC_LDR);

  u8 logical_id = (getAPICId() % 8);
  val = ((1 << logical_id) << 24) | (val & 0x00ffffff);
  _setReg(LAPIC_LDR, val);

  // Accept all interrupts
  setPriority(0);

  // Add error handler
  irq::manager.setHandler(LAPIC_VECTOR_ERR, error_interrupt_handler);
  setLocalInterruptVector(LAPIC_LVT_ERR, LAPIC_VECTOR_ERR);

  irq::manager.setHandler(LAPIC_VECTOR_SPURIOUS, spurious_interrupt_handler);
  setLocalInterruptVector(LAPIC_SPURIOUS, LAPIC_VECTOR_SPURIOUS);

  // Enable the local apic
  val = _getReg(LAPIC_SPURIOUS);
  val = LAPIC_ENABLE | val | 1 << 9;
  _setReg(LAPIC_SPURIOUS, val);
  log::info("LAPIC") << "init";
}

void LAPICManager::setPriority(u8 prio)
{
  _setReg(LAPIC_TPR, prio);
}

void LAPICManager::sendIPI(u8 cpu_apic_id,
                           enum DestinationShortand destination_shorthand,
                           enum DestinationMode destination_mode,
                           enum DeliveryMode delivery_mode,
                           u8 vector)
{
  u32 high = cpu_apic_id << 24;
  u32 low =  destination_shorthand << 18
             | 1 << 14
             | destination_mode << 11
             | delivery_mode << 8
             | vector;

  /* Wait for Idle to send the ipi */
  while (_getReg(LAPIC_ICR_LOW) & LAPIC_PENDING_IPI)
    continue;

  _setReg(LAPIC_ICR_HIGH, high);
  _setReg(LAPIC_ICR_LOW, low);
}

void LAPICManager::_setReg(u32 reg, u32 value)
{
  *(volatile u32*)(_addr + reg) = value;
}

u32 LAPICManager::_getReg(u32 reg)
{
  return *(volatile u32*)(_addr + reg);
}

uint_ptr LAPICManager::getLAPICAddr()
{
  u32 eax, edx;

  asmf::cpu::getMSR(IA32_APIC_BASE_MSR, &eax, &edx);
  return (eax & 0xfffff000);
}

void lapic_timer_handler(struct irq::RegsDump* regs)
{
  scheduler::scheduler.schedule(regs);
}

void LAPICManager::initPeriodic()
{
  static u32 divider = 0;

  irq::manager.setHandler(LAPIC_VECTOR_TIMER, lapic_timer_handler);
  setLocalInterruptVector(LAPIC_LVT_TIMER, LAPIC_VECTOR_TIMER);

  u32 reg = _getReg(LAPIC_REG_TIMER_DIV);
  reg = (reg & 0xfffffff0) | LAPIC_TIMER_DIV_16;
  _setReg(LAPIC_REG_TIMER_DIV, reg);

  if (!divider)
  {
    reg = _getReg(LAPIC_LVT_TIMER);
    reg = (reg & 0xfff9ffff) | (LAPIC_TIMER_ONESHOT << 17);
    _setReg(LAPIC_LVT_TIMER, reg);

    _setReg(LAPIC_REG_TIMER_INITIAL, 0xffffffff);

    time::manager.msleep(1);
    mask(LAPIC_VECTOR_TIMER);

    u32 fhz = (0xffffffff - _getReg(LAPIC_REG_TIMER_CURRENT)) * 1000;
    divider = fhz / 100;
  }

  reg = _getReg(LAPIC_LVT_TIMER);
  reg = (reg & 0xfff9ffff) | (LAPIC_TIMER_PERIODIC << 17);
  _setReg(LAPIC_LVT_TIMER, reg);

  log::debug("LAPIC") << "Calibrate with divider: " << divider;
  // WARNING: Set this regs to a too low value will crash qemu...
  _setReg(LAPIC_REG_TIMER_INITIAL, divider);
  unmask(LAPIC_VECTOR_TIMER);
}

}
