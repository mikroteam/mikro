/*
 * File: mp.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Manipulate the Multi processor struct defined by Intel
 *
 */

#include <logger.hh>
#include <paging.hh>
#include <errors.hh>
#include <arch/bios.hh>

#include <arch/mp.hh>

namespace smp
{

struct MPManager::MPEntry MPManager::_parse_entry_table[] =
{
  { .size = MP_CPU_ENTRY_SIZE, .parse_function = &MPManager::_parseCPU },
  { .size = MP_BUS_ENTRY_SIZE, .parse_function = &MPManager::_parseBUS},
  { .size = MP_IOAPIC_ENTRY_SIZE, .parse_function = &MPManager::_parseIOAPIC},
  { .size = MP_IO_INT_ENTRY_SIZE, .parse_function = &MPManager::_parseIOInt},
  { .size = MP_LO_INT_ENTRY_SIZE, .parse_function = &MPManager::_parseLOInt}
};

MPManager::MPManager()
  : _mpf (0),
    _mcth (0),
    _lapic_addr (0),
    _cpu_count (0),
    _bsp_apic_id (0),
    _mpf_page_addr (0),
    _mcth_page_addr (0),
    _ioapic_count (0),
    _cpu_apic_ids (0)
{
}

MPManager::~MPManager()
{
  release();
}

void MPManager::release()
{
  if (_mpf_page_addr)
    mem::kernelAD.munmap(_mpf_page_addr);
  if (_mcth_page_addr)
    mem::kernelAD.munmap(_mcth_page_addr);
  if (_cpu_apic_ids)
    delete[] _cpu_apic_ids;
}

bool MPManager::init()
{
  _cpu_apic_ids = new u8[CPU_MAX];
  assert(_cpu_apic_ids);
  _bus_names = new char*[BUS_MAX];
  assert(_bus_names);

  return (_searchFloating() && _parseConfigTableHeader());
}

bool MPManager::_searchFloatingInZone(void* addr, size_t len)
{
  char* addr_ptr = (char*)addr;

  for ( ; len; len--, addr_ptr++)
  {
    /* Could be the signature */
    if (addr_ptr[0] == MP_FLOATING_SIGN[0])
    {
      if (addr_ptr[1] == MP_FLOATING_SIGN[1] &&
          addr_ptr[2] == MP_FLOATING_SIGN[2] &&
          addr_ptr[3] == MP_FLOATING_SIGN[3])
      {
        /* Yup, we found it */
        _mpf = (struct MPFloating*)addr_ptr;
        return true;
      }
    }
  }
  return false;
}

bool MPManager::_searchFloating()
{
  /*
   * According to the spec, search in:
   * 1/ the first 1K of the EBDA
   * 2/ 639K - 640K
   * 3/ 0xf0000 - 0xfffff
   */

  size_t ebda_addr = bios::getEBDAPhysAddr();

  if (ebda_addr)
  {
    _mpf_page_addr = mem::kernelAD.mmap(0, (void*)ebda_addr, 1, mem::READ);
    if (_searchFloatingInZone(_mpf_page_addr, 0x400))
        return true;
  }

  mem::kernelAD.munmap(_mpf_page_addr);
  _mpf_page_addr = mem::kernelAD.mmap(0, (void*)(0x400 * 639), 1, mem::READ);
  if (_searchFloatingInZone(_mpf_page_addr, 0x400))
    return true;

  mem::kernelAD.munmap(_mpf_page_addr);
  _mpf_page_addr = mem::kernelAD.mmap(0, (void*)(0xf0000), 0x10000 / PAGE_SIZE, mem::READ);
  if (_searchFloatingInZone(_mpf_page_addr, 0x10000))
  {
    void* phys_mpf = (void*)((char*)_mpf - (char*)_mpf_page_addr + 0xf0000);
    void* phys_aligned_addr = mem::tools::alignOnPage(phys_mpf);
    u32 offset = (char*)phys_mpf - (char*)phys_aligned_addr;

    mem::kernelAD.munmap(_mpf_page_addr);
    _mpf_page_addr = mem::kernelAD.mmap(0, phys_aligned_addr, 1, mem::READ);
    _mpf = (struct MPFloating*)((char*)_mpf_page_addr + offset);
    return true;
  }

  mem::kernelAD.munmap(_mpf_page_addr);
  return false;
}

bool MPManager::_parseBUS(char* addr_ptr)
{
  struct MPBusEntry* bus_ent = reinterpret_cast<struct MPBusEntry*>(addr_ptr);
  char bus_name[7];

  bus_name[6] = 0;
  memcpy(bus_name, bus_ent->bus_type_str, 6);
  log::debug("MP") << "Found a bus named : " << bus_name
                   << " with ID : " << (unsigned int)bus_ent->bus_id;
  _bus_names[bus_ent->bus_id] = bus_ent->bus_type_str;
  return true;
}

bool MPManager::_parseIOAPIC(char* addr_ptr)
{
  struct MPIOApicEntry* ioapic_ent = reinterpret_cast<struct MPIOApicEntry*>(addr_ptr);
  log::debug("MP") << "Found an IOAPIC with ID: " << (unsigned int)ioapic_ent->ioapic_id
                   << " at addr : " << dsp::hex << ioapic_ent->ioapic_addr << dsp::dec
                   << " enabled : " << ioapic_ent->ioapic_enable;
  _ioapic_count++;
  return true;
}

bool MPManager::_parseInt(struct MPInterrupt* interrupt)
{
  log::debug("MP") << "\n\tType: " << (u32)interrupt->interrupt_type
                   << "\n\tSource bus: " << (u32)interrupt->source_bus_id
                   << "\n\tIRQ on bus: " << (u32)interrupt->source_bus_irq
                   << "\n\tAPIC concerned: " << (u32)interrupt->apic_id
                   << "\n\tOn pin: " << (u32)interrupt->apic_int;
  return true;
}

bool MPManager::_parseIOInt(char* addr_ptr)
{
  struct MPInterrupt* interrupt = reinterpret_cast<struct MPInterrupt*>(addr_ptr);
  log::debug("MP") << "Found an IO interrupt:";
  return _parseInt(interrupt);
}

bool MPManager::_parseLOInt(char* addr_ptr)
{
  struct MPInterrupt* interrupt = reinterpret_cast<struct MPInterrupt*>(addr_ptr);
  log::debug("MP") << "Found a local interrupt:";
  return _parseInt(interrupt);
}

bool MPManager::_parseCPU(char* addr_ptr)
{
  struct MPCpuEntry* cpu_ent = reinterpret_cast<struct MPCpuEntry*>(addr_ptr);

  log::debug("MP") << "Found a CPU with ID : " << (unsigned int)cpu_ent->local_apic_id
                   << " enabled : " << (u32)cpu_ent->cpu_usable
                   << " BSP : " << (u32)cpu_ent->cpu_bsp;

  if (!cpu_ent->cpu_usable)
    return false;

  if (cpu_ent->cpu_bsp)
  {
    _bsp_apic_id = cpu_ent->local_apic_id;
  }

  if (_cpu_count == CPU_MAX)
  {
    log::err("MP") << "CPU number limit reached, will only use " << CPU_MAX;
    return false;
  }

  _cpu_apic_ids[_cpu_count++] = cpu_ent->local_apic_id;

  return true;
}

bool MPManager::_parseBaseTable()
{
  char* addr_ptr = reinterpret_cast<char*>(_mcth) + sizeof (struct MPConfigTableHeader);

  for (u32 i = 0; i < _mcth->entry_count; i++)
  {
    if (addr_ptr[0] > LO_INT_ASSIGN)
      return false;

    (this->*_parse_entry_table[(int)addr_ptr[0]].parse_function)(addr_ptr);
    addr_ptr += _parse_entry_table[(int)addr_ptr[0]].size;
  }

  return true;
}

bool MPManager::_parseConfigTableHeader()
{
  if (!_mpf)
    return false;

  log::verbose("MP") << "Multiprocessor Specification v1." << _mpf->spec_rev;

  /* MP Configuration Table present */
  if (!_mpf->mp_features[0])
  {
    if (_mpf->phys_addr)
    {
      void* aligned_addr = mem::tools::alignOnPage((void*)_mpf->phys_addr);
      u32 offset = (char*)_mpf->phys_addr - (char*)aligned_addr;

      _mcth_page_addr = mem::kernelAD.mmap(0, aligned_addr, 1, mem::READ);
      _mcth = (struct MPConfigTableHeader*)((char*)_mcth_page_addr + offset);

      // Check that one page is enough ?
      if (_mcth->base_table_len > PAGE_SIZE)
      {
        u32 page_nb = mem::tools::length2nrPages(_mcth->base_table_len + offset);

        mem::kernelAD.munmap(_mcth_page_addr);
        _mcth_page_addr = mem::kernelAD.mmap(0, aligned_addr,
                                                page_nb, mem::READ);
        _mcth = (struct MPConfigTableHeader*)((char*)_mcth_page_addr + offset);
      }

      if (_mcth->signature[0] == MP_TABLE_HEADER_SIGN[0] &&
          _mcth->signature[1] == MP_TABLE_HEADER_SIGN[1] &&
          _mcth->signature[2] == MP_TABLE_HEADER_SIGN[2] &&
          _mcth->signature[3] == MP_TABLE_HEADER_SIGN[3])
      {
        _lapic_addr = _mcth->lapic_addr;
        return _parseBaseTable();
      }
    }
  }
  /* Default configuration must be loaded */
  /* FIXME: Handle the default config */
  else
  {
    log::err("MP") << "Default config not supported";
    return false;
  }

  log::err("MP") << "Invalid MP Floating stucture";
  return false;
}

void MPManager::_configureIOAPIC(struct MPIOApicEntry* ioapic_entry,
                                IOAPICManager* ioapicm)
{
  if (!ioapic_entry->ioapic_enable)
    return;

  class IOAPIC* ioapic = new class IOAPIC(ioapic_entry->ioapic_id,
                                          ioapic_entry->ioapic_addr,
                                          ioapicm->getMaxIRQ());
  assert(ioapic);
  ioapicm->addIOAPIC(ioapic);
}

bool MPManager::configureIOAPICs(IOAPICManager* ioapicm)
{
  char* addr_ptr = reinterpret_cast<char*>(_mcth) + sizeof (struct MPConfigTableHeader);

  for (u32 i = 0; i < _mcth->entry_count; i++)
  {
    if (addr_ptr[0] == MP_IOAPIC_ENTRY_TYPE)
      _configureIOAPIC(reinterpret_cast<struct MPIOApicEntry*>(addr_ptr),
                       ioapicm);

    addr_ptr += _parse_entry_table[(int)addr_ptr[0]].size;
  }

  return true;
}

enum smp::IOAPIC::Polarity MPManager::_convertPolarityForIOAPIC(u16 polarity)
{
  switch (polarity)
  {
    case HIGH:
      return smp::IOAPIC::HighActive;
    case LOW:
      return smp::IOAPIC::LowActive;
    default:
      log::err("MP") << "Unknown polarity, default to HIGH";
      return smp::IOAPIC::HighActive;
  }
}

enum smp::IOAPIC::TriggerMode MPManager::_convertTriggerModeForIOAPIC(u16 trigger)
{
  switch (trigger)
  {
    case EDGE:
      return smp::IOAPIC::Edge;
    case LEVEL:
      return smp::IOAPIC::Level;
    default:
      log::err("MP") << "Unknown Trigger Mode, default to edge";
      return smp::IOAPIC::Edge;
  }
}

enum smp::IOAPIC::DeliveryMode MPManager::_convertTypeForIOAPIC(u16 type)
{
  switch (type)
  {
    case INT:
      return smp::IOAPIC::Low;
    case NMI:
      return smp::IOAPIC::NMI;
    case SMI:
      return smp::IOAPIC::SMI;
    case ExtINT:
      return smp::IOAPIC::ExtINT;
    default:
      log::err("MP") << "Unknown type, default to Lowest";
      return smp::IOAPIC::Low;
  }
}

enum smp::LAPICManager::Polarity MPManager::_convertPolarityForLAPIC(u16 polarity)
{
  switch (polarity)
  {
    case HIGH:
      return smp::LAPICManager::HighActive;
    case LOW:
      return smp::LAPICManager::LowActive;
    default:
      log::err("MP") << "Unknown polarity, default to HIGH";
      return smp::LAPICManager::HighActive;
  }
}

enum smp::LAPICManager::TriggerMode MPManager::_convertTriggerModeForLAPIC(u16 trigger)
{
  switch (trigger)
  {
    case EDGE:
      return smp::LAPICManager::Edge;
    case LEVEL:
      return smp::LAPICManager::Level;
    default:
      log::err("MP") << "Unknown Trigger Mode, default to edge";
      return smp::LAPICManager::Edge;
  }
}

enum smp::LAPICManager::DeliveryMode MPManager::_convertTypeForLAPIC(u16 type)
{
  switch (type)
  {
    case INT:
      return smp::LAPICManager::Fixed;
    case NMI:
      return smp::LAPICManager::NMI;
    case SMI:
      return smp::LAPICManager::SMI;
    case ExtINT:
      return smp::LAPICManager::ExtINT;
    default:
      log::err("MP") << "Unknown type, default to fixed";
      return smp::LAPICManager::Fixed;
  }
}

u8 MPManager::_getDefaultPolarityForBus(u8 bus_id)
{
  if (!strncmp("PCI   ", _bus_names[bus_id], 6))
    return LOW;
  else if (!strncmp("ISA   ", _bus_names[bus_id], 6))
    return HIGH;
  else
    log::err("MP") << "Unkown bus type, default polarity";
  return HIGH;
}

u8 MPManager::_getDefaultTriggerModeForBus(u8 bus_id)
{
  if (!strncmp("PCI   ", _bus_names[bus_id], 6))
    return LEVEL;
  else if (!strncmp("ISA   ", _bus_names[bus_id], 6))
    return EDGE;
  else
    log::err("MP") << "Unkown bus type, default trigger mode";
  return EDGE;
}

void MPManager::_configureIOAPIC(struct MPInterrupt* intr,
                                 IOAPICManager* ioapicm,
                                 class IOAPIC* ioapic)

{
  u8 polarity = intr->polarity;
  u8 trigger = intr->trigger_mode;
  if (polarity == POLARITY_CONFORMS)
    polarity = _getDefaultPolarityForBus(intr->source_bus_id);
  if (trigger == MODE_CONFORMS)
    trigger = _getDefaultTriggerModeForBus(intr->source_bus_id);

  if (!strncmp(_bus_names[intr->source_bus_id], "ISA   ", 6))
    ioapicm->addInterruptRedirection(ioapic->getFirstIRQ() + intr->apic_int,
                                     intr->source_bus_irq);
  ioapic->configureLine(intr->apic_int + ioapic->getFirstIRQ(),
                        _convertTypeForIOAPIC(intr->interrupt_type),
                        _convertPolarityForIOAPIC(polarity),
                        _convertTriggerModeForIOAPIC(trigger));
}

void MPManager::_configureIOAPICLine(struct MPInterrupt* intr,
                                     IOAPICManager* ioapicm)
{
  class IOAPIC* ioapic;

  if (intr->apic_id == ALL_APIC)
  {
    for (u8 i = 0; i < _ioapic_count; i++)
    {
      ioapic = ioapicm->getIOAPICbyIRQOrder(intr->apic_id);
      _configureIOAPIC(intr, ioapicm, ioapic);
    }
  }

  ioapic = ioapicm->getIOAPICbyId(intr->apic_id);
  if (!ioapic)
  {
    log::fatal("MP") << "Try to configure an invalid IOAPIC";
    panic();
  }
  _configureIOAPIC(intr, ioapicm, ioapic);
}

bool MPManager::configureIOAPICsLines(IOAPICManager* ioapicm)
{
  char* addr_ptr = reinterpret_cast<char*>(_mcth) + sizeof (struct MPConfigTableHeader);

  for (u32 i = 0; i < _mcth->entry_count; i++)
  {
    if (addr_ptr[0] == MP_IO_INT_ENTRY_TYPE)
      _configureIOAPICLine(reinterpret_cast<struct MPInterrupt*>(addr_ptr),
                           ioapicm);

    addr_ptr += _parse_entry_table[(int)addr_ptr[0]].size;
  }

  return true;
}

void MPManager::_configureLAPICLine(struct MPInterrupt* intr, u8 apic_id)
{
  if (intr->apic_id != ALL_APIC && intr->apic_id != apic_id)
    return;

  u8 polarity = intr->polarity;
  u8 trigger = intr->trigger_mode;
  if (polarity == POLARITY_CONFORMS)
    polarity = _getDefaultPolarityForBus(intr->source_bus_id);
  if (trigger == MODE_CONFORMS)
    trigger = _getDefaultTriggerModeForBus(intr->source_bus_id);

  smp::lapic->configureLine(intr->apic_int,
                            _convertTypeForLAPIC(intr->interrupt_type),
                            _convertPolarityForLAPIC(polarity),
                            _convertTriggerModeForLAPIC(trigger));
}

bool MPManager::configureLAPICLines()
{
  u8 apic_id = smp::lapic->getAPICId();
  char* addr_ptr = reinterpret_cast<char*>(_mcth) + sizeof (struct MPConfigTableHeader);

  for (u32 i = 0; i < _mcth->entry_count; i++)
  {
    if (addr_ptr[0] == MP_LO_INT_ENTRY_TYPE)
      _configureLAPICLine(reinterpret_cast<struct MPInterrupt*>(addr_ptr),
                          apic_id);

    addr_ptr += _parse_entry_table[(int)addr_ptr[0]].size;
  }
  return true;
}

}
