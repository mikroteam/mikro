/*
 * File: percpu.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Per cpu variable
 *
 */

#include <cpu.hh>
#include <paging.hh>
#include <memory_manager.hh>
#include <arch/gdt_manager.hh>

extern "C" char _scpuvar[];
extern "C" char _ecpuvar[];

namespace smp
{

  void initCpuVar(u32 id)
  {
    static int cpuvar_size = (_ecpuvar - _scpuvar);
    int page_nb = mem::tools::length2nrPages(cpuvar_size);
    void* addr = mem::kernelAD.mmap(0, page_nb, mem::READ | mem::WRITE);

    memset(addr, 0, cpuvar_size);
    mem::manager.arch.gdtm.setGate(KERNEL_CPU_DATA, (size_t)addr, page_nb,
                                   GDT_DATA_ACCESS, 0x1);
    asmf::regs::set_fs(KERNEL_CPU_DATA_SEL);

    cpu::myself = new cpu::CPU(id);
    if (!cpu::myself)
    {
      log::fatal("PANIC") << "Could not allocate memory for Cpu(s) objects.";
      panic();
    }

    // BSP has id 0
    if (id == 0)
    {
      cpu::cpus = new cpu::CPU*[cpu::number];

      if (!cpu::cpus)
      {
        log::fatal("PANIC") << "Could not allocate memory for Cpu(s) objects.";
        panic();
      }
    }

    cpu::cpus[id] = cpu::myself;
  }
}
