/*
 * File: smp_info.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Deliver informations on a SMP system
 *
 */

#include <arch/acpi.hh>
#include <arch/mp.hh>
#include <errors.hh>

#include <arch/smp_info.hh>

namespace smp
{

SMPInfo* info = 0;

bool SMPInfo::init()
{
  if (acpi::manager.rsdt)
  {
    acpi::MADTManager* madt = new acpi::MADTManager();
    assert(madt);

    if (!madt->init(acpi::manager.rsdt))
    {
      log::debug("ACPI") << "Unable to find MADT";
      delete madt;
    }
    else
    {
      madt->parse();
      info = madt;
      return true;
    }
  }

  MPManager* mp = new MPManager;
  assert(mp);

  if (mp->init())
  {
    info = mp;
    return true;
  }
  else
  {
    log::debug("SMP") << "Unable to find MP Table";
    mp->release();
    delete mp;
  }

  return false;
}

void SMPInfo::release()
{
  if (info)
  {
    delete info;
    info = 0;
  }
}

}
