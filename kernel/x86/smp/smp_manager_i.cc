/*
 * File: smp_manager_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Handle SMP on x86 arch
 *
 */

#include <compiler_tools.hh>
#include <logger.hh>
#include <paging.hh>
#include <arch/trampoline.hh>
#include <arch/asm.hh>
#include <arch/percpu.hh>
#include <cpu.hh>
#include <cpu_info.hh>
#include <init.hh>
#include <scheduler.hh>
#include <algorithm.hh>

#include <arch/smp_info.hh>
#include <arch/smp_manager_i.hh>
#include <arch/ioapic_manager.hh>

extern "C" char trampoline_start[];
extern "C" char trampoline_end[];

extern "C" char trampolineCR3[];
extern "C" char trampolineStack[];
extern "C" char trampolineJmpAddr[];

static volatile u32 wakeCPUCount;
static u32 currentCpuId = 0;

extern "C" __asmcall void startAPKernel()
{
  /* Entry point in kernel for APs */
  u32 myId = currentCpuId++;
  wakeCPUCount++;

  log::debug("AP") << "Up!";
  initAp(myId);
  log::debug("AP") << "Init done";

  scheduler::scheduler.init();
}

namespace smp
{

SMPManagerImpl<x86>::~SMPManagerImpl()
{
}

void SMPManagerImpl<x86>::init()
{
  u32 cpu_count = 0;

  if (!smp::info)
  {
    /* In this case, we have no IOAPIC */
    /* But the CPU can have a local APIC ! */
    cpu_count = 1;

    if (cpu::info.arch.hasAPIC())
    {
      uint_ptr lapic_addr = smp::LAPICManager::getLAPICAddr();
      smp::lapic = new LAPICManager((void*)lapic_addr);
      smp::lapic->initHandlers();
      smp::lapic->init();
      smp::lapic->initPeriodic();

      /*
       * In this configuration we have a PIC device connected
       * to one of the local lines, unmask to receive interrupts
       */
      smp::lapic->unmask(LAPIC_VECTOR_LINT0);
      smp::lapic->unmask(LAPIC_VECTOR_LINT1);
    }
  }
  else
  {
    cpu_count = smp::info->getCpuCount();

    if (!smp::info->getIOAPICCount() || !cpu::info.arch.hasAPIC())
    {
      // This case should be impossible...
      log::fatal("SMP") << "SMP System with no IOAPIC or Local APIC";
      panic();
    }

    smp::lapic = new LAPICManager((void*)smp::info->getAPICAddr());
    _bsp_apic_id = smp::lapic->getAPICId();
    smp::lapic->initHandlers();
    smp::lapic->init();
    smp::lapic->initPeriodic();
    smp::info->configureLAPICLines();
  }

  cpu::number = cpu_count;
  log::info("SMP") << cpu_count << " logical CPU(s) found";
  smp::initCpuVar(currentCpuId++);

}

void SMPManagerImpl<x86>::_wakeAllAPs(u32 cpu_count, u8 bsp_id)
{
  wakeCPUCount = 1;

  // Prepare the trampoline
  memcpy((void*)(TR_START_AP_ADDR),
         trampoline_start,
         trampoline_end - trampoline_start);

  for (u32 i = 0; i < cpu_count; i++)
  {
    u8 apic_id = smp::info->getAPICId(i);

    /* Do not wake the current processor */
    if (apic_id == bsp_id)
      continue;

    _wakeAP(apic_id);
  }
}

void SMPManagerImpl<x86>::_wakeAP(u8 local_apic_id)
{
  u32 currentCpuCount = wakeCPUCount;
  u32* trampoline_cr3 = (u32*)(TR_START_AP_ADDR + (trampolineCR3 - trampoline_start));
  u32* trampoline_stack = (u32*)(TR_START_AP_ADDR + (trampolineStack - trampoline_start));
  u32* trampoline_jmp_addr = (u32*)(TR_START_AP_ADDR + (trampolineJmpAddr - trampoline_start));

  log::debug("SMP") << "Wake AP : " << local_apic_id;

  // FIXME: Unmap this page later ?
  void* stack = mem::kernelAD.mmap(0, 1, mem::READ | mem::WRITE);

  trampoline_cr3[0] = asmf::regs::get_cr3();

  // Stack grows downwards, take the end of the page
  trampoline_stack[0] = (u32)((char*)stack + PAGE_SIZE);
  trampoline_jmp_addr[0] = (u32)startAPKernel;

  // INIT-SIPI-SIPI sequence
  smp::lapic->sendIPI(local_apic_id, LAPICManager::NoShorthand,
                      LAPICManager::Physical, LAPICManager::INIT, 0x0);
  smp::lapic->sendIPI(local_apic_id, LAPICManager::NoShorthand,
                      LAPICManager::Physical, LAPICManager::StartUP,
                      TR_START_AP_VECTOR);
  smp::lapic->sendIPI(local_apic_id, LAPICManager::NoShorthand,
                      LAPICManager::Physical, LAPICManager::StartUP,
                      TR_START_AP_VECTOR);

  /* Wait for the AP */
  while (currentCpuCount == wakeCPUCount)
    continue;
}

}
