/*
 * File: syscall_io.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: In and Out syscalls
 *
 */

#include <arch/asm.hh>
#include <types.hh>

#include <arch/syscall_i.hh>

int sys_in(u16 port, u8 size)
{
  if (size == sizeof (u8))
    return asmf::io::inb(port);
  else if (size == sizeof (u16))
    return asmf::io::inw(port);
  else if (size == sizeof (u32))
    return asmf::io::inl(port);
  else
    return -1;
}

int sys_out(u16 port, u32 value, u8 size)
{
  if (size == sizeof (u8))
  {
    asmf::io::outb(port, (u8)value);
    return 0;
  }
  if (size == sizeof (u16))
  {
    asmf::io::outw(port, (u16)value);
    return 0;
  }
  if (size == sizeof (u32))
  {
    asmf::io::outl(port, (u32)value);
    return 0;
  }

  return -1;
}
