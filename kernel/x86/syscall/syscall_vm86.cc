/**
 * \file syscall_vm86.cc
 * \author Victor Aperce <viaxxx@lse.epita.fr>
 * \brief vm86 syscall
 */

#include "arch/asm.hh"
#include "arch/vm86_macro.hh"
#include "cpu.hh"
#include "cstring.hh"
#include "irq_manager.hh"
#include "spinlock.hh"
#include "syscall.hh"
#include "task_manager.hh"

extern "C" void gotoVM86();
extern void *taskVM86;
extern void *taskVM86_end;
extern void *taskVM86_opcode;

Spinlock vm86_lock;
VM86Regs *vm86_regs = 0;

void vm86Interrupt(irq::RegsDump *regs)
{
  log::debug("VM8086") << "Returning to Protected #"
    << cpu::myself->current_thread->getPid();

  irq::RegsDumpI386 *regsI386 = (irq::RegsDumpI386 *) regs;
  VM86Regs regsRes;
  mem::AddressSpace *as = cpu::myself->current_thread->getAS();

  // TODO: issue with page fault when in VM8086 => spinlock must be unlocked
  as->munmap((void *) 0x90000);
  as->munmap((void *) VM86_TASK_EIP);
  as->munmapVM86();

  regsRes.di = regsI386->edi;
  regsRes.si = regsI386->esi;
  regsRes.bp = regsI386->ebp;
  regsRes.bx = regsI386->ebx;
  regsRes.dx = regsI386->edx;
  regsRes.cx = regsI386->ecx;
  regsRes.ax = regsI386->eax;

  assert(irq::manager.setHandler(VM86_RETURN_INT, 0));

  if (!syscall::manager.copyToUser(vm86_regs, (void *) &regsRes,
        sizeof(VM86Regs)))
    cpu::myself->current_thread->setReturn(-1);
  else
    cpu::myself->current_thread->setReturn(0);

  cpu::myself->current_thread->restore(regs);
  vm86_lock.unlock();
}

int sys_vm86(VM86Regs *regs, u8 intNum)
{
  void *copyPtr;
  char *opcode = (char *) &taskVM86_opcode;
  mem::AddressSpace *as = cpu::myself->current_thread->getAS();

  vm86_lock.lock();
  vm86_regs = regs;

  cpu::myself->current_thread->save(irq::manager.getCurrentRegsDump());
  if (!irq::manager.setHandler(VM86_RETURN_INT, vm86Interrupt))
    return -1;


  /* Map all needed stuffs in the userland AS */
  as->mmapVM86(); // Map V: 0x0 P: 0x0 which is normally unauthorized

  if (as->mmap((void *) VM86_TASK_EIP, 1, mem::READ | mem::WRITE | mem::EXEC)
      != (void *) VM86_TASK_EIP)
    goto err1;

  if (as->mmap((void *) 0x90000, (void *) 0x90000, 0x70,
        mem::READ | mem::WRITE | mem::EXEC)
       != (void *) 0x90000)
    goto err2;

  copyPtr = mem::kernelAD.mmap((void *) 0x0, *as, (void *) 0x1000, 1,
      mem::READ | mem::WRITE);

  if (!copyPtr)
    goto err3;

  /* Set int opcode */
  opcode[0] = 0xCD;
  opcode[1] = intNum;

  /* Copy binary and stack */
  memcpy(copyPtr, &taskVM86, (size_t) &taskVM86_end - (size_t) &taskVM86);
  if (!syscall::manager.copyFromUser(
        (void *) ((char *) copyPtr + VM86_TASK_ESP_OFF),
        (void *) regs,
        sizeof(VM86Regs)))
    goto err3;

  if (!mem::kernelAD.munmap(copyPtr))
    goto err3;

  /* Go to VM86 */
  log::debug("VM8086") << "Going to VM8086 #"
    << cpu::myself->current_thread->getPid();
  gotoVM86();
  /* This code is not reachable */

err3:
  as->munmap((void *) 0x90000);
err2:
  as->munmap((void *) VM86_TASK_EIP);
err1:
  as->munmapVM86();
  return -1;
}

