#include "arch/vm86_macro.hh"

.global gotoVM86
.global taskVM86
.global taskVM86_opcode
.global taskVM86_end

.code32
gotoVM86:

movl %cr4, %eax
orl $0x1, %eax
movl %eax, %cr4

pushl $0x0              /* GS */
pushl $0x0              /* FS */
pushl $0x0              /* DS */
pushl $0x0              /* ES */
pushl $0x0              /* SS */
pushl $(VM86_TASK_ESP)  /* ESP */

/* EFLAGS on stack */
pushfl
popl %eax
orl $(1 << 17), %eax    /* VM */
orl $(1 << 14), %eax    /* NT as required by intel manual */
orl $(3 << 12), %eax    /* Class 3 interrupt */
pushl %eax

pushl $0x0              /* CS */
pushl $(VM86_TASK_EIP)  /* EIP (16 bits) */

iret

.code16
taskVM86:
pop %di
pop %si
pop %bp
pop %bx
pop %dx
pop %cx
pop %ax
taskVM86_opcode:
.word 0
int $(VM86_RETURN_INT)

taskVM86_end:
