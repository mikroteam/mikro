#include <arch/pit_manager.hh>
#include <irq_manager.hh>
#include <arch/irq_manager_i.hh>
#include <arch/asm.hh>
#include <time.hh>

namespace time
{
  extern "C" void pit_handler(struct irq::RegsDump *regs)
  {
    (void)regs;
    jiffies++;
  }

  void PitManager::initPeriodic()
  {
    uint16_t count;

    count = PIT_ORG_FREQ / TICK_HZ;

    asmf::io::outb(PIT_CMD_PORT, PIT_CNT_BINARY | PIT_MODE_RATE_GEN
                                 | PIT_CNT_SET_LSB_MSB | PIT_CNT1);
    asmf::io::outb(PIT_CNT1_PORT, count & 0x00FF);
    asmf::io::outb(PIT_CNT1_PORT, count >> 8);

    irq::manager.setHandler(IRQ_VECTOR_PIT, pit_handler);
    irq::manager.unmaskInterrupt(IRQ_VECTOR_PIT);
  }
}
