/*
 * File: time_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Time implementation
 *
 */

#include <time.hh>
#include <arch/time_i.hh>

namespace time
{

void TimeManagerImpl<x86>::msleep(u32 msecs)
{
  if (!msecs)
    return;

  u64 target_time = time::jiffies + msecs * (TICK_HZ / 1000);

  // Allow ticks to be handled
  asmf::irq::sti();

  while (time::jiffies < target_time)
    continue;

  asmf::irq::cli();
}

}
