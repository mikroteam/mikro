/*
 * File: timer_i.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Timer on x86
 *
 */

#include <errors.hh>
#include <timer.hh>
#include <arch/timer_i.hh>
#include <arch/pit_manager.hh>

namespace time
{

void init()
{
  periodic_timer = new PitManager();
  assert(periodic_timer);
  periodic_timer->initPeriodic();
  log::info("Timer") << "init";
}

}
