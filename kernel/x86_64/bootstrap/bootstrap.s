.set STACK_SIZE, 0x4000 # Size of the stack is 16k
.lcomm stack, STACK_SIZE

.globl k_entry

k_entry:
  mov $(stack + STACK_SIZE), %esp
	push %rbx	# multiboot info
	push %rax	# magic

  mov  $start_ctors, %ebx             # call the constructors
  jmp  2f
1:
  call *(%ebx)
  add  $4, %ebx
2:
  cmp  $end_ctors, %ebx
  jb   1b

	call k_main	# kernel entry point

  mov  $end_dtors, %ebx               # call the destructors
  jmp  4f
3:
  sub  $4, %ebx
  call *(%ebx)
4:
  cmp  $start_dtors, %ebx
  ja   3b

  cli
end:
  hlt
  jmp end
  
