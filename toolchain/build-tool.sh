#!/bin/sh

TARGET_ARCH=`uname -m`
PREFIX=/usr/local
PROGRAM=binutils

for i
do
  case $i in
    "--arch="*)
      TARGET_ARCH="${i##--arch=}"
      ;;
    "--prefix="*)
      PREFIX="${i##--prefix=}"
      ;;
    "--prog="*)
      PROGRAM="${i##--prog=}"
  esac
done

# You can change the version to the latest available if you want
case $PROGRAM in
  "gdb")
    PROGRAM_VER=7.5.1
    ;;
  "binutils")
    PROGRAM_VER=2.23
    ;;
  *)
    echo "Not supported Program"
    exit 1
    ;;
esac

PROGRAM_ARCHIVE=$PROGRAM-$PROGRAM_VER.tar.gz
PROGRAM_URL=http://ftp.gnu.org/gnu/$PROGRAM/$PROGRAM_ARCHIVE

if [ ! -f $PROGRAM_ARCHIVE ]
then
  curl $PROGRAM_URL -o $PROGRAM_ARCHIVE || exit 2
fi

# Force some env values to build
export CC=clang
export CXX=clang++
export CFLAGS=""
export CXXFLAGS=""

tar xf $PROGRAM_ARCHIVE
mv $PROGRAM-$PROGRAM_VER $PROGRAM-$PROGRAM_VER-$TARGET_ARCH 
cd $PROGRAM-$PROGRAM_VER-$TARGET_ARCH
./configure --target=$TARGET_ARCH-mikro-elf --prefix=$PREFIX --disable-werror
make -j && make install

