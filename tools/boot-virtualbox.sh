#!/bin/sh

usage()
{
  echo "Boot a VirtualBox VM with mikro"
  echo "Usage: $0 mikro-hdd.img mikro-hdd.vdi"
  echo ""
}

if [ $# -lt 2 ]
then
  usage
  exit 0
fi

MIKRO_IMG=$1
shift
MIKRO_VDI=$1
MIKRO_ROOT=$(cd "$(dirname "$0")/../"; pwd)

MIKRO_HDD_UUID="c589ca68-9461-4957-9f65-250d86fafd4d"
MIKRO_VM_NAME="mikro"

rm -f $MIKRO_VDI
VBoxManage convertfromraw $MIKRO_IMG $MIKRO_VDI \
                          --format VDI \
                          --uuid $MIKRO_HDD_UUID

# check that vm exists
VBoxManage list vms | grep $MIKRO_VM_NAME > /dev/null
if [ $? -ne 0 ]
then
  VBoxManage createvm --name $MIKRO_VM_NAME --register
  VBoxManage storagectl $MIKRO_VM_NAME --name "IDE" --add ide --bootable on
  VBoxManage storageattach $MIKRO_VM_NAME \
                          --storagectl "IDE" \
                          --device 0 \
                          --port 0 \
                          --type hdd \
                          --medium $MIKRO_VDI
  VBoxManage modifyvm $MIKRO_VM_NAME --uart1 0x3F8 4 \
                          --uartmode1 file $MIKRO_ROOT/serial.out
  VBoxManage modifyvm $MIKRO_VM_NAME --uart2 0x2F8 3 \
                          --uartmode2 server /tmp/vboxserial
fi

VBoxManage startvm $MIKRO_VM_NAME
