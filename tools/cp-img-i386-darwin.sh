#!/bin/sh

usage()
{
  echo "Copy files easily to a virtual disk"
  echo "Usage: $0 virtual_disk mount_point files_to_copy"
  echo ""
}

if [ $# -lt 3 ]
then
  usage
  exit 0
fi

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
    esac
done

IMG=$1
shift
MNT=$1
shift
FILES=$@

# Attach the img as a virtual disk
DISK=`hdiutil attach $IMG -nomount | tail -n 1 | cut -f 1`

# Mount the disk
fuse-ext2 -o force $DISK $MNT

# It takes some times to actually mount the disk...  
sleep 1

# Copy file
cp -R $FILES $MNT

# unmount the virtual disk
hdiutil detach $DISK

