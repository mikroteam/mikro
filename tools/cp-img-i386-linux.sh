#!/bin/sh

usage()
{
  echo "Copy files easily to a virtual disk"
  echo "Usage: $0 virtual_disk mount_point files_to_copy"
  echo ""
}

if [ $# -lt 3 ]
then
  usage
  exit 0
fi

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
    esac
done

IMG=$1
shift
MNT=$1
shift
FILES=$@

LOOP=`sudo kpartx -av $IMG | head -n 1 | cut -d " " -f 3`
sleep 1
sudo mount /dev/mapper/$LOOP $MNT
sudo cp -R $FILES $MNT
sleep 1
sudo umount $MNT
sudo kpartx -d $IMG
