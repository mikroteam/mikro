#!/bin/sh

usage()
{
  echo "Generate a config file for the bootloader"
  echo "Usage: $0 bootloader_name output_file daemon_list"
  echo ""
}

if [ $# -lt 2 ]
then
  usage
  exit 0
fi

BOOTLOADER=$1
BOOTLOADER_CONFIG_FILE=$2
KERNEL_ARGS=""
BOOT_DAEMONS=""

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
      "--args="*)
        KERNEL_ARGS="${i##--args=}"
        ;;
      "--daemons="*)
        BOOT_DAEMONS="${i##--daemons=}"
        ;;
    esac
done

case $BOOTLOADER in
  "mikrob")
    rm -f $BOOTLOADER_CONFIG_FILE
    echo "k mikro $KERNEL_ARGS" >> $BOOTLOADER_CONFIG_FILE
    for d in $BOOT_DAEMONS
    do
      echo "m $d" >> $BOOTLOADER_CONFIG_FILE
    done
    echo "b" >> $BOOTLOADER_CONFIG_FILE
    ;;
  "extlinux")
    rm -f $BOOTLOADER_CONFIG_FILE
    echo "DEFAULT mikro" >> $BOOTLOADER_CONFIG_FILE
    echo "LABEL mikro" >> $BOOTLOADER_CONFIG_FILE
    echo "KERNEL mboot.c32" >> $BOOTLOADER_CONFIG_FILE
    APPEND_LINE="APPEND mikro $KERNEL_ARGS"
    for d in $BOOT_DAEMONS
    do
      APPEND_LINE="$APPEND_LINE --- $d"
    done
    echo "$APPEND_LINE" >> $BOOTLOADER_CONFIG_FILE
    ;;
esac
