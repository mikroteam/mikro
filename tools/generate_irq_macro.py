#!/usr/bin/env python

error_code_list = [8, 10, 11, 12, 13, 14, 17]
dpl_3_list = [128]

def get_dpl(numb):
  if numb in dpl_3_list:
    return "3"
  else:
    return "0"

def get_error_push(numb):
  if numb in error_code_list:
    return "nop"
  else:
    return "push $0"

with open("irq_asm_handler.def", "w") as f:
  i = 0
  while i < 256:
    f.write("IRQ_ASM_HANDLER(asm_handler_" + str(i) 
                             + ", " + str(i) + 
                             ", " + get_error_push(i) + ", "
                             + get_dpl(i) + ")\n")
    i += 1
