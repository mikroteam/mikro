#!/bin/sh

VERSION=5.10
URL="https://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-$VERSION.tar.bz2"

usage()
{
  echo "Install extlinux to a virtual disk"
  echo "Usage: $0 virtual_disk mount_point"
  echo ""
}

if [ $# -lt 2 ]
then
  usage
  exit 0
fi

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
    esac
done

IMG=$1
shift
MNT=$1

SYSLINUX_PATH=`mktemp -d`
EXTLINUX_PATH=/boot/extlinux

if [ ! -f syslinux-$VERSION.tar.bz2 ]
then
  curl $URL -o $SYSLINUX_PATH/syslinux-$VERSION.tar.bz2 || exit 2
fi
tar -C $SYSLINUX_PATH -xf $SYSLINUX_PATH/syslinux-$VERSION.tar.bz2

MBR_IMG_PATH=$SYSLINUX_PATH/syslinux-$VERSION/mbr/mbr.bin
dd conv=notrunc bs=440 count=1 if=$MBR_IMG_PATH of=$IMG
sudo mount -oloop,offset=32256 $IMG $MNT
mkdir -p ${MNT}${EXTLINUX_PATH}
sudo $SYSLINUX_PATH/syslinux-$VERSION/extlinux/extlinux --install ${MNT}${EXTLINUX_PATH}
cp $SYSLINUX_PATH/syslinux-$VERSION/com32/mboot/mboot.c32 ${MNT}${EXTLINUX_PATH}
cp $SYSLINUX_PATH/syslinux-$VERSION/com32/lib/libcom32.c32 ${MNT}${EXTLINUX_PATH}
sudo umount $MNT
