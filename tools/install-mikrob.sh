#!/bin/sh

usage()
{
  echo "Install mikrob to a virtual disk"
  echo "Usage: $0 bootloader_bin_path virtual_disk"
  echo ""
}

if [ $# -lt 2 ]
then
  usage
  exit 0
fi

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
    esac
done

BOOTLOADER_BIN_PATH=$1
shift
VDISK=$1

STAGE1_BIN=$BOOTLOADER_BIN_PATH/stage1/x86/stage1.bin
STAGE2_BIN=$BOOTLOADER_BIN_PATH/stage2/x86/stage2.bin

dd if=$STAGE1_BIN of=$VDISK conv=notrunc bs=1 count=440
dd if=$STAGE2_BIN of=$VDISK conv=notrunc seek=1 obs=512
