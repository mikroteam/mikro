#!/bin/bash

div() {
  echo $(( 1 + (($1 - 1) / $2) ))
}

# i386 functions
i386_pdeNr() {
  sq_1024=$(( 1024 * 1024 ))
  echo $(div $1 $sq_1024)
}

i386_mapping() {
  nr_pages=$(div $1 4096)
  sq_1024=$(( 1024 * 1024 ))
  echo $(( $(div $nr_pages 1024) + 1 ))
}

if [ $# -ne 4 ]; then
  echo 'maping_size.sh ARCH AS_SIZE KERNEL_BINARY_SIZE USABLE_SIZE'
  echo 'Supported ARCH are:'
  echo ' - i386'
  echo 'AS_SIZE (Adress Space) is in bytes'
  echo 'KERNEL_BINARY_SIZE is in bytes'
  echo 'USABLE_SIZE is in bytes'

  exit 1
fi

ARCH=$1
AS_SIZE=$2
KERNEL_BINARY_SIZE=$3
USABLE_SIZE=$4

case "$1" in
  "i386")
    nrPages4paging=$(i386_mapping $AS_SIZE)
    nrPages4kernel=$(div $KERNEL_BINARY_SIZE 4096)
    nrPages4usable=$(div $USABLE_SIZE 4096)

    totalPages=$(( $nrPages4paging + $nrPages4kernel + $nrPages4usable ))

    mapPages=$(i386_mapping $(( $totalPages * 4096 )))
    mapSizeKB=$(( $mapPages * 4 ))

    echo "Results:"
    echo "$nrPages4paging pages needed to map the whole address space"
    echo "$totalPages pages needed to be mapped using $mapSizeKB KB for" \
      "mapping"
    echo "pdt: 1, pde: $(i386_pdeNr $mapPages)"
    ;;
  *)
    echo "Unsuported architechture"
    exit 2
    ;;
esac
