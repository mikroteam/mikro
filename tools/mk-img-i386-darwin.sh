#!/bin/sh

usage()
{
  echo "Create a virtual disk with a ext2 partition and a valid MBR"
  echo "Usage: $0 virtual_disk"
}

if [ $# -lt 1 ]
then
  usage
  exit 0
fi

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
    esac
done

IMG_FILE=$1
MKFS_EXT2=fuse-ext2.mke2fs
FDISK="fdisk -e"
SECTOR_BEFORE_EXT2=63
SYSTEM_ID=83 # ID for Linux partition (ext2)
EXT2_PARTITION_SIZE=32000
EXT2_BLOCK_SIZE=1024
DISK_BLOCK_SIZE=512

if [ ! -f $IMG_FILE ]; then
  TMP_FILE1=`mktemp XXXXXX`
  dd if=/dev/zero of=$TMP_FILE1 count=$SECTOR_BEFORE_EXT2 bs=$DISK_BLOCK_SIZE
  TMP_FILE2=`mktemp XXXXXX`
  dd if=/dev/zero of=$TMP_FILE2 count=$EXT2_PARTITION_SIZE bs=$EXT2_BLOCK_SIZE
  $MKFS_EXT2 -T ext2 -F -b $EXT2_BLOCK_SIZE $TMP_FILE2 || exit 1
  cat $TMP_FILE1 $TMP_FILE2 > $IMG_FILE
  rm -f $TMP_FILE1 $TMP_FILE2
  echo "y
  edit 1
  $SYSTEM_ID
  n
  $SECTOR_BEFORE_EXT2
  $(( (EXT2_BLOCK_SIZE * EXT2_PARTITION_SIZE) / DISK_BLOCK_SIZE ))
  flag 1
  write
  quit
  " | $FDISK $IMG_FILE
fi
