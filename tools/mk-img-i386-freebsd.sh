#!/bin/sh

usage()
{
  echo "Create a virtual disk with a ext2 partition and a valid MBR"
  echo "Usage: $0 virtual_disk"
}

if [ $# -lt 1 ]
then
  usage
  exit 0
fi

for i
do
    case $i in
      "--help")
        usage
        exit 0
        ;;
    esac
done

IMG_FILE=$1
MKFS_EXT2=mkfs.ext2
FDISK="fdisk -i -f -"
SECTOR_BEFORE_EXT2=63
SYSTEM_ID="0x83" # ID for Linux partition (ext2)
EXT2_PARTITION_SIZE=32000
EXT2_BLOCK_SIZE=1024
DISK_BLOCK_SIZE=512

if [ ! -f $IMG_FILE ]; then
  TMP_FILE1=`mktemp XXXXXX`
  dd if=/dev/zero of=$TMP_FILE1 count=$SECTOR_BEFORE_EXT2 bs=$DISK_BLOCK_SIZE
  TMP_FILE2=`mktemp XXXXXX`
  dd if=/dev/zero of=$TMP_FILE2 count=$EXT2_PARTITION_SIZE bs=$EXT2_BLOCK_SIZE
  $MKFS_EXT2 -T ext2 -F -b $EXT2_BLOCK_SIZE $TMP_FILE2 || exit 1
  cat $TMP_FILE1 $TMP_FILE2 > $IMG_FILE
  rm -f $TMP_FILE1 $TMP_FILE2
  MD=`sudo mdconfig -a -t vnode -f $IMG_FILE`
  echo "g c3 h255 s63
        p 1 $SYSTEM_ID $SECTOR_BEFORE_EXT2 $(( (EXT2_BLOCK_SIZE * EXT2_PARTITION_SIZE) / DISK_BLOCK_SIZE ))
        a 1" | sudo $FDISK /dev/$MD
  sudo mdconfig -d -u $MD
fi
