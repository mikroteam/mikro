#!/usr/bin/env python2

# This is bad pyhton, I guess...

import os
import re
import subprocess
import sys

if len(sys.argv) != 4:
  print('stack.py ADDR2LINE MIKRO_BIN MIKRO_LOG')
  sys.exit(1)

fl_in = open(sys.argv[3])
fl_out = open(sys.argv[3] + "~", 'w')

stack_dump = False

r1 = re.compile(r'.*(mikro/kernel/.*)')
r2 = re.compile(r'.*(mikro/)(.*)')

stack_id = 0
for line in fl_in.readlines():
  if line.rstrip("\n") == "*** Mikro Rest In Peace ***":
    stack_dump = False

  if stack_dump:
    addrs = line.split(" ")
    for addr in addrs:
      if addr != "\n":
        p = subprocess.Popen([sys.argv[1], "-e", sys.argv[2], "-Cf", addr], \
            stdout=subprocess.PIPE)
        output, error = p.communicate()

        outputs = output.split("\n")
        func = outputs[0]
        file_line = outputs[1]

        if r1.match(file_line):
          file_line = r1.sub(r'\1', file_line)
        elif r2.match(file_line):
          file_line = r2.sub(r'\1kernel/\2', file_line)

        fl_out.write("#" + str(stack_id) + " " + addr + " in " + func + \
            " at " + file_line + "\n")
        stack_id = stack_id + 1
  else:
    fl_out.write(line)

  if line.rstrip("\n") == "Stack:":
    stack_dump = True

os.remove(sys.argv[3])
os.rename(sys.argv[3] +  "~", sys.argv[3])
