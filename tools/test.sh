#!/bin/sh

#TODO: set warning following the current folder

make_test ()
{
  make boot QEMU_OPTION="-smp -cores=2 -curses"&
  pid=`echo $!`
  sleep 4
  kill $pid
  err=`cat serial.out | grep "Panic" | wc -l`
  if [ $err -eq 0 ]; then
    exit 0
  fi
  exit 42
}

make_test
